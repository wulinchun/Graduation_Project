
### 毕业设计


#### 介绍
基于spring boot的ERP系统实现
演示地址：http://182.92.178.13:8080/erp/main_view **（已停用）** 演示账户：yszh 密码：123456 角色：用户

新的演示地址：http://124.221.47.230:8082/erp/main_view  演示账户：yszh 密码：123456 角色：用户
#### 软件架构
软件架构说明


#### 技术框架

1、核心框架：SpringBoot 2.0.0
2、持久层框架：Mybatis 1.3.2
3、日志管理：LogBack 2.10.0
4、JS框架：Jquery 1.8.0
5、UI框架: EasyUI 1.9.4
6、项目管理框架: Maven 3.2.3

#### 开发环境

IDE: IntelliJ IDEA 2017+
DB: Mysql5.7+
JDK: JDK1.8
Maven: Maven3.2.3+

#### 服务器环境

数据库服务器：Mysql5.7+
JAVA平台: JRE1.8
操作系统：Windows等


#### 系统美图

首页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111313_2010f83d_5735648.png "首页.png")

仓库设置
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111329_499e3070_5735648.png "仓库设置.png")

入库单
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111342_4804fbdf_5735648.png "入库单.png")

出库单
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111354_69360a47_5735648.png "出库单.png")

库存统计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111406_b97cd20f_5735648.png "库存统计.png")

库存预警
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111420_063dfc9a_5735648.png "库存预警.png")

采购申请
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111444_97d50466_5735648.png "采购申请.png")

采购审核
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111458_1c0da2c9_5735648.png "采购审核.png")

采购清单
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111515_ee44dc57_5735648.png "采购清单.png")

申请预算
![输入图片说明](https://images.gitee.com/uploads/images/2020/0412/111526_33987598_5735648.png "申请预算.png")

