/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-05-07 20:04:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('administrator', '123456');
INSERT INTO `admin` VALUES ('superAdmin', '123456');

-- ----------------------------
-- Table structure for `budget_apply`
-- ----------------------------
DROP TABLE IF EXISTS `budget_apply`;
CREATE TABLE `budget_apply` (
  `index_no` varchar(100) NOT NULL COMMENT '序列号',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `file_path` varchar(500) DEFAULT NULL COMMENT '文件路径',
  `username_apply` varchar(100) DEFAULT NULL COMMENT '用户名（申请）',
  `dept` varchar(100) NOT NULL COMMENT '部门',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `status` varchar(100) DEFAULT NULL COMMENT '状态（未审核，已批准，不批准）',
  `username_deal` varchar(100) DEFAULT NULL COMMENT '用户名（处理）',
  `deal_time` datetime DEFAULT NULL,
  PRIMARY KEY (`index_no`,`dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of budget_apply
-- ----------------------------
INSERT INTO `budget_apply` VALUES ('10', '李四_预算申请单.xlsx', 'D://spring//upload//budget_apply_table//李四//', '李四', '采购部', '2020-04-06 20:39:41', '审批通过', '王五', '2020-04-07 20:54:30');
INSERT INTO `budget_apply` VALUES ('9', '20200209_采购申请.xlsx', 'D://spring//upload//budget_apply_table//李四//', '李四', '采购部', '2020-02-10 21:24:42', '审批通过', '王五', '2020-02-12 21:31:15');

-- ----------------------------
-- Table structure for `common_file`
-- ----------------------------
DROP TABLE IF EXISTS `common_file`;
CREATE TABLE `common_file` (
  `index_no` varchar(100) NOT NULL COMMENT '索引号',
  `file_name` varchar(100) DEFAULT NULL COMMENT '预算申请表文件名',
  `uploader` varchar(100) DEFAULT NULL COMMENT '上传者',
  `file_path` varchar(500) DEFAULT NULL COMMENT '预算申请表文件路径',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`index_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of common_file
-- ----------------------------
INSERT INTO `common_file` VALUES ('3', '预算申请单_样表.xlsx', 'administrator', 'D://spring//upload//common_file//吴邪//预算申请单_样表.xlsx', '预算申请单_样表');
INSERT INTO `common_file` VALUES ('5', '收入明细表_样表.xlsx', 'administrator', 'D://spring//upload//common_file//吴邪//收入明细表_样表.xlsx', '收入明细表_样表');
INSERT INTO `common_file` VALUES ('6', '支出明细表_样表.xlsx', 'administrator', 'D://spring//upload//common_file//吴邪//支出明细表_样表.xlsx', '支出明细表_样表');
INSERT INTO `common_file` VALUES ('7', '销售统计表_样表.xlsx', '吴邪', 'D://spring//upload//common_file//吴邪//销售统计表_样表.xlsx', '销售统计表_样表');

-- ----------------------------
-- Table structure for `entry_out_again`
-- ----------------------------
DROP TABLE IF EXISTS `entry_out_again`;
CREATE TABLE `entry_out_again` (
  `flow_number` varchar(100) DEFAULT NULL COMMENT '入库/出库流水号',
  `status` varchar(100) DEFAULT NULL COMMENT '入库/出库'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of entry_out_again
-- ----------------------------
INSERT INTO `entry_out_again` VALUES ('60', '已入库');
INSERT INTO `entry_out_again` VALUES ('62', '已入库');
INSERT INTO `entry_out_again` VALUES ('64', '已入库');
INSERT INTO `entry_out_again` VALUES ('66', '已入库');
INSERT INTO `entry_out_again` VALUES ('68', '已入库');
INSERT INTO `entry_out_again` VALUES ('93', '已入库');
INSERT INTO `entry_out_again` VALUES ('114', '已入库');
INSERT INTO `entry_out_again` VALUES ('cp62', '已出库');
INSERT INTO `entry_out_again` VALUES ('64', '已出库');

-- ----------------------------
-- Table structure for `entry_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `entry_warehouse`;
CREATE TABLE `entry_warehouse` (
  `flow_number` varchar(100) DEFAULT NULL COMMENT '流水号',
  `list_no` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `goods_no` varchar(100) DEFAULT NULL COMMENT '编号',
  `kind` varchar(100) DEFAULT NULL COMMENT '种类',
  `specification` varchar(100) DEFAULT NULL COMMENT '规格',
  `goods_name` varchar(100) DEFAULT NULL COMMENT '名称',
  `single_price` double DEFAULT NULL COMMENT '单价',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `manufacturer` varchar(100) DEFAULT NULL COMMENT '供应商名称',
  `entry_date` datetime DEFAULT NULL COMMENT '入库日期',
  `status` varchar(100) DEFAULT NULL COMMENT '状态（未入库，已入库）',
  `warehouse_no` varchar(100) DEFAULT NULL COMMENT '仓库编号',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of entry_warehouse
-- ----------------------------
INSERT INTO `entry_warehouse` VALUES ('60', '2020032118040361', 'Intel酷睿i5_9400F', 'cpu', 'JSM-253', 'Intel酷睿i5_9400F', '300', '500', '150000', '英特尔', '2020-03-21 18:04:03', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('62', '2020032118064063', 'Intel酷睿i3 9100F', 'cpu', 'TRL-100', 'Intel酷睿i3 9100F', '200', '1000', '200000', '英特尔', '2020-03-21 18:06:40', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('64', '2020032118075265', 'Intel酷睿i7_9700K', 'cpu', 'GB/7-1200', 'Intel酷睿i7_9700K', '800', '80', '64000', '英特尔', '2020-03-21 18:07:52', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('66', '2020032119133967', 'Intel酷睿i9_9900K ', 'cpu', 'HY369-0', 'Intel酷睿i9_9900K ', '2000', '200', '400000', '英特尔', '2020-03-21 19:13:39', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('68', '2020032119242669', 'Intel酷睿i5_9600K', 'cpu', 'Ice Lake(第9代)', 'Intel酷睿i5_9600K', '1000', '800', '800000', '英特尔', '2020-03-21 19:24:26', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('70', '2020032119262671', '华硕TUF_B360M-PLUS_GAMING', '主板', '4 DDR4 DIMM', '华硕TUF_B360M-PLUS_GAMING', '500', '100', '50000', '华硕', '2020-03-21 19:26:26', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('72', '2020032119285573', '技嘉Z390_GAMING_X', '主板', '4 DDR4 DIMM', '技嘉Z390_GAMING_X', '800', '50', '40000', '技嘉', '2020-03-21 19:28:55', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('74', '2020032119301575', '微星B450M MORTAR', '主板', 'AMD B450', '微星B450M MORTAR', '500', '100', '50000', '微星', '2020-03-21 19:30:15', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('76', '2020032120382677', '华硕TUF3-GeForce_GTX1660-O6G-GAMING_OC ', '显卡', 'GeForce GTX 1660', '华硕TUF3-GeForce_GTX1660-O6G-GAMING_OC ', '1000', '80', '80000', '华硕', '2020-03-21 20:38:26', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('78', '2020032120400879', '影驰GTX_1050_Ti_大将', '显卡', 'NVIDIA GeForce GTX ', '影驰GTX_1050_Ti_大将', '300', '100', '30000', '影驰', '2020-03-21 20:40:08', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('80', '2020032120420181', '金士顿DDR4_2400_8G', '内存条', 'DDR IV', '金士顿DDR4_2400_8G', '100', '2000', '200000', '金士顿', '2020-03-21 20:42:01', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('82', '2020032120432483', '威刚XPG_Z1_DDR4_2400', '内存条', 'DDR4 2400', '威刚XPG_Z1_DDR4_2400', '80', '1000', '80000', '威刚', '2020-03-21 20:43:24', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('84', '2020032120495185', '西部数据蓝盘_1TB_64M_SATA3_硬盘(WD10EZEX)', '硬盘', '3.5寸', '西部数据蓝盘_1TB_64M_SATA3_硬盘(WD10EZEX)', '180', '20000', '3600000', '西部数据', '2020-03-21 20:49:51', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('86', '2020032120513687', 'AOC_24G2', '显示器', '23.8英寸', 'AOC_24G2', '500', '1500', '750000', '冠捷', '2020-03-21 20:51:36', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('88', '2020032120525689', '三星C27H580FDC', '显示器', '27英寸', '三星C27H580FDC', '800', '500', '400000', '三星', '2020-03-21 20:52:56', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('90', '2020031820541091', '罗技MK275无线光电键鼠套装', '键鼠', '2.4GHz无线', '罗技MK275无线光电键鼠套装', '100', '25000', '2500000', '罗技', '2020-03-18 20:54:10', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('93', '2020032321094394', '戴尔U2718Q', '显示器', '', '显示器', '300', '1', '300', '戴尔', '2020-03-23 21:09:43', '已入库', 'common_warehouse', '');
INSERT INTO `entry_warehouse` VALUES ('95', '2020032611093196', '三星SE-224DB升级版', '光驱', '148.2×170×42mm', '三星SE-224DB升级版', '100', '100', '10000', '三星', '2020-03-26 11:09:31', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('97', '2020032611114598', '华硕BW-16D1HT', '光驱', '蓝光刻录机', '华硕BW-16D1HT', '300', '80', '24000', '华硕', '2020-03-26 11:11:45', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('99', '20200326111351100', '技嘉B360M-D3V ', '主板', '2_DDR4_DIMM', '技嘉B360M-D3V ', '600', '200', '120000', '技嘉', '2020-03-26 11:13:51', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('102', '20200326111515103', '华硕ROG_STRIX_B450-F_GAMING', '主板', '支持Ryzen 3/5/7系列', '华硕ROG_STRIX_B450-F_GAMING', '900', '50', '45000', '华硕', '2020-03-26 11:15:15', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('104', '20200326111712105', '微星B450MPRO-M2', '主板', '2_DDR2_DIMM', '微星B450MPRO-M2', '400', '20', '8000', '微星', '2020-03-26 11:17:12', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('106', '20200326111820107', '微星B450I_GAMING_PLUS_AC', '主板', '支持Ryzen 3/5/7系列', '微星B450I_GAMING_PLUS_AC', '1000', '10', '10000', '微星', '2020-03-26 11:18:20', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('110', '20200326112441111', '威刚万紫千红_DDR4_2400_8G', '内存条', '8G', '威刚万紫千红_DDR4_2400_8G', '300', '300', '90000', '威刚', '2020-03-26 11:24:41', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('112', '20200326112648113', '金士顿DDR3_1333_4GB(KVR13N9S8/4) ', '内存条', '单条2×4G', '金士顿DDR3_1333_4GB(KVR13N9S8/4) ', '199', '10', '1990', '金士顿', '2020-03-26 11:26:48', '', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('114', '20200326124526115', 'notebook_S20', '办公用品', '20cm*10cm', '笔记本', '0.5', '200', '100', '晨光', '2020-03-26 12:45:26', '已入库', 'common_warehouse', '');
INSERT INTO `entry_warehouse` VALUES ('116', '20200328114948117', '联想天逸510_Pro', '整机', '家用台式机商用台式机', '联想天逸510_Pro', '500', '10', '5000', '联想', '2020-03-28 11:49:48', '已入库', 'product_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('118', '20200406150917119', '美商海盗船_CM4X8GD3000C', '内存条', '8GB', '美商海盗船_CM4X8GD3000C', '300', '20', '6000', '美商海盗船', '2020-04-06 15:09:17', '已入库', 'raw_warehouse_001', '');
INSERT INTO `entry_warehouse` VALUES ('120', '20200406202329121', 'broom_001', '清洁用品', 'S8848', '扫帚', '10', '10', '100', '上海乐宜美清洁用品有限公司', '2020-04-06 20:23:29', '已入库', 'common_warehouse', '');

-- ----------------------------
-- Table structure for `expenditure`
-- ----------------------------
DROP TABLE IF EXISTS `expenditure`;
CREATE TABLE `expenditure` (
  `index_no` varchar(100) NOT NULL COMMENT '索引号',
  `list_no` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `list_time` datetime DEFAULT NULL COMMENT '单据时间',
  `related_company` varchar(100) DEFAULT NULL COMMENT '收款单位',
  `receive_account` varchar(100) DEFAULT NULL COMMENT '收款账户',
  `deal_person` varchar(100) DEFAULT NULL COMMENT '处理人',
  `pay_account` varchar(100) DEFAULT NULL COMMENT '付款账户',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `appendix` varchar(500) DEFAULT NULL COMMENT '附件',
  PRIMARY KEY (`index_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of expenditure
-- ----------------------------
INSERT INTO `expenditure` VALUES ('14', '2020011421523960', '2020-01-14 21:52:39', '华硕', '13254654896323', '王五', '20161946', '7000', '', 'D://spring//upload//expenditure//王五//华硕_20200114.xlsx');
INSERT INTO `expenditure` VALUES ('15', '2020021821545261', '2020-02-18 21:54:52', '华硕', '16654896532132', '王五', '20161946', '10000', '', 'D://spring//upload//expenditure//王五//华硕_20200218.xlsx');
INSERT INTO `expenditure` VALUES ('16', '2020032622001662', '2020-03-26 22:00:16', '华硕', '13135453123212', '王五', '20161946', '20000', '', 'D://spring//upload//expenditure//王五//华硕_20200326.xlsx');
INSERT INTO `expenditure` VALUES ('17', '2020040711080865', '2020-04-07 11:08:08', '联想', '102001234010005938', '王五', '102432512010005938', '8000', '', 'D://spring//upload//expenditure//吴邪//联想_20200407.xlsx');

-- ----------------------------
-- Table structure for `income`
-- ----------------------------
DROP TABLE IF EXISTS `income`;
CREATE TABLE `income` (
  `index_no` varchar(100) DEFAULT NULL COMMENT '索引号',
  `list_no` varchar(100) DEFAULT NULL,
  `list_time` datetime DEFAULT NULL COMMENT '单据日期',
  `related_company` varchar(100) DEFAULT NULL COMMENT '往来单位',
  `pay_account` varchar(100) DEFAULT NULL COMMENT '付款账户',
  `deal_person` varchar(100) DEFAULT NULL COMMENT '经手人',
  `receive_account` varchar(100) DEFAULT NULL COMMENT '收款账号',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `appendix` varchar(500) DEFAULT NULL COMMENT '附件'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of income
-- ----------------------------
INSERT INTO `income` VALUES ('54', '2020011521314555', '2020-01-15 21:31:45', '上海海隆软件有限公司', '000213546789', '王五', '20161946', '10000', '', 'D://spring//upload//income//王五//海隆软件_20200115.xlsx');
INSERT INTO `income` VALUES ('56', '2020020921403957', '2020-02-09 21:40:39', '上海招商银行', '13559623256512', '王五', '20161946', '60000', '', 'D://spring//upload//income//王五//上海招商银行_20200209.xlsx');
INSERT INTO `income` VALUES ('58', '2020032621461459', '2020-03-26 21:46:14', '上海农商银行', '2665476821264232', '王五', '20161946', '50000', '', 'D://spring//upload//income//王五//上海农商银行_20200326.xlsx');

-- ----------------------------
-- Table structure for `logback`
-- ----------------------------
DROP TABLE IF EXISTS `logback`;
CREATE TABLE `logback` (
  `operation_module` varchar(100) DEFAULT NULL COMMENT '操作模块',
  `operation_detail` varchar(1000) DEFAULT NULL COMMENT '操作详情',
  `operator` varchar(100) DEFAULT NULL COMMENT '操作人员',
  `operation_status` varchar(100) DEFAULT NULL COMMENT '操作状态',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of logback
-- ----------------------------
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:20:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:38:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-01 13:40:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:24');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:40:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:41:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:43:01');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:44:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:51:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 13:52:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:16:57');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 14:17:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:23:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:25:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:33:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:34:06');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:35:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:36:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:37:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:39:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 15:42:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-01 19:15:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-02 15:25:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-02 15:25:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-02 15:25:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-03 14:00:56');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-03 14:00:56');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-03 14:00:56');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-04 15:13:18');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-04 15:15:23');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-04 15:17:43');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-04 15:30:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:13:24');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:13:50');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-03-05 15:14:40');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:14:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:14:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:15:07');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-03-05 15:15:16');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-03-05 15:15:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:18:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:19:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:21');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:40');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:21:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '失败', '2020-03-05 15:38:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:38:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:39:13');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-03-05 15:40:36');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:26');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-05 15:41:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:16');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-08 16:16:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 10:56:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 10:57:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:00:37');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:02:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:05:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:06:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:08:58');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:12:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:14:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:16:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:21:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:22:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:24:25');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:24:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:14');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:49');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:29:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 14:04:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 15:02:51');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 15:03:07');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:38:50');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:40:22');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:40:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:41:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:50:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:54:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:57:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:05:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:20:52');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:21:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:21:58');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:26:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:30:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:34:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:35:50');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:36:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:36:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 10:56:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 10:57:40');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:00:37');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:02:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:05:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:06:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:08:58');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:12:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:14:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:16:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:18:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:21:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:22:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:24:25');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:24:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:26:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:14');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:28:49');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 11:29:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 14:04:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 15:02:51');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 15:03:07');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:38:50');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:40:22');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:40:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:41:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:50:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:54:41');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 19:57:21');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:05:11');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:20:52');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:21:51');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:21:58');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:26:20');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:30:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:34:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:35:50');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:36:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 20:36:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-09 21:42:17');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:01:46');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:16:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 16:20:09');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:54:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 17:56:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:00:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:08:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 18:12:30');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:49:35');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:50:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:51:26');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:52:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 19:58:29');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:03:42');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:06:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:09:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:10:55');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:11:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-03-11 20:15:10');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:51:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:56');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '失败', '2020-04-06 10:53:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:03:32');
INSERT INTO `logback` VALUES ('用户', '登录id:Yszh', 'Yszh', '失败', '2020-04-06 11:07:24');
INSERT INTO `logback` VALUES ('用户', '登录id: yszh', ' yszh', '失败', '2020-04-06 11:14:12');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '成功', '2020-04-06 11:15:00');
INSERT INTO `logback` VALUES ('用户', '注销id:yszh', 'yszh', '成功', '2020-04-06 11:15:05');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '失败', '2020-04-06 11:16:07');
INSERT INTO `logback` VALUES ('用户', '登录id:admin', 'admin', '失败', '2020-04-06 11:18:20');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-04-06 11:18:51');
INSERT INTO `logback` VALUES ('用户', '注销id:administrator', 'administrator', '成功', '2020-04-06 11:18:56');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:20:37');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:20:51');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:21:00');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:28:45');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:32:08');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:32:31');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-04-06 11:35:46');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:35:51');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:36:48');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:36:53');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:38:01');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:38:22');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:38:36');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:39:24');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:59:04');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:59:23');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:59:33');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 12:04:07');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 12:04:16');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 13:57:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:03:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:15:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:18:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:13');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:25');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:30:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:30:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:31:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:27');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:34:13');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:40:59');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:51:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:52:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:52:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:13:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:35:23');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:49:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:57:59');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:58:17');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:58:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:05:07');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:05:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:07:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:53:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:55:21');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 18:55:30');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 18:55:57');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-04-06 18:56:06');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-04-06 18:56:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:56:43');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:58:51');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:59:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:59:06');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-04-06 18:59:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:51:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:53');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 10:52:56');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '失败', '2020-04-06 10:53:05');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:03:32');
INSERT INTO `logback` VALUES ('用户', '登录id:Yszh', 'Yszh', '失败', '2020-04-06 11:07:24');
INSERT INTO `logback` VALUES ('用户', '登录id: yszh', ' yszh', '失败', '2020-04-06 11:14:12');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '成功', '2020-04-06 11:15:00');
INSERT INTO `logback` VALUES ('用户', '注销id:yszh', 'yszh', '成功', '2020-04-06 11:15:05');
INSERT INTO `logback` VALUES ('用户', '登录id:yszh', 'yszh', '失败', '2020-04-06 11:16:07');
INSERT INTO `logback` VALUES ('用户', '登录id:admin', 'admin', '失败', '2020-04-06 11:18:20');
INSERT INTO `logback` VALUES ('用户', '登录id:administrator', 'administrator', '成功', '2020-04-06 11:18:51');
INSERT INTO `logback` VALUES ('用户', '注销id:administrator', 'administrator', '成功', '2020-04-06 11:18:56');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:20:37');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 11:20:51');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:21:00');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:28:45');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:32:08');
INSERT INTO `logback` VALUES ('用户', '个人设置张三:张三', '张三', '成功', '2020-04-06 11:32:31');
INSERT INTO `logback` VALUES ('用户', '个人设置null:null', 'null', '成功', '2020-04-06 11:35:46');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:35:51');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:36:48');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:36:53');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:38:01');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:38:22');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:38:36');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:39:24');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:59:04');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 11:59:23');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 11:59:33');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 12:04:07');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 12:04:16');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 13:57:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:03:22');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:15:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:18:32');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:13');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:25');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:23:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:30:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:30:54');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:31:08');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:27');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:39');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:33:52');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:34:13');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:40:59');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:51:48');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:52:36');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 14:52:45');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:13:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:35:23');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:49:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:57:59');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:58:17');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 15:58:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:05:07');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:05:19');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 16:07:12');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:53:44');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:55:21');
INSERT INTO `logback` VALUES ('用户', '登录id:张三', '张三', '成功', '2020-04-06 18:55:30');
INSERT INTO `logback` VALUES ('用户', '注销id:张三', '张三', '成功', '2020-04-06 18:55:57');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-04-06 18:56:06');
INSERT INTO `logback` VALUES ('用户', '注销id:李四', '李四', '成功', '2020-04-06 18:56:34');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:56:43');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:58:51');
INSERT INTO `logback` VALUES ('管理员', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:59:02');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-06 18:59:06');
INSERT INTO `logback` VALUES ('用户', '登录id:李四', '李四', '成功', '2020-04-06 18:59:15');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 10:12:38');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:02:31');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:12:03');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:29:28');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:33:33');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:49:14');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-07 11:54:16');
INSERT INTO `logback` VALUES ('用户', '登录id:吴邪', '吴邪', '成功', '2020-04-09 23:03:35');

-- ----------------------------
-- Table structure for `out_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `out_warehouse`;
CREATE TABLE `out_warehouse` (
  `flow_number` varchar(100) DEFAULT NULL COMMENT '流水号',
  `list_no` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `goods_no` varchar(100) DEFAULT NULL COMMENT '编号',
  `kind` varchar(100) DEFAULT NULL COMMENT '种类',
  `specification` varchar(100) DEFAULT NULL COMMENT '规格',
  `goods_name` varchar(100) DEFAULT NULL COMMENT '名称',
  `single_price` double DEFAULT NULL COMMENT '单价',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `manufacturer` varchar(100) DEFAULT NULL COMMENT '供应商',
  `out_date` datetime DEFAULT NULL COMMENT '出库日期',
  `status` varchar(100) DEFAULT NULL COMMENT '状态（未出库，已出库）',
  `warehouse_no` varchar(100) DEFAULT NULL COMMENT '仓库号',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of out_warehouse
-- ----------------------------
INSERT INTO `out_warehouse` VALUES ('cp62', '2020032220574063', 'Intel酷睿i7_9700K', 'cpu', 'GB/7-1200', 'Intel酷睿i7_9700K', '800', '10', '8000', '英特尔', '2020-03-22 20:57:40', '已出库', 'raw_warehouse_001', '');
INSERT INTO `out_warehouse` VALUES ('64', '20200326205608017', '联想拯救者刃7000性能版', '整机', '', '联想拯救者刃7000性能版', '0', '10', '0', 'local', '2020-03-26 21:08:46', '已出库', '', '');
INSERT INTO `out_warehouse` VALUES ('65', '202003262104142', '联想天逸510_Pro', '', '', '联想天逸510_Pro', '500', '80', '40000', 'local', '2020-03-28 12:04:00', '已出库', 'product_warehouse_001', '');
INSERT INTO `out_warehouse` VALUES ('66', '20200326205608017', '联想拯救者刃7000性能版', null, null, '联想拯救者刃7000性能版', '0', '10', '60000', 'local', null, '未出库', null, null);
INSERT INTO `out_warehouse` VALUES ('cp67', '2020040715431968', '美商海盗船_CM4X8GD3000C', '内存条', '8GB', '美商海盗船_CM4X8GD3000C', '300', '10', '3000', '美商海盗船', '2020-04-07 15:43:19', '已出库', 'raw_warehouse_001', '');
INSERT INTO `out_warehouse` VALUES ('69', '202004091116333', '联想天逸510_Pro', null, null, '联想天逸510_Pro', '0', '20', '10000', 'local', null, '未出库', null, null);

-- ----------------------------
-- Table structure for `purchase_apply`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_apply`;
CREATE TABLE `purchase_apply` (
  `index_no` varchar(100) DEFAULT NULL COMMENT '索引号',
  `username` varchar(100) DEFAULT NULL COMMENT '申请者用户名',
  `name` varchar(100) DEFAULT NULL COMMENT '申请者姓名',
  `dept` varchar(100) DEFAULT NULL COMMENT '申请部门',
  `kind` varchar(100) DEFAULT NULL COMMENT '种类',
  `goods_name` varchar(100) DEFAULT NULL COMMENT '物品名称',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `apply_time` datetime DEFAULT NULL COMMENT '申请日期',
  `status` varchar(100) DEFAULT NULL COMMENT '状态（未审核，通过、未通过）',
  `manufacturer` varchar(100) DEFAULT NULL COMMENT '供货商',
  `total_price` double DEFAULT NULL COMMENT '总金额'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of purchase_apply
-- ----------------------------
INSERT INTO `purchase_apply` VALUES ('23', '郑十', '郑十', '行政部', '办公用品', '中性笔', '1000', '2020-01-01 21:13:55', '已确认', '上海晨光有限公司', '200');
INSERT INTO `purchase_apply` VALUES ('24', '郑十', '郑十', '行政部', '办公用品', '笔记本', '500', '2020-02-19 21:15:21', '已确认', '上海晨光有限公司', '200');
INSERT INTO `purchase_apply` VALUES ('25', '郑十', '郑十', '行政部', '办公用品', '计算器', '20', '2020-03-26 21:17:32', '已确认', '上海晨光有限公司', '100');
INSERT INTO `purchase_apply` VALUES ('27', '吴邪', '吴邪', '研发部', '清洁用品', '酒精', '10', '2020-04-01 21:35:54', '通过', '', '0');
INSERT INTO `purchase_apply` VALUES ('28', '李四', '李四', '采购部', '清洁用品', '扫帚', '10', '2020-04-06 19:05:58', '已确认', '上海乐宜美清洁用品有限公司', '100');
INSERT INTO `purchase_apply` VALUES ('30', '李四', '李四', '采购部', '清洁用品', '拖把', '20', '2020-04-06 19:28:15', '未审核', '', '0');

-- ----------------------------
-- Table structure for `sale_bill`
-- ----------------------------
DROP TABLE IF EXISTS `sale_bill`;
CREATE TABLE `sale_bill` (
  `index_no` varchar(100) DEFAULT NULL COMMENT '索引号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `company_name` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `list_no` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `list_date` datetime DEFAULT NULL COMMENT '单据日期',
  `goods_no` varchar(100) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `operator` varchar(100) DEFAULT NULL COMMENT '操作员',
  `status` varchar(100) DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sale_bill
-- ----------------------------
INSERT INTO `sale_bill` VALUES ('16', '王经理', '上海方正有限公司', '2020010820312216', '2020-01-08 20:31:22', '联想天逸510_Pro', '联想天逸510_Pro', '30', '15000', '张三', '已审核', null);
INSERT INTO `sale_bill` VALUES ('17', '李董事长', '上海华依科技有限公司', '20200326205608017', '2020-02-11 20:56:08', '联想拯救者刃7000性能版', '联想拯救者刃7000性能版', '10', '60000', '张三', '销售出库', null);
INSERT INTO `sale_bill` VALUES ('18', '钱十一', '上海自动化研究所', '202003262104142', '2020-03-26 21:04:14', '联想天逸510_Pro', '联想天逸510_Pro', '80', '40000', '张三', '销售出库', null);
INSERT INTO `sale_bill` VALUES ('19', '赵经理', '苏州三星电子', '202004091116333', '2020-04-09 11:16:33', '联想天逸510_Pro', '联想天逸510_Pro', '20', '10000', '张三', '销售出库', null);

-- ----------------------------
-- Table structure for `sale_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `sale_statistics`;
CREATE TABLE `sale_statistics` (
  `index_no` varchar(100) NOT NULL COMMENT '序列号',
  `upload_time` datetime DEFAULT NULL COMMENT '上传时间',
  `uploader` varchar(100) DEFAULT NULL COMMENT '上传者',
  `dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `appendix` varchar(500) DEFAULT NULL COMMENT '附件',
  PRIMARY KEY (`index_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sale_statistics
-- ----------------------------
INSERT INTO `sale_statistics` VALUES ('13', '2020-03-24 20:43:25', '吴邪', '销售部', '', 'D://spring//upload//sale_statistics//吴邪//A509.xlsx');
INSERT INTO `sale_statistics` VALUES ('18', '2020-04-07 11:45:11', '张三', '销售部', '', 'D://spring//upload//sale_statistics//吴邪//张三_销售_20200407.xlsx');

-- ----------------------------
-- Table structure for `sequence`
-- ----------------------------
DROP TABLE IF EXISTS `sequence`;
CREATE TABLE `sequence` (
  `seq_name` varchar(50) NOT NULL,
  `current_val` int(11) NOT NULL,
  `increment_val` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`seq_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sequence
-- ----------------------------
INSERT INTO `sequence` VALUES ('seq_budget_apply_index_no', '10', '1');
INSERT INTO `sequence` VALUES ('seq_common_file_index_no', '7', '1');
INSERT INTO `sequence` VALUES ('seq_entry_warehouse_flow_number', '121', '1');
INSERT INTO `sequence` VALUES ('seq_expenditure_index_no', '18', '1');
INSERT INTO `sequence` VALUES ('seq_income_index_no', '65', '1');
INSERT INTO `sequence` VALUES ('seq_out_warehouse_flow_number', '69', '1');
INSERT INTO `sequence` VALUES ('seq_purchase_apply_index_no', '30', '1');
INSERT INTO `sequence` VALUES ('seq_sale_list_index_no', '19', '1');
INSERT INTO `sequence` VALUES ('seq_sale_statistics_index_no', '3', '1');
INSERT INTO `sequence` VALUES ('seq_stock_statistics_index_no', '403', '1');
INSERT INTO `sequence` VALUES ('seq_test', '2', '1');
INSERT INTO `sequence` VALUES ('seq_user_power', '152', '1');
INSERT INTO `sequence` VALUES ('seq_warehouse_no', '30', '1');
INSERT INTO `sequence` VALUES ('stock_statistics_index_no', '1', '1');

-- ----------------------------
-- Table structure for `stock_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `stock_statistics`;
CREATE TABLE `stock_statistics` (
  `index_no` varchar(100) DEFAULT NULL COMMENT '索引号',
  `warehouse_no` varchar(100) DEFAULT NULL COMMENT '仓库编号',
  `list_no` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `goods_no` varchar(100) DEFAULT NULL COMMENT '商品编号',
  `kind` varchar(100) DEFAULT NULL COMMENT '种类',
  `specification` varchar(100) DEFAULT NULL COMMENT '规格',
  `goods_name` varchar(100) DEFAULT NULL,
  `single_price` double DEFAULT NULL COMMENT '单价',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` double DEFAULT NULL COMMENT '总金额',
  `manufacturer` varchar(100) DEFAULT NULL COMMENT '制造商',
  `entry_date` date DEFAULT NULL COMMENT '入库日期',
  `out_date` date DEFAULT NULL COMMENT '出库日期',
  `status` varchar(100) DEFAULT NULL COMMENT '状态（过少，正常，过多）',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_statistics
-- ----------------------------
INSERT INTO `stock_statistics` VALUES ('394', 'raw_warehouse_001', '2020032118040361', 'Intel酷睿i5_9400F', 'cpu', 'JSM-253', 'Intel酷睿i5_9400F', '300', '1500', '450000', '英特尔', null, null, '过多', '');
INSERT INTO `stock_statistics` VALUES ('395', 'raw_warehouse_001', '2020032118075265', 'Intel酷睿i7_9700K', 'cpu', 'GB/7-1200', 'Intel酷睿i7_9700K', '800', '140', '112000', '英特尔', null, null, '过多', '');
INSERT INTO `stock_statistics` VALUES ('396', 'common_warehouse', '2020032321094394', '戴尔U2718Q', '显示器', '', '显示器', '300', '1', '300', '戴尔', null, null, '', '');
INSERT INTO `stock_statistics` VALUES ('397', 'raw_warehouse_001', '2020032118064063', 'Intel酷睿i3_9100F', 'cpu', 'TRL-100', 'Intel酷睿i3_9100F', '200', '1000', '200000', '英特尔', null, null, '过多', '');
INSERT INTO `stock_statistics` VALUES ('398', 'raw_warehouse_001', '2020032119133967', 'Intel酷睿i9_9900K ', 'cpu', 'HY369-0', 'Intel酷睿i9_9900K ', '2000', '200', '400000', '英特尔', null, null, '过多', '');
INSERT INTO `stock_statistics` VALUES ('399', 'raw_warehouse_001', '2020032119242669', 'Intel酷睿i5_9600K', 'cpu', 'Ice Lake(第9代)', 'Intel酷睿i5_9600K', '1000', '800', '800000', '英特尔', null, null, '', '');
INSERT INTO `stock_statistics` VALUES ('400', 'common_warehouse', '20200326124526115', 'notebook_S20', '办公用品', '20cm*10cm', '笔记本', '0.5', '200', '100', '晨光', null, null, '', '');
INSERT INTO `stock_statistics` VALUES ('401', 'product_warehouse_001', null, '联想拯救者刃7000性能版', '整机', '家用台式机,游戏台式机', '联想拯救者刃7000性能版', '6000', '790', '4740000', null, null, null, '', '');
INSERT INTO `stock_statistics` VALUES ('402', 'product_warehouse_001', null, '联想天逸510_Pro', '整机', '家用台式机商用台式机', '联想天逸510_Pro', '500', '530', '265000', null, null, null, '', '');
INSERT INTO `stock_statistics` VALUES ('403', 'raw_warehouse_001', '20200406150917119', '美商海盗船_CM4X8GD3000C', '内存条', '8GB', '美商海盗船_CM4X8GD3000C', '300', '20', '6000', '美商海盗船', null, null, '过少', '');

-- ----------------------------
-- Table structure for `stock_warning`
-- ----------------------------
DROP TABLE IF EXISTS `stock_warning`;
CREATE TABLE `stock_warning` (
  `warehouse_no` varchar(100) DEFAULT NULL COMMENT '仓库编号',
  `goods_no` varchar(100) DEFAULT NULL COMMENT '物品编号',
  `max` int(11) DEFAULT NULL COMMENT '最大预警',
  `min` int(11) DEFAULT NULL COMMENT '最小预警'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_warning
-- ----------------------------
INSERT INTO `stock_warning` VALUES ('common_warehouse', '戴尔U2718Q', '100', '0');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', 'Intel酷睿i5_9400F', '410', '20');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', 'Intel酷睿i7_9700K', '100', '30');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', 'Intel酷睿i3_9100F', '0', '0');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', 'Intel酷睿i9_9900K ', '0', '0');
INSERT INTO `stock_warning` VALUES ('common_warehouse', 'notebook_S20', '0', '0');
INSERT INTO `stock_warning` VALUES ('product_warehouse_001', '联想拯救者刃7000性能版', '0', '0');
INSERT INTO `stock_warning` VALUES ('product_warehouse_001', '联想天逸510_Pro', '0', '0');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', '美商海盗船 CM4X8GD3000C', '0', '0');
INSERT INTO `stock_warning` VALUES ('', '', '0', '0');
INSERT INTO `stock_warning` VALUES ('raw_warehouse_001', '美商海盗船_CM4X8GD3000C', '200', '30');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(100) NOT NULL COMMENT '用户名（职工编号）',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `name` varchar(100) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别',
  `tel` varchar(100) DEFAULT NULL COMMENT '联系方式',
  `dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `position` varchar(100) DEFAULT NULL COMMENT '职位',
  `power` varchar(100) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('yszh', '123456', 'yszh', '男', '', '销售部', '', '');
INSERT INTO `user` VALUES ('司徒正美', '123456', '司徒正美', '男', '13856478958', '研发部', '前端架构师', '');
INSERT INTO `user` VALUES ('吴九', '123456', '吴九', '男', '15478965862', '产品部', '产品部经理', '');
INSERT INTO `user` VALUES ('吴邪', '123456', '吴邪', '男', '13900002985', '研发部', '研发部经理', '');
INSERT INTO `user` VALUES ('周八', '123456', '周八', '女', '18956478958', '质检部', '质检部经理', '');
INSERT INTO `user` VALUES ('孙七', '123456', '孙七', '男', '13356987458', '人事部', '人事部经理', '');
INSERT INTO `user` VALUES ('张三', '123456', '张三', '男', '1391701234', '销售部', '销售部经理', '');
INSERT INTO `user` VALUES ('李四', '123456', '李四', '男', '13310025898', '采购部', '采购部经理', '');
INSERT INTO `user` VALUES ('王五', '123456', '王五', '男', '133123456', '财务部', '财务部经理', '');
INSERT INTO `user` VALUES ('赵六', '123456', '赵六', '男', '13265478958', '测试部', '测试部经理', '');
INSERT INTO `user` VALUES ('郑十', '123456', '郑十', '女', '1547895689', '行政部', '行政部经理', '');

-- ----------------------------
-- Table structure for `user_power`
-- ----------------------------
DROP TABLE IF EXISTS `user_power`;
CREATE TABLE `user_power` (
  `index_no` varchar(100) NOT NULL COMMENT '索引号',
  `tablename` varchar(100) NOT NULL COMMENT '表名',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `power` varchar(100) NOT NULL COMMENT '权限',
  PRIMARY KEY (`tablename`,`username`,`power`,`index_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_power
-- ----------------------------
INSERT INTO `user_power` VALUES ('67', 'budget_apply', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('66', 'budget_apply', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('105', 'budget_apply', '李四', 'add');
INSERT INTO `user_power` VALUES ('106', 'budget_apply', '李四', 'delete');
INSERT INTO `user_power` VALUES ('108', 'budget_apply', '李四', 'select');
INSERT INTO `user_power` VALUES ('107', 'budget_apply', '李四', 'update');
INSERT INTO `user_power` VALUES ('146', 'budget_apply', '王五', 'add');
INSERT INTO `user_power` VALUES ('147', 'budget_apply', '王五', 'delete');
INSERT INTO `user_power` VALUES ('149', 'budget_apply', '王五', 'select');
INSERT INTO `user_power` VALUES ('148', 'budget_apply', '王五', 'update');
INSERT INTO `user_power` VALUES ('85', 'common_file', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('86', 'common_file', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('88', 'common_file', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('87', 'common_file', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('128', 'common_file', '张三', 'select');
INSERT INTO `user_power` VALUES ('100', 'common_file', '李四', 'select');
INSERT INTO `user_power` VALUES ('35', 'entry_warehouse', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('36', 'entry_warehouse', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('38', 'entry_warehouse', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('37', 'entry_warehouse', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('1', 'entry_warehouse', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('2', 'entry_warehouse', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('3', 'entry_warehouse', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('4', 'entry_warehouse', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('110', 'entry_warehouse', '张三', 'select');
INSERT INTO `user_power` VALUES ('91', 'entry_warehouse', '李四', 'add');
INSERT INTO `user_power` VALUES ('93', 'entry_warehouse', '李四', 'delete');
INSERT INTO `user_power` VALUES ('95', 'entry_warehouse', '李四', 'select');
INSERT INTO `user_power` VALUES ('94', 'entry_warehouse', '李四', 'update');
INSERT INTO `user_power` VALUES ('90', 'entry_warehouse', '王五', 'select');
INSERT INTO `user_power` VALUES ('76', 'expenditure', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('78', 'expenditure', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('80', 'expenditure', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('79', 'expenditure', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('123', 'expenditure', '张三', 'select');
INSERT INTO `user_power` VALUES ('142', 'expenditure', '王五', 'add');
INSERT INTO `user_power` VALUES ('143', 'expenditure', '王五', 'delete');
INSERT INTO `user_power` VALUES ('145', 'expenditure', '王五', 'select');
INSERT INTO `user_power` VALUES ('144', 'expenditure', '王五', 'update');
INSERT INTO `user_power` VALUES ('77', 'income', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('74', 'income', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('73', 'income', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('75', 'income', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('122', 'income', '张三', 'select');
INSERT INTO `user_power` VALUES ('138', 'income', '王五', 'add');
INSERT INTO `user_power` VALUES ('139', 'income', '王五', 'delete');
INSERT INTO `user_power` VALUES ('141', 'income', '王五', 'select');
INSERT INTO `user_power` VALUES ('140', 'income', '王五', 'update');
INSERT INTO `user_power` VALUES ('56', 'logback', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('57', 'logback', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('59', 'logback', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('58', 'logback', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('64', 'logback', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('39', 'out_warehouse', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('40', 'out_warehouse', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('42', 'out_warehouse', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('41', 'out_warehouse', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('5', 'out_warehouse', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('6', 'out_warehouse', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('7', 'out_warehouse', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('8', 'out_warehouse', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('111', 'out_warehouse', '张三', 'select');
INSERT INTO `user_power` VALUES ('96', 'out_warehouse', '李四', 'add');
INSERT INTO `user_power` VALUES ('97', 'out_warehouse', '李四', 'delete');
INSERT INTO `user_power` VALUES ('99', 'out_warehouse', '李四', 'select');
INSERT INTO `user_power` VALUES ('98', 'out_warehouse', '李四', 'update');
INSERT INTO `user_power` VALUES ('131', 'out_warehouse', '王五', 'select');
INSERT INTO `user_power` VALUES ('150', 'purchase_apply', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('72', 'purchase_apply', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('65', 'purchase_apply', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('151', 'purchase_apply', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('114', 'purchase_apply', '张三', 'add');
INSERT INTO `user_power` VALUES ('115', 'purchase_apply', '张三', 'delete');
INSERT INTO `user_power` VALUES ('117', 'purchase_apply', '张三', 'select');
INSERT INTO `user_power` VALUES ('116', 'purchase_apply', '张三', 'update');
INSERT INTO `user_power` VALUES ('101', 'purchase_apply', '李四', 'add');
INSERT INTO `user_power` VALUES ('102', 'purchase_apply', '李四', 'delete');
INSERT INTO `user_power` VALUES ('104', 'purchase_apply', '李四', 'select');
INSERT INTO `user_power` VALUES ('103', 'purchase_apply', '李四', 'update');
INSERT INTO `user_power` VALUES ('134', 'purchase_apply', '王五', 'add');
INSERT INTO `user_power` VALUES ('135', 'purchase_apply', '王五', 'delete');
INSERT INTO `user_power` VALUES ('137', 'purchase_apply', '王五', 'select');
INSERT INTO `user_power` VALUES ('136', 'purchase_apply', '王五', 'update');
INSERT INTO `user_power` VALUES ('69', 'sale_bill', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('71', 'sale_bill', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('68', 'sale_bill', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('70', 'sale_bill', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('118', 'sale_bill', '张三', 'add');
INSERT INTO `user_power` VALUES ('119', 'sale_bill', '张三', 'delete');
INSERT INTO `user_power` VALUES ('121', 'sale_bill', '张三', 'select');
INSERT INTO `user_power` VALUES ('120', 'sale_bill', '张三', 'update');
INSERT INTO `user_power` VALUES ('81', 'sale_statistics', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('82', 'sale_statistics', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('84', 'sale_statistics', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('83', 'sale_statistics', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('124', 'sale_statistics', '张三', 'add');
INSERT INTO `user_power` VALUES ('125', 'sale_statistics', '张三', 'delete');
INSERT INTO `user_power` VALUES ('127', 'sale_statistics', '张三', 'select');
INSERT INTO `user_power` VALUES ('126', 'sale_statistics', '张三', 'update');
INSERT INTO `user_power` VALUES ('43', 'stock_statistics', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('44', 'stock_statistics', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('46', 'stock_statistics', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('45', 'stock_statistics', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('9', 'stock_statistics', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('10', 'stock_statistics', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('11', 'stock_statistics', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('12', 'stock_statistics', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('112', 'stock_statistics', '张三', 'select');
INSERT INTO `user_power` VALUES ('129', 'stock_statistics', '李四', 'select');
INSERT INTO `user_power` VALUES ('132', 'stock_statistics', '王五', 'select');
INSERT INTO `user_power` VALUES ('48', 'stock_warning', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('49', 'stock_warning', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('51', 'stock_warning', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('50', 'stock_warning', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('13', 'stock_warning', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('14', 'stock_warning', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('15', 'stock_warning', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('27', 'stock_warning', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('113', 'stock_warning', '张三', 'select');
INSERT INTO `user_power` VALUES ('130', 'stock_warning', '李四', 'select');
INSERT INTO `user_power` VALUES ('133', 'stock_warning', '王五', 'select');
INSERT INTO `user_power` VALUES ('52', 'user', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('53', 'user', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('55', 'user', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('54', 'user', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('28', 'user', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('29', 'user', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('28', 'user', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('30', 'user', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('60', 'user_power', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('61', 'user_power', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('63', 'user_power', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('62', 'user_power', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('24', 'user_power', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('25', 'user_power', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('27', 'user_power', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('32', 'warehouse', 'administrator', 'add');
INSERT INTO `user_power` VALUES ('33', 'warehouse', 'administrator', 'delete');
INSERT INTO `user_power` VALUES ('31', 'warehouse', 'administrator', 'select');
INSERT INTO `user_power` VALUES ('34', 'warehouse', 'administrator', 'update');
INSERT INTO `user_power` VALUES ('152', 'warehouse', '司徒正美', 'select');
INSERT INTO `user_power` VALUES ('17', 'warehouse', '吴邪', 'add');
INSERT INTO `user_power` VALUES ('23', 'warehouse', '吴邪', 'delete');
INSERT INTO `user_power` VALUES ('19', 'warehouse', '吴邪', 'select');
INSERT INTO `user_power` VALUES ('21', 'warehouse', '吴邪', 'update');
INSERT INTO `user_power` VALUES ('109', 'warehouse', '张三', 'select');
INSERT INTO `user_power` VALUES ('92', 'warehouse', '李四', 'select');
INSERT INTO `user_power` VALUES ('89', 'warehouse', '王五', 'select');

-- ----------------------------
-- Table structure for `warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `warehouse_no` varchar(100) NOT NULL DEFAULT '' COMMENT '仓库编号',
  `name` varchar(100) DEFAULT NULL COMMENT '仓库名称',
  `location` varchar(100) DEFAULT NULL COMMENT '地址',
  `volume` double(11,0) DEFAULT NULL COMMENT '容量',
  `rent` double(11,0) DEFAULT NULL COMMENT '租金'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('raw_warehouse_001', '原材料仓库', '上海市浦东新区', '10000', '10000');
INSERT INTO `warehouse` VALUES ('product_warehouse_001', '成品仓库', '上海市浦东新区', '10000', '10000');
INSERT INTO `warehouse` VALUES ('common_warehouse', '普通仓库', '上海市浦东新区', '10000', '10000');
INSERT INTO `warehouse` VALUES ('test1', '测试仓库1', '上海市静安区', '12', '65');

-- ----------------------------
-- Procedure structure for `proc_login`
-- ----------------------------
DROP PROCEDURE IF EXISTS `proc_login`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_login`(in username varchar(50))
begin
select * from user where username = username;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `proc_login_admin`
-- ----------------------------
DROP PROCEDURE IF EXISTS `proc_login_admin`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_login_admin`(in username varchar(50))
begin
select * from admin where username = username;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `currval`
-- ----------------------------
DROP FUNCTION IF EXISTS `currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `currval`(v_seq_name VARCHAR(50)) RETURNS int(11)
begin
 declare value integer;
 set value = 0;
 select current_val into value  from sequence where seq_name = v_seq_name;
   return value;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `nextval`
-- ----------------------------
DROP FUNCTION IF EXISTS `nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextval`(v_seq_name VARCHAR(50)) RETURNS int(11)
begin
    update sequence set current_val = current_val + increment_val  where seq_name = v_seq_name;
    return currval(v_seq_name);
end
;;
DELIMITER ;
