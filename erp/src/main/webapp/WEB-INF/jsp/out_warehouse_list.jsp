<%@ page import="java.util.List" %>
<%@ page import="com.springboot.erp.entity.Out_Warehouse" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.springboot.erp.entity.Warehouse" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/2/9
  Time: 20:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>出库单</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_flow_number");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_out_warehouse_by_flow_number",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if (data.result == "success") {
                            alert("删除成功");
                            location.reload(true);
                        } else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_flow_number");
            //dlg=document.getElementById("edit_dlg")
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_out_warehouse",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_flow_number').textbox("setValue", data.json_flow_number);  //一定要加 ; 给easyui的textbox赋值
                        $('#dlg_txt_list_no').textbox("setValue", data.json_list_no);
                        $('#dlg_txt_goods_no').textbox("setValue", data.json_goods_no);
                        $('#dlg_txt_kind').textbox("setValue", data.json_kind);
                        $('#dlg_txt_specification').textbox("setValue", data.json_specification);
                        $('#dlg_txt_goods_name').textbox("setValue", data.json_goods_name); //dlg_txt_number
                        $('#dlg_txt_single_price').textbox("setValue", data.json_single_price);
                        $('#dlg_txt_count').textbox("setValue", data.json_count);
                        $('#dlg_txt_total_price').textbox("setValue", data.json_total_price);
                        $('#dlg_txt_manufacturer').textbox("setValue", data.json_manufacturer);
                        $('#dlg_txt_out_date').textbox("setValue", data.json_out_date);
                        $('#dlg_txt_status').combobox("setValue", data.json_status);
                        $('#dlg_txt_warehouse_no').textbox("setValue", data.json_warehouse_no);
                        $('#dlg_txt_remarks').textbox("setValue", data.json_remarks);
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_out_warehouse",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"dlg_txt_flow_number": $("#dlg_txt_flow_number").val(),"dlg_txt_list_no": $("#dlg_txt_list_no").val(), "dlg_txt_goods_no": $("#dlg_txt_goods_no").val(),"dlg_txt_kind": $("#dlg_txt_kind").val(),
                    "dlg_txt_specification": $("#dlg_txt_specification").val(),"dlg_txt_goods_name": $("#dlg_txt_goods_name").val(),"dlg_txt_single_price": $("#dlg_txt_single_price").val(),"dlg_txt_count": $("#dlg_txt_count").val(),"dlg_txt_total_price": $("#dlg_txt_total_price").val(),"dlg_txt_manufacturer": $("#dlg_txt_manufacturer").val(),
                    "dlg_txt_out_date": $("#dlg_txt_out_date").val(),"dlg_txt_status": $("#dlg_txt_status").val(),"dlg_txt_warehouse_no": $("#dlg_txt_warehouse_no").val(),"dlg_txt_remarks": $("#dlg_txt_remarks").val()},  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("修改成功");
                        location.reload(true);
                    }
                    else {
                        alert("修改失败，库存余额不足/重复出库");
                    }
                },
                failure: function () {
                    alert("修改失败，库存余额不足/重复出库");
                }
            });
        }

        function add_out_warehouse() {
            $.ajax({
                type: "post",
                url: "/erp/add_out_warehouse",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_flow_number": $("#add_dlg_txt_flow_number").val(),"add_dlg_txt_list_no": $("#add_dlg_txt_list_no").val(), "add_dlg_txt_goods_no": $("#add_dlg_txt_goods_no").val(),"add_dlg_txt_kind": $("#add_dlg_txt_kind").val(),
                    "add_dlg_txt_specification": $("#add_dlg_txt_specification").val(),"add_dlg_txt_goods_name": $("#add_dlg_txt_goods_name").val(),"add_dlg_txt_single_price": $("#add_dlg_txt_single_price").val(),"add_dlg_txt_count": $("#add_dlg_txt_count").val(),"add_dlg_txt_total_price": $("#add_dlg_txt_total_price").val(),"add_dlg_txt_manufacturer": $("#add_dlg_txt_manufacturer").val(),
                    "add_dlg_txt_out_date": $("#add_dlg_txt_out_date").val(),"add_dlg_txt_status": $("#add_dlg_txt_status").val(),"add_dlg_txt_warehouse_no": $("#add_dlg_txt_warehouse_no").val(),"add_dlg_txt_remarks": $("#add_dlg_txt_remarks").val()},  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("添加成功");
                        location.reload(true);
                    }else {
                        alert("添加失败，库存余额不足");
                    }
                },
                failure: function () {
                    alert("添加失败，库存余额不足");
                }
            });
        }

    </script>
</head>
<body>
<%
    List<Out_Warehouse> out_warehouseList=new ArrayList<Out_Warehouse>();
    if(session.getAttribute("session_all_out_warehouse")!=null){
        out_warehouseList=(List)session.getAttribute("session_all_out_warehouse");
        session.removeAttribute("session_all_out_warehouse");
    }
    List<Warehouse> warehouseList=new ArrayList<Warehouse>();
    if(session.getAttribute("session_all_warehouse")!=null){
        warehouseList=(List)session.getAttribute("session_all_warehouse");
        session.removeAttribute("session_all_warehouse");
    }
    int number=(Integer)session.getAttribute("session_out_warehouse_PageCount")/20+1;
    int count=(Integer)session.getAttribute("session_out_warehouse_PageCount");
    session.removeAttribute("session_out_warehouse_PageCount");
    int pageNo=(Integer)session.getAttribute("session_out_warehouse_PageNo");
    session.removeAttribute("session_out_warehouse_PageNo");
    int no=(Integer)session.getAttribute("session_out_warehouse_record_No");
    session.removeAttribute("session_out_warehouse_record_No");

%>
<h2>出库列表</h2>
<form action="/erp/query_out_warehouse" method="post">
    流水号：
    <input type="text" name="query_txt_flow_number" class="easyui-textbox" id="query_txt_flow_number">
    单据编号：
    <input type="text" name="query_txt_list_no" class="easyui-textbox">
    商品名称：
    <input type="text" name="query_txt_goods_name" class="easyui-textbox" width="100px" height="20px">
    <input class="easyui-datetimebox" label="开始日期:" labelPosition="left" style="width:15%;" name="query_txt_start_date">
    --
    <input class="easyui-datetimebox" label="截止日期:" labelPosition="left" style="width:15%;" name="query_txt_end_date">
    <br>
    状态：
    <select class="easyui-combobox" name="query_txt_status" id="query_txt_status" style="width:80px;">
        <option value=""></option>
        <option value="未出库">未出库</option>
        <option value="已出库">已出库</option>
    </select>
    仓库编号：
    <input type="text" name="select_warehouse_no" class="easyui-textbox" width="100px" height="20px">
    <%--<select class="easyui-combobox" name="select_warehouse_no" id="select_warehouse_no" style="width:80px;">
        <option value=""></option>
        <%
            for(Warehouse wh:warehouseList ){
        %>
        <option value=<%=wh.getWarehouse_no()%>><%=wh.getWarehouse_no()%></option>
        <%
            }
        %>
    </select>--%>
    <input type="submit" value="查询" class="mybutton">
</form>
<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>流水号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_flow_number" id="dlg_txt_flow_number">
            </td>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_list_no" id="dlg_txt_list_no">
            </td>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_goods_no" id="dlg_txt_goods_no" >
            </td>
        </tr>
        <tr>
            <td>种类：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_kind" id="dlg_txt_kind">
            </td>
            <td>规格：</td>
            <td><input type="text" class="easyui-textbox" name="dlg_txt_specification" id="dlg_txt_specification">
            </td>
            <td>名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_goods_name" id="dlg_txt_goods_name">
            </td>
        </tr>
        <tr>
            <td>单价：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_single_price" id="dlg_txt_single_price">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_count" id="dlg_txt_count">
            </td>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_total_price" id="dlg_txt_total_price">
            </td>
        </tr>
        <tr>
            <td>制造商：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_manufacturer" id="dlg_txt_manufacturer">
            </td>
            <td>出库日期：</td>
            <td>
                <input class="easyui-datetimebox"   style="width:100%;" name="dlg_txt_out_date" id="dlg_txt_out_date">
                <%-- <input type="text" class="easyui-textbox" name="dlg_txt_entry_date" id="dlg_txt_entry_date">--%>
            </td>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_status" id="dlg_txt_status" style="width:80px;">
                    <option value="" ></option>
                    <option value="未出库" >未出库</option>
                    <option value="已出库">已出库</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_no" id="dlg_txt_warehouse_no">
            </td>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_remarks" id="dlg_txt_remarks">
            </td>
        </tr>
        <tr align="center">
            <td colspan="6">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>流水号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_flow_number" id="add_dlg_txt_flow_number" readonly>
            </td>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_list_no" id="add_dlg_txt_list_no">
            </td>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_no" id="add_dlg_txt_goods_no" >
            </td>
        <tr>
            <td>种类：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_kind" id="add_dlg_txt_kind">
            </td>

            <td>规格：</td>
            <td><input type="text" class="easyui-textbox" name="add_dlg_txt_specification" id="add_dlg_txt_specification">
            </td>
            <td>名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_name" id="add_dlg_txt_goods_name">
            </td>
        </tr>
        <tr>
            <td>单价：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_single_price" id="add_dlg_txt_single_price">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_count" id="add_dlg_txt_count">
            </td>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_total_price" id="add_dlg_txt_total_price">
            </td>
        </tr>
        <tr>
            <td>制造商：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_manufacturer" id="add_dlg_txt_manufacturer">
            </td>
            <td>出库日期：</td>
            <td>
                <input class="easyui-datetimebox"   style="width:100%;" name="add_dlg_txt_out_date" id="add_dlg_txt_out_date">
                <%-- <input type="text" class="easyui-textbox" name="dlg_txt_entry_date" id="dlg_txt_entry_date">--%>
            </td>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_status" id="add_dlg_txt_status" style="width:80px;">
                    <option value="" ></option>
                    <option value="未出库" >未出库</option>
                    <option value="已出库">已出库</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_no" id="add_dlg_txt_warehouse_no">
            </td>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_no" id="add_dlg_txt_remarks">
            </td>
        </tr>
        <tr align="center">
            <td colspan="6">
                <input type="button" value="确定" class="mybutton" onclick="add_out_warehouse()">
            </td>
        </tr>
    </table>
</div>

<table border="1" class="mytable" width="80%" align="center">
    <tr>
        <th colspan="2"><font size="1px">序号</font></th>
        <th><font size="1px">流水号</font></th>
        <th><font size="1px">单据编号</font></th>
        <th><font size="1px">商品编号</font></th>
        <th><font size="1px">种类</font></th>
        <th><font size="1px">规格</font></th>
        <th><font size="1px">名称</font></th>
        <th><font size="1px">单价</font></th>
        <th><font size="1px">数量</font></th>
        <th><font size="1px">总金额</font></th>
        <th><font size="1px">制造商</font></th>
        <th><font size="1px">出入库日期</font></th>
        <th><font size="1px">状态</font></th>
        <th><font size="1px">仓库编号</font></th>
        <th><font size="1px">备注</font></th>
    </tr>
    <%
        for(Out_Warehouse ow:out_warehouseList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_flow_number"  value=<%=ow.getFlow_number()%>></td>
        <td align="center"><%=no++%></td>
        <td align="center" width="90px">
            <%=ow.getFlow_number()%>
            <%--<img src="../images/main/编辑图标.jpg" width="10px" height="10px" onclick="edit_dialog()">&emsp;--%>
            <%-- <a href="javascript:edit_dialog()"><img src="../images/main/编辑图标.jpg" width="10px" height="10px"></a>--%>
        </td>
        <td align="center"><input size="10" style="background-color:#fafafa;" type="text" value=<%=ow.getList_no()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getGoods_no()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getKind()%>></td>
        <td align="center"><input size="10" style="background-color:#fafafa;" type="text" value=<%=ow.getSpecification()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getGoods_name()%>></td>
        <td align="center"><input size="3" style="background-color:#fafafa;" type="text" value=<%=ow.getSingle_price()%>></td>
        <td align="center"><input size="3" style="background-color:#fafafa;" type="text" value=<%=ow.getCount()%>></td>
        <td align="center"><input size="3" style="background-color:#fafafa;" type="text" value=<%=ow.getTotal_price()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getManufacturer()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getOut_date()%>></td>
        <td align="center"><input size="3" style="background-color:#fafafa;" type="text" value=<%=ow.getStatus()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getWarehouse_no()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ow.getRemarks()%>></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="16" height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/out_warehouse_list?op=previous">上一页</a>
            <a href="/erp/out_warehouse_list?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
            <%--<a href="javascript:update_submit()">修改</a>--%>
            <a href="javascript:show_selected_dialog()" <%--onclick="$('#edit_dlg').dialog('open')"--%>>编辑</a>
        </td>
    </tr>
</table>
</body>
</html>
