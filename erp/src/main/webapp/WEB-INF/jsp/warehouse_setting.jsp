<%@ taglib prefix="border-color" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.List" %>
<%@ page import="com.springboot.erp.entity.Warehouse" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.springboot.erp.entity.Stock_statistics" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/1/27
  Time: 20:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>仓库设置</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_warehouse_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_warehouse_by_warehouse_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if (data.result == "success") {
                        alert("删除成功");
                        location.reload(true);
                        }
                        else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_warehouse_no");
            //dlg=document.getElementById("edit_dlg")
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_warehouse",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_warehouse_no').textbox("setValue",data.json_warehouse_no);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_warehouse_name').textbox("setValue",data.json_name);
                        $('#dlg_txt_warehouse_location').textbox("setValue",data.json_location);
                        $('#dlg_txt_warehouse_volume').textbox("setValue",data.json_volume);
                        $('#dlg_txt_warehouse_rent').textbox("setValue",data.json_rent);
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_warehouse",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"dlg_txt_warehouse_no": $("#dlg_txt_warehouse_no").val(),"dlg_txt_warehouse_name": $("#dlg_txt_warehouse_name").val(), "dlg_txt_warehouse_location": $("#dlg_txt_warehouse_location").val(),"dlg_txt_warehouse_volume": $("#dlg_txt_warehouse_volume").val(),
                    "dlg_txt_warehouse_rent": $("#dlg_txt_warehouse_rent").val()},  //id选择器
                success: function (data) {
                    if (data.result == "success") {
                        alert("修改成功");
                        location.reload(true);
                    }
                    else if(data.result=="illegal"){
                        alert("输入不合法，修改失败");
                    }
                    else {
                        alert("您没有权限修改！！！");
                    }
                }
            });

        }

        function add_warehouse() {
            $.ajax({
                type: "post",
                url: "/erp/add_warehouse",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_warehouse_no": $("#add_dlg_txt_warehouse_no").val(),"add_dlg_txt_warehouse_name": $("#add_dlg_txt_warehouse_name").val(), "add_dlg_txt_warehouse_location": $("#add_dlg_txt_warehouse_location").val(),"add_dlg_txt_warehouse_volume": $("#add_dlg_txt_warehouse_volume").val(),
                    "add_dlg_txt_warehouse_rent": $("#add_dlg_txt_warehouse_rent").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success"){
                    alert("添加成功");
                    location.reload(true);
                    }
                    else if(data.result=="illegal"){
                        alert("输入不合法，添加失败");
                    }
                    else {
                        alert("您没有权限添加！！！");
                    }
                }
            });
        }

        /*function show_selected_warehouse_detail() {
            obj = document.getElementsByName("ck_warehouse_no");
            //dlg=document.getElementById("edit_dlg")
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_warehouse_detail",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                       /!* $('#dlg_txt_warehouse_no').textbox("setValue",data.json_warehouse_no);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_warehouse_name').textbox("setValue",data.json_name);
                        $('#dlg_txt_warehouse_location').textbox("setValue",data.json_location);
                        $('#dlg_txt_warehouse_volume').textbox("setValue",data.json_volume);
                        $('#dlg_txt_warehouse_rent').textbox("setValue",data.json_rent);*!/
                        $('#warning_dlg').dialog('open');
                    }
                });
            }
        }*/
    </script>




</head>
<body>
<%
   List<Warehouse> warehouseList=new ArrayList<Warehouse>();
    if(session.getAttribute("all_warehouses_session")!=null){
        warehouseList=(List)session.getAttribute("all_warehouses_session");
        session.removeAttribute("all_warehouses_session");
    }
    int number=(Integer)session.getAttribute("warehousesPageCount_session")/20+1;
    int count=(Integer)session.getAttribute("warehousesPageCount_session");
    session.removeAttribute("warehousesPageCount_session");
    int pageNo=(Integer)session.getAttribute("warehousesPageNo_session");
    session.removeAttribute("warehousesPageNo_session");
    int no=(Integer)session.getAttribute("warehouse_distribution_record_No");
    session.removeAttribute("warehouse_distribution_record_No");
  /*  List<Stock_statistics> stock_statistics_detail_List=new ArrayList<Stock_statistics>();
    if(session.getAttribute("session_show_selected_warehouse_detail")!=null){
        stock_statistics_detail_List=(List)session.getAttribute("session_show_selected_warehouse_detail");
    }*/
%>
<br>
<form action="/erp/query_warehouse" method="post">
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
    编号：<input type="text" value="" name="query_txt_warehouse_no" class="easyui-textbox">&emsp;
    名称：<input type="text" value="" name="query_txt_name" class="easyui-textbox">&emsp;
    地址：<input type="text" value="" name="query_txt_location" class="easyui-textbox">&emsp;
    <%--容积：<input type="text" value="" name="query_txt_volume" class="mytext">&emsp;--%>
    <input type="submit" class="mybutton" value="查询">
</form>
<div id="edit_dlg" class="easyui-dialog" title="编辑仓库信息" data-options="iconCls:'icon-save'"
     style="width:700px;height:250px;padding:10px" closed="true">
    <table>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_no" id="dlg_txt_warehouse_no">
            </td>
            <td>仓库名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_name" id="dlg_txt_warehouse_name">
            </td>
        </tr>
        <tr>
            <td>仓库地址：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_location" id="dlg_txt_warehouse_location">
            </td>
            <td>仓库容量：</td>
            <td><input type="text" class="easyui-textbox" name="dlg_txt_warehouse_volume" id="dlg_txt_warehouse_volume">
            </td>
        </tr>
        <tr>
            <td>仓库租金：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_rent" id="dlg_txt_warehouse_rent">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<div id="add_dlg" class="easyui-dialog" title="添加仓库信息" data-options="iconCls:'icon-save'"
     style="width:700px;height:250px;padding:10px" closed="true">
    <table>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_no" id="add_dlg_txt_warehouse_no">
            </td>
            <td>仓库名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_name" id="add_dlg_txt_warehouse_name">
            </td>
        </tr>
        <tr>
            <td>仓库地址：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_location" id="add_dlg_txt_warehouse_location">
            </td>
            <td>仓库容量：</td>
            <td><input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_volume" id="add_dlg_txt_warehouse_volume">
            </td>
        </tr>
        <tr>
            <td>仓库租金：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_rent" id="add_dlg_txt_warehouse_rent">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="add_warehouse()">
            </td>
        </tr>
    </table>
</div>

<%--<div id="warning_dlg" class="easyui-dialog" title="设置库存预警" data-options="iconCls:'icon-save'"
     style="width:700px;height:250px;padding:10px" closed="true">
    <table>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="warning_dlg_txt_warehouse_no" id="warning_dlg_txt_warehouse_no"
                       value=<%=stock_statistics_detail_List.get(0).getWarehouse_no()%>>
            </td>
        </tr>
        <%
            for (Stock_statistics ss:stock_statistics_detail_List)
        %>
        <tr>
            <input type="text" class="easyui-textbox" name="warning_dlg_txt_warehouse_no" id="warning_dlg_txt_warehouse_no"
                   value=<%=stock_statistics_detail_List.get(0).getWarehouse_no()%>>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="add_warehouse()">
            </td>
        </tr>
    </table>
</div>--%>

<table border="1" class="mytable" width="70%" align="center" >
<%--<table class="easyui-datagrid" title="所有仓库信息" style="width:1000px;height:800px">--%>
   <%-- <p><%=num%></p>--%>
    <tr>
       <th colspan="2">序号</th>
        <th>仓库编号</th>
        <th>仓库名称</th>
        <th>仓库地址</th>
        <th>仓库容量</th>
        <th>仓库租金</th>

    </tr>


    <%
        for(Warehouse wh:warehouseList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_warehouse_no"  value=<%=wh.getWarehouse_no()%>></td>
        <td align="center"><%=no++%></td>
        <td align="center"><%=wh.getWarehouse_no()%></td>
        <td align="center"><%=wh.getName()%></td>
        <td align="center"><%=wh.getLocation()%></td>
        <td align="center"><%=wh.getVolume()%></td>
        <td align="center"><%=wh.getRent()%></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="7" height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/warehouse_distribution?op=previous">上一页</a>
            <a href="/erp/warehouse_distribution?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
            <a href="javascript:show_selected_dialog()">编辑</a>
            <%--<a href="javascript">设置库存预警</a>--%>
        </td>
    </tr>
</table>
</body>
</html>
