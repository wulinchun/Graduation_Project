<%@ page import="com.springboot.erp.entity.Budget_apply" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/9
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>预算申请</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_budget_apply_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success") {
                            alert("删除成功");
                            location.reload(true);
                        }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}

        function show_selected_budget_apply() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_budget_apply",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_index_no').textbox("setValue",data.json_index_no);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_username_apply').textbox("setValue",data.json_username_apply);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_dept').textbox("setValue",data.json_dept);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_apply_time').textbox("setValue",data.json_apply_time);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_username_deal').textbox("setValue",data.json_username_deal);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_deal_time').textbox("setValue",data.json_deal_time);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_status').textbox("setValue",data.json_status);  //一定要加' ;' 给easyui的textbox赋值
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

    </script>
</head>
<%
    List<Budget_apply> budget_applyList=new ArrayList<Budget_apply>();
    if(session.getAttribute("session_budget_apply")!=null){
        budget_applyList=(List)session.getAttribute("session_budget_apply");
        session.removeAttribute("session_budget_apply");
    }
    int number=(Integer)session.getAttribute("session_budget_apply_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_budget_apply_PageCount");
    session.removeAttribute("session_budget_apply_PageCount");
    int pageNo=(Integer)session.getAttribute("session_budget_apply_PageNo");
    session.removeAttribute("session_budget_apply_PageNo");
    int no=(Integer)session.getAttribute("session_budget_apply_record_No");
    session.removeAttribute("session_budget_apply_record_No");
%>

<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>申请者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_username_apply" id="dlg_txt_username_apply" readonly>
                <input type="hidden" class="easyui-textbox" name="dlg_txt_index_no" id="dlg_txt_index_no">
            </td>
            <td>申请部门：</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_dept" id="dlg_txt_dept" style="width:80px;" readonly="">
                    <option value="销售部" >销售部</option>
                    <option value="人事部">人事部</option>
                    <option value="采购部" >采购部</option>
                    <option value="财务部">财务部</option>
                    <option value="产品部" >产品部</option>
                    <option value="测试部">测试部</option>
                    <option value="研发部" >研发部</option>
                    <option value="行政部">行政部</option>
                    <option value="项目部" >项目部</option>
                    <option value="质检部">质检部</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>申请时间：</td>
            <td>
                <input class="easyui-datetimebox" label="申请时间:" labelPosition="left" style="width:100%;" name="dlg_txt_apply_time" id="dlg_txt_apply_time" readonly>
            </td>
        </tr>
        <tr>
            <td>处理者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_username_deal" id="dlg_txt_username_deal" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="处理时间:" labelPosition="left" style="width:100%;" name="dlg_txt_deal_time" id="dlg_txt_deal_time" readonly>
            </td>
        </tr>
        <tr>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_dept" id="dlg_txt_status" name="dlg_txt_status" style="width:80px;" readonly>
                    <option value="审批通过" >审批通过</option>
                    <option value="审批不通过">审批不通过</option>
                </select>
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<h2>提交预算申请表</h2>
<form method="post" action="/erp/upload_budget_apply_table" enctype="multipart/form-data">
    <label>文件名：</label>
    <input type="text" class="easyui-textbox" name="filename">
    <input class="easyui-datetimebox" label="发布时间:" labelPosition="left" style="width:20%;" name="upload_time">
    <input type="file" name="file">
    <input type="submit" class="mybutton" value="发布">
</form>

<form method="post" action="/erp/query_budget_apply">
    申请者：
    <input type="text" class="easyui-textbox" name="txt_apply_user">
    <input type="submit" class="mybutton" value="查询">
</form>

<br>
<%--<a href="/erp/download_budget_apply_table"><h2>点击此处下载预算申请表</h2></a>--%>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>申请者</th>
        <th>部门</th>
        <th>申请时间</th>
        <th>处理者</th>
        <th>处理时间</th>
        <th>状态</th>
        <th>下载附件</th>
    </tr>
    <%
        for(Budget_apply budget_apply:budget_applyList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=budget_apply.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getUsername_apply()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getDept()%>></td>
        <td><input size="15" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getApply_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getUsername_deal()%>></td>
        <td><input size="15" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getDeal_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getStatus()%>></td>
        <td><a href="/erp/download_appendix?ck_index_no=<%=budget_apply.getIndex_no()%>">下载附件</a> </td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="9"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/budget_apply?op=previous">上一页</a>
            <a href="/erp/budget_apply?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:show_selected_budget_apply()">查看</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
<body>
</body>
</html>
