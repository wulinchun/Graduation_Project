<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.springboot.erp.entity.Logback" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/2/28
  Time: 20:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>日志管理</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
</head>
<%--<style type="text/css">
    #logback_table
    {
        font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
        width:100%;
        border-collapse:collapse;
    }
    #logback_table td, #logback_table th
    {
        font-size:1em;
        border:1px solid #98bf21;
        padding:3px 7px 2px 7px;
    }
    #logback_table th
    {
        font-size:1.1em;
        text-align:left;
        padding-top:5px;
        padding-bottom:4px;
        background-color:#A7C942;
        color:#ffffff;
    }
    #logback_table tr.alt td
    {
        color:#000000;
        background-color:#EAF2D3;
    }
</style>--%>

<script type="text/javascript">
    function delete_all() {

        if(confirm("确实要删除吗？")){
            $.ajax({        //注意：写ajax时一定要先导入jquery.js
                type: "post",
                url: "/erp/delete_all_logback",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                traditional: true,
                data:{"username":$("#username").val()},
                success: function (data) {
                    if (data.result == "success") {
                    alert("删除成功");
                    location.reload(true);
                }else {
                        alert("您的权限不够")
                    }
                }
            });
        }else {
            alert("已经取消了删除操作");
        }}
</script>
<body>
<%
    String op="all";  //op=all表示显示所有，op=query表示条件查询
    if(session.getAttribute("session_op")!=null){
        op=(String)session.getAttribute("session_op");
    }
    List<Logback> logback_list=new ArrayList<Logback>();
    if(session.getAttribute("session_logback")!=null){
        logback_list=(List)session.getAttribute("session_logback");
        session.removeAttribute("session_logback");
    }

    int number=(Integer)session.getAttribute("session_logback_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_logback_PageCount");
    session.removeAttribute("session_logback_PageCount");
    int pageNo=(Integer)session.getAttribute("session_logback_PageNo");
    session.removeAttribute("session_logback_PageNo");
    int no=(Integer)session.getAttribute("session_logback_record_No");
    session.removeAttribute("session_logback_record_No");
    String username="";
    if (session.getAttribute("session_username")!=null){
        username=(String)session.getAttribute("session_username");
    }
%>
<form action="/erp/logback" method="get">
    操作模块：
    <input type="text" name="query_txt_operation_module" class="easyui-textbox" id="query_txt_operation_module">
    操作人员：
    <input type="text" name="query_txt_operator" class="easyui-textbox" id="query_txt_operator">
    <input class="easyui-datetimebox" label="开始时间:" labelPosition="left" style="width:20%;" name="query_txt_start_time">
    --
    <input class="easyui-datetimebox" label="结束时间:" labelPosition="left" style="width:20%;" name="query_txt_end_time">
    操作状态：
    <select class="easyui-combobox" name="query_operation_status" id="query_operation_status" style="width:80px;">
        <option value=""></option>
        <option value="成功">成功</option>
        <option value="失败">失败</option>
    </select>
    <input type="hidden" name="op" value="query"><input type="hidden" name="op1" value="query">
    <input type="submit" value="查询" class="mybutton">
</form>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
<input type="button" value="清除所有日志" class="mybutton" onclick="delete_all()">
<input type="hidden" id="username" value=<%=username%>>
<table border="1" class="mytable" width="80%" align="center">
    <tr>
        <th>序号</th>
        <th>操作模块</th>
        <th>操作详情</th>
        <th>操作人员</th>
        <th>操作状态</th>
        <th>操作时间</th>
    </tr>
    <%
        for(Logback logback:logback_list){
    %>
    <tr class="alt">
        <td align="center"><%=no++%></td>
        <td><font size="1px"><%=logback.getOperation_module()%></font></td>
        <td><font size="1px"><%=logback.getOperation_detail()%></font></td>
        <td><font size="1px"><%=logback.getOperator()%></font></td>
        <td><font size="1px"><%=logback.getOperation_status()%></font></td>
        <td><font size="1px"><%=logback.getOperation_time()%></font></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="6"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/logback?op=<%=op%>&&op1=previous">上一页</a>
            <a href="/erp/logback?op=<%=op%>&&op1=next">下一页</a>
            总共<%=count%>条
        </td>
    </tr>
</table>
</body>
</html>
