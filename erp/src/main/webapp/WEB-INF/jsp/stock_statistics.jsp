<%@ page import="com.springboot.erp.entity.Stock_statistics" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.springboot.erp.entity.Stock_statistics" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/2/13
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>库存统计</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
</head>
<body>
<%
    List<Stock_statistics> stock_statisticsList=new ArrayList<Stock_statistics>();
    if(session.getAttribute("session_all_stock_statistics")!=null){
        stock_statisticsList=(List)session.getAttribute("session_all_stock_statistics");
        //System.out.println("stock_statistics："+stock_statisticsList.size());
        session.removeAttribute("session_all_stock_statistics");

    }
    int number=(Integer)session.getAttribute("session_all_stock_statistics_PageCount")/20+1;
    int count=(Integer)session.getAttribute("session_all_stock_statistics_PageCount");
    session.removeAttribute("session_all_stock_statistics_PageCount");
    int pageNo=(Integer)session.getAttribute("session_all_stock_statistics_PageNo");
    session.removeAttribute("session_all_stock_statistics_PageNo");
    int no=(Integer)session.getAttribute("session_stock_statistics_record_No");
    session.removeAttribute("session_stock_statistics_record_No");
%>
<script type="text/javascript">
    function show_selected_dialog() {
        obj = document.getElementsByName("ck_index_no");
        check_val = [];
        for (k in obj) {
            if (obj[k].checked)
                check_val.push(obj[k].value);
        }
        if (check_val.length > 1) {
            alert("只能选取一行");
        } else if (check_val.length == 1) {
            $.ajax({        //注意：写ajax时一定要先导入jquery.js
                type: "post",
                url: "/erp/show_selected_stock_statistics",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                traditional: true,
                data: {'check_val': check_val},
                success: function (data) {
                    $('#dlg_txt_index_no').textbox("setValue",data.json_index_no)
                    $('#dlg_txt_warehouse_no').textbox("setValue",data.json_warehouse_no);
                    $('#dlg_txt_goods_no').textbox("setValue",data.json_goods_no);
                    $('#dlg_txt_kind').textbox("setValue",data.json_kind);
                    $('#dlg_txt_specification').textbox("setValue",data.json_specification);
                    $('#dlg_txt_goods_name').textbox("setValue",data.json_goods_name); //dlg_txt_number
                    $('#dlg_txt_single_price').textbox("setValue",data.json_single_price);
                    $('#dlg_txt_count').textbox("setValue",data.json_count);
                    $('#dlg_txt_total_price').textbox("setValue",data.json_total_price);
                    $('#dlg_txt_remarks').textbox("setValue",data.json_remarks);
                    $('#edit_dlg').dialog('open');
                }
            });
        }
    }

    function update_selected() {
        $.ajax({
            type: "post",
            url: "/erp/update_stock_statistics",
            dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
            data: {"dlg_txt_index_no": $("#dlg_txt_index_no").val(),"dlg_txt_warehouse_no": $("#dlg_txt_warehouse_no").val(), "dlg_txt_goods_no": $("#dlg_txt_goods_no").val(),"dlg_txt_kind": $("#dlg_txt_kind").val(),
                "dlg_txt_specification": $("#dlg_txt_specification").val(),"dlg_txt_goods_name": $("#dlg_txt_goods_name").val(),"dlg_txt_single_price": $("#dlg_txt_single_price").val(),"dlg_txt_count": $("#dlg_txt_count").val(),
                "dlg_txt_total_price": $("#dlg_txt_total_price").val(),"dlg_txt_remarks": $("#dlg_txt_remarks").val()},  //id选择器
            success: function (data) {
                if(data.result=="success") {
                    alert("修改成功");
                    location.reload(true);
                }
                else {
                    alert("您没有权限修改！！！");
                }
            }
        });
    }

    function add_stock_statistics() {
        $.ajax({
            type: "post",
            url: "/erp/add_stock_statistics",
            dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
            data: {"add_dlg_txt_warehouse_no": $("#add_dlg_txt_warehouse_no").val(),"add_dlg_txt_goods_no": $("#add_dlg_txt_goods_no").val(), "add_dlg_txt_kind": $("#add_dlg_txt_kind").val(),"add_dlg_txt_specification": $("#add_dlg_txt_specification").val(),
                "add_dlg_txt_goods_name": $("#add_dlg_txt_goods_name").val(),"add_dlg_txt_single_price": $("#add_dlg_txt_single_price").val(),"add_dlg_txt_count": $("#add_dlg_txt_count").val(),"add_dlg_txt_remarks": $("#add_dlg_txt_remarks").val()},  //id选择器
            success: function (data) {
                if(data.result=="success"){
                alert("添加成功");
                location.reload(true);
            }else {
                alert("您没有权限添加！！！");
    }
        }});
    }

    function delete_submit() {
        obj = document.getElementsByName("ck_index_no");
        check_val = [];
        for(k in obj){
            if(obj[k].checked)
                check_val.push(obj[k].value);

        }
        if(confirm("确实要删除吗？")){
            $.ajax({        //注意：写ajax时一定要先导入jquery.js
                type: "post",
                url: "/erp/delete_stock_statistics",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                traditional: true,
                data:{'check_val':check_val},
                success: function (data) {
                    if (data.result == "success") {
                        alert("删除成功");
                        location.reload(true);
                    }else {
                        alert("您没有权限删除！！！");
                    }
                }});
        }else {
            alert("已经取消了删除操作");
        }}
</script>
<form action="/erp/query_stock_statistics" method="post">
    商品编号：
    <input type="text" name="query_txt_goods_no" class="easyui-textbox" id="query_txt_goods_no">
    名称：
    <input type="text" name="query_txt_goods_name" class="easyui-textbox" id="query_txt_goods_name">
    仓库编号：
    <input type="text" name="query_txt_warehouse_no" class="easyui-textbox" id="query_txt_warehouse_no">
    <input type="submit" value="查询" class="mybutton">
</form>
<br>

<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td><input type="hidden" class="easyui-textbox" name="dlg_txt_index_no" id="dlg_txt_index_no"></td>
        </tr>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_warehouse_no" id="dlg_txt_warehouse_no">
            </td>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_goods_no" id="dlg_txt_goods_no" >
            </td>
            <td>种类：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_kind" id="dlg_txt_kind">
            </td>
        </tr>
        <tr>
            <td>规格：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_specification" id="dlg_txt_specification">
            </td>
            <td>名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_goods_name" id="dlg_txt_goods_name">
            </td>
            <td>单价：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_single_price" id="dlg_txt_single_price">
            </td>
        </tr>
        <tr>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_count" id="dlg_txt_count">
            </td>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_total_price" id="dlg_txt_total_price">
            </td>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_remarks" id="dlg_txt_remarks">
            </td>
        </tr>
        <tr align="center">
            <td colspan="11">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td><input type="hidden" class="easyui-textbox" name="add_dlg_txt_index_no" id="add_dlg_txt_index_no" readonly></td>
        </tr>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_no" id="add_dlg_txt_warehouse_no">
            </td>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_no" id="add_dlg_txt_goods_no" >
            </td>
            <td>种类：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_kind" id="add_dlg_txt_kind">
            </td>
        </tr>
        <tr>
            <td>规格：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_specification" id="add_dlg_txt_specification">
            </td>
            <td>名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_name" id="add_dlg_txt_goods_name">
            </td>
            <td>单价：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_single_price" id="add_dlg_txt_single_price">
            </td>
        </tr>
        <tr>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_count" id="add_dlg_txt_count">
            </td>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_total_price" id="add_dlg_txt_total_price" readonly>
            </td>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_remarks" id="add_dlg_txt_remarks">
            </td>
        </tr>
        <tr align="center">
            <td colspan="11">
                <input type="button" value="确定" class="mybutton" onclick="add_stock_statistics()">
            </td>
        </tr>
    </table>
</div>

<table border="1" class="mytable" width="80%" align="center">
    <tr>
        <th colspan="2">序号</th>
        <th>仓库编号</th>
        <th>商品编号</th>
        <th>种类</th>
        <th>规格</th>
        <th>名称</th>
        <th>单价</th>
        <th>数量</th>
        <th>总金额</th>
        <th>状态</th>
        <th>备注</th>
    </tr>
    <%
        for(Stock_statistics ss:stock_statisticsList){
    %>
    <tr>
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=ss.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getWarehouse_no()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getGoods_no()%>></td>
        <td align="center"><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getKind()%>></td>
        <td align="center" ><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getSpecification()%>></td>
        <td align="center" ><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getGoods_name()%>></td>
        <td align="center" ><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getSingle_price()%>></td>
        <td align="center" ><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getCount()%>></td>
        <td align="center" ><input size="5" style="background-color:#fafafa;" type="text" value=<%=ss.getTotal_price()%>></td>
        <%
            if(ss.getStatus().equals("过少")){
        %>
        <td align="center" width="80px" height="20px" bgcolor="yellow"><%=ss.getStatus()%></td>
        <%
            }
            if(ss.getStatus().equals("过多")){
        %>
        <td align="center" width="80px" height="20px" bgcolor="red"><%=ss.getStatus()%></td>
        <%
            }else if((!ss.getStatus().equals("过少"))&&(!ss.getStatus().equals("过多"))){
        %>
        <td align="center" width="80px" height="20px"><%=ss.getStatus()%></td>
        <%
            }
        %>
        <td align="center" width="80px" height="20px"><%=ss.getRemarks()%></td>
    </tr>
<%
    }
%>
    <tr class="alt">
        <td align="center" colspan="12"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/stock_statistics?op=previous">上一页</a>
            <a href="/erp/stock_statistics?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
            <a href="javascript:show_selected_dialog()" onclick="$('#edit_dlg').dialog('open')">编辑</a>
        </td>
    </tr>
</table>
</body>
</html>
