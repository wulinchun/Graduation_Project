<%@ page import="com.springboot.erp.entity.Purchase_apply" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/8
  Time: 20:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>采购清单</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_purchase_list_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if (data.result == "success") {
                            alert("删除成功");
                            location.reload(true);
                        }
                        else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/purchase_list_confirm",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#apply_dlg_txt_index_no').textbox("setValue",data.json_index_no);  //一定要加' ;' 给easyui的textbox赋值
                        $('#apply_dlg_txt_username').textbox("setValue",data.json_username);
                        $('#apply_dlg_txt_name').textbox("setValue",data.json_name);
                        $('#apply_dlg_txt_dept').textbox("setValue",data.json_dept);
                        $('#apply_dlg_txt_kind').textbox("setValue",data.json_kind);
                        $('#apply_dlg_txt_goods_name').textbox("setValue",data.json_goods_name); //dlg_txt_number
                        $('#apply_dlg_txt_count').textbox("setValue",data.json_count);
                        $('#apply_dlg_txt_date').textbox("setValue",data.json_apply_time);
                        $('#apply_dlg_txt_status').textbox("setValue",data.json_status);
                        $('#apply_dlg_txt_manufacturer').textbox("setValue",data.json_manufacturer);
                        $('#apply_dlg_txt_total_price').textbox("setValue",data.json_total_price);

                        $('#confirm_dlg').dialog('open');
                    }
                });
            }
        }

        function show_selected_dialog_2() {   //采购转入库
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/purchase_list_confirm",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#add_dlg_txt_kind').textbox("setValue",data.json_kind);
                        $('#add_dlg_txt_goods_name').textbox("setValue",data.json_goods_name); //dlg_txt_number
                        $('#add_dlg_txt_count').textbox("setValue",data.json_count);
                        $('#apply_dlg_txt_status').textbox("setValue",data.json_status);
                        $('#add_dlg_txt_manufacturer').textbox("setValue",data.json_manufacturer);
                        $('#add_dlg_txt_total_price').textbox("setValue",data.json_total_price);

                        $('#add_dlg').dialog('open');
                    }
                });
            }
        }

        function confirm_submit() {
            $.ajax({
                type: "post",
                url: "/erp/purchase_list_confirm_submit",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"apply_dlg_txt_index_no": $("#apply_dlg_txt_index_no").val(),"apply_dlg_txt_username": $("#apply_dlg_txt_username").val(), "apply_dlg_txt_name": $("#apply_dlg_txt_name").val(),"apply_dlg_txt_dept": $("#apply_dlg_txt_dept").val(),
                    "apply_dlg_txt_kind": $("#apply_dlg_txt_kind").val(),"apply_dlg_txt_goods_name": $("#apply_dlg_txt_goods_name").val(),"apply_dlg_txt_count": $("#apply_dlg_txt_count").val(),"apply_dlg_txt_date": $("#apply_dlg_txt_date").val(),
                    "apply_dlg_txt_status": $("#apply_dlg_txt_status").val(),"apply_dlg_txt_manufacturer": $("#apply_dlg_txt_manufacturer").val(),
                    "apply_dlg_txt_total_price": $("#apply_dlg_txt_total_price").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success") {
                        alert("确认成功");
                        location.reload(true);
                    }else {
                        alert("您没有权限修改！！！");
                    }

                }
            });
        }

        function purchase_shift_to_entry_warehouse() {   //采购转入库
            $.ajax({
                type: "post",
                url: "/erp/add_entry_warehouse",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_flow_number": $("#add_dlg_txt_flow_number").val(),"add_dlg_txt_list_no": $("#add_dlg_txt_list_no").val(), "add_dlg_txt_goods_no": $("#add_dlg_txt_goods_no").val(),"add_dlg_txt_kind": $("#add_dlg_txt_kind").val(),
                    "add_dlg_txt_specification": $("#add_dlg_txt_specification").val(),"add_dlg_txt_goods_name": $("#add_dlg_txt_goods_name").val(),"add_dlg_txt_single_price": $("#add_dlg_txt_single_price").val(),"add_dlg_txt_count": $("#add_dlg_txt_count").val(),"add_dlg_txt_total_price": $("#add_dlg_txt_total_price").val(),"add_dlg_txt_manufacturer": $("#add_dlg_txt_manufacturer").val(),
                    "add_dlg_txt_entry_date": $("#add_dlg_txt_entry_date").val(),"add_dlg_txt_status": $("#add_dlg_txt_status").val(),"add_dlg_txt_warehouse_no": $("#add_dlg_txt_warehouse_no").val(),"add_dlg_txt_remarks": $("#add_dlg_txt_remarks").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success") {
                        alert("添加成功");
                        location.reload(true);
                    }else {
                        alert("您没有权限添加！！！");
                    }
                }
            });
        }
    </script>
</head>
<body>
<%
    List<Purchase_apply> purchase_apply_pass_List=new ArrayList<Purchase_apply>();
    if(session.getAttribute("session_purchase_apply_pass")!=null){
        purchase_apply_pass_List=(List)session.getAttribute("session_purchase_apply_pass");
        session.removeAttribute("session_purchase_apply_pass");
    }
    int number=(Integer)session.getAttribute("session_purchase_apply_pass_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_purchase_apply_pass_PageCount");
    session.removeAttribute("session_purchase_apply_pass_PageCount");
    int pageNo=(Integer)session.getAttribute("session_purchase_apply_pass_PageNo");
    session.removeAttribute("session_purchase_apply_pass_PageNo");
    int no=(Integer)session.getAttribute("session_purchase_apply_pass_record_No");
    session.removeAttribute("session_purchase_apply_pass_record_No");
%>
<h2>采购清单</h2>
<form action="/erp/query_purchase_list" method="post">
用户名：
    <input type="text" name="query_txt_username" class="easyui-textbox" id="query_txt_username">
    <input class="easyui-datetimebox" label="开始日期:" labelPosition="left" style="width:15%;" name="query_txt_start_date">
    --
    <input class="easyui-datetimebox" label="截止日期:" labelPosition="left" style="width:15%;" name="query_txt_end_date">
    <input type="submit" value="查询" class="mybutton">
</form>

<div id="confirm_dlg" class="easyui-dialog" title="采购订单确认" data-options="iconCls:'icon-save'"
     style="width:800px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>单据号：</td>
            <td><input type="text" class="easyui-textbox" name="apply_dlg_txt_index_no" id="apply_dlg_txt_index_no"></td>
        </tr>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_username" id="apply_dlg_txt_username">
            </td>
            <td>姓名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_name" id="apply_dlg_txt_name">
            </td>
        </tr>
        <tr>
            <td>部门：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_dept" id="apply_dlg_txt_dept">
            </td>
            <td>种类</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_kind" id="apply_dlg_txt_kind">
            </td>
        </tr>
        <tr>
            <td>物品名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_goods_name" id="apply_dlg_txt_goods_name">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_count" id="apply_dlg_txt_count">
            </td>
        </tr>
        <tr>
            <td>申请日期：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_date" id="apply_dlg_txt_date">
            </td>
            <td>
                <select class="easyui-combobox" name="apply_dlg_txt_status" id="apply_dlg_txt_status" style="width:200px;">
                    <option value="" ></option>
                    <option value="已确认">已确认</option>
                    <option value="已购买">已购买</option>
                    <option value="已到货">已到货</option>
                    <option value="已入库">已入库</option>
                    <option value="通过">通过</option>
                    <option value="不通过">不通过</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>供应商：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_manufacturer" id="apply_dlg_txt_manufacturer">
            </td>
            <td>预算总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_total_price" id="apply_dlg_txt_total_price">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="confirm_submit()">
            </td>
        </tr>
    </table>
</div>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>流水号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_flow_number" id="add_dlg_txt_flow_number" readonly>
            </td>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_list_no" id="add_dlg_txt_list_no">
            </td>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_no" id="add_dlg_txt_goods_no" >
            </td>
        </tr>
        <tr>
            <td>种类：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_kind" id="add_dlg_txt_kind">
            </td>
            <td>规格：</td>
            <td><input type="text" class="easyui-textbox" name="add_dlg_txt_specification" id="add_dlg_txt_specification">
            </td>
            <td>名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_name" id="add_dlg_txt_goods_name">
            </td>
        </tr>
        <tr>
            <td>单价：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_single_price" id="add_dlg_txt_single_price">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_count" id="add_dlg_txt_count">
            </td>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_total_price" id="add_dlg_txt_total_price">
            </td>
        </tr>
        <tr>
            <td>制造商：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_manufacturer" id="add_dlg_txt_manufacturer">
            </td>
            <td>入库日期：</td>
            <td>
                <input class="easyui-datetimebox"   style="width:100%;" name="add_dlg_txt_entry_date" id="add_dlg_txt_entry_date">
                <%-- <input type="text" class="easyui-textbox" name="dlg_txt_entry_date" id="dlg_txt_entry_date">--%>
            </td>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_status" id="add_dlg_txt_status" style="width:80px;">
                    <option value="未入库" >未入库</option>
                    <option value="已入库">已入库</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>仓库编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_warehouse_no" id="add_dlg_txt_warehouse_no">
            </td>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_remarks" id="add_dlg_txt_remarks">
            </td>
        </tr>
        </tr>
        <tr align="center">
            <td colspan="6">
                <input type="button" value="确定" class="mybutton" onclick="purchase_shift_to_entry_warehouse()">
            </td>
        </tr>
    </table>
</div>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>用户名</th>
        <th>部门</th>
        <th>种类</th>
        <th>物品名称</th>
        <th>数量</th>
        <th>申请日期</th>
        <th>状态</th>
        <th>供应商</th>
        <th>预算总金额</th>
    </tr>
    <%
        for (Purchase_apply purchase_apply:purchase_apply_pass_List){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=purchase_apply.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getUsername()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getDept()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getKind()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getGoods_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getCount()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getApply_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getStatus()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getManufacturer()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getTotal_price()%>></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="11"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/purchase_list?op=previous">上一页</a>
            <a href="/erp/purchase_list?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:show_selected_dialog()">采购单确认</a>
            <a href="javascript:show_selected_dialog_2()">采购转入库</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
