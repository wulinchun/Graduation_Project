<%@ page import="com.springboot.erp.entity.Stock_statistics" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.springboot.erp.entity.Stock_warning" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/2/25
  Time: 20:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>库存预警</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
</head>
<body>
<%

    List<Stock_warning> stock_warningList=new ArrayList<Stock_warning>();
    if(session.getAttribute("session_stock_warning")!=null){
        stock_warningList=(List)session.getAttribute("session_stock_warning");
        session.removeAttribute("session_stock_warning");
    }
    String warehouse_no="";
    if (session.getAttribute("session_goods_no")!=null){
        warehouse_no=(String)session.getAttribute("session_goods_no");
        //session.removeAttribute("session_goods_no");
    }
%>
<script type="text/javascript">
    function update_stock_warning() {
        var sw_warehouse_no= document.getElementsByName("sw_warehouse_no");
        var sw_goods_no = document.getElementsByName("sw_goods_no");
        var sw_goodsname = document.getElementsByName("sw_goodsname");
        var sw_max = document.getElementsByName("sw_max");
        var sw_min = document.getElementsByName("sw_min");
        sw_warehouse_no_array=[];
        sw_goods_no_array = [];
        sw_goods_name_array = [];
        sw_max_array = [];
        sw_min_array = [];
        for (k in sw_warehouse_no) {
            sw_warehouse_no_array.push(sw_warehouse_no[k].value);
        }
        for (k in sw_goods_no) {
            sw_goods_no_array.push(sw_goods_no[k].value);
        }
        for (k in sw_goodsname) {
            sw_goods_name_array.push(sw_goodsname[k].value);
        }
        for (k in sw_max) {
            sw_max_array.push(sw_max[k].value);
        }
        for (k in sw_min) {
            sw_min_array.push(sw_min[k].value);
        }
            $.ajax({        //注意：写ajax时一定要先导入jquery.js
                type: "post",

                url: "/erp/update_stock_warning",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                traditional: true,
                data: {'sw_warehouse_no_array': sw_warehouse_no_array,'sw_goods_no_array': sw_goods_no_array,'sw_goods_name_array': sw_goods_name_array,'sw_max_array': sw_max_array,'sw_min_array': sw_min_array},
                success: function (data) {
                    if(data.result=="success"){
                   alert("修改成功");
                    location.reload(true);
                }else {
                        alert("您没有权限修改！！！");
                    }
                }
            });

    }
</script>
<form action="/erp/show_stock_statistics_warning" method="post">
    仓库编号：
    <input type="text" name="query_txt_warehouse_no" class="easyui-textbox" id="query_txt_warehouse_no">
    <input type="submit" value="查询" class="mybutton">
</form>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
<form action="/erp/add_delete_stock_statistics_warning" method="post">
    仓库编号：
    <input type="text" name="txt_warehouse_no" class="easyui-textbox" id="txt_warehouse_no">
    物品编号：
    <input type="text" name="txt_goods_no" class="easyui-textbox" id="txt_goods_no">
    <input type="submit" name="btn_submit" value="添加" class="mybutton">
    <input type="submit" name="btn_submit" value="删除" class="mybutton">
</form>

<br>


<table border="1" class="mytable" width="80%" align="center">
    <tr>
        <th colspan="4" align="center"><p>仓库编号：<%=warehouse_no%></p></th>
    </tr>
    <tr>
        <th>物品编号</th>
        <th>最大预警</th>
        <th>最小预警</th>

    </tr>
    <%--<form action="/erp/update_stock_warning" method="post">--%>
    <%
        for(Stock_warning sw:stock_warningList){
    %>
    <tr class="alt">

        <td align="center">
            <input type="hidden" name="sw_warehouse_no" value=<%=sw.getWarehouse_no()%>>
            <input type="text" size="50" style="background-color:#fafafa;" name="sw_goods_no" value=<%=sw.getGoods_no()%>>
        </td>
        <%--<td align="center" width="80px" height="20px"><input type="text" style="width:80px" name="sw_goods_name" value=<%=sw.getGoods_name()%>></td>--%>
        <td><input type="text" size="10" style="background-color:#fafafa;" name="sw_max" value=<%=sw.getMax()%>></td>
        <td><input type="text" size="10" style="background-color:#fafafa;" name="sw_min" value=<%=sw.getMin()%>></td>

    </tr>
    <%
        }
    %>
        <tr><td colspan="3" align="center"><input class="mybutton" onclick="update_stock_warning()" value="修改" readonly></td></tr>
    <%--</form>--%>
</table>
</body>
</html>
