<%@ page import="com.springboot.erp.entity.User_Power" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/4
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>权限管理</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_user_power_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success"){
                        alert("删除成功");
                        location.reload(true);
                    }
                    else {
                        alert("您没有权限删除！！！");
            }
                }});
            }else {
                alert("已经取消了删除操作");
            }}

        function add_user_power() {
            $.ajax({
                type: "post",
                url: "/erp/add_user_power",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_tablename": $("#add_dlg_txt_tablename").val(),"add_dlg_txt_username": $("#add_dlg_txt_username").val(),
                    "add_dlg_txt_power": $("#add_dlg_txt_power").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success"){
                    alert("添加成功");
                    location.reload(true);
                }else {
                        alert("您没有权限添加！！！");
                    }}
            });
        }
    </script>
</head>
<body>
<%
    List<User_Power> user_powerList=new ArrayList<User_Power>();
    if(session.getAttribute("session_user_power")!=null){
        user_powerList=(List)session.getAttribute("session_user_power");
        session.removeAttribute("session_user_power");
    }
    int number=(Integer)session.getAttribute("session_user_power_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_user_power_PageCount");
    session.removeAttribute("session_user_power_PageCount");
    int pageNo=(Integer)session.getAttribute("session_user_power_PageNo");
    session.removeAttribute("session_user_power_PageNo");
    int no=(Integer)session.getAttribute("session_user_power_record_No");
    session.removeAttribute("session_user_power_record_No");
%>
<form action="/erp/query_user_power" method="post">
    表名：
    <input type="text" name="query_txt_tablename" class="easyui-textbox" id="query_txt_tablename">
    用户名：
    <input type="text" name="query_txt_username" class="easyui-textbox" id="query_txt_username">
    <input type="submit" value="查询" class="mybutton">
</form>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:600px;height:300px;padding:10px" closed="true">
    <table>
        <tr>
            <td>表名：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_tablename" id="add_dlg_txt_tablename" style="width:200px;">
                    <option value="warehouse" >warehouse</option>
                    <option value="entry_warehouse">entry_warehouse</option>
                    <option value="out_warehouse" >out_warehouse</option>
                    <option value="stock_statistics">stock_statistics</option>
                    <option value="stock_warning" >stock_warning</option>
                    <option value="user" >user</option>
                    <option value="logback" >logback</option>
                    <option value="user_power" >user_power</option>
                    <option value="purchase_apply" >purchase_apply</option>
                    <option value="budget_apply" >budget_apply</option>
                    <option value="sale_bill" >sale_bill</option>
                    <option value="income" >income</option>
                    <option value="expenditure" >expenditure</option>
                    <option value="sale_statistics" >sale_statistics</option>
                    <option value="common_file" >common_file</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_username" id="add_dlg_txt_username" >
            </td>
        </tr>
        <tr>
            <td>权限：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_power" id="add_dlg_txt_power" style="width:100px;">
                    <option value="add" >add</option>
                    <option value="delete">delete</option>
                    <option value="update" >update</option>
                    <option value="select">select</option>
                </select>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2">
                <input type="button" value="确定" class="mybutton" onclick="add_user_power()">
            </td>
        </tr>
    </table>
</div>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>表名</th>
        <th>用户名</th>
        <th>权限</th>
    </tr>
    <%
        for(User_Power user_power:user_powerList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=user_power.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td align="center"><font size="1px"><%=user_power.getTablename()%></font></td>
        <td align="center"><font size="1px"><%=user_power.getUsername()%></font></td>
        <td align="center"><font size="1px"><%=user_power.getPower()%></font></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="5"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/power_manage?op=previous">上一页</a>
            <a href="/erp/power_manage?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
