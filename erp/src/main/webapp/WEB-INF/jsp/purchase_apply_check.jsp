<%@ page import="com.springboot.erp.entity.Purchase_apply" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/8
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>采购申请审核</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_purchase_apply_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if (data.result == "success") {
                            alert("删除成功");
                            location.reload(true);
                        }
                        else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}
    </script>
</head>
<body>
<%
    List<Purchase_apply> purchase_applyList=new ArrayList<Purchase_apply>();
    if(session.getAttribute("session_all_purchase_apply")!=null){
        purchase_applyList=(List)session.getAttribute("session_all_purchase_apply");
        session.removeAttribute("session_all_purchase_apply");
    }
    int number=(Integer)session.getAttribute("session_all_purchase_apply_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_all_purchase_apply_PageCount");
    session.removeAttribute("session_all_purchase_apply_PageCount");
    int pageNo=(Integer)session.getAttribute("session_all_purchase_apply_PageNo");
    session.removeAttribute("session_all_purchase_apply_PageNo");
    int no=(Integer)session.getAttribute("session_all_purchase_apply_record_No");
    session.removeAttribute("session_all_purchase_apply_record_No");
%>
<form action="/erp/query_purchase_apply" method="post">
    用户名：
    <input type="text" name="query_txt_username" class="easyui-textbox" id="query_txt_username">
    <select class="easyui-combobox" name="query_txt_status" id="query_txt_status" style="width:80px;">
        <option value=""></option>
        <option value="全部">全部</option>
        <option value="未审核">未审核</option>
        <option value="通过">通过</option>
        <option value="不通过">不通过</option>
    </select>
    <input type="submit" value="查询" class="mybutton">
</form>
<table class="mytable">
    <tr>
        <th align="center" colspan="2">序号</th>
        <th align="center">用户名</th>
        <th align="center">姓名</th>
        <th align="center">部门</th>
        <th align="center">种类</th>
        <th align="center">物品名称</th>
        <th align="center">数量</th>
        <th align="center">申请日期</th>
        <th align="center" colspan="3">状态</th>
    </tr>
    <%
        for (Purchase_apply purchase_apply:purchase_applyList){
    %>
    <form action="/erp/update_purchase_apply_status" method="post">
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=purchase_apply.getIndex_no()%>></td>
        <td><%=no++%><input type="hidden" name="txt_index_no" value=<%=purchase_apply.getIndex_no()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getUsername()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getName()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getDept()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getKind()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getGoods_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getCount()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getApply_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getStatus()%>></td>
        <td><select name="select_status">
            <option value=""></option>
            <option value="通过">通过</option>
            <option value="不通过">不通过</option>
        </select></td>
        <td><input type="submit" value="确定"></td>
    </tr>
    </form>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="12"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/purchase_apply_check?op=previous">上一页</a>
            <a href="/erp/purchase_apply_check?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
