<%@ page import="com.springboot.erp.entity.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.springboot.erp.entity.User_Power" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/1
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>用户管理</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_username");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_user_by_username",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success"){
                        alert("删除成功");
                        location.reload(true);
                    }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_username");
            //dlg=document.getElementById("edit_dlg")
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_user",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_username').textbox("setValue",data.json_username);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_password').textbox("setValue",data.json_password);
                        $('#dlg_txt_name').textbox("setValue",data.json_name);
                        $('#dlg_txt_sex').textbox("setValue",data.json_sex);
                        $('#dlg_txt_tel').textbox("setValue",data.json_tel);
                        $('#dlg_txt_dept').textbox("setValue",data.json_dept); //dlg_txt_number
                        $('#dlg_txt_position').textbox("setValue",data.json_position);
                        $('#dlg_txt_power').textbox("setValue",data.json_power);
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_user",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"dlg_txt_username": $("#dlg_txt_username").val(),"dlg_txt_password": $("#dlg_txt_password").val(), "dlg_txt_name": $("#dlg_txt_name").val(),
                    "dlg_txt_sex": $("#dlg_txt_sex").val(), "dlg_txt_tel": $("#dlg_txt_tel").val(),"dlg_txt_dept": $("#dlg_txt_dept").val(),"dlg_txt_position": $("#dlg_txt_position").val(),
                    "dlg_txt_power": $("#dlg_txt_power").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success") {
                        alert("修改成功");
                        location.reload(true);
                    }else {
                        alert("您没有权限修改！！！");
                    }
                }
            });

        }

        function add_user() {
            $.ajax({
                type: "post",
                url: "/erp/add_user",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_username": $("#add_dlg_txt_username").val(),"add_dlg_txt_password": $("#add_dlg_txt_password").val(), "add_dlg_txt_name": $("#add_dlg_txt_name").val(),
                    "add_dlg_txt_sex": $("#add_dlg_txt_sex").val(), "add_dlg_txt_tel": $("#add_dlg_txt_tel").val(),"add_dlg_txt_dept": $("#add_dlg_txt_dept").val(),"add_dlg_txt_position": $("#add_dlg_txt_position").val(),
                    "add_dlg_txt_power": $("#add_dlg_txt_power").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success"){
                    alert("添加成功");
                    location.reload(true);
                }else {
                        alert("您没有权限添加！！！");
                    }
                }
            });
        }

       /* function power_update() {
            $.ajax({
                type: "post",
                url: "/erp/power_update",
                dataType: "text",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_username": $("#add_dlg_txt_username").val(),"add_dlg_txt_password": $("#add_dlg_txt_password").val(), "add_dlg_txt_name": $("#add_dlg_txt_name").val(),
                    "add_dlg_txt_sex": $("#add_dlg_txt_sex").val(), "add_dlg_txt_tel": $("#add_dlg_txt_tel").val(),"add_dlg_txt_dept": $("#add_dlg_txt_dept").val(),"add_dlg_txt_position": $("#add_dlg_txt_position").val(),
                    "add_dlg_txt_power": $("#add_dlg_txt_power").val()},  //id选择器
                success: function () {
                    alert("添加成功");
                    location.reload(true);
                }
            });
        }*/

        function show_selected_user_power() {
            obj = document.getElementsByName("ck_username");
            //dlg=document.getElementById("edit_dlg")
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_user_power",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#power_dlg_txt_username').textbox("setValue",data.json_power_username);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_warehouse').textbox("setValue",data.json_power_table_warehouse);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_entry_warehouse').textbox("setValue",data.json_power_entry_warehouse);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_out_warehouse').textbox("setValue",data.json_power_out_warehouse);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_stock_statistics').textbox("setValue",data.json_power_stock_statistics);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_stock_warning').textbox("setValue",data.json_power_stock_warning);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_logback').textbox("setValue",data.json_power_logback);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_user').textbox("setValue",data.json_power_user);  //一定要加' ;' 给easyui的textbox赋值

                        $('#power_dlg_txt_table_income').textbox("setValue",data.json_power_income);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_expenditure').textbox("setValue",data.json_power_expenditure);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_sale_bill').textbox("setValue",data.json_power_sale_bill);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_sale_statistics').textbox("setValue",data.json_power_sale_statistics);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_user_power').textbox("setValue",data.json_power_user_power);  //一定要加' ;' 给easyui的textbox赋值

                        $('#power_dlg_txt_table_budget_apply').textbox("setValue",data.json_power_budget_apply);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_common_file').textbox("setValue",data.json_power_common_file);  //一定要加' ;' 给easyui的textbox赋值
                        $('#power_dlg_txt_table_purchase_apply').textbox("setValue",data.json_power_purchase_apply);  //一定要加' ;' 给easyui的textbox赋值

                        $('#power_dlg').dialog('open');
                    }
                });
            }
        }

    </script>
</head>
<body>
<%
    List<User> userList=new ArrayList<User>();
    if(session.getAttribute("session_allusers")!=null){
      userList=(List)session.getAttribute("session_allusers");
      session.removeAttribute("session_allusers");
    }
    int number=(Integer)session.getAttribute("session_user_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_user_PageCount");
    session.removeAttribute("session_user_PageCount");
    int pageNo=(Integer)session.getAttribute("session_user_PageNo");
    session.removeAttribute("session_user_PageNo");
    int no=(Integer)session.getAttribute("session_user_record_No");
    session.removeAttribute("session_user_record_No");

    /*List<User_Power> user_powerList_warehouse=new ArrayList<User_Power>();
    List<User_Power> user_powerList_entry_warehouse=new ArrayList<User_Power>();
    List<User_Power> user_powerList_out_warehouse=new ArrayList<User_Power>();
    List<User_Power> user_powerList_stock_statistics=new ArrayList<User_Power>();
    List<User_Power> user_powerList_stock_warning=new ArrayList<User_Power>();
    List<User_Power> user_powerList_logback=new ArrayList<User_Power>();

    if(session.getAttribute("session_table_warehouse")!=null){
        user_powerList_warehouse=(List)session.getAttribute("session_table_warehouse");
        session.removeAttribute("session_table_warehouse");
    }
    if(session.getAttribute("session_table_entry_warehouse")!=null){
        user_powerList_entry_warehouse=(List)session.getAttribute("session_table_entry_warehouse");
        session.removeAttribute("session_table_entry_warehouse");
    }
    if(session.getAttribute("session_table_out_warehouse")!=null){
        user_powerList_out_warehouse=(List)session.getAttribute("session_table_out_warehouse");
        session.removeAttribute("session_table_out_warehouse");
    }
    if(session.getAttribute("session_table_stock_statistics")!=null){
        user_powerList_stock_statistics=(List)session.getAttribute("session_table_stock_statistics");
        session.removeAttribute("session_table_stock_statistics");
    }
    if(session.getAttribute("session_table_stock_warning")!=null){
        user_powerList_stock_warning=(List)session.getAttribute("session_table_stock_warning");
        session.removeAttribute("session_table_stock_warning");
    }
    if(session.getAttribute("session_table_logback")!=null){
        user_powerList_logback=(List)session.getAttribute("session_table_logback");
        session.removeAttribute("session_table_logback");
    }*/

%>
<form action="/erp/query_user" method="post">
    姓名：
    <input type="text" name="query_txt_name" class="easyui-textbox" id="query_txt_name">
    部门：
    <input type="text" name="query_txt_dept" class="easyui-textbox" id="query_txt_dept">
    <input type="submit" value="查询" class="mybutton">
</form>
<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:800px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_username" id="dlg_txt_username">
            </td>
            <td>密码：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_password" id="dlg_txt_password">
            </td>
        </tr>
        <tr>
            <td>姓名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_name" id="dlg_txt_name" >
            </td>
            <td>性别</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_sex" id="dlg_txt_sex" style="width:80px;">
                    <option value="男" >男</option>
                    <option value="女">女</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>电话：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_tel" id="dlg_txt_tel">
            </td>
            <td>部门：</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_dept" id="dlg_txt_dept" style="width:80px;">
                    <option value="销售部" >销售部</option>
                    <option value="人事部">人事部</option>
                    <option value="采购部" >采购部</option>
                    <option value="财务部">财务部</option>
                    <option value="产品部" >产品部</option>
                    <option value="测试部">测试部</option>
                    <option value="研发部" >研发部</option>
                    <option value="行政部">行政部</option>
                    <option value="项目部" >项目部</option>
                    <option value="质检部">质检部</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>职位：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_position" id="dlg_txt_position">
            </td>
        </tr>
        <tr>
            <td>权限</td>
            <td>
               <input class="easyui-textbox" multiline="true" style="width:400px;height:100px" name="dlg_txt_power" id="dlg_txt_power">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:600px;height:300px;padding:10px" closed="true">
    <table>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_username" id="add_dlg_txt_username">
            </td>
            <td>密码：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_password" id="add_dlg_txt_password">
            </td>
        </tr>
        <tr>
            <td>姓名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_name" id="add_dlg_txt_name" >
            </td>
            <td>性别</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_sex" id="add_dlg_txt_sex" style="width:80px;">
                    <option value="男" >男</option>
                    <option value="女">女</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>电话：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_tel" id="add_dlg_txt_tel">
            </td>
            <td>部门：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_dept" id="add_dlg_txt_dept" style="width:80px;">
                    <option value="销售部" >销售部</option>
                    <option value="人事部">人事部</option>
                    <option value="采购部" >采购部</option>
                    <option value="财务部">财务部</option>
                    <option value="产品部" >产品部</option>
                    <option value="测试部">测试部</option>
                    <option value="研发部" >研发部</option>
                    <option value="行政部">行政部</option>
                    <option value="项目部" >项目部</option>
                    <option value="质检部">质检部</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>职位：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_position" id="add_dlg_txt_position">
            </td>
            <td>权限：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_power" id="add_dlg_txt_power">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="add_user()">
            </td>
        </tr>
    </table>
</div>

<div id="power_dlg" class="easyui-dialog" title="查看权限" data-options="iconCls:'icon-save'"
     style="width:800px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="power_dlg_txt_username" id="power_dlg_txt_username">
            </td>
        </tr>
        <tr>
            <td>warehouse</td>
            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_warehouse" id="power_dlg_txt_table_warehouse"></td>

        </tr>
        <tr>
            <td>entry_warehouse</td>
            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_entry_warehouse" id="power_dlg_txt_table_entry_warehouse"></td>
        </tr>
        <tr>
            <td>out_warehouse</td>
            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_out_warehouse" id="power_dlg_txt_table_out_warehouse"></td>
        <tr>
            <td>stock_statistics</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_stock_statistics" id="power_dlg_txt_table_stock_statistics"></td>

        </tr>
        <tr>
            <td>stock_warning</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_stock_warning" id="power_dlg_txt_table_stock_warning"></td>

        </tr>
        <tr>
            <td>logback</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_logback" id="power_dlg_txt_table_logback"></td>

        </tr>
        <tr>
            <td>user</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_user" id="power_dlg_txt_table_user"></td>

        </tr>
        <tr>
            <td>income</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_income" id="power_dlg_txt_table_income"></td>
        </tr>
        <tr>
            <td>expenditure</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_expenditure" id="power_dlg_txt_table_expenditure"></td>
        </tr>
        <tr>
            <td>sale_bill</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_sale_bill" id="power_dlg_txt_table_sale_bill"></td>
        </tr>
        <tr>
        <td>sale_statistics</td>

        <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_sale_statistics" id="power_dlg_txt_table_sale_statistics"></td>
        </tr>
        <tr>
            <td>user_power</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_user_power" id="power_dlg_txt_table_user_power"></td>
        </tr>
        <tr>
            <td>budget_apply</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_budget_apply" id="power_dlg_txt_table_budget_apply"></td>
        </tr>
        <tr>
            <td>common_file</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_common_file" id="power_dlg_txt_table_common_file"></td>
        </tr>
        <tr>
            <td>purchase_apply</td>

            <td><input type="text" class="easyui-textbox" name="power_dlg_txt_table_purchase_apply" id="power_dlg_txt_table_purchase_apply"></td>
        </tr>

        <tr align="center">
            <td colspan="7">
                <input type="button" value="确定" class="mybutton" onclick="">
            </td>
        </tr>
    </table>
</div>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>用户名</th>
        <th>密码</th>
        <th>姓名</th>
        <th>性别</th>
        <th>电话</th>
        <th>部门</th>
        <th>职位</th>
    </tr>
    <%
        for(User user:userList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_username"  value=<%=user.getUsername()%>></td>
        <td align="center"><%=no++%></td>
        <td align="center"><font size="1px"><%=user.getUsername()%></font></td>
        <td align="center"><font size="1px"><%=user.getPassword()%></font></td>
        <td align="center"><font size="1px"><%=user.getName()%></font></td>
        <td align="center"><font size="1px"><%=user.getSex()%></font></td>
        <td align="center"><font size="1px"><%=user.getTel()%></font></td>
        <td align="center"><font size="1px"><%=user.getDept()%></font></td>
        <td align="center"><font size="1px"><%=user.getPosition()%></font></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="9"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/user_mamage?op=previous">上一页</a>
            <a href="/erp/user_mamage?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
            <a href="javascript:show_selected_dialog()">编辑</a>
            <a href="javascript:show_selected_user_power()">查看权限</a>
        </td>
    </tr>
</table>
</body>
</html>
