<%@ page import="com.springboot.erp.entity.Budget_apply" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/9
  Time: 15:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>预算审核</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function show_selected_budget_apply() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_budget_apply",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_index_no').textbox("setValue",data.json_index_no);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_username_apply').textbox("setValue",data.json_username_apply);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_dept').textbox("setValue",data.json_dept);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_apply_time').textbox("setValue",data.json_apply_time);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_username_deal').textbox("setValue",data.json_username_deal);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_deal_time').textbox("setValue",data.json_deal_time);  //一定要加' ;' 给easyui的textbox赋值
                        $('#dlg_txt_status').textbox("setValue",data.json_status);  //一定要加' ;' 给easyui的textbox赋值
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_selected_budget_apply",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"dlg_txt_index_no": $("#dlg_txt_index_no").val(),
                    "dlg_txt_username_apply": $("#dlg_txt_username_apply").val(),
                    "dlg_txt_dept": $("#dlg_txt_dept").val(),
                    "dlg_txt_apply_time": $("#dlg_txt_apply_time").val(),
                    "dlg_txt_username_deal": $("#dlg_txt_username_deal").val(),
                    "dlg_txt_deal_time": $("#dlg_txt_deal_time").val(),
                    "dlg_txt_status": $("#dlg_txt_status").val()},  //id选择器
                success: function (data) {
                    if(data.result=="success") {
                        alert("修改成功");
                        location.reload(true);
                    }else {
                        alert("您没有权限修改！！！");
                    }

                }
            });

        }

        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_budget_apply_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success") {
                            alert("删除成功");
                            location.reload(true);
                        }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}
    </script>
</head>
<body>
<%
    List<Budget_apply> budget_apply_checkList=new ArrayList<Budget_apply>();
    if(session.getAttribute("session_budget_apply_check")!=null){
        budget_apply_checkList=(List)session.getAttribute("session_budget_apply_check");
        session.removeAttribute("session_budget_apply_check");
    }
    int number=(Integer)session.getAttribute("session_budget_apply_check_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_budget_apply_check_PageCount");
    session.removeAttribute("session_budget_apply_check_PageCount");
    int pageNo=(Integer)session.getAttribute("session_budget_apply_check_PageNo");
    session.removeAttribute("session_budget_apply_check_PageNo");
    int no=(Integer)session.getAttribute("session_budget_apply_check_record_No");
    session.removeAttribute("session_budget_apply_check_record_No");
%>

<form method="post" action="/erp/query_budget_check">
    申请者：
    <input type="text" class="easyui-textbox" name="txt_apply_user">
    状态：
    <select class="easyui-combobox" id="txt_status" name="txt_status" style="width:80px;">
        <option value="" ></option>
        <option value="审批通过" >审批通过</option>
        <option value="审批不通过">审批不通过</option>
    </select>
    <input type="submit" class="mybutton" value="查询">
</form>

<%--<p>发布预算申请表样式</p>
<form method="post" action="/erp/upload_common_file" enctype="multipart/form-data">
    <label>文件名：</label>
    <input type="text" class="easyui-textbox" name="filename">
    <label>备注</label>
    <input type="text" class="easyui-textbox" name="remark">
    <input type="file" name="file">
    <input type="submit" class="mybutton" value="发布">
</form>
<br>--%>
<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1000px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>申请者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_username_apply" id="dlg_txt_username_apply">
                <input type="hidden" class="easyui-textbox" name="dlg_txt_index_no" id="dlg_txt_index_no">
            </td>
            <td>申请部门：</td>
            <td>
                <select class="easyui-combobox" name="dlg_txt_dept" id="dlg_txt_dept" style="width:80px;">
                    <option value="销售部" >销售部</option>
                    <option value="人事部">人事部</option>
                    <option value="采购部" >采购部</option>
                    <option value="财务部">财务部</option>
                    <option value="产品部" >产品部</option>
                    <option value="测试部">测试部</option>
                    <option value="研发部" >研发部</option>
                    <option value="行政部">行政部</option>
                    <option value="项目部" >项目部</option>
                    <option value="质检部">质检部</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>申请时间：</td>
            <td>
                <input class="easyui-datetimebox" label="申请时间:" labelPosition="left" style="width:100%;" name="dlg_txt_apply_time" id="dlg_txt_apply_time" readonly>
            </td>
        </tr>
        <tr>
            <td>处理者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="dlg_txt_username_deal" id="dlg_txt_username_deal">
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="处理时间:" labelPosition="left" style="width:100%;" name="dlg_txt_deal_time" id="dlg_txt_deal_time">
            </td>
        </tr>
        <tr>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" id="dlg_txt_status" name="dlg_txt_status" style="width:80px;">
                    <option value="" ></option>
                    <option value="审批通过" >审批通过</option>
                    <option value="审批不通过">审批不通过</option>
                </select>
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>申请者</th>
        <th>部门</th>
        <th>申请时间</th>
        <th>处理者</th>
        <th>处理时间</th>
        <th>状态</th>
        <th>下载附件</th>
    </tr>
    <%
        for(Budget_apply budget_apply:budget_apply_checkList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=budget_apply.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getUsername_apply()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getDept()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getApply_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getUsername_deal()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getDeal_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=budget_apply.getStatus()%>></td>
        <td><a href="/erp/download_appendix?ck_index_no=<%=budget_apply.getIndex_no()%>">下载附件</a> </td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="9"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/budget_check_view?op=previous">上一页</a>
            <a href="/erp/budget_check_view?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:show_selected_budget_apply()">编辑</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
