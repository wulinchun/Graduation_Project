<%@ page import="com.springboot.erp.entity.User" %>
<%@ page import="com.springboot.erp.entity.Admin" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/1/27
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<link  type="text/css" rel="stylesheet"  href="../css/main/style123.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/index123.css"/>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>ERP系统</title>
    <%--<link rel="stylesheet" type="text/css" href="http://www.w3cschool.cc/try/jeasyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="http://www.w3cschool.cc/try/jeasyui/themes/icon.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="http://www.w3cschool.cc/try/jeasyui/jquery.easyui.min.js"></script>--%>
        <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
     <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
        <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
        <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
        <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        function addTab(title, url){
            if ($('#tt').tabs('exists', title)){
                $('#tt').tabs('select', title);
            } else {
                var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
                $('#tt').tabs('add',{
                    title:title,
                    content:content,
                    closable:true
                });
            }
        }

        function logout() {
            if(confirm("确定要注销吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "get",
                    url: "/erp/logout",
                    dataType: "text",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{},
                    success: function () {
                        alert("注销成功");
                        location.reload(true);
                    }
                });
            }else {
                alert("已经取消了注销操作");
            }}

        function update_personal() {
            $.ajax({
                type: "post",
                url: "/erp/update_personal",
                dataType: "text",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"txt_username": $("#txt_username").val(),"txt_password": $("#txt_password").val(), "txt_name": $("#txt_name").val(),"txt_sex": $("#txt_sex").val(),
                    "txt_tel": $("#txt_tel").val(),"txt_dept": $("#txt_dept").val(),"txt_position": $("#txt_position").val(),"txt_power": $("#txt_power").val()},  //id选择器
                success: function () {
                    alert("修改成功");
                    location.reload(true);
                }
            });

        }
    </script>
        <!--主要！！！easyui在线CSS,js类库中的函数，类的可能会与项目本地css文件及js文件中的类函数冲突！！！
        使用easyui在线类库时要特别注意！！！！！！！！！！！！！！！！！！！！！！！！-->
        <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <<script type="text/javascript"  src="../js/menu.js"></script>
    <script type="text/javascript"  src="../js/menu_main.js"></script>
</head>
<body>
<%
    String current_user="";
    if(session.getAttribute("session_username")!=null){
        current_user="你好！ "+(String)session.getAttribute("session_username");
    }
    User user=new User();
    Admin admin=new Admin();
    if(session.getAttribute("session_user")!=null){
        user=(User)session.getAttribute("session_user");
        session.removeAttribute("session_user");
    }
    else if(session.getAttribute("session_admin")!=null){
        admin=(Admin)session.getAttribute("session_admin");
        session.removeAttribute("session_admin");
    }

%>

<div class="cont-top">
    <%--<img src="../images/main/华依logo.jpg" width="100px" height="80px">--%>
    <img src="../images/main/用户头像.jpg" width="60px" height="50px">
        <%
            if(!current_user.equals("")){
        %>
    <p><%=current_user%>
        &emsp;
        <font color="red">在线</font>
        &emsp;
        <a href="" onclick="logout()"><font color="red">注销</font></a>
        &emsp;
        <a href="javascript:void(0)" onclick="$('#personal_dlg').dialog('open')"><font color="red">个人设置</font></a>
    </p>

    <%
        }else{
    %>
        <p><a href="/erp/login_view">登录</a></p>
    <%
        }
    %>
</div>
<div id="personal_dlg" class="easyui-dialog" title="个人设置" data-options="iconCls:'icon-save'"
     style="width:600px;height:300px;padding:10px" closed="true">
    <table>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_username" id="txt_username" readonly value=<%=user.getUsername()%>>
            </td>
            <td>密码：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_password" id="txt_password" value=<%=user.getPassword()%>>
            </td>
        </tr>
        <tr>
            <td>姓名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_name" id="txt_name" value=<%=user.getName()%>>
            </td>
            <td>性别：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_sex" id="txt_sex" value=<%=user.getSex()%>>
            </td>
        </tr>
        <tr>
            <td>电话：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_tel" id="txt_tel" value=<%=user.getTel()%>>
            </td>
            <td>部门：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_dept" id="txt_dept" value=<%=user.getDept()%>>
            </td>
        </tr>
        <tr>
            <td>职位：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_position" id="txt_position" value=<%=user.getPosition()%>>
            </td>
            <td>权限：</td>
            <td>
                <input type="text" class="easyui-textbox" name="txt_power" id="txt_power" value=<%=user.getPower()%>>
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="update_personal()">
            </td>
        </tr>
    </table>
</div>
<div class="left-menu" style="height:949px;">
    <div class="menu-list">
        <ul>
            <li class="menu-list-01" >
                <p class="fumenu">仓库管理</p>
                <img class="xiala" src="../images/main/xiala.png" />
                <div class="list-p">
                    <p class="zcd"><a href="#" class="" onclick="addTab('仓库设置','/erp/warehouse_setting?op=all')">
                        <font color="white">仓库设置</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('入库明细','/erp/entry_warehouse_list?op=all')">
                        <font color="white">入库单</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('出库明细','/erp/out_warehouse_list?op=all')">
                        <font color="white">出库单</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('库存统计','/erp/stock_statistics?op=all')">
                        <font color="white">库存统计</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('库存预警','/erp/stock_warning')">
                        <font color="white">库存预警</font></a></p>
                </div>
            </li>
            <li class="menu-list-02" >
                <p class="fumenu">采购管理</p>
                <img class="xiala" src="../images/main/xiala.png" />
                <div class="list-p">
                    <p class="zcd"><a href="#" class="" onclick="addTab('采购申请','/erp/my_purchase_apply?op=all')">
                        <font color="white">采购申请</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('采购审核','/erp/purchase_apply_check?op=all')">
                        <font color="white">采购审核</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('采购清单','/erp/purchase_list?op=all')">
                        <font color="white">采购清单</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('申请预算','/erp/budget_apply?op=all')">
                        <font color="white">申请预算</font></a></p>
                </div>
            </li>
            <li class="menu-list-01" >
                <p class="fumenu">财务管理</p>
                <img class="xiala" src="../images/main/xiala.png" />
                <div class="list-p">
                    <p class="zcd"><a href="#" class="" onclick="addTab('预算审核','/erp/budget_check_view?op=all')">
                        <font color="white">预算审核</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('收入单','/erp/income?op=all')">
                        <font color="white">收入单</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('支出单','/erp/expenditure?op=all')">
                        <font color="white">支出单</font></a></p>
                </div>
            </li>
            <li class="menu-list-02" >
                <p class="fumenu">销售管理</p>
                <img class="xiala" src="../images/main/xiala.png" />
                <div class="list-p">
                    <p class="zcd"><a href="#" class="" onclick="addTab('销售订单','/erp/sale_bill?op=all')">
                        <font color="white">销售订单</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('销售统计','/erp/sale_statistics?op=all')">
                        <font color="white">销售统计</font></a></p>
                </div>
            </li>
            <li class="menu-list-01" >
                <p class="fumenu">系统管理</p>
                <img class="xiala" src="../images/main/xiala.png" />
                <div class="list-p">
                    <p class="zcd"><a href="#" class="" onclick="addTab('日志管理','/erp/logback?op=all&&op1=all')">
                        <font color="white">日志管理</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('用户管理','/erp/user_mamage?op=all')">
                        <font color="white">用户管理</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('权限管理','/erp/power_manage?op=all')">
                        <font color="white">权限管理</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('公共文件','/erp/common_file?op=all')">
                        <font color="white">公共文件</font></a></p>
                    <p class="zcd"><a href="#" class="" onclick="addTab('新手教程','/erp/guide')">
                        <font color="white">新手教程</font></a></p>
                </div>
            </li>
        </ul>
    </div>
</div></div>
<%
    double []total_income_array={0,0,0,0,0,0,0,0,0,0,0,0};
    double []total_expenditure_array={0,0,0,0,0,0,0,0,0,0,0,0};
    double []total_profit_array={0,0,0,0,0,0,0,0,0,0,0,0};
    double []total_sale_array={0,0,0,0,0,0,0,0,0,0,0,0};
    double []total_purchase_array={0,0,0,0,0,0,0,0,0,0,0,0};
    if(session.getAttribute("session_total_income_array")!=null){
        total_income_array=(double[])session.getAttribute("session_total_income_array");
        session.removeAttribute("session_total_income_array");
    }
    if(session.getAttribute("session_total_expenditure_array")!=null){
        total_expenditure_array=(double[])session.getAttribute("session_total_expenditure_array");
        session.removeAttribute("session_total_expenditure_array");
    }
    if(session.getAttribute("session_total_profit_array")!=null){
        total_profit_array=(double[])session.getAttribute("session_total_profit_array");
        session.removeAttribute("session_total_profit_array");
    }
    if(session.getAttribute("session_total_sale_array")!=null){
        total_sale_array=(double[])session.getAttribute("session_total_sale_array");
        session.removeAttribute("session_total_sale_array");
    }
    if(session.getAttribute("session_total_purchase_array")!=null){
        total_purchase_array=(double[])session.getAttribute("session_total_purchase_array");
        session.removeAttribute("session_total_purchase_array");
    }
%>
<div class="right-menu">
    <div id="tt" class="easyui-tabs" style="width:1400px;height:1000px;">
        <div title="首页">
            <body style="height: 100%; margin: 0">
            <div id="container" style="height: 30%"></div>
            <script type="text/javascript" src="../js/echarts.min.js"></script>
            <script type="text/javascript">
                var dom = document.getElementById("container");
                var myChart = echarts.init(dom);
                var app = {};
                option = null;
                option = {
                    title: {
                        text: '收支统计'
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'cross',
                            label: {
                                backgroundColor: '#6a7985'
                            }
                        }
                    },
                    legend: {
                        data: ['收入', '支出',"盈利"]
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '收入',
                            type: 'line',
                            stack: '总量',
                            areaStyle: {},
                            data: [<%=total_income_array[0]%>, <%=total_income_array[1]%>, <%=total_income_array[2]%>,
                                <%=total_income_array[3]%>, <%=total_income_array[4]%>, <%=total_income_array[5]%>,
                                <%=total_income_array[6]%>,<%=total_income_array[7]%>,<%=total_income_array[8]%>,
                                <%=total_income_array[9]%>,<%=total_income_array[10]%>,<%=total_income_array[11]%>]
                        },

                        {
                            name: '支出',
                            type: 'line',
                            stack: '总量',
                            areaStyle: {},
                            data: [<%=total_expenditure_array[0]%>, <%=total_expenditure_array[1]%>, <%=total_expenditure_array[2]%>,
                                <%=total_expenditure_array[3]%>, <%=total_expenditure_array[4]%>, <%=total_expenditure_array[5]%>,
                                <%=total_expenditure_array[6]%>,<%=total_expenditure_array[7]%>,<%=total_expenditure_array[8]%>,
                                <%=total_expenditure_array[9]%>,<%=total_expenditure_array[10]%>,<%=total_expenditure_array[11]%>]
                        },

                        {
                            name: '盈利',
                            type: 'line',
                            stack: '总量',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'top'
                                }
                            },
                            areaStyle: {},
                            data: [<%=total_profit_array[0]%>, <%=total_profit_array[1]%>, <%=total_profit_array[2]%>,
                                <%=total_profit_array[3]%>, <%=total_profit_array[4]%>, <%=total_profit_array[5]%>,
                                <%=total_profit_array[6]%>,-<%=total_profit_array[7]%>,<%=total_profit_array[8]%>,
                                <%=total_profit_array[9]%>,<%=total_profit_array[10]%>,<%=total_profit_array[11]%>]
                        }
                    ]
                };
                ;
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }
            </script>
<br><br>
            <div id="container1" style="height: 30%"></div>
            <script type="text/javascript" src="../js/echarts.min.js"></script>
            <script type="text/javascript">
                var dom = document.getElementById("container1");
                var myChart = echarts.init(dom);
                var app = {};
                option = null;
                option = {
                    title: {
                        text: '销售统计'
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    /*legend: {
                        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                    },*/
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name: '销售统计',
                            type: 'line',
                            stack: '总量',
                            data: [<%=total_sale_array[0]%>, <%=total_sale_array[1]%>, <%=total_sale_array[2]%>,
                                <%=total_sale_array[3]%>, <%=total_sale_array[4]%>, <%=total_sale_array[5]%>,
                                <%=total_sale_array[6]%>,<%=total_sale_array[7]%>, <%=total_sale_array[8]%>,
                                <%=total_sale_array[9]%>, <%=total_sale_array[10]%>, <%=total_sale_array[11]%>]
                        },
                    ]
                };
                ;
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }
            </script>
            <br><br>
            <div id="container2" style="height: 30%"></div>
            <script type="text/javascript" src="../js/echarts.min.js"></script>
            <script type="text/javascript">
                var dom = document.getElementById("container2");
                var myChart = echarts.init(dom);
                var app = {};
                option = null;
                option = {
                    title: {
                        text: '采购统计'
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    /*legend: {
                        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                    },*/
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name: '采购统计',
                            type: 'line',
                            stack: '总量',
                            data: [<%=total_purchase_array[0]%>, <%=total_purchase_array[1]%>, <%=total_purchase_array[2]%>,
                                <%=total_purchase_array[3]%>, <%=total_purchase_array[4]%>, <%=total_purchase_array[5]%>,
                                <%=total_purchase_array[6]%>,<%=total_purchase_array[7]%>, <%=total_purchase_array[8]%>,
                                <%=total_purchase_array[9]%>, <%=total_purchase_array[10]%>, <%=total_purchase_array[11]%>]
                        },
                    ]
                };
                ;
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }
            </script>
            </body>
        </div>
    </div></div>
</body>
</html>



