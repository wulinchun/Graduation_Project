<%@ page import="com.springboot.erp.entity.Expenditure" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/23
  Time: 11:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>支出单</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_expenditure_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success") {
                            alert("删除成功");
                            location.reload(true);
                        }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}
        /*function add_income() {   //添加销售订单
            $.ajax({
                type: "post",
                url: "/erp/add_income",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_list_no": $("#add_dlg_txt_list_no").val(),
                    "add_dlg_txt_list_date": $("#add_dlg_txt_list_date").val(),
                    "add_dlg_txt_related_company": $("#add_dlg_txt_related_company").val(),
                    "add_dlg_txt_pay_account": $("#add_dlg_txt_pay_account").val(),
                    "add_dlg_txt_deal_person": $("#add_dlg_txt_deal_person").val(),
                    "add_dlg_txt_receive_account": $("#add_dlg_txt_receive_account").val(),
                    "add_dlg_txt_total_price": $("#add_dlg_txt_total_price").val(),
                    "add_dlg_txt_remark": $("#add_dlg_txt_remark").val(),
                    "filename": $("#filename").val(),
                    "file": $("#file").val(),},  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("添加成功");
                        location.reload(true);
                    }else {
                        alert("添加失败，您没有权限添加");
                    }
                },
            });
        }*/

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_expenditure",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#dlg_txt_index_no').textbox("setValue", data.json_index_no);  //一定要加 ; 给easyui的textbox赋值
                        $('#edit_dlg_txt_list_no').textbox("setValue", data.json_list_no);
                        $('#edit_dlg_txt_list_date').textbox("setValue", data.json_list_time);
                        $('#edit_dlg_txt_related_company').textbox("setValue", data.json_related_company);
                        $('#edit_dlg_txt_pay_account').textbox("setValue", data.json_pay_account);
                        $('#edit_dlg_txt_deal_person').textbox("setValue", data.json_deal_person);
                        $('#edit_dlg_txt_receive_account').textbox("setValue", data.json_receive_account);
                        $('#edit_dlg_txt_total_price').textbox("setValue", data.json_total_price);
                        $('#edit_dlg_txt_remark').textbox("setValue", data.json_remark);
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_expenditure",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"dlg_txt_index_no": $("#dlg_txt_index_no").val(),
                    "edit_dlg_txt_list_no": $("#edit_dlg_txt_list_no").val(),
                    "edit_dlg_txt_list_date": $("#edit_dlg_txt_list_date").val(),
                    "edit_dlg_txt_related_company": $("#edit_dlg_txt_related_company").val(),
                    "edit_dlg_txt_pay_account": $("#edit_dlg_txt_pay_account").val(),
                    "edit_dlg_txt_deal_person": $("#edit_dlg_txt_deal_person").val(),
                    "edit_dlg_txt_receive_account": $("#edit_dlg_txt_receive_account").val(),
                    "edit_dlg_txt_total_price": $("#edit_dlg_txt_total_price").val(),
                    "edit_dlg_txt_remark": $("#edit_dlg_txt_remark").val(),
                },  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("修改成功");
                        location.reload(true);
                    }
                    else {
                        alert("修改失败，您没有权限修改");
                    }
                },
                failure: function () {
                    alert("修改失败");
                }
            });
        }
    </script>

</head>
<body>
<%
    List<Expenditure> expenditureList=new ArrayList<Expenditure>();
    if(session.getAttribute("session_expenditure")!=null){
        expenditureList=(List)session.getAttribute("session_expenditure");
        session.removeAttribute("session_expenditure");
    }
    int number=(Integer)session.getAttribute("session_expenditure_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_expenditure_PageCount");
    session.removeAttribute("session_expenditure_PageCount");
    int pageNo=(Integer)session.getAttribute("session_expenditure_PageNo");
    session.removeAttribute("session_expenditure_PageNo");
    int no=(Integer)session.getAttribute("session_expenditure_record_No");
    session.removeAttribute("session_expenditure_record_No");
%>
<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1200px;height:500px;padding:10px" closed="true">
    <form method="post" action="/erp/add_expenditure" enctype="multipart/form-data">
        <table>
            <tr>
                <td>单据编号：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_list_no" id="add_dlg_txt_list_no">
                </td>
            </tr>
            <tr>
                <td>
                    <input class="easyui-datetimebox" label="单据日期:" labelPosition="left" style="width:100%;" name="add_dlg_txt_list_date" id="add_dlg_txt_list_date">
                </td>
            </tr>
            <tr>
                <td>收款单位：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_related_company" id="add_dlg_txt_related_company">
                </td>
                <td>收款账户：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_receive_account" id="add_dlg_txt_receive_account">
                </td>
            </tr>
            <tr>
                <td>处理人：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_deal_person" id="add_dlg_txt_deal_person">
                </td>
                <td>付款账户：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_pay_account" id="add_dlg_txt_pay_account">
                </td>
            </tr>
            <tr>
                <td>总金额：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_total_price" id="add_dlg_txt_total_price">
                </td>
            </tr>
            <tr>
                <td>备注：</td>
                <td>
                    <input type="text" class="easyui-textbox" multiline="true" name="add_dlg_txt_remark" id="add_dlg_txt_remark" style="width:100%;height:120px">
                </td>
            </tr>
            <tr>
                <td>文件名：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="filename" id="filename">
                </td>
                <td><input type="file" name="file" id="file"></td>
            </tr>
            <tr align="center">
                <td colspan="8">
                    <input type="submit" value="确定" class="mybutton">
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1200px;height:500px;padding:10px" closed="true">
    <table>
        <tr>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_list_no" id="edit_dlg_txt_list_no" readonly>
                <input type="hidden" class="easyui-textbox" name="dlg_txt_index_no" id="dlg_txt_index_no">
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="单据日期:" labelPosition="left" style="width:100%;" name="edit_dlg_txt_list_date" id="edit_dlg_txt_list_date">
            </td>
        </tr>
        <tr>
            <td>收款单位：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_related_company" id="edit_dlg_txt_related_company">
            </td>
            <td>收款账户：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_receive_account" id="edit_dlg_txt_receive_account">
            </td>
        </tr>
        <tr>
            <td>处理人：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_deal_person" id="edit_dlg_txt_deal_person">
            </td>
            <td>付款账户：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_pay_account" id="edit_dlg_txt_pay_account">
            </td>
        </tr>
        <tr>
            <td>总金额：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_total_price" id="edit_dlg_txt_total_price">
            </td>
        </tr>
        <tr>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" multiline="true" name="edit_dlg_txt_remark" id="edit_dlg_txt_remark" style="width:100%;height:120px">
            </td>
        </tr>
        <%-- <tr>
             <td>文件名：</td>
             <td>
                 <input type="text" class="easyui-textbox" name="filename" id="filename" readonly>
             </td>
             <td><input type="file" name="file" id="file" readonly></td>
         </tr>--%>
        <tr align="center">
            <td colspan="8">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<form action="/erp/query_expenditure" method="post">
    单据编号：
    <input type="text" name="query_txt_list_no" class="easyui-textbox" id="query_txt_list_no">
    单据日期：
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_start_time">
    --
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_end_time">
    <input type="submit" value="查询" class="mybutton">
</form>

<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>单据编号</th>
        <th>单据日期</th>
        <th>收款单位</th>
        <th>收款账户</th>
        <th>处理人</th>
        <th>付款账户</th>
        <th>总金额</th>
        <th>备注</th>
        <th>下载附件</th>
    </tr>
    <%
        for (Expenditure expenditure:expenditureList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=expenditure.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getList_no()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getList_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getRelated_company()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getReceive_account()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getDeal_person()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getPay_account()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getTotal_price()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=expenditure.getRemark()%>></td>
        <td><a href="/erp/download_expenditure_appendix?index_no=<%=expenditure.getIndex_no()%>">下载附件</a> </td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="11"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/expenditure?op=previous">上一页</a>
            <a href="/erp/expenditure?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:show_selected_dialog()">编辑</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
