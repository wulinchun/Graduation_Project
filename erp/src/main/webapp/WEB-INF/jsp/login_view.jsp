<%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/1/19
  Time: 20:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<!--注意：！！！css,images,js等文件不能放在WEB-INF目录下，要放在webapp下-->
<!--注意：！！！启动该springboot项目必须联网-->
<head>
    <title>用户登录(erpDocker)</title>
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="easyui help you build your web page easily!">
    <link type="text/css" rel="stylesheet" href="../css/login/login.css" />
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../js/login/jquery-1.11.1.min.js"></script>



    <script type="text/javascript">
        $(function () {
            $("#login_btn").click(function () { //使用js判断radio的选中情况并给status赋值
                if(document.getElementById('is_user').checked){
                    document.getElementById('status').value='is_user'
                }
                else if (document.getElementById('is_admin').checked){
                document.getElementById('status').value='is_admin'
            }
                $.ajax({
                    type: "post",
                    url: "/erp/login",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    data: {"username": $("#username").val(), "password": $("#password").val(),
                        "status":$("#status").val()},  //id选择器
                    success: function (data) {
                        if (data.result == "success") {
                            alert("登录成功");
                            window.location.href = "/erp/main_view";  //登录成功点击“确定”，实现后台跳转
                        } else if(data.result=="empty"){
                            alert("请选择登录身份");
                        }else {
                            alert("用户名或密码错误");
                        }
                    },
                    failure: function () {
                        alert("用户名或密码错误");
                    }
                });
            });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var height=$(document).height();
            $('.main').css('height',height);
        })
    </script>
</head>
    <body>
    <%--<img src="../images/login/ERPlogo.jpg" width="200px" height="100px"/>--%>
    <div class="main">
        <%--<img src="../images/login/ERPlogo.jpg" width="200px" height="100px"/>--%>
        <div class="main0">
            <img src="../images/login/ERPlogo.jpg" width="200px" height="100px"/>
            <div class="main_left">
                <img src="../images/login/login-image-3.png" class="theimg"/>
                <img src="../images/login/login-image-2.png" class="secimg"/>
                <img src="../images/login/login-image-1.png" class="firimg"/>
            </div>
            <div class="main_right">
                <div class="main_r_up">
                    <img src="../images/login/user.png" />
                    <div class="pp">登录</div>
                </div>
                <div class="sub"><p>还没有账号？<a href="zhuce.html"><span class="blue">立即注册</span></a></p></div>
                <div class="txt">
                    <span style="letter-spacing:8px;">用户名:</span>
                    <input name="username" id="username" type="text" class="txtphone" placeholder="请输入用户名"/>
                </div>
                <div class="txt">
                    <span style="letter-spacing:4px;">登录密码:</span>
                    <input name="password" id="password" type="password" class="txtphone" placeholder="请输入密码"/>
                </div>
                <%--<div class="txt">
                    <span style=" float:left;letter-spacing:8px;">验证码:</span>
                    <input name="" type="text" class="txtyzm" placeholder="请输入页面验证码"/>
                    <img src="images/login/yanzhengma.png" class="yzmimg"/>
                </div>--%>
                <div class="xieyi">
                    <input type="radio" name="status" id="is_user" value="is_user"/>&emsp;用户&emsp;&emsp;&emsp;&emsp;&emsp;
                    <input type="radio" name="status" id="is_admin" value="is_admin"/>&emsp;管理员
                    <input type="hidden" id="status">
                </div>
                <div class="xieyi">
                    <input name="" type="checkbox" value="" checked="checked"/>
                    记住我 <a href="password.html"><span class="blue" style=" padding-left:130px; cursor:pointer">忘记密码?</span></a>
                </div>
                <div class="xiayibu" id="login_btn">登录</div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="footer0">
            <div class="footer_l">毕业设计 | 基于spring boot的ERP系统</div>
            <div class="footer_r">© 2020 （上海电力大学）2016软件工程    吴林春</div>
        </div>
    </div>
</body>
</html>
