<%@ page import="com.springboot.erp.entity.Sale_bill" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/11
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>销售订单</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_sale_bill_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success") {
                            alert("删除成功");
                            location.reload(true);
                        }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}
        function add_sale_bill() {   //添加销售订单
            $.ajax({
                type: "post",
                url: "/erp/add_sale_bill",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"add_dlg_txt_customer_name": $("#add_dlg_txt_customer_name").val(),
                    "add_dlg_txt_company_name": $("#add_dlg_txt_company_name").val(),
                    "add_dlg_txt_list_no": $("#add_dlg_txt_list_no").val(),
                    "add_dlg_txt_list_date": $("#add_dlg_txt_list_date").val(),
                    "add_dlg_txt_goods_no": $("#add_dlg_txt_goods_no").val(),
                    "add_dlg_txt_goods_name": $("#add_dlg_txt_goods_name").val(),
                    "add_dlg_txt_count": $("#add_dlg_txt_count").val(),
                    "add_dlg_txt_total_price": $("#add_dlg_txt_total_price").val(),
                    "add_dlg_txt_operator": $("#add_dlg_txt_operator").val(),
                    "add_dlg_txt_status": $("#add_dlg_txt_status").val(),
                    "add_dlg_txt_remark": $("#add_dlg_txt_remark").val()},  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("添加成功");
                        location.reload(true);
                    }else {
                        alert("添加失败，您没有权限添加");
                    }
                },
            });
        }

        function show_selected_dialog() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for (k in obj) {
                if (obj[k].checked)
                    check_val.push(obj[k].value);
            }
            if (check_val.length > 1) {
                alert("只能选取一行");
            } else if (check_val.length == 1) {
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/show_selected_sale_bill",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data: {'check_val': check_val},
                    success: function (data) {
                        $('#edit_dlg_txt_index_no').textbox("setValue", data.json_index_no);  //一定要加 ; 给easyui的textbox赋值
                        $('#edit_dlg_txt_customer_name').textbox("setValue", data.json_customer_name);
                        $('#edit_dlg_txt_company_name').textbox("setValue", data.json_company_name);
                        $('#edit_dlg_txt_list_no').textbox("setValue", data.json_list_no);
                        $('#edit_dlg_txt_list_date').textbox("setValue", data.json_list_date);
                        $('#edit_dlg_txt_goods_no').textbox("setValue", data.json_goods_no);
                        $('#edit_dlg_txt_goods_name').textbox("setValue", data.json_goods_name);
                        $('#edit_dlg_txt_count').textbox("setValue", data.json_count);
                        $('#edit_dlg_txt_total_price').textbox("setValue", data.json_total_price);
                        $('#edit_dlg_txt_operator').textbox("setValue", data.json_operator);
                        $('#edit_dlg_txt_status').textbox("setValue", data.json_status);
                        $('#edit_dlg_txt_remarks').textbox("setValue", data.json_remarks);
                        $('#edit_dlg').dialog('open');
                    }
                });
            }
        }

        function update_selected() {
            $.ajax({
                type: "post",
                url: "/erp/update_sale_bill",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                data: {"edit_dlg_txt_index_no": $("#edit_dlg_txt_index_no").val(),
                    "edit_dlg_txt_customer_name": $("#edit_dlg_txt_customer_name").val(),
                    "edit_dlg_txt_company_name": $("#edit_dlg_txt_company_name").val(),
                    "edit_dlg_txt_list_no": $("#edit_dlg_txt_list_no").val(),
                    "edit_dlg_txt_list_date": $("#edit_dlg_txt_list_date").val(),
                    "edit_dlg_txt_goods_no": $("#edit_dlg_txt_goods_no").val(),
                    "edit_dlg_txt_goods_name": $("#edit_dlg_txt_goods_name").val(),
                    "edit_dlg_txt_count": $("#edit_dlg_txt_count").val(),
                    "edit_dlg_txt_total_price": $("#edit_dlg_txt_total_price").val(),
                    "edit_dlg_txt_operator": $("#edit_dlg_txt_operator").val(),
                    "edit_dlg_txt_status": $("#edit_dlg_txt_status").val(),
                    "edit_dlg_txt_remarks": $("#edit_dlg_txt_remarks").val()},  //id选择器
                success: function (data) {
                    if(data.result == "success") {
                        alert("修改成功");
                        location.reload(true);
                    }
                    else {
                        alert("修改失败，您没有权限修改");
                    }
                },
                failure: function () {
                    alert("修改失败");
                }
            });
        }
    </script>
</head>
<body>
<%
    List<Sale_bill> sale_billList=new ArrayList<Sale_bill>();
    if(session.getAttribute("session_sale_bill")!=null){
        sale_billList=(List)session.getAttribute("session_sale_bill");
        session.removeAttribute("session_sale_bill");
    }
    int number=(Integer)session.getAttribute("session_sale_bill_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_sale_bill_PageCount");
    session.removeAttribute("session_sale_bill_PageCount");
    int pageNo=(Integer)session.getAttribute("session_sale_bill_PageNo");
    session.removeAttribute("session_sale_bill_PageNo");
    int no=(Integer)session.getAttribute("session_sale_bill_record_No");
    session.removeAttribute("session_sale_bill_record_No");
%>
<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1200px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>流水号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_index_no" id="add_dlg_txt_index_no" readonly>
            </td>
        </tr>
        <tr>
            <td>客户名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_customer_name" id="add_dlg_txt_customer_name">
            </td>
            <td>公司名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_company_name" id="add_dlg_txt_company_name">
            </td>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_list_no" id="add_dlg_txt_list_no" >
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="单据日期:" labelPosition="left" style="width:100%;" name="add_dlg_txt_list_date" id="add_dlg_txt_list_date">
            </td>
        </tr>
        <tr>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_goods_no" id="add_dlg_txt_goods_no">
            </td>
            <td>商品名称：</td>
            <td><input type="text" class="easyui-textbox" name="add_dlg_txt_goods_name" id="add_dlg_txt_goods_name">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_count" id="add_dlg_txt_count">
            </td>
        </tr>
        <tr>
            <td>总金额</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_total_price" id="add_dlg_txt_total_price">
            </td>
            <td>操作者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="add_dlg_txt_operator" id="add_dlg_txt_operator">
            </td>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="add_dlg_txt_status" id="add_dlg_txt_status" style="width:80px;">
                    <option value="未审核" >未审核</option>
                    <option value="已审核">已审核</option>
                    <option value="审核不通过">审核不通过</option>
                    <option value="销售出库">销售出库</option>
                    <option value="退货入库">退货入库</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" multiline="true" name="add_dlg_txt_remark" id="add_dlg_txt_remark" style="width:100%;height:120px">
            </td>
        </tr>
        <tr align="center">
            <td colspan="6">
                <input type="button" value="确定" class="mybutton" onclick="add_sale_bill()">
            </td>
        </tr>
    </table>
</div>

<div id="edit_dlg" class="easyui-dialog" title="编辑" data-options="iconCls:'icon-save'"
     style="width:1200px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>流水号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_index_no" id="edit_dlg_txt_index_no" readonly>
            </td>
        </tr>
        <tr>
            <td>客户名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_customer_name" id="edit_dlg_txt_customer_name" >
            </td>
            <td>公司名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_company_name" id="edit_dlg_txt_company_name" >
            </td>
            <td>单据编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_list_no" id="edit_dlg_txt_list_no" >
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="单据日期:" labelPosition="left" style="width:100%;" name="edit_dlg_txt_list_date" id="edit_dlg_txt_list_date" >
            </td>
        </tr>
        <tr>
            <td>商品编号：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_goods_no" id="edit_dlg_txt_goods_no" >
            </td>
            <td>商品名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_goods_name" id="edit_dlg_txt_goods_name" >
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_count" id="edit_dlg_txt_count" >
            </td>
        </tr>
        <tr>
            <td>总金额</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_total_price" id="edit_dlg_txt_total_price" >
            </td>
            <td>操作者：</td>
            <td>
                <input type="text" class="easyui-textbox" name="edit_dlg_txt_operator" id="edit_dlg_txt_operator" >
            </td>
            <td>状态：</td>
            <td>
                <select class="easyui-combobox" name="edit_dlg_txt_status" id="edit_dlg_txt_status" style="width:80px;">
                    <option value="未审核" >未审核</option>
                    <option value="已审核">已审核</option>
                    <option value="审核不通过">审核不通过</option>
                    <option value="销售出库">销售出库</option>
                    <option value="退货入库">退货入库</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>备注：</td>
            <td>
                <input type="text" class="easyui-textbox" multiline="true" name="edit_dlg_txt_remark" id="edit_dlg_txt_remark" style="width:100%;height:120px" >
            </td>
        </tr>
        <tr align="center">
            <td colspan="6">
                <input type="button" value="确定" class="mybutton" onclick="update_selected()">
            </td>
        </tr>
    </table>
</div>

<form action="/erp/query_sale_bill" method="post">
    单据编号：
    <input type="text" name="query_txt_list_no" class="easyui-textbox" id="query_txt_list_no">
    商品名称：
    <input type="text" name="query_txt_goods_name" class="easyui-textbox" id="query_txt_goods_name">
    单据日期：
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_start_time">
    --
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_end_time">
    状态：
    <select class="easyui-combobox" name="query_txt_status" id="query_txt_status" style="width:80px;">
        <option value="" ></option>
        <option value="未审核" >未审核</option>
        <option value="已审核">已审核</option>
        <option value="审核不通过">审核不通过</option>
        <option value="销售出库">销售出库</option>
        <option value="退货入库">退货入库</option>
    </select>
    <input type="submit" value="查询" class="mybutton">
</form>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>客户姓名</th>
        <th>公司名称</th>
        <th>单据编号</th>
        <th>单据日期</th>
        <th>商品编号</th>
        <th>商品名称</th>
        <th>数量</th>
        <th>总金额</th>
        <th>操作员</th>
        <th>状态</th>
        <th>备注</th>
    </tr>
    <%
        for (Sale_bill sale_bill:sale_billList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=sale_bill.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getCustomer_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getCompany_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getList_no()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getList_date()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getGoods_no()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getGoods_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getCount()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getTotal_price()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getOperator()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getStatus()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_bill.getRemark()%>></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="13"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/sale_bill?op=previous">上一页</a>
            <a href="/erp/sale_bill?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:show_selected_dialog()">编辑</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
