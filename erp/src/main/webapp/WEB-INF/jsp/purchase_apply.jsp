<%@ page import="com.springboot.erp.entity.Purchase_apply" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/7
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>采购申请</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
</head>
<body>
<%
    List<Purchase_apply> purchase_applyList=new ArrayList<Purchase_apply>();
    if(session.getAttribute("session_purchase_apply_my")!=null){
        purchase_applyList=(List)session.getAttribute("session_purchase_apply_my");
        session.removeAttribute("session_purchase_apply_my");
    }
    int number=(Integer)session.getAttribute("session_purchase_apply_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_purchase_apply_PageCount");
    session.removeAttribute("session_purchase_apply_PageCount");
    int pageNo=(Integer)session.getAttribute("session_purchase_apply_PageNo");
    session.removeAttribute("session_purchase_apply_PageNo");
    int no=(Integer)session.getAttribute("session_purchase_apply_record_No");
    session.removeAttribute("session_purchase_apply_record_No");
%>
<script type="text/javascript">
    function delete_submit() {
        obj = document.getElementsByName("ck_index_no");
        check_val = [];
        for(k in obj){
            if(obj[k].checked)
                check_val.push(obj[k].value);

        }
        if(confirm("确实要删除吗？")){
            $.ajax({        //注意：写ajax时一定要先导入jquery.js
                type: "post",
                url: "/erp/delete_my_purchase_apply",
                dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                traditional: true,
                data:{'check_val':check_val},
                success: function (data) {
                    if(data.result=="success"){
                        alert("删除成功");
                        location.reload(true);
                }else{
                    alert("删除失败")}
                }
            });
        }else {
            alert("已经取消了删除操作");
        }}

    function apply_submit() {
        //apply_dlg_txt_date = document.getElementsByName("apply_dlg_txt_date");
        $.ajax({
            type: "post",
            url: "/erp/add_my_purchase_apply",
            dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
            data: {"apply_dlg_txt_username": $("#apply_dlg_txt_username").val(),"apply_dlg_txt_name": $("#apply_dlg_txt_name").val(), "apply_dlg_txt_dept": $("#apply_dlg_txt_dept").val(),
                "apply_dlg_txt_kind": $("#apply_dlg_txt_kind").val(), "apply_dlg_txt_goods_name": $("#apply_dlg_txt_goods_name").val(),"apply_dlg_txt_count": $("#apply_dlg_txt_count").val(),
                "apply_dlg_txt_date": $("#apply_dlg_txt_date").val()},  //id选择器
            success: function (data) {
                if(data.result=="success"){
                    alert("申请发布成功");
                    location.reload(true);
            }else{
                alert("申请发布失败");
            }
        }});
    }
</script>
<div id="apply_dlg" class="easyui-dialog" title="采购申请" data-options="iconCls:'icon-save'"
     style="width:800px;height:400px;padding:10px" closed="true">
    <table>
        <tr>
            <td>用户名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_username" id="apply_dlg_txt_username">
            </td>
            <td>姓名：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_name" id="apply_dlg_txt_name">
            </td>
        </tr>
        <tr>
            <td>部门：</td>
            <td>
                <select class="easyui-combobox" name="apply_dlg_txt_dept" id="apply_dlg_txt_dept" style="width:80px;">
                    <option value="销售部" >销售部</option>
                    <option value="人事部">人事部</option>
                    <option value="采购部" >采购部</option>
                    <option value="财务部">财务部</option>
                    <option value="产品部" >产品部</option>
                    <option value="测试部">测试部</option>
                    <option value="研发部" >研发部</option>
                    <option value="行政部">行政部</option>
                    <option value="项目部" >项目部</option>
                    <option value="质检部">质检部</option>
                </select>
            </td>
            <td>种类</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_kind" id="apply_dlg_txt_kind">
            </td>
        </tr>
        <tr>
            <td>物品名称：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_goods_name" id="apply_dlg_txt_goods_name">
            </td>
            <td>数量：</td>
            <td>
                <input type="text" class="easyui-textbox" name="apply_dlg_txt_count" id="apply_dlg_txt_count">
            </td>
        </tr>
        <tr>
            <td>
                <input class="easyui-datetimebox" label="申请日期:" labelPosition="left" style="width:100%;" name="apply_dlg_txt_date" id="apply_dlg_txt_date">
            </td>
        </tr>
        <tr align="center">
            <td colspan="4">
                <input type="button" value="确定" class="mybutton" onclick="apply_submit()">
            </td>
        </tr>
    </table>
</div>
<input type="button" value="采购申请" class="mybutton" onclick="$('#apply_dlg').dialog('open')">
<%--<input class="easyui-datebox" label="申请日期:" labelPosition="left" style="width:20%;">--%>
<table class="mytable">
    <tr>
        <th align="center" colspan="2">序号</th>
        <th align="center">用户名</th>
        <th align="center">姓名</th>
        <th align="center">部门</th>
        <th align="center">种类</th>
        <th align="center">物品名称</th>
        <th align="center">数量</th>
        <th align="center">申请日期</th>
        <th align="center">状态</th>
    </tr>
    <%
        for (Purchase_apply purchase_apply:purchase_applyList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=purchase_apply.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getUsername()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getName()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getDept()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getKind()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getGoods_name()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getCount()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getApply_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=purchase_apply.getStatus()%>></td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="10"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/my_purchase_apply?op=previous">上一页</a>
            <a href="/erp/my_purchase_apply?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
