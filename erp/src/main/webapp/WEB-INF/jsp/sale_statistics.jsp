<%@ page import="com.springboot.erp.entity.Sale_statistics" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Atlantide
  Date: 2020/3/24
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="jquery,ui,easy,easyui,web">
    <meta name="description" content="easyui help you build your web page easily!">
    <title>销售统计</title>
    <link  type="text/css" rel="stylesheet"  href="../css/main/main_view.css"/>
    <link  type="text/css" rel="stylesheet"  href="../css/main/warehouse_distribution.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/default/easyui.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/themes/icons.css"/>
    <link  type="text/css" rel="stylesheet"  href="../jquery-easyui-1.7.0/demo/demo.css"/>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript"  src="../jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        function delete_submit() {
            obj = document.getElementsByName("ck_index_no");
            check_val = [];
            for(k in obj){
                if(obj[k].checked)
                    check_val.push(obj[k].value);

            }
            if(confirm("确实要删除吗？")){
                $.ajax({        //注意：写ajax时一定要先导入jquery.js
                    type: "post",
                    url: "/erp/delete_sale_statistics_by_index_no",
                    dataType: "json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
                    traditional: true,
                    data:{'check_val':check_val},
                    success: function (data) {
                        if(data.result=="success") {
                            alert("删除成功");
                            location.reload(true);
                        }else {
                            alert("您没有权限删除！！！");
                        }
                    }
                });
            }else {
                alert("已经取消了删除操作");
            }}
    </script>
</head>
<body>
<%
    List<Sale_statistics> sale_statisticsList=new ArrayList<Sale_statistics>();
    if(session.getAttribute("session_sale_statistics")!=null){
        sale_statisticsList=(List)session.getAttribute("session_sale_statistics");
        session.removeAttribute("session_sale_statistics");
    }
    int number=(Integer)session.getAttribute("session_sale_statistics_PageCount")/25+1;
    int count=(Integer)session.getAttribute("session_sale_statistics_PageCount");
    session.removeAttribute("session_sale_statistics_PageCount");
    int pageNo=(Integer)session.getAttribute("session_sale_statistics_PageNo");
    session.removeAttribute("session_sale_statistics_PageNo");
    int no=(Integer)session.getAttribute("session_sale_statistics_record_No");
    session.removeAttribute("session_sale_statistics_record_No");
%>

<div id="add_dlg" class="easyui-dialog" title="添加" data-options="iconCls:'icon-save'"
     style="width:1200px;height:500px;padding:10px" closed="true">
    <form method="post" action="/erp/add_sale_statistics" enctype="multipart/form-data">
        <table>
            <tr>
                <td>
                    <input class="easyui-datetimebox" label="上传日期:" labelPosition="left" style="width:100%;" name="add_dlg_txt_upload_time" id="add_dlg_txt_upload_time">
                </td>
            </tr>
            <tr>
                <td>上传者：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="add_dlg_txt_uploader" id="add_dlg_txt_uploader">
                </td>
                <td>部门：</td>
                <td>
                    <select class="easyui-combobox" name="dlg_txt_dept" id="dlg_txt_dept" style="width:80px;">
                        <option value="销售部" >销售部</option>
                        <option value="人事部">人事部</option>
                        <option value="采购部" >采购部</option>
                        <option value="财务部">财务部</option>
                        <option value="产品部" >产品部</option>
                        <option value="测试部">测试部</option>
                        <option value="研发部" >研发部</option>
                        <option value="行政部">行政部</option>
                        <option value="项目部" >项目部</option>
                        <option value="质检部">质检部</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>文件名：</td>
                <td>
                    <input type="text" class="easyui-textbox" name="filename" id="filename">
                </td>
                <td><input type="file" name="file" id="file"></td>
            </tr>
            <tr>
                <td>备注：</td>
                <td>
                    <input type="text" class="easyui-textbox" multiline="true" name="add_dlg_txt_remark" id="add_dlg_txt_remark" style="width:100%;height:120px">
                </td>
            </tr>
            <tr align="center">
                <td colspan="4">
                    <input type="submit" value="确定" class="mybutton">
                </td>
            </tr>
        </table>
    </form>
</div>
<form action="/erp/query_sale_statistics" method="post">
    上传日期：
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_start_time">
    --
    <input class="easyui-datetimebox" label="" labelPosition="left" style="width:20%;" name="query_txt_end_time">
    <input type="submit" value="查询" class="mybutton">
</form>
<table class="mytable">
    <tr>
        <th colspan="2">序号</th>
        <th>上传时间</th>
        <th>上传者</th>
        <th>部门</th>
        <th>备注</th>
        <th>下载附件</th>
    </tr>
    <%
        for (Sale_statistics sale_statistics:sale_statisticsList){
    %>
    <tr class="alt">
        <td align="center"><input type="checkbox" name="ck_index_no"  value=<%=sale_statistics.getIndex_no()%>></td>
        <td align="center"><%=no++%></td>
        <td><input size="15" style="background-color:#fafafa;" type="text" value=<%=sale_statistics.getUpload_time()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_statistics.getUploader()%>></td>
        <td><input size="5" style="background-color:#fafafa;" type="text" value=<%=sale_statistics.getDept()%>></td>
        <td><input size="15" style="background-color:#fafafa;" type="text" value=<%=sale_statistics.getRemark()%>></td>
        <td><a href="/erp/download_sale_statistics_appendix?index_no=<%=sale_statistics.getIndex_no()%>">下载附件</a> </td>
    </tr>
    <%
        }
    %>
    <tr class="alt">
        <td align="center" colspan="7"  height="10px">
            总共<%=number%>页&emsp;
            第<%=pageNo%>页&emsp;
            <a href="/erp/sale_statistics?op=previous">上一页</a>
            <a href="/erp/sale_statistics?op=next">下一页</a>
            总共<%=count%>条
            <a href="javascript:void(0)" onclick="$('#add_dlg').dialog('open')">添加</a>
            <a href="javascript:delete_submit()">删除</a>
        </td>
    </tr>
</table>
</body>
</html>
