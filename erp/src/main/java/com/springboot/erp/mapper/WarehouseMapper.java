package com.springboot.erp.mapper;

import com.springboot.erp.entity.Warehouse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;
@Mapper
@Component
public interface WarehouseMapper {
    //获取表warehouse中所有信息
    @Select("select warehouse_no,name,location,volume,rent from warehouse")
    List<Warehouse> getAllwarehouse();

    //获取表中一共有多少条记录
    @Select("select count(*) from warehouse")
    int getRecordCount();

    //分页显示所有仓库信息
    @Select("select warehouse_no,name,location,volume,rent from warehouse limit #{start},#{count}")
    List<Warehouse> getWarehouseByPage(int start,int count);

    //删除选中的数据
    @Select("delete from warehouse where warehouse_no = #{warehouse_no}")
    void delete_warehouse_by_warehouse_no(String warehouse_no);

    /**
     * 根据条件查询warehouse
     */
    //查询(模糊查询）
    @Select("select warehouse_no,name,location,volume,rent from warehouse where warehouse_no like CONCAT('%',#{warehouse_no},'%') and name like CONCAT('%',#{name},'%') and location like CONCAT('%',#{location},'%')" )
    List<Warehouse> queryWarehouse_By(String warehouse_no,String name,String location);

    //修改选中的warehouse信息
    @Update("update warehouse set name=#{name}, location=#{location},volume=#{volume}, rent=#{rent} where warehouse_no=#{warehouse_no}")
    void updateWarehouse(String warehouse_no,String name,String location,double volume,double rent);

    //获取seq_warehouse_no仓库编号序列号
    @Select("select nextval('seq_warehouse_no')")
    String get_seq_warehouse_no();

    //添加warehouse信息
    @Insert("insert into warehouse (warehouse_no,name,location,volume,rent) values (#{warehouse_no},#{name},#{location},#{volume},#{rent})")
    void add_warehouse(String warehouse_no,String name,String location,double volume,double rent);

    //根据warehouse_no查找相应的warehouse
    @Select("select warehouse_no,name,location,volume,rent from warehouse where warehouse_no=#{warehouse_no}")
    Warehouse get_warehouse_by_warehouse_no(String warehouse_no);



   /* //根据warehouse_no,name查询
    @Select("select warehouse_no,name,location,volume,rent from warehouse where warehouse_no like CONCAT('%',#{warehouse_no},'%') and name like CONCAT('%',#{name},'%') ")
    List<Warehouse> getWarehouse_By_warehouse_no(String warehouse_no);*/

}
