package com.springboot.erp.mapper;

import com.springboot.erp.entity.Income;
import com.springboot.erp.entity.Warehouse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface IncomeMapper {
    //获取索引号
    @Select("select nextval('seq_income_index_no')")
    String get_seq_income_index_no();

    //获取表income中所有信息
    @Select("select index_no,list_no,list_time,related_company,pay_account,deal_person,receive_account,total_price,remark,appendix from income limit #{start},#{count}")
    List<Income> getAllIncome_by_page(int start,int count);

    //添加income信息
    @Insert("insert into income (index_no,list_no,list_time,related_company,pay_account,deal_person,receive_account,total_price,remark,appendix) " +
            "values (#{index_no},#{list_no},#{list_time},#{related_company},#{pay_account},#{deal_person},#{receive_account},#{total_price},#{remark},#{appendix})")
    void add_income(Income income);

    //删除选中的income信息
    @Select("delete from income where index_no = #{index_no}")
    void delete_income_by_index_no(String index_no);

    //根据单据编号查询income信息
    @Select("select index_no,list_no,list_time,related_company,pay_account,deal_person,receive_account,total_price,remark,appendix from income where list_no=#{list_no}")
    List<Income> query_income_by_list_no(String list_no);

    //根据日期时间查询income信息
    @Select("select index_no,list_no,list_time,related_company,pay_account,deal_person,receive_account,total_price,remark,appendix from income where list_time between #{start_time} and #{end_time}")
    List<Income> query_income_by_time(Timestamp start_time, Timestamp end_time);

    //获取表中一共有多少条记录
    @Select("select count(*) from income")
    int getRecordCount();

    //根据index_no获取income信息
    @Select("select index_no,list_no,list_time,related_company,pay_account,deal_person,receive_account,total_price,remark,appendix from income where index_no=#{index_no}")
    Income get_by_index_no(String index_no);

    //修改选中的income信息
    @Update("update income set list_time=#{list_time}, related_company=#{related_company},pay_account=#{pay_account}, deal_person=#{deal_person}, " +
            "receive_account=#{receive_account},total_price=#{total_price},remark=#{remark} where index_no=#{index_no}")
    void updateIncome(Timestamp list_time,String related_company,String pay_account,String deal_person,String receive_account,double total_price,String remark,String index_no);



}
