package com.springboot.erp.mapper;

import com.springboot.erp.entity.Expenditure;
import com.springboot.erp.entity.Warehouse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface ExpenditureMapper {
    //分页显示所有支出单
    @Select("select index_no,list_no,list_time,related_company,receive_account,deal_person,pay_account,total_price,remark,appendix " +
            "from expenditure limit #{start},#{count}")
    List<Expenditure> get_Expenditure_ByPage(int start, int count);

    //获取表中一共有多少条记录
    @Select("select count(*) from expenditure")
    int getRecordCount();

    //删除选中的数据
    @Select("delete from expenditure where index_no = #{index_no}")
    void delete_expenditure_by_index_no(String index_no);

    //根据单据编号查询
    @Select("select index_no,list_no,list_time,related_company,receive_account,deal_person,pay_account,total_price,remark,appendix " +
            "from expenditure where list_no=#{list_no}")
    List<Expenditure> get_by_list_no(String list_no);

    //根据单据日期查询
    @Select("select index_no,list_no,list_time,related_company,receive_account,deal_person,pay_account,total_price,remark,appendix " +
            "from expenditure where list_time between #{start_time} and #{end_time}")
    List<Expenditure> get_by_list_time(Timestamp start_time,Timestamp end_time);

    //获取索引号
    @Select("select nextval('seq_expenditure_index_no')")
    String get_seq_expenditure_index_no();

    //添加expenditure信息
    @Insert("insert into expenditure (index_no,list_no,list_time,related_company,receive_account,deal_person,pay_account,total_price,remark,appendix)" +
            " values (#{index_no},#{list_no},#{list_time},#{related_company},#{receive_account},#{deal_person},#{pay_account},#{total_price},#{remark},#{appendix})")
    void add_expenditure(Expenditure expenditure);

    //修改选中的expenditure信息
    @Update("update expenditure set list_time=#{list_time}, related_company=#{related_company},receive_account=#{receive_account}, deal_person=#{deal_person}, " +
            "pay_account=#{pay_account},total_price=#{total_price},remark=#{remark} where index_no=#{index_no}")
    void update_expenditure(Timestamp list_time,String related_company,String receive_account,String deal_person,String pay_account,double total_price,String remark,String index_no);

    //根据index_no获取
    @Select("select index_no,list_no,list_time,related_company,receive_account,deal_person,pay_account,total_price,remark,appendix " +
            "from expenditure where index_no=#{index_no}")
    Expenditure get_by_index_no(String index_no);


}
