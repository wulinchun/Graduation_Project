package com.springboot.erp.mapper;

import com.springboot.erp.entity.Stock_warning;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface Stock_warningMapper {
    //获取库存预警表中的信息与库存统计表做一个连接
    @Select("select ss.warehouse_no,ss.goods_no,ss.goods_name,sw.max,sw.min from stock_warning sw,stock_statistics ss where sw.goods_no=ss.goods_no and sw.warehouse_no=#{warehouse_no}")
    List<Stock_warning> get_by_warehouse_no(String warehouse_no);

    //
    @Insert("insert into stock_warning (warehouse_no,goods_no,max,min) values (#{warehouse_no},#{goods_no},#{max},#{min})")
    void add_stock_warning(String warehouse_no,String goods_no,int max,int min);

    //修改库存预警中的最大预警和最小预警
    @Update("update stock_warning set max=#{max}, min=#{min} where warehouse_no=#{warehouse_no} and goods_no=#{goods_no}")
    void update_max_min(int max,int min,String warehouse_no,String goods_no);

    //根据warehouse_no和goods_no获取相应的Stock_warning
    @Select("select warehouse_no,goods_no,max,min from stock_warning where warehouse_no=#{warehouse_no} and goods_no=#{goods_no}")
    Stock_warning get_by_warehouse_no_goods_no(String warehouse_no,String goods_no);

    //根据商品编号和仓库编号删除
    @Select("delete from stock_warning where warehouse_no = #{warehouse_no} and goods_no=#{goods_no}")
    void delete_by_warehouse_no_goods_no(String warehouse_no,String goods_no);


}
