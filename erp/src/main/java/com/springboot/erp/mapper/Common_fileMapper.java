package com.springboot.erp.mapper;

import com.springboot.erp.entity.Common_file;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface Common_fileMapper {
    //获取所有
    @Select("select index_no,file_name,uploader,file_path,remark from common_file limit #{start},#{count}")
    List<Common_file> get_all_common_file_bypage(int start,int count);

    //将上传的文件信息插入数据库common_file表中
    @Insert("insert into common_file (index_no,file_name,uploader,file_path,remark) values (#{index_no},#{file_name},#{uploader},#{file_path},#{remark})")
    void add_common_file(Common_file common_file);

    //根据index_no删除相应的数据
    @Select("delete from common_file where index_no = #{index_no}")
    void delete_common_file_by_index_no(String index_no);

    //根据索引号获取common_file信息
    @Select("select index_no,file_name,uploader,file_path,remark from common_file where index_no=#{index_no}")
    Common_file get_by_index_no(String index_no);

    //获取索引号
    @Select("select nextval('seq_common_file_index_no')")
    String get_seq_common_file_index_no();

    //获取表中一共有多少条记录
    @Select("select count(*) from common_file")
    int getRecordCount();
}
