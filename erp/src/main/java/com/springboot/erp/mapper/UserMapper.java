package com.springboot.erp.mapper;

import com.springboot.erp.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserMapper {
    //根据username获取用户信息
    //@Select({"call proc_login(#{username})"})
    @Select({"select username,password from user where username=#{username}"})
    User userLogin(String username);

    //显示所有用户
    @Select({"select username,password,name,sex,tel,dept,position,power from user "})
    List<User> all_user();

    //显示所有用户（分页）
    @Select({"select username,password,name,sex,tel,dept,position,power from user limit #{start},#{count} "})
    List<User> getAllUsers_bypage(int start,int count);

    //根据用户名搜素用户
    @Select({"select username,password,name,sex,tel,dept,position,power from user where username=#{username}"})
    User get_by_username(String username);

    //根据姓名搜素用户
    @Select({"select username,password,name,sex,tel,dept,position,power from user where name=#{name}"})
    List<User> get_by_name(String name);

    //根据部门搜素用户
    @Select({"select username,password,name,sex,tel,dept,position,power from user where dept=#{dept}"})
    List<User> get_by_dept(String dept);

    //根据姓名和部门搜素
    @Select({"select username,password,name,sex,tel,dept,position,power from user where name=#{name} and dept=#{dept}"})
    List<User> get_by_name_dept(String name,String dept);

    //根据用户名删除用户
    @Select("delete from user where username = #{username}")
    void delete_by_username(String username);

    //添加用户
    @Insert("insert into user (username,password,name,sex,tel,dept,position,power) values (#{username},#{password}," +
            "#{name},#{sex},#{tel},#{dept},#{position},#{power})")
    void add_user(User user);

    //修改用户
    @Update("update user set  password=#{password},name=#{name},sex=#{sex}, tel=#{tel} , dept=#{dept}, position=#{position}, " +
            "power=#{power} where username=#{username}")
    void update_user(String password,String name,String sex,String tel,String dept,String position,String power,String username);

    //获取user
    @Select({"select count(*) from user"})
    int getCount();

}
