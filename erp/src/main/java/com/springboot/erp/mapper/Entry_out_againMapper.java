package com.springboot.erp.mapper;

import com.springboot.erp.entity.Budget_apply;
import com.springboot.erp.entity.Entry_out_again;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface Entry_out_againMapper {
    //获取表entry_out_again中所有信息
    @Select("select flow_number,status from entry_out_again")
    List<Entry_out_again> get_All_Entry_out_again();

    //根据status获取表entry_out_again中信息
    @Select("select flow_number from entry_out_again where status=#{status}")
    List<String> get_flow_number_by_status(String status);


}
