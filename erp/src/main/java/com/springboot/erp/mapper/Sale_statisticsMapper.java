package com.springboot.erp.mapper;

import com.springboot.erp.entity.Sale_statistics;
import com.springboot.erp.entity.Warehouse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface Sale_statisticsMapper {
    //获取索引号
    @Select("select nextval('seq_sale_statistics_index_no')")
    String get_seq_sale_statistics_index_no();

    //分页显示所有销售统计信息
    @Select("select index_no,upload_time,uploader,dept,remark,appendix from sale_statistics limit #{start},#{count}")
    List<Sale_statistics> get_All_ByPage(int start, int count);

    //获取表中一共有多少条记录
    @Select("select count(*) from sale_statistics")
    int getRecordCount();

    //删除选中的数据
    @Select("delete from sale_statistics where index_no = #{index_no}")
    void delete_sale_statistics_by_index_no(String index_no);

    //根据上传时间查询
    @Select("select index_no,upload_time,uploader,dept,remark,appendix from sale_statistics where upload_time between #{start_time} and #{end_time}")
    List<Sale_statistics> query_sale_statistics_by_upload_time(Timestamp start_time,Timestamp end_time);

    //根据index_no获取sale_statistics
    @Select("select index_no,upload_time,uploader,dept,remark,appendix from sale_statistics where index_no=#{index_no}")
   Sale_statistics get_sale_statistics_by_index_no(String index_no);

    //添加sale_statistics信息
    @Insert("insert into sale_statistics (index_no,upload_time,uploader,dept,remark,appendix) values " +
            "(#{index_no},#{upload_time},#{uploader},#{dept},#{remark},#{appendix})")
    void add_sale_statistics(Sale_statistics sale_statistics);





}

