package com.springboot.erp.mapper;

import com.springboot.erp.entity.Entry_Warehouse;
import com.springboot.erp.entity.Purchase_apply;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface Purchase_applyMapper {
    //根据用户名获取采购申请信息（分页）
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where username=#{username} limit #{start},#{count}")
    List<Purchase_apply> get_Purchase_apply_Byusername_ByPage(String username,int start,int count);

    //获取所有采购申请信息（分页）
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply limit #{start},#{count}")
    List<Purchase_apply> get_all_Purchase_apply_ByPage(int start,int count);

    //获取表中一共有多少条记录（根据username)
    @Select("select count(*) from purchase_apply where username=#{username}")
    int getRecordCountByUsername(String username);

    //获取表中一共有多少条记录
    @Select("select count(*) from purchase_apply")
    int getRecordCount();

    //获取表中一共有多少条记录（根据status)
    @Select("select count(*) from purchase_apply where status=#{status}")
    int getRecordCountByStatus(String status);

    //获取表中状态为：“通过”，“已确认”，“已购买”，“已到货”，“已入库”
    @Select("select count(*) from purchase_apply where status=#{status1} or status=#{status2} or status=#{status3} or status=#{status4} or status=#{status5}")
    int getRecordCount_passed(String status1,String status2,String status3,String status4,String status5);

    //根据审核状态获取申请信息
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where status=#{status}")
    List<Purchase_apply> get_Purchase_apply_Bystatus(String status);

    //添加申请信息
    @Insert("insert into purchase_apply (index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price) values " +
            "(#{index_no},#{username},#{name},#{dept},#{kind},#{goods_name},#{count},#{apply_time},#{status},#{manufacturer},#{total_price})")
    void add_purchase_apply(Purchase_apply purchase_apply);

    //删除申请信息
    @Select("delete from purchase_apply where index_no = #{index_no}")
    void delete_by_index_no(String index_no);

    //获取申请信息的索引号
    @Select("select nextval('seq_purchase_apply_index_no')")
    String get_seq_purchase_apply_index_no();

    //根据index_no修改purchase_apply表中的status信息
    @Update("update purchase_apply set status=#{status} where index_no=#{index_no}")
    void update_purchase_apply(String index_no,String status);

    //根据username和status查询purchase_apply表中的信息
    List<Purchase_apply> get_Purchase_apply_By_username_status(String username,String status);

    //根据状态获取采购申请信息（分页）
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where status=#{status} limit #{start},#{count}")
    List<Purchase_apply> get_Purchase_apply_Bystatus_ByPage(String status,int start,int count);

    //根据状态获取采购申请信息（分页）
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply " +
            "where status=#{status1} or status=#{status2} or status=#{status3} or status=#{status4} or status=#{status5} limit #{start},#{count}")
    List<Purchase_apply> get_Purchase_apply_passed_ByPage(String status1,String status2,String status3,String status4,String status5,int start,int count);

    //根据index_no获取信息
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where index_no=#{index_no}")
    Purchase_apply get_by_index_no(String index_no);

    //根据index_no确认订单信息
    @Update("update purchase_apply set status=#{status},manufacturer=#{manufacturer}, total_price=#{total_price} where index_no=#{index_no}")
    void confirm_purchase_apply(String status,String manufacturer,double total_price,String index_no);


    //根据用户名查询订单信息
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where username=#{username}")
    List<Purchase_apply> get_Purchase_apply_passed_by_username(String username);

    //根据日期查询订单信息
    @Select("select index_no,username,name,dept,kind,goods_name,count,apply_time,status,manufacturer,total_price from purchase_apply where apply_time between #{start_date} and #{end_date}")
    List<Purchase_apply> get_Purchase_apply_passed_by_date(Timestamp start_date, Timestamp end_date);

}
