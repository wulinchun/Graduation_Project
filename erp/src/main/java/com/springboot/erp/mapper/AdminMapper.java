package com.springboot.erp.mapper;

import com.springboot.erp.entity.Admin;
import com.springboot.erp.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AdminMapper {
    //根据username获取用户信息
    //@Select({"call proc_login_admin(#{username})"})
    @Select({"select username,password from admin where username=#{username}"})
    Admin adminLogin(String username);
}
