package com.springboot.erp.mapper;

import com.springboot.erp.entity.Budget_apply;
import com.springboot.erp.entity.Entry_Warehouse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface Budget_applyMapper {
    //获取表budget_apply中所有信息（分页）
    @Select("select index_no,file_name,file_path,username_apply,dept,apply_time,status,username_deal,deal_time from budget_apply limit #{start},#{count}")
    List<Budget_apply> get_All_Budget_apply_byPage(int start,int count);

    //根据status获取budget_apply信息
    @Select("select index_no,file_name,file_path,username_apply,dept,apply_time,status,username_deal,deal_time from budget_apply where status=#{status}")
    List<Budget_apply> get_by_status(String status);

    //根据username_apply获取信息
    @Select("select index_no,file_name,file_path,username_apply,dept,apply_time,status,username_deal,deal_time from budget_apply where username_apply=#{username_apply}")
    List<Budget_apply> get_by_username_apply(String username_apply);


    //向budget_apply中插入数据
    @Insert("insert into budget_apply (index_no,file_name,file_path,username_apply,dept,apply_time,status,username_deal,deal_time) values " +
            "(#{index_no},#{file_name},#{file_path},#{username_apply},#{dept},#{apply_time},#{status},#{username_deal},#{deal_time})")
    void add_budget_apply(Budget_apply budget_apply);

    //根据index_no删除budget_apply
    @Select("delete from budget_apply where index_no = #{index_no}")
    void delete_budget_apply_by_index_no(String index_no);

    //获取序列号
    @Select("select nextval('seq_budget_apply_index_no')")
    String get_seq_budget_apply_index_no();

    //获取总记录数
    @Select("select count(*) from budget_apply")
    int getRecordCount();

    //根据index_no获取budget_apply
    @Select("select index_no,file_name,file_path,username_apply,dept,apply_time,status,username_deal,deal_time from budget_apply where index_no=#{index_no}")
    Budget_apply get_by_index_no(String index_no);

    //根据index_no修改budget_apply的信息
    @Update("update budget_apply set status=#{status}, username_deal=#{username_deal},deal_time=#{deal_time} where index_no=#{index_no}")
    void update_by_index_no(String status, String username_deal, Timestamp deal_time, String index_no);

}
