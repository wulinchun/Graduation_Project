package com.springboot.erp.mapper;

import com.springboot.erp.entity.Sale_bill;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface Sale_billMapper {
    //获取表Sale_bill表中所有信息（分页）
    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill limit #{start},#{count}")
    List<Sale_bill> get_All_Sale_list_byPage(int start, int count);

    //获取序列号
    @Select("select nextval('seq_sale_list_index_no')")
    String get_seq_sale_list_index_no();

    //添加sale_list信息
    @Insert("insert into sale_bill (index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark) " +
            "values (#{index_no},#{customer_name},#{company_name},#{list_no},#{list_date},#{goods_no},#{goods_name},#{count},#{total_price},#{operator},#{status},#{remark})")
    void add_sale_list(Sale_bill sale_bill);

    //根据index_no删除sale_bill
    @Select("delete from sale_bill where index_no = #{index_no}")
    void delete_sale_bill_by_index_no(String index_no);

    //获取总记录数
    @Select("select count(*) from sale_bill")
    int getRecordCount();

    //根据index_no获取sale_bill信息
    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill where index_no=#{index_no}")
    Sale_bill get_by_index_no(String index_no);


    //修改指定的订单信息
    @Update("update sale_bill set customer_name=#{customer_name}, company_name=#{company_name},list_no=#{list_no},list_date=#{list_date}," +
            "goods_no=#{goods_no},goods_name=#{goods_name},count=#{count},total_price=#{total_price},operator=#{operator},status=#{status},remark=#{remark} " +
            "where index_no=#{index_no}")
    void updateSale_bill(String customer_name, String company_name, String list_no, Timestamp list_date, String goods_no,String goods_name,
                         int count, double total_price,String operator,String status,String remark,String index_no);

    //根据条件查询订单信息
    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill where list_no=#{list_no}")
    List<Sale_bill> get_by_list_no(String list_no);

    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill where goods_name=#{goods_name}")
    List<Sale_bill> get_by_goods_name(String goods_name);

    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill " +
            "where list_date between #{start_date} and #{end_date}")
    List<Sale_bill> get_by_list_date(Timestamp start_date,Timestamp end_date);

    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill where status=#{status}")
    List<Sale_bill> get_by_status(String status);

    @Select("select index_no,customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark from sale_bill " +
            "where list_date between #{start_date} and #{end_date} and status=#{status}")
    List<Sale_bill> get_by_list_date_status(Timestamp start_date,Timestamp end_date,String status);


}
