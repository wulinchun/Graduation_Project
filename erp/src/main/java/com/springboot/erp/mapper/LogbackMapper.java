package com.springboot.erp.mapper;

import com.springboot.erp.entity.Logback;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Component
public interface LogbackMapper {

    //添加日志信息
    @Insert("insert into logback (operation_module,operation_detail,operator,operation_status,operation_time) " +
            "values (#{operation_module},#{operation_detail},#{operator},#{operation_status},#{operation_time})")
    void add_daily_record(Logback logback);

    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback limit #{start},#{count}")
    List<Logback> get_all_daily_record_by_page(int start,int count);

    @Select("select count(*) from logback")
    int getRecordCount();

    //根据操作模块查询
    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback " +
            "where operation_module=#{operation_module} limit #{start},#{count}")
    List<Logback> get_by_operation_module(String operation_module,int start,int count);

    @Select("select count(*) from logback where operation_module=#{operation_module}")
    int getRecordCount_operation_module(String operation_module);

    //根据时间查询
    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback " +
            "where operation_time between #{time_start} and #{time_end} limit #{start},#{count}")
    List<Logback> get_by_operation_time(Timestamp time_start,Timestamp time_end,int start,int count);

    @Select("select count(*) from logback where operation_time between #{time_start} and #{time_end}")
    int getRecordCount_operation_time(Timestamp time_start,Timestamp time_end);

    //根据操作人员查询
    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback " +
            "where operator=#{operator} limit #{start},#{count}")
    List<Logback> get_by_operator(String operator,int start,int count);

    @Select("select count(*) from logback where operator=#{operator}")
    int getRecordCount_operator(String operator);

    //根据状态查询
    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback " +
            "where operation_status=#{operation_status} limit #{start},#{count}")
    List<Logback> get_by_operation_status(String operation_status,int start,int count);

    @Select("select count(*) from logback where operation_status=#{operation_status}")
    int getRecordCount_operation_status(String operation_status);

   //根据操作模块，时间，人员查询
    @Select("select operation_module,operation_detail,operator,operation_status,operation_time from logback " +
            "where operation_module=#{operation_module} and operation_time between #{time_start} and #{time_end}" +
            "and operator=#{operator} and operation_status=#{operation_status} limit #{start},#{count}")
    List<Logback> get_by_operation_module_time_operator_status(String operation_module,Timestamp time_start,Timestamp time_end,String operator,
                                                        String operation_status ,int start,int count);

    @Select("select count(*) from logback where operation_module=#{operation_module} and operation_time between #{time_start} and #{time_end}\" +\n" +
            "            \"and operator=#{operator} and operation_status=#{operation_status}")
    int getRecordCount_operation_module_time_operator_status(String operation_module,Timestamp time_start,Timestamp time_end,String operator,
                                        String operation_status);

    //清空所有日志
    @Select("delete from logback ")
    void delete_all();
}
