package com.springboot.erp.mapper;

import com.springboot.erp.entity.Logback;
import com.springboot.erp.entity.User_Power;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface User_PowerMapper {
    //获取表user_power中所有信息
    @Select("select index_no,tablename,username,power from user_power")
    List<User_Power> get_all();

    //获取表user_power中所有信息（分页）
    @Select("select index_no,tablename,username,power from user_power limit #{start},#{count}")
    List<User_Power> get_all_by_page(int start, int count);

    //获取表user_power中的总记录数
    @Select("select count(*) from user_power")
    int getRecordCount();


    //根据tablename检索表user_power中的信息
    @Select("select index_no,tablename,username,power from user_power where tablename=#{tablename}")
    List<User_Power> get_by_tablename(String tablename);

    //根据username检索user_power中的信息
    @Select("select index_no,tablename,username,power from user_power where username=#{username}")
    List<User_Power> get_by_username(String username);

    //根据username和tablename检索user_power中的信息
    @Select("select index_no,tablename,username,power from user_power where username=#{username} and tablename=#{tablename}")
    List<User_Power> get_by_username_tablename(String username,String tablename);

    //根据tablename修改权限
    @Update("update user_power set username=#{username}, power=#{power} where tablename=#{tablename}")
    void update_by_tablename(String username,String power,String tablename);

    //根据username修改权限
    @Update("update user_power set tablename=#{tablename}, power=#{power} where username=#{username}")
    void update_by_username(String tablename,String power,String username);

    //删除某个用户的某项权限
    @Select("delete from user_power where tablename = #{tablename} and username=#{username} and power=#{power}")
    void delete_by_tup(String tablename,String username,String power);

    @Select("delete from user_power where index_no = #{index_no}")
    void delete_by_index_no(String index_no);

    //添加一条权限
    @Insert("insert into user_power (index_no,tablename,username,power) values (#{index_no},#{tablename},#{username},#{power})")
    void add_user_power(User_Power user_power);

    //获取索引号
    @Select("select nextval('seq_user_power')")
    String get_seq_user_power();

    //根据索引号获取用户权限信息
    @Select("select index_no,tablename,username,power from user_power where index_no=#{index_no}")
    User_Power get_by_index_no(String index_no);

    //根据用户名，表名，权限检索用户权限信息
    @Select("select index_no,tablename,username,power from user_power where tablename=#{tablename} and username=#{username} and power=#{power}")
    User_Power get_by_tup(String tablename,String username,String power);

}
