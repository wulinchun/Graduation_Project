package com.springboot.erp;

import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//@ServletComponentScan  //（打war用）
@SpringBootApplication
public class ErpApplication extends SpringBootServletInitializer {



    public static void main(String[] args) {
        SpringApplication.run(ErpApplication.class, args);
    }


    //（打war用）
    /**
     *新增此方法
     */
  /*@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        // 注意这里要指向原先用main方法执行的Application启动类
        return builder.sources(ErpApplication .class);
    }*/






}
