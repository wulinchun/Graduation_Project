package com.springboot.erp.service;

import com.springboot.erp.entity.User_Power;
import com.springboot.erp.mapper.User_PowerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class User_PowerService {
    private User_PowerMapper user_powerMapper;

    @Autowired  //使用构造器自动装配

    public User_PowerService(User_PowerMapper user_powerMapper) {
        this.user_powerMapper = user_powerMapper;
    }

    public List<User_Power> get_all(){
        return user_powerMapper.get_all();
    }

    public List<User_Power> get_by_tablename(String tablename){
        return user_powerMapper.get_by_tablename(tablename);
    }

    public List<User_Power> get_by_username(String username){
        return user_powerMapper.get_by_username(username);
    }

    public void update_by_tablename(String username,String power,String tablename){
        user_powerMapper.update_by_tablename(username,power,tablename);
    }

    public void update_by_username(String tablename,String power,String username){
        user_powerMapper.update_by_username(tablename,power,username);
    }

    public void delete_by_tup(String tablename,String username,String power){
        user_powerMapper.delete_by_tup(tablename,username,power);
    }

    public void add_user_power(User_Power user_power){
        user_powerMapper.add_user_power(user_power);
    }

    public List<User_Power> get_by_username_tablename(String username,String tablename){
        return user_powerMapper.get_by_username_tablename(username,tablename);
    }

    public List<User_Power> get_all_by_page(int start, int count){
        return user_powerMapper.get_all_by_page(start,count);
    }

    public int getRecordCount(){
        return user_powerMapper.getRecordCount();
    }

    public void delete_by_index_no(String index_no){
        user_powerMapper.delete_by_index_no(index_no);
    }

    public String get_seq_user_power(){
        return user_powerMapper.get_seq_user_power();
    }

    public User_Power get_by_index_no(String index_no){
        return user_powerMapper.get_by_index_no(index_no);
    }

    public User_Power get_by_tup(String tablename,String username,String power){
        return user_powerMapper.get_by_tup(tablename,username,power);
    }
}
