package com.springboot.erp.service;

import com.springboot.erp.entity.Entry_Warehouse;
import com.springboot.erp.mapper.Entry_WarehouseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class Entry_WarehouseService {
    private Entry_WarehouseMapper entry_warehouseMapper;

    @Autowired
    public Entry_WarehouseService(Entry_WarehouseMapper entry_warehouseMapper) {
        this.entry_warehouseMapper = entry_warehouseMapper;
    }

    //显示所有入库信息
    public List<Entry_Warehouse> get_All_Entry_Warehouse(){
        return entry_warehouseMapper.get_All_Entry_Warehouse();
    }

    //分页显示所有入库信息
    public List<Entry_Warehouse> get_All_Entry_Warehouse_ByPage(int start,int count){
        return entry_warehouseMapper.get_All_Entry_Warehouse_ByPage(start,count);
    }

    //一共有多少条记录
    public int getRecordCount(){
        return entry_warehouseMapper.getRecordCount();
    }

    //根据流水号删除入库信息
    public void delete_entry_warehouse_by_flow_number(String flow_number){
        entry_warehouseMapper.delete_entry_warehouse_by_flow_number(flow_number);
    }

    /*******************************************************************************************************************************************/
    //根据条件查询指定信息

    //根据warehouse_no查询
    public List<Entry_Warehouse> queryEntry_Warehouse_By_warehouse_no(String warehouse_no){
        return entry_warehouseMapper.queryEntry_Warehouse_By_warehouse_no(warehouse_no);
    }
    // 根据flow_number
    public Entry_Warehouse queryEntry_Warehouse_By_flow_number(String flow_number){
        return  entry_warehouseMapper.queryEntry_Warehouse_By_flow_number(flow_number);
    }

    //根据list_no
    public List<Entry_Warehouse> queryEntry_Warehouse_By_list_no(String list_no){
        return entry_warehouseMapper.queryEntry_Warehouse_By_list_no(list_no);
    }

    //根据goods_name
    public List<Entry_Warehouse> queryEntry_Warehouse_By_goods_name(String goods_name){
        return entry_warehouseMapper.queryEntry_Warehouse_By_goods_name(goods_name);
    }

    //根据entry_date范围查询
    public  List<Entry_Warehouse> queryEntry_Warehouse_By_entry_date(Timestamp start_date, Timestamp end_date){
        return entry_warehouseMapper.queryEntry_Warehouse_By_entry_date(start_date,end_date);
    }

    //根据list_no和goods_name查询
    public List<Entry_Warehouse> queryEntry_Warehouse_By_list_no_And_goods_name(String list_no,String goods_name){
        return entry_warehouseMapper.queryEntry_Warehouse_By_list_no_And_goods_name(list_no,goods_name);
    }

    //根据list_no和goods_name以及entry_date查询
    public List<Entry_Warehouse> queryEntry_Warehouse_By_list_no_And_goods_name_And_entry_date(String list_no,String goods_name,
                                                                                               Timestamp start_date,Timestamp end_date){
        return entry_warehouseMapper.queryEntry_Warehouse_By_list_no_And_goods_name_And_entry_date(list_no,goods_name,start_date,end_date);
    }

    //根据list_no和entry_date查询查询
    public List<Entry_Warehouse> queryEntry_Warehouse_By_list_no_And_entry_date(String list_no,Timestamp start_date,Timestamp end_date){
        return entry_warehouseMapper.queryEntry_Warehouse_By_list_no_And_entry_date(list_no,start_date,end_date);
    }

    //根据status
    public List<Entry_Warehouse> queryEntry_Warehouse_By_status(String status){
        return entry_warehouseMapper.queryEntry_Warehouse_By_status(status);
    }

    //更新entry_warehouse
    public void updateEntry_Warehouse(String flow_number,String list_no, String goods_no, String kind, String specification, String goods_name, double single_price,int count,double total_price,String manufacturer, Timestamp entry_date, String status, String warehouse_no,String remarks){
        entry_warehouseMapper.updateEntry_Warehouse(flow_number,list_no,goods_no,kind,specification,goods_name,single_price,count,total_price,manufacturer,entry_date,status,warehouse_no,remarks);
    }
    /*******************************************************************************************************************************************/

    //添加entry_warehouse
    public void addEntry_Warehouse(Entry_Warehouse entry_warehouse){
        entry_warehouseMapper.addEntry_Warehouse(entry_warehouse);
    }

    public String get_seq_entry_warehouse_flow_number(){
        return entry_warehouseMapper.get_seq_entry_warehouse_flow_number();
    }

    //统计某个已入库的goods_no的物品数量
    public int get_goods_no_Count_entry(){
        return entry_warehouseMapper.get_goods_no_Count_entry();
    }

    //分别显示每个商品编号的商品的入库数量
    public List<Entry_Warehouse> get_count_entry_house_groupby_goods_no(){
        return entry_warehouseMapper.get_count_entry_house_groupby_goods_no();
    }

    //统计指定商品编号的入库数量
    public int get_entry_goods_no_Record(String goods_no){
        return entry_warehouseMapper.get_entry_goods_no_Record(goods_no);
    }

    public List<Entry_Warehouse> get_by_goods_no(String goods_no){
        return entry_warehouseMapper.get_by_goods_no(goods_no);
    }

   /* //根据flow_number查询相应的信息
    public Entry_Warehouse get_entry_warehouse_By_flow_number(String flow_number){
        return entry_warehouseMapper.get_entry_warehouse_By_flow_number(flow_number);
    }*/
}
