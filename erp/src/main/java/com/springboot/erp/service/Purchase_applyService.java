package com.springboot.erp.service;

import com.springboot.erp.entity.Purchase_apply;
import com.springboot.erp.mapper.Purchase_applyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class Purchase_applyService {
    private Purchase_applyMapper purchase_applyMapper;

    @Autowired
    public Purchase_applyService(Purchase_applyMapper purchase_applyMapper) {
        this.purchase_applyMapper = purchase_applyMapper;
    }

    public List<Purchase_apply> get_Purchase_apply_Byusername_ByPage(String username,int start,int count){
        return purchase_applyMapper.get_Purchase_apply_Byusername_ByPage(username,start,count);
    }

    public int getRecordCountByUsername(String username){
        return purchase_applyMapper.getRecordCountByUsername(username);
    }

    public int getRecordCount(){
        return purchase_applyMapper.getRecordCount();
    }

    public List<Purchase_apply> get_Purchase_apply_Bystatus(String status){
        return purchase_applyMapper.get_Purchase_apply_Bystatus(status);
    }

    public void add_purchase_apply(Purchase_apply purchase_apply){
        purchase_applyMapper.add_purchase_apply(purchase_apply);
    }

    public void delete_by_index_no(String index_no){
        purchase_applyMapper.delete_by_index_no(index_no);
    }

    public String get_seq_purchase_apply_index_no(){
        return purchase_applyMapper.get_seq_purchase_apply_index_no();
    }

    public List<Purchase_apply> get_all_Purchase_apply_ByPage(int start,int count){
        return purchase_applyMapper.get_all_Purchase_apply_ByPage(start,count);
    }

    public void update_purchase_apply(String index_no,String status){
        purchase_applyMapper.update_purchase_apply(index_no,status);
    }

    public List<Purchase_apply> get_Purchase_apply_By_username_status(String username,String status){
        return purchase_applyMapper.get_Purchase_apply_By_username_status(username,status);
    }

    public List<Purchase_apply> get_Purchase_apply_passed_ByPage(String status1,String status2,String status3,String status4,String status5,int start,int count){
        return purchase_applyMapper.get_Purchase_apply_passed_ByPage(status1,status2,status3,status4,status5,start,count);
    }

    public int getRecordCountByStatus(String status){
        return purchase_applyMapper.getRecordCountByStatus(status);
    }

    public int getRecordCount_passed(String status1,String status2,String status3,String status4,String status5){
        return purchase_applyMapper.getRecordCount_passed(status1,status2,status3,status4,status5);
    }

    public Purchase_apply get_by_index_no(String index_no){
        return purchase_applyMapper.get_by_index_no(index_no);
    }

    public void confirm_purchase_apply(String status,String manufacturer,double total_price,String index_no){
        purchase_applyMapper.confirm_purchase_apply(status,manufacturer,total_price,index_no);
    }

    public List<Purchase_apply> get_Purchase_apply_passed_by_username(String username){
        return purchase_applyMapper.get_Purchase_apply_passed_by_username(username);
    }

    public List<Purchase_apply> get_Purchase_apply_passed_by_date(Timestamp start_date, Timestamp end_date){
        return purchase_applyMapper.get_Purchase_apply_passed_by_date(start_date, end_date);
    }
}
