package com.springboot.erp.service;

import com.springboot.erp.entity.Entry_out_again;
import com.springboot.erp.mapper.Entry_out_againMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Entry_out_againService {
    private Entry_out_againMapper entry_out_againMapper;

    @Autowired
    public Entry_out_againService(Entry_out_againMapper entry_out_againMapper) {
        this.entry_out_againMapper = entry_out_againMapper;
    }

    public List<Entry_out_again> get_All_Entry_out_again(){
        return entry_out_againMapper.get_All_Entry_out_again();
    }

    public List<String> get_flow_number_by_status(String status){
        return entry_out_againMapper.get_flow_number_by_status(status);
    }
}

