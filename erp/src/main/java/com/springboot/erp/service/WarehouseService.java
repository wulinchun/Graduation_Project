package com.springboot.erp.service;

import com.springboot.erp.entity.Warehouse;
import com.springboot.erp.mapper.WarehouseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseService {
    private WarehouseMapper warehouseMapper;

    @Autowired
    public WarehouseService(WarehouseMapper warehouseMapper){
        this.warehouseMapper = warehouseMapper;
    }

    //显示所有仓库信息
    public List<Warehouse> getAllwarehouse(){
        return warehouseMapper.getAllwarehouse();
    }

    //显示一共有多少条记录
    public int getRecordCount(){
        return warehouseMapper.getRecordCount();
    }

    //分页显示仓库信息
    public  List<Warehouse> getWarehouseByPage(int start,int count){
        return warehouseMapper.getWarehouseByPage(start,count);
    }

    //删除选中的仓库信息
    public void delete_warehouse_by_warehouse_no(String warehouse_no){
        warehouseMapper.delete_warehouse_by_warehouse_no(warehouse_no);
    }

    //根据warehouse_no查询
    public List<Warehouse> queryWarehouse_By(String warehouse_no,String name,String location){
        return warehouseMapper.queryWarehouse_By(warehouse_no,name,location);
    }

    //修改选中的warehouse信息
    public void updateWarehouse(String warehouse_no,String name,String location,double volume,double rent){
        warehouseMapper.updateWarehouse(warehouse_no,name,location,volume,rent);
    }

    //获取seq_warehouse_no仓库编号序列号
    public String get_seq_warehouse_no(){
        return warehouseMapper.get_seq_warehouse_no();
    }

    //添加仓库信息
    public void add_warehouse(String warehouse_no,String name,String location,double volume,double rent){
        warehouseMapper.add_warehouse(warehouse_no,name,location,volume,rent);
    }

    //根据warehouse_no显示相应的warehouse信息
    public Warehouse get_warehouse_by_warehouse_no(String warehouse_no){
        return warehouseMapper.get_warehouse_by_warehouse_no(warehouse_no);
    }
}
