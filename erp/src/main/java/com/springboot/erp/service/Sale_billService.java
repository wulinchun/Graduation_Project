package com.springboot.erp.service;

import com.springboot.erp.entity.Sale_bill;
import com.springboot.erp.mapper.Sale_billMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class Sale_billService {
    private Sale_billMapper sale_billMapper;

    @Autowired
    public Sale_billService(Sale_billMapper sale_billMapper) {
        this.sale_billMapper = sale_billMapper;
    }

    public List<Sale_bill> get_All_Sale_list_byPage(int start, int count){
        return sale_billMapper.get_All_Sale_list_byPage(start,count);
    }

    public String get_seq_sale_list_index_no(){
        return sale_billMapper.get_seq_sale_list_index_no();
    }

    public void add_sale_list(Sale_bill sale_bill){
        sale_billMapper.add_sale_list(sale_bill);
    }


    public int getRecordCount(){
        return sale_billMapper.getRecordCount();
    }

    public Sale_bill get_by_index_no(String index_no){
        return sale_billMapper.get_by_index_no(index_no);
    }



    public void updateSale_bill(String customer_name, String company_name, String list_no, Timestamp list_date, String goods_no,String goods_name,
                                int count, double total_price, String operator, String status, String remark, String index_no){
        sale_billMapper.updateSale_bill(customer_name,company_name,list_no,list_date,goods_no,goods_name,count,total_price,operator,status,remark,index_no);
    }

    public List<Sale_bill> get_by_list_no(String list_no){
        return sale_billMapper.get_by_list_no(list_no);
    }

    public List<Sale_bill> get_by_goods_name(String goods_name){
        return sale_billMapper.get_by_goods_name(goods_name);
    }

    public List<Sale_bill> get_by_list_date(Timestamp start_date,Timestamp end_date){
        return sale_billMapper.get_by_list_date(start_date,end_date);
    }

    public List<Sale_bill> get_by_status(String status){
        return sale_billMapper.get_by_status(status);
    }

    public List<Sale_bill> get_by_list_date_status(Timestamp start_date,Timestamp end_date,String status){
        return sale_billMapper.get_by_list_date_status(start_date,end_date,status);
    }

    public void delete_sale_bill_by_index_no(String index_no){
        sale_billMapper.delete_sale_bill_by_index_no(index_no);
    }
}
