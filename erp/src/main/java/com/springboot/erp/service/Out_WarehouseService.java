package com.springboot.erp.service;

import com.springboot.erp.entity.Entry_Warehouse;
import com.springboot.erp.entity.Out_Warehouse;
import com.springboot.erp.mapper.Entry_WarehouseMapper;
import com.springboot.erp.mapper.Out_WarehouseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class Out_WarehouseService {
    private Out_WarehouseMapper out_warehouseMapper;

    @Autowired
    public Out_WarehouseService(Out_WarehouseMapper out_warehouseMapper) {
        this.out_warehouseMapper = out_warehouseMapper;
    }

    //显示所有出库信息
    public List<Out_Warehouse> get_All_Out_Warehouse(){
        return out_warehouseMapper.get_All_Out_Warehouse();
    }

    //分页显示所有出库信息
    public List<Out_Warehouse> get_All_Out_Warehouse_ByPage(int start, int count){
        return out_warehouseMapper.get_All_Out_Warehouse_ByPage(start,count);
    }

    //一共有多少条记录
    public int getRecordCount(){
        return out_warehouseMapper.getRecordCount();
    }

    //根据流水号删除出库信息
    public void delete_out_warehouse_by_flow_number(String flow_number){
        out_warehouseMapper.delete_out_warehouse_by_flow_number(flow_number);
    }

    /*******************************************************************************************************************************************/
    //根据条件查询指定信息
    public List<Out_Warehouse> queryOut_Warehouse_By_warehouse_no(String warehouse_no){
        return out_warehouseMapper.queryOut_Warehouse_By_warehouse_no(warehouse_no);
    }
    // 根据flow_number
    public Out_Warehouse queryOut_Warehouse_By_flow_number(String flow_number){
        return  out_warehouseMapper.queryOut_Warehouse_By_flow_number(flow_number);
    }

    //根据list_no
    public List<Out_Warehouse> queryOut_Warehouse_By_list_no(String list_no){
        return out_warehouseMapper.queryOut_Warehouse_By_list_no(list_no);
    }

    //根据goods_name
    public List<Out_Warehouse> queryOut_Warehouse_By_goods_name(String goods_name){
        return out_warehouseMapper.queryOut_Warehouse_By_goods_name(goods_name);
    }

    //根据out_date范围查询
    public  List<Out_Warehouse> queryOut_Warehouse_By_out_date(Timestamp start_date, Timestamp end_date){
        return out_warehouseMapper.queryOut_Warehouse_By_out_date(start_date,end_date);
    }

    //根据list_no和goods_name查询
    public List<Out_Warehouse> queryOut_Warehouse_By_list_no_And_goods_name(String list_no,String goods_name){
        return out_warehouseMapper.queryOut_Warehouse_By_list_no_And_goods_name(list_no,goods_name);
    }

    //根据list_no和goods_name以及entry_date查询
    public List<Out_Warehouse> queryOut_Warehouse_By_list_no_And_goods_name_And_out_date(String list_no,String goods_name,
                                                                                         Timestamp start_date,Timestamp end_date){
        return out_warehouseMapper.queryOut_Warehouse_By_list_no_And_goods_name_And_Out_date(list_no,goods_name,start_date,end_date);
    }

    //根据list_no和entry_date查询查询
    public List<Out_Warehouse> queryOut_Warehouse_By_list_no_And_out_date(String list_no,Timestamp start_date,Timestamp end_date){
        return out_warehouseMapper.queryOut_Warehouse_By_list_no_And_out_date(list_no,start_date,end_date);
    }

    //根据status查询
    public List<Out_Warehouse> queryOut_Warehouse_By_status(String status){
        return out_warehouseMapper.queryOut_Warehouse_By_status(status);
    }

    //更新out_warehouse
    public void updateOut_Warehouse(String flow_number,String list_no, String goods_no, String kind, String specification, String goods_name, double single_price,int count,double total_price,String manufacturer, Timestamp out_date, String status, String warehouse_no,String remarks){
        out_warehouseMapper.updateOut_Warehouse(flow_number,list_no,goods_no,kind,specification,goods_name,single_price,count,total_price,manufacturer,out_date,status,warehouse_no,remarks);
    }

    //添加out_warehouse
    public void addOut_Warehouse(Out_Warehouse out_warehouse){
        out_warehouseMapper.addOut_Warehouse(out_warehouse);
    }

    //获取序列号
    public String get_seq_out_warehouse_flow_number(){
        return out_warehouseMapper.get_seq_out_warehouse_flow_number();
    }

    //统计指定goods_no的数量
    public int get_goods_no_Count_out(){
        return out_warehouseMapper.get_goods_no_Count_out();
    }

    //分别显示每个商品编号的商品的出库数量
    public List<Out_Warehouse> get_count_out_warehouse_groupby_goods_no(){
        return out_warehouseMapper.get_count_out_warehouse_groupby_goods_no();
    }

    //显示指定商品编号商品出库数量
    public int get_out_goods_no_Record(String goods_no){
        return out_warehouseMapper.get_out_goods_no_Record(goods_no);
    }

    public List<Out_Warehouse> get_by_goods_no(String goods_no){
        return out_warehouseMapper.get_by_goods_no(goods_no);
    }
}
