package com.springboot.erp.service;

import com.springboot.erp.entity.Common_file;
import com.springboot.erp.mapper.Common_fileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Common_fileService {
    private Common_fileMapper common_fileMapper;

    @Autowired
    public Common_fileService(Common_fileMapper common_fileMapper) {
        this.common_fileMapper = common_fileMapper;
    }

    public List<Common_file> get_all_common_file_bypage(int start, int count){
        return common_fileMapper.get_all_common_file_bypage(start,count);
    }

    public void add_common_file(Common_file common_file){
        common_fileMapper.add_common_file(common_file);
    }

    public void delete_common_file_by_remark(String index_no){
        common_fileMapper.delete_common_file_by_index_no(index_no);
    }

    public  String get_seq_common_file_index_no(){
        return common_fileMapper.get_seq_common_file_index_no();
    }

    public int getRecordCount(){
        return common_fileMapper.getRecordCount();
    }

    public Common_file get_by_index_no(String index_no){
        return common_fileMapper.get_by_index_no(index_no);
    }
}
