package com.springboot.erp.service;

import com.springboot.erp.entity.Income;
import com.springboot.erp.mapper.IncomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class IncomeService {
    private IncomeMapper incomeMapper;

    @Autowired
    public IncomeService(IncomeMapper incomeMapper) {
        this.incomeMapper = incomeMapper;
    }

    public String get_seq_income_index_no(){
        return incomeMapper.get_seq_income_index_no();
    }

    public List<Income> getAllIncome_by_page(int start,int count){
        return incomeMapper.getAllIncome_by_page(start, count);
    }

    public void add_income(Income income){
        incomeMapper.add_income(income);
    }

    public void delete_income_by_index_no(String index_no){
        incomeMapper.delete_income_by_index_no(index_no);
    }

    public List<Income> query_income_by_list_no(String list_no){
        return incomeMapper.query_income_by_list_no(list_no);
    }

    public List<Income> query_income_by_time(Timestamp start_time, Timestamp end_time){
        return incomeMapper.query_income_by_time(start_time, end_time);
    }

    public int getRecordCount(){
        return incomeMapper.getRecordCount();
    }

    public Income get_by_index_no(String index_no){
        return incomeMapper.get_by_index_no(index_no);
    }

    public void updateIncome(Timestamp list_time,String related_company,String pay_account,
                                String deal_person,String receive_account,double total_price,String remark,String index_no){
        incomeMapper.updateIncome(list_time, related_company, pay_account, deal_person, receive_account, total_price, remark, index_no);
    }
}
