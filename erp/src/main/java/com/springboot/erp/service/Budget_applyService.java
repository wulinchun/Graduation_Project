package com.springboot.erp.service;

import com.springboot.erp.entity.Budget_apply;
import com.springboot.erp.mapper.Budget_applyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class Budget_applyService {
    private Budget_applyMapper budget_applyMapper;

    @Autowired
    public Budget_applyService(Budget_applyMapper budget_applyMapper) {
        this.budget_applyMapper = budget_applyMapper;
    }


    public List<Budget_apply> get_All_Budget_apply_byPage(int start, int count){
        return budget_applyMapper.get_All_Budget_apply_byPage(start,count);
    }

    public List<Budget_apply> get_by_status(String status){
        return budget_applyMapper.get_by_status(status);
    }

    public void add_budget_apply(Budget_apply budget_apply){
        budget_applyMapper.add_budget_apply(budget_apply);
    }

    public void delete_budget_apply_by_index_no(String index_no){
        budget_applyMapper.delete_budget_apply_by_index_no(index_no);
    }

    public String get_seq_budget_apply_index_no(){
        return budget_applyMapper.get_seq_budget_apply_index_no();
    }

    public int getRecordCount(){
        return budget_applyMapper.getRecordCount();
    }

    public Budget_apply get_by_index_no(String index_no){
        return budget_applyMapper.get_by_index_no(index_no);
    }

    public void update_by_index_no(String status, String username_deal, Timestamp deal_time, String index_no){
        budget_applyMapper.update_by_index_no(status,username_deal,deal_time,index_no);
    }

    public  List<Budget_apply> get_by_username_apply(String username_apply){
        return budget_applyMapper.get_by_username_apply(username_apply);
    }


}
