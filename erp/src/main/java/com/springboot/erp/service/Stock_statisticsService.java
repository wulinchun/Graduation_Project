package com.springboot.erp.service;

import com.springboot.erp.entity.Stock_statistics;
import com.springboot.erp.mapper.Stock_statisticsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class Stock_statisticsService {
    private Stock_statisticsMapper stockstatisticsMapper;

    @Autowired
    public Stock_statisticsService(Stock_statisticsMapper stockstatisticsMapper) {
        this.stockstatisticsMapper = stockstatisticsMapper;
    }

    public List<Stock_statistics> get_All_Stock_statistics_ByPage(int start, int count){
        return stockstatisticsMapper.get_All_Stock_statistics_ByPage(start,count);
    }

    public  List<Stock_statistics> get_All_Stock_statistics(){
        return stockstatisticsMapper.get_All_Stock_statistics();
    }

    public int getRecordCount(){
        return stockstatisticsMapper.getRecordCount();
    }

    public void renew_goods_no_count(int newcount,String goods_no){
        stockstatisticsMapper.renew_goods_no_count(newcount,goods_no);
    }

    public void update_status(String status,String warehouse_no,String goods_no){
        stockstatisticsMapper.update_status(status,warehouse_no,goods_no);
    }

    public Stock_statistics get_by_warehouse_no_goods_no(String warehouse_no,String goods_no){
        return stockstatisticsMapper.get_by_warehouse_no_goods_no(warehouse_no,goods_no);
    }

    /*public void renew_goods_no_count_add(int morecount,String goods_no){
        stockstatisticsMapper.renew_goods_no_count_add(morecount,goods_no);
    }*/

    public void add_stock_statistics_items(Stock_statistics stock_statistics){
        stockstatisticsMapper.add_stock_statistics_items(stock_statistics);
    }

    public String get_seq_stock_statistics_index_no(){
        return stockstatisticsMapper.get_seq_stock_statistics_index_no();
    }

    public void update_add_stock_statistics_items_count_if_exist(String goods_no){
        stockstatisticsMapper.update_add_stock_statistics_items_count_if_exist(goods_no);
    }

    public void update_minus_stock_statistics_items_count_if_exist(String goods_no){
        stockstatisticsMapper.update_minus_stock_statistics_items_count_if_exist(goods_no);
    }

    public Stock_statistics get_by_goods_no(String goods_no){
        return stockstatisticsMapper.get_by_goods_no(goods_no);
    }

    public void delete_by_goods_no(String goods_no){
        stockstatisticsMapper.delete_by_goods_no(goods_no);
    }

    public void delete_by_index_no(String index_no){
        stockstatisticsMapper.delete_by_index_no(index_no);
    }

    public Stock_statistics get_by_index_no(String index_no){
        return stockstatisticsMapper.get_by_index_no(index_no);
    }

    public void update_stock_statistics_by_index_no(String index_no,String warehouse_no,String goods_no,String kind,String specification,String goods_name,
                                                         double single_price,int count,double total_price,String remarks){
        stockstatisticsMapper.update_stock_statistics_by_index_no(index_no,warehouse_no,goods_no,kind,specification,goods_name,single_price,count,total_price,remarks);
    }

    public List<Stock_statistics> get_by_warehouse_no(String warehouse_no){
        return stockstatisticsMapper.get_by_warehouse_no(warehouse_no);
    }

    /*public List<Warehouse_remain> get_All_Warehouse_remain(){
        return stockstatisticsMapper.get_All_Warehouse_remain();
    }

    public List<Warehouse_remain> get_All_Warehouse_remain_ByPage(int start,int count){
        return stockstatisticsMapper.get_All_Warehouse_remain_ByPage(start,count);
    }

    public int getRecordCount(){
        return stockstatisticsMapper.getRecordCount();
    }

    //根据入库信息修改库存
    public  void renew_goods_no_count(int count,String goods_no){
        stockstatisticsMapper.renew_goods_no_count(count,goods_no);
    }

    //根据goods_no获取库存余额信息
    public Warehouse_remain get_warehouse_remain_by_goods_no(String goods_no){
        return stockstatisticsMapper.get_warehouse_remain_by_goods_no(goods_no);
    }*/
}
