package com.springboot.erp.service;

import com.springboot.erp.entity.Logback;
import com.springboot.erp.mapper.LogbackMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class LogbackService {
    private LogbackMapper logbackMapper;

    @Autowired
    public LogbackService(LogbackMapper logbackMapper) {
        this.logbackMapper = logbackMapper;
    }

    public void add_daily_record(Logback logback){
        logbackMapper.add_daily_record(logback);
    }

    public List<Logback> get_all_daily_record_by_page(int start,int count){
        return logbackMapper.get_all_daily_record_by_page(start,count);
    }

    public int getRecordCount(){
        return logbackMapper.getRecordCount();
    }

    public List<Logback> get_by_operation_module(String operation_module,int start,int count){
        return logbackMapper.get_by_operation_module(operation_module,start,count);
    }

    public List<Logback> get_by_operation_time(Timestamp time_start, Timestamp time_end, int start, int count){
        return logbackMapper.get_by_operation_time(time_start,time_end,start,count);
    }

    public List<Logback> get_by_operator(String operator,int start,int count){
        return logbackMapper.get_by_operator(operator,start,count);
    }

    public List<Logback> get_by_operation_status(String operation_status,int start,int count){
        return logbackMapper.get_by_operation_status(operation_status,start,count);
    }

    public List<Logback> get_by_operation_module_time_operator_status(String operation_module,Timestamp time_start,Timestamp time_end,String operator,
                                                               String operation_status,int start,int count){
        return logbackMapper.get_by_operation_module_time_operator_status(operation_module,time_start,time_end,operator,operation_status,start,count);
    }

    public int getRecordCount_operation_module(String operation_module){
        return logbackMapper.getRecordCount_operation_module(operation_module);
    }

    public int getRecordCount_operation_time(Timestamp time_start,Timestamp time_end){
        return logbackMapper.getRecordCount_operation_time(time_start,time_end);
    }

    public int getRecordCount_operator(String operator){
        return logbackMapper.getRecordCount_operator(operator);
    }

    public int getRecordCount_operation_status(String operation_status){
        return logbackMapper.getRecordCount_operation_status(operation_status);
    }

    public  int getRecordCount_operation_module_time_operator_status(String operation_module,Timestamp time_start,Timestamp time_end,String operator,
                                                                     String operation_status){
        return logbackMapper.getRecordCount_operation_module_time_operator_status(operation_module,time_start,time_end,operator,operation_status);
    }

    public void delete_all(){
        logbackMapper.delete_all();
    }
}
