package com.springboot.erp.service;

import com.springboot.erp.entity.Stock_warning;
import com.springboot.erp.mapper.Stock_warningMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Stock_warningService {
    private Stock_warningMapper stock_warningMapper;

    @Autowired
    public Stock_warningService(Stock_warningMapper stock_warningMapper) {
        this.stock_warningMapper = stock_warningMapper;
    }

    public List<Stock_warning> get_by_warehouse_no(String warehouse_no){
        return stock_warningMapper.get_by_warehouse_no(warehouse_no);
    }

    public void add_stock_warning(String warehouse_no,String goods_no,int max,int min){
        stock_warningMapper.add_stock_warning(warehouse_no,goods_no,max,min);
    }

    public void update_max_min(int max,int min,String warehouse_no,String goods_no){
        stock_warningMapper.update_max_min(max,min,warehouse_no,goods_no);
    }

    public Stock_warning get_by_warehouse_no_goods_no(String warehouse_no,String goods_no){
        return stock_warningMapper.get_by_warehouse_no_goods_no(warehouse_no,goods_no);
    }

    public void delete_by_warehouse_no_goods_no(String warehouse_no,String goods_no){
        stock_warningMapper.delete_by_warehouse_no_goods_no(warehouse_no,goods_no);
    }
}
