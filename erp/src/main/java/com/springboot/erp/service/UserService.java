package com.springboot.erp.service;

import com.springboot.erp.entity.Admin;
import com.springboot.erp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private com.springboot.erp.mapper.UserMapper userMapper;
    private com.springboot.erp.mapper.AdminMapper adminMapper;

    @Autowired  //使用构造器自动装配
    public UserService(com.springboot.erp.mapper.UserMapper userMapper,
                       com.springboot.erp.mapper.AdminMapper adminMapper){
        this.userMapper = userMapper;
        this.adminMapper = adminMapper;
    }
    //private com.springboot.erp.mapper.userMapper userMapper;

    //用户登录
    public User userLogin(String username){
        return userMapper.userLogin(username);
    }

    //管理员登录
    public Admin adminLogin(String username){
        return adminMapper.adminLogin(username);
    }

    public List<User> all_user(){
        return userMapper.all_user();
    }

    public List<User> get_by_name(String name){
        return userMapper.get_by_name(name);
    }

    public List<User> get_by_dept(String dept){
        return userMapper.get_by_dept(dept);
    }

    public List<User> get_by_name_dept(String name,String dept){
        return userMapper.get_by_name_dept(name,dept);
    }

    public void delete_by_username(String username){
        userMapper.delete_by_username(username);
    }

    public void add_user(User user){
        userMapper.add_user(user);
    }

    public void update_user(String password,String name,String sex,String tel,String dept,String position,String power,String username){
        userMapper.update_user(password,name,sex,tel,dept,position,power,username);
    }

    public User get_by_username(String username){
        return userMapper.get_by_username(username);
    }

    public List<User> getAllUsers_bypage(int start,int count){
        return userMapper.getAllUsers_bypage(start,count);
    }

    public int getCount(){
        return userMapper.getCount();
    }
}
