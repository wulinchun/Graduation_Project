package com.springboot.erp.service;

import com.springboot.erp.entity.Sale_statistics;
import com.springboot.erp.mapper.Sale_statisticsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class Sale_statisticsService {
    private Sale_statisticsMapper sale_statisticsMapper;

    @Autowired
    public Sale_statisticsService(Sale_statisticsMapper sale_statisticsMapper) {
        this.sale_statisticsMapper = sale_statisticsMapper;
    }

    public List<Sale_statistics> get_All_ByPage(int start, int count){
        return sale_statisticsMapper.get_All_ByPage(start, count);
    }

    public int getRecordCount(){
        return sale_statisticsMapper.getRecordCount();
    }

    public void delete_sale_statistics_by_index_no(String index_no){
        sale_statisticsMapper.delete_sale_statistics_by_index_no(index_no);
    }

    public  List<Sale_statistics> query_sale_statistics_by_upload_time(Timestamp start_time, Timestamp end_time){
        return sale_statisticsMapper.query_sale_statistics_by_upload_time(start_time, end_time);
    }

    public void add_sale_statistics(Sale_statistics sale_statistics){
        sale_statisticsMapper.add_sale_statistics(sale_statistics);
    }

    public String get_seq_sale_statistics_index_no(){
        return sale_statisticsMapper.get_seq_sale_statistics_index_no();
    }

    public Sale_statistics get_sale_statistics_by_index_no(String index_no){
        return sale_statisticsMapper.get_sale_statistics_by_index_no(index_no);
    }


}
