package com.springboot.erp.service;

import com.springboot.erp.entity.Expenditure;
import com.springboot.erp.mapper.ExpenditureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class ExpenditureService {
    private ExpenditureMapper expenditureMapper;

    @Autowired
    public ExpenditureService(ExpenditureMapper expenditureMapper) {
        this.expenditureMapper = expenditureMapper;
    }

    public List<Expenditure> get_Expenditure_ByPage(int start, int count){
        return expenditureMapper.get_Expenditure_ByPage(start,count);
    }

    public int getRecordCount(){
        return expenditureMapper.getRecordCount();
    }

    public void delete_expenditure_by_index_no(String index_no){
        expenditureMapper.delete_expenditure_by_index_no(index_no);
    }

    public List<Expenditure> get_by_list_no(String list_no){
        return expenditureMapper.get_by_list_no(list_no);
    }

    public List<Expenditure> get_by_list_time(Timestamp start_time, Timestamp end_time){
        return expenditureMapper.get_by_list_time(start_time, end_time);
    }

    public String get_seq_expenditure_index_no(){
        return expenditureMapper.get_seq_expenditure_index_no();
    }

    public void add_expenditure(Expenditure expenditure){
        expenditureMapper.add_expenditure(expenditure);
    }

    public void update_expenditure(Timestamp list_time,String related_company,String receive_account,
                                   String deal_person,String pay_account,double total_price,String remark,String index_no){
        expenditureMapper.update_expenditure(list_time, related_company, receive_account, deal_person, pay_account, total_price, remark, index_no);
    }

    public Expenditure get_by_index_no(String index_no){
        return expenditureMapper.get_by_index_no(index_no);
    }
}
