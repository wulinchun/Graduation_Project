package com.springboot.erp.controller;

import com.springboot.erp.Tool.Tool;
import com.springboot.erp.entity.*;
import com.springboot.erp.service.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = {"/erp"})
public class ErpController {
    private UserService userService;
    static int user_start = 0;  //select * from user limit ?,?
    static int user_pageNo = 1;  //分页显示的页码
    static int user_record_No = 1;  //每条记录的序号

    private Logger logger= LoggerFactory.getLogger(getClass());

    private WarehouseService warehouseService;
    static int warehouse_distribution_start = 0;   //select * from warehouse limit ?,?
    static int warehouse_distribution_pageNo = 1; //分页显示的页码  warehouse_distribution
    static int warehouse_distribution_No = 1;   //每条记录的序号  warehouse_distribution

    private Entry_WarehouseService entry_warehouseService;
    static int entry_warehouse_start = 0;  //select * from entry_warehouse limit ?,?
    static int entry_warehouse_pageNo = 1; //分页显示的页码  entry_warehouse
    static int entry_warehouse_record_No = 1; //每条记录的序号  entry_warehouse

    private Out_WarehouseService out_warehouseService;
    static int out_warehouse_start = 0;  //select * from out_warehouse limit ?,?
    static int out_warehouse_pageNo = 1; //分页显示的页码  out_warehouse
    static int out_warehouse_record_No = 1;  //每条记录的序号  out_warehouse

    private Stock_statisticsService stockstatisticsService;
    static int stock_statistics_start = 0;  //select * from stock_statistics limit ?,?
    static int stock_statistics_pageNo = 1;  //分页显示的页码
    static int stock_statistics_record_No = 1;  //每条记录的序号

    private Stock_warningService stock_warningService;

    private LogbackService logbackService;
    static int logback_start = 0;  //select * from logback limit ?,?
    static int logback_pageNo = 1;  //分页显示的页码
    static int logback_record_No = 1;  //每条记录的序号

    static int query_logback_start = 0;  //select * from logback where limit ?,?
    static int query_logback_pageNo = 1;  //分页显示的页码
    static int query_logback_record_No = 1;  //每条记录的序号

    private User_PowerService user_powerService;
    static int user_power_start = 0;  //select * from user_power limit ?,?
    static int user_power_pageNo = 1;  //分页显示的页码
    static int user_power_record_No = 1;  //每条记录的序号

    private Purchase_applyService purchase_applyService;
    static int purchase_apply_start = 0;  //select * from purchase_apply where username=  limit ?,?
    static int purchase_apply_pageNo = 1;  //分页显示的页码
    static int purchase_apply_record_No = 1;  //每条记录的序号

    static int all_purchase_apply_start = 0;  //select * from purchase_apply limit ?,?
    static int all_purchase_apply_pageNo = 1;  //分页显示的页码
    static int all_purchase_apply_record_No = 1;  //每条记录的序号

    static int purchase_apply_pass_start = 0;  //select * from purchase_apply where status="通过" or status="已确认" limit ?,?
    static int purchase_apply_pass_pageNo = 1;  //分页显示的页码
    static int purchase_apply_pass_record_No = 1;  //每条记录的序号

    private Budget_applyService budget_applyService;
    static int budget_apply_start = 0;  //select * from budget_apply limit ?,?
    static int budget_apply_pageNo = 1;  //分页显示的页码
    static int budget_apply_record_No = 1;  //每条记录的序号

    static int budget_apply_check_start = 0;  //select * from budget_apply limit ?,?
    static int budget_apply_check_pageNo = 1;  //分页显示的页码
    static int budget_apply_check_record_No = 1;  //每条记录的序号

    private Sale_billService sale_billService;
    static int sale_bill_start = 0;  //select * from sale_list limit ?,?
    static int sale_bill_pageNo = 1;  //分页显示的页码
    static int sale_bill_record_No = 1;  //每条记录的序号

    private IncomeService incomeService;
    static int income_start = 0;  //select * from income limit ?,?
    static int income_pageNo = 1;  //分页显示的页码
    static int income_record_No = 1;  //每条记录的序号

    private ExpenditureService expenditureService;
    static int expenditure_start = 0;  //select * from expenditure limit ?,?
    static int expenditure_pageNo = 1;  //分页显示的页码
    static int expenditure_record_No = 1;  //每条记录的序号

    private Sale_statisticsService sale_statisticsService;
    static int sale_statistics_start = 0;  //select * from sale_statistics limit ?,?
    static int sale_statistics_pageNo = 1;  //分页显示的页码
    static int sale_statistics_record_No = 1;  //每条记录的序号

    private Common_fileService common_fileService;
    static int common_file_start = 0;  //select * from common_file limit ?,?
    static int common_file_pageNo = 1;  //分页显示的页码
    static int common_file_record_No = 1;  //每条记录的序号

    private Entry_out_againService entry_out_againService;

    //private String final_warehouse_no;

    /*static int warehouse_remain_limit_start = 0;
    static int warehouse_remain_limit_pageNo = 1;
    static int warehouse_remain_limit_record_No = 1;*/

    @Autowired   //使用构造器自动装配
    public ErpController(UserService userService,
                         WarehouseService warehouseService,
                         Entry_WarehouseService entry_warehouseService,
                         Out_WarehouseService out_warehouseService,
                         Stock_statisticsService stockstatisticsService,
                         Stock_warningService stock_warningService,
                         LogbackService logbackService,
                         User_PowerService user_powerService,
                         Purchase_applyService purchase_applyService,
                         Common_fileService common_fileService,
                         Budget_applyService budget_applyService,
                         Sale_billService sale_billService,
                         IncomeService incomeService,
                         ExpenditureService expenditureService,
                         Sale_statisticsService sale_statisticsService,
                         Entry_out_againService entry_out_againService) {
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.entry_warehouseService = entry_warehouseService;
        this.out_warehouseService = out_warehouseService;
        this.stockstatisticsService = stockstatisticsService;
        this.stock_warningService=stock_warningService;
        this.logbackService=logbackService;
        this.user_powerService=user_powerService;
        this.purchase_applyService=purchase_applyService;
        this.common_fileService=common_fileService;
        this.budget_applyService=budget_applyService;
        this.sale_billService = sale_billService;
        this.incomeService=incomeService;
        this.expenditureService=expenditureService;
        this.sale_statisticsService=sale_statisticsService;
        this.entry_out_againService=entry_out_againService;

    }

    //跳转用户登录页面
    @RequestMapping(value = {"/login_view"}, method = RequestMethod.GET)
    public String login_view() {
        return "login_view";
    }

    //用户登录验证
    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public void login(HttpSession session, HttpServletRequest request, HttpServletResponse response)
            throws IOException, JSONException {
        PrintWriter out = response.getWriter();
        String username = request.getParameter("username");  //与 id 属性一一对应
        String password = request.getParameter("password");
        //String is_user=request.getParameter("is_user");
        String status = request.getParameter("status");
        //String is_admin=request.getParameter("is_admin");
        //注意这里的request.getParameter("username")取的是data里面的那个json对象的username,而非<input>里面那个，同理password也一样。
        JSONObject json = new JSONObject();
        User user = new User();
        Admin admin = new Admin();
        System.out.println(status);
        if((!username.equals(""))&&(!status.equals(""))) {
            if (status.equals("is_user")) {
                if(userService.get_by_username(username)!=null) {
                    user = userService.get_by_username(username);
                }
                session.setAttribute("session_user", user);
                //System.out.println(user.toString());
            }
            if (status.equals("is_admin")) {
                if(userService.adminLogin(username)!=null) {
                    admin = userService.adminLogin(username);
                }
                session.setAttribute("session_admin", admin);
            }
            if ((user!=null||admin!=null)&&(username.equals(user.getUsername()) && password.equals(user.getPassword())) ||
                    (username.equals(admin.getUsername()) && password.equals(admin.getPassword()))) {
                session.setAttribute("session_username", username);
                json.put("result", "success");
                logger.info("用户,登录id:" + username + "." + username + ";" + "成功");  //写入日志
            } else {
                json.put("result", "failure");
                logger.error("用户,登录id:" + username + "." + username + ";" + "失败");  //写入日志
            }
        }
        else if (status.equals("")){
            json.put("result", "empty");
        }
        out.print(json);
    }

    //注销
    @RequestMapping(value = {"/logout"},method = RequestMethod.GET)
    public void logout(HttpSession session, HttpServletRequest request,HttpServletResponse response){
        if(session.getAttribute("session_username")!=null){
            String username=(String)session.getAttribute("session_username") ;
            logger.info("用户,注销id:"+username+"."+username+";"+"成功");  //写入日志
            session.removeAttribute("session_username");
        }
    }

    //用户个人设置update_personal
    @RequestMapping(value = {"/update_personal"}, method = RequestMethod.POST)
    public void update_personal(HttpServletRequest request,HttpServletResponse response,HttpSession session){
        String username=request.getParameter("txt_username");
        String password=request.getParameter("txt_password");
        String name=request.getParameter("txt_name");
        String sex=request.getParameter("txt_sex");
        String tel=request.getParameter("txt_tel");
        String dept=request.getParameter("txt_dept");
        String position=request.getParameter("txt_position");
        String power=request.getParameter("txt_power");
        userService.update_user(password,name,sex,tel,dept,position,power,username);
        User user = userService.get_by_username(username);
        session.setAttribute("session_user",user);
        logger.info("用户,个人设置"+username+":"+username+"."+username+";"+"成功");  //写入日志


    }
    //跳转到主界面
    @RequestMapping(value = {"/main_view"}, method = RequestMethod.GET)
    public String main_view(HttpSession session) throws ParseException {
        Tool tool=new Tool();
        String []start_time_array={"2020-01-01 00:00:00","2020-02-01 00:00:00","2020-03-01 00:00:00","2020-04-01 00:00:00",
                "2020-05-01 00:00:00","2020-06-01 00:00:00","2020-07-01 00:00:00","2020-08-01 00:00:00","2020-09-01 00:00:00",
                "2020-10-01 00:00:00","2020-11-01 00:00:00","2020-12-01 00:00:00"};
        String []end_time_array={"2020-01-31 23:59:59","2020-02-31 23:59:59","2020-03-31 23:59:59",
                "2020-04-31 23:59:59","2020-05-31 23:59:59","2020-06-31 23:59:59","2020-07-31 23:59:59","2020-08-31 23:59:59","2020-09-31 23:59:59",
                "2020-10-31 23:59:59","2020-11-31 23:59:59","2020-12-31 23:59:59"};

        List<Income> incomeList=new ArrayList<Income>();
        List<Expenditure> expenditureList=new ArrayList<Expenditure>();
        List<Sale_bill> sale_billList=new ArrayList<Sale_bill>();
        List<Purchase_apply> purchase_applyList=new ArrayList<Purchase_apply>();
        double []total_income_array={0,0,0,0,0,0,0,0,0,0,0,0};
        double []total_expenditure_array={0,0,0,0,0,0,0,0,0,0,0,0};
        double []total_profit_array={0,0,0,0,0,0,0,0,0,0,0,0};
        double []total_sale_array={0,0,0,0,0,0,0,0,0,0,0,0};
        double []total_purchase_array={0,0,0,0,0,0,0,0,0,0,0,0};
        for (int i=0;i<12;i++) {
            incomeList=incomeService.query_income_by_time(tool.change_string_to_sql_timestamp(start_time_array[i]),tool.change_string_to_sql_timestamp(end_time_array[i]));
            for (Income income : incomeList) {
                total_income_array[i] += income.getTotal_price();
            }
        }
        for(int i=0;i<12;i++){
            expenditureList=expenditureService.get_by_list_time(tool.change_string_to_sql_timestamp(start_time_array[i]),tool.change_string_to_sql_timestamp(end_time_array[i]));
            for(Expenditure expenditure:expenditureList){
                total_expenditure_array[i]+=expenditure.getTotal_price();
            }
        }
        for(int i=0;i<12;i++){
            total_profit_array[i]=total_income_array[i]-total_expenditure_array[i];
        }
        for(int i=0;i<12;i++){
            sale_billList=sale_billService.get_by_list_date(tool.change_string_to_sql_timestamp(start_time_array[i]),tool.change_string_to_sql_timestamp(end_time_array[i]));
            for(Sale_bill sale_bill:sale_billList){
                total_sale_array[i]+=sale_bill.getTotal_price();
            }
        }
        for(int i=0;i<12;i++){
            purchase_applyList=purchase_applyService.get_Purchase_apply_passed_by_date(tool.change_string_to_sql_timestamp(start_time_array[i]),tool.change_string_to_sql_timestamp(end_time_array[i]));
            for(Purchase_apply purchase_apply:purchase_applyList){
                total_purchase_array[i]+=purchase_apply.getTotal_price();
            }
        }
        session.setAttribute("session_total_income_array",total_income_array);
        session.setAttribute("session_total_expenditure_array",total_expenditure_array);
        session.setAttribute("session_total_profit_array",total_profit_array);
        session.setAttribute("session_total_sale_array",total_sale_array);
        session.setAttribute("session_total_purchase_array",total_purchase_array);


        return "main_view";
    }

    //跳转到仓库设置界面
    @RequestMapping(value = {"/warehouse_setting"}, method = RequestMethod.GET)
    public String warehouse_setting(HttpSession session, HttpServletRequest request) {
        List<Warehouse> warehouseList = new ArrayList<Warehouse>();
        String op = request.getParameter("op");
        session.setAttribute("warehouse_distribution_record_No", warehouse_distribution_No);
        //int firstNo=(Integer)session.getAttribute("warehouse_distribution_record_No");
        System.out.println("第" + warehouse_distribution_pageNo + "页" + warehouse_distribution_start);
        if (op.equals("previous")) {
            if (warehouse_distribution_start > 20) {
                warehouse_distribution_start = warehouse_distribution_start - 20;
                //warehouse_distribution_end=warehouse_distribution_end-20;
                warehouse_distribution_pageNo--;
                System.out.println("第" + warehouse_distribution_pageNo + "页" + warehouse_distribution_start);
            } else {
                warehouse_distribution_start = 0;
                warehouse_distribution_pageNo--;
            }
            warehouse_distribution_No = warehouse_distribution_No - 20;
            session.setAttribute("warehouse_distribution_record_No", warehouse_distribution_No);
        } else if (op.equals("next")) {
            warehouse_distribution_start = warehouse_distribution_start + 20;
            //warehouse_distribution_end=warehouse_distribution_end+20;
            warehouse_distribution_pageNo++;
            warehouse_distribution_No = warehouse_distribution_No + 20;
            session.setAttribute("warehouse_distribution_record_No", warehouse_distribution_No);
            System.out.println("第" + warehouse_distribution_pageNo + "页" + warehouse_distribution_start);
        }
        warehouseList = warehouseService.getWarehouseByPage(warehouse_distribution_start, 20);
        if (session.getAttribute("all_warehouses_session") == null) {
            session.setAttribute("all_warehouses_session", warehouseList);
        } else if (op.equals("update_warehouse")) {
            session.setAttribute("all_warehouses_session", warehouseList);
        }
        session.setAttribute("warehousesPageCount_session", warehouseService.getRecordCount());  //总记录数
        session.setAttribute("warehousesPageNo_session", warehouse_distribution_pageNo);    //第?页
        //权限认证
        String current_username="";
        if(session.getAttribute("session_username")!=null){
            current_username=(String)session.getAttribute("session_username");
        }
        User_Power user_power=user_powerService.get_by_tup("warehouse",current_username,"select");
        if(user_power==null){
            return "no_power";
        }
        return "warehouse_setting";
    }

    //删除warehouse中选中的数据
    @RequestMapping(value = {"/delete_warehouse_by_warehouse_no"}, method = RequestMethod.POST)
    public void delete_warehouse_by_warehouse_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String warehouse_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power=user_powerService.get_by_tup("warehouse",username,"delete");
        if(user_power!=null) {
            json.put("result", "success");

            for (int i = 0; i < check_val.length; i++) {
                warehouseService.delete_warehouse_by_warehouse_no(check_val[i].toString());
                warehouse_no = warehouse_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除仓库" + warehouse_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //显示选中的仓库信息
    @RequestMapping(value = {"/show_selected_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Warehouse warehouse = new Warehouse();
        warehouse = warehouseService.get_warehouse_by_warehouse_no(check_val[0].toString());
        json.put("json_warehouse_no", warehouse.getWarehouse_no());
        json.put("json_name", warehouse.getName());
        json.put("json_location", warehouse.getLocation());
        json.put("json_volume", warehouse.getVolume());
        json.put("json_rent", warehouse.getRent());
        out.print(json);
    }

    //修改warehouse信息
    @RequestMapping(value = {"/update_warehouse"}, method = RequestMethod.POST)
    //ajax会返回response请求，因此想要执行回调函数，需要声明response请求
    public void update_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String dlg_txt_warehouse_no = request.getParameter("dlg_txt_warehouse_no");
        String dlg_txt_warehouse_name = request.getParameter("dlg_txt_warehouse_name");
        String dlg_txt_warehouse_location = request.getParameter("dlg_txt_warehouse_location");
        String dlg_txt_warehouse_volume = request.getParameter("dlg_txt_warehouse_volume");
        String dlg_txt_warehouse_rent = request.getParameter("dlg_txt_warehouse_rent");
        String username = "";
        boolean volume_judge=dlg_txt_warehouse_volume.matches("^[0-9]+(.[0-9]+)?$");  //正则表达式
        boolean rent_judge=dlg_txt_warehouse_rent.matches("^[0-9]+(.[0-9]+)?$");  //正则表达式
        if (session.getAttribute("session_username") != null) {
            username = (String) session.getAttribute("session_username");
        }
        //输入合法性判断
        if(volume_judge==true&&rent_judge==true) {
            //权限认证
            User_Power user_power = user_powerService.get_by_tup("warehouse", username, "update");
            if (user_power != null) {
                json.put("result", "success");
                warehouseService.updateWarehouse(dlg_txt_warehouse_no, dlg_txt_warehouse_name, dlg_txt_warehouse_location,
                        Double.valueOf(dlg_txt_warehouse_volume), Double.valueOf(dlg_txt_warehouse_rent));
                logger.info("用户,修改仓库" + dlg_txt_warehouse_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            }
        }
        else {
            json.put("result", "illegal");
        }
        out.print(json);
    }
    //根据条件查询warehouse信息
    @RequestMapping(value = {"/query_warehouse"}, method = RequestMethod.POST)
    public String getWarehouse_By_warehouse_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String query_txt_warehouse_no = request.getParameter("query_txt_warehouse_no");
        String query_txt_name = request.getParameter("query_txt_name");
        String query_txt_location = request.getParameter("query_txt_location");
        //String query_txt_volume=request.getParameter("query_txt_volume");
        List<Warehouse> warehouseList = new ArrayList<Warehouse>();
        warehouseList = warehouseService.queryWarehouse_By(query_txt_warehouse_no, query_txt_name, query_txt_location);
        session.setAttribute("all_warehouses_session", warehouseList);
        return "redirect:/erp/warehouse_setting?op=query";
    }

   /* //修改warehouse信息
    @RequestMapping(value = {"/update_warehouse"}, method = RequestMethod.POST)
    //ajax会返回response请求，因此想要执行回调函数，需要声明response请求
    public void update_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        String[] warehouse_no = request.getParameterValues("array_warehouse_no");
        String[] warehouse_name = request.getParameterValues("array_warehouse_name");
        String[] location = request.getParameterValues("array_location");
        String[] volume = request.getParameterValues("array_volume");
        String[] rent = request.getParameterValues("array_rent");
        for (int i = 0; i < warehouse_no.length; i++) {
            //System.out.println(warehouse_no[i]+" "+warehouse_name[i]+location[i]+" "+volume[i]+" "+rent[i]);
            warehouseService.updateWarehouse(warehouse_no[i], warehouse_name[i], location[i], Double.valueOf(volume[i]), Double.valueOf(rent[i]));
        }
        //return "redirect:/erp/warehouse_distribution?op=update_warehouse";
    }*/

  /*  //跳转到add_warehouse页面
    @RequestMapping(value = {"/add_warehouse"}, method = RequestMethod.GET)
    public void add_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
       String add_dlg_txt_warehouse_no=request.getParameter("add_dlg_txt_warehouse_no");
       String add_dlg_txt_warehouse_name=request.getParameter("add_dlg_txt_warehouse_name");
       String add_dlg_txt_warehouse_location=request.getParameter("add_dlg_txt_warehouse_location");
       String add_dlg_txt_warehouse_volume=request.getParameter("add_dlg_txt_warehouse_volume");
       String add_dlg_txt_warehouse_rent=request.getParameter("add_dlg_txt_warehouse_rent");
       warehouseService.add_warehouse(add_dlg_txt_warehouse_no,add_dlg_txt_warehouse_name,add_dlg_txt_warehouse_location,
               Double.valueOf(add_dlg_txt_warehouse_volume),Double.valueOf(add_dlg_txt_warehouse_rent));
    }*/

    //实现添加仓库信息
    @RequestMapping(value = {"/add_warehouse"}, method = RequestMethod.POST)
    public void add_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String warehouse_no = request.getParameter("add_dlg_txt_warehouse_no");
        String warehouse_name = request.getParameter("add_dlg_txt_warehouse_name");
        String warehouse_location = request.getParameter("add_dlg_txt_warehouse_location");
        String warehouse_volume = request.getParameter("add_dlg_txt_warehouse_volume");
        String warehouse_rent = request.getParameter("add_dlg_txt_warehouse_rent");
        String final_warehouse_no = warehouse_no;
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        boolean volume_judge=warehouse_volume.matches("^[0-9]+(.[0-9]+)?$");  //正则表达式
        boolean rent_judge=warehouse_rent.matches("^[0-9]+(.[0-9]+)?$");  //正则表达式
        //输入合法性判断
        if(volume_judge==true&&rent_judge==true) {
            //权限认证
            User_Power user_power = user_powerService.get_by_tup("warehouse", username, "add");
            if (user_power != null) {
                json.put("result", "success");
                warehouseService.add_warehouse(final_warehouse_no, warehouse_name, warehouse_location, Double.parseDouble(warehouse_volume), Double.parseDouble(warehouse_rent));
                logger.info("用户,添加仓库" + warehouse_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            }
        }
        else {
            json.put("result", "illegal");
        }
        out.print(json);
    }

    /*//显示选中仓库的库存情况
    @RequestMapping(value = {"/show_selected_warehouse_detail"}, method = RequestMethod.POST)
    public void show_selected_warehouse_detail(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        String[] check_val = request.getParameterValues("check_val");
        List<Stock_statistics> stock_statisticsList=new ArrayList<Stock_statistics>();
        stock_statisticsList=stockstatisticsService.get_by_warehouse_no(check_val[0].toString());
        session.setAttribute("session_show_selected_warehouse_detail",stock_statisticsList);
    }*/

    /*******************************************************************/
    /*******************************************************************/
    /***********************entry_warehouse******************************/

    //跳转到入库单界面
    @RequestMapping(value = {"/entry_warehouse_list"}, method = RequestMethod.GET)
    public String entry_warehouse_list(HttpServletRequest request, HttpSession session) {
        List<Entry_Warehouse> entry_warehouseList = new ArrayList<Entry_Warehouse>();
        String op = request.getParameter("op");
        session.setAttribute("session_entry_warehouse_record_No", entry_warehouse_record_No);
        List<Warehouse> warehouseList=new ArrayList<Warehouse>();
        warehouseList=warehouseService.getAllwarehouse();
        session.setAttribute("session_all_warehouse",warehouseList);
        if (op.equals("previous")) {
            if (entry_warehouse_start > 25) {
                entry_warehouse_start = entry_warehouse_start - 25;
                entry_warehouse_pageNo--;
            } else {
                entry_warehouse_start = 0;
                entry_warehouse_pageNo--;
            }
            entry_warehouse_record_No = entry_warehouse_record_No - 25;
            session.setAttribute("session_entry_warehouse_record_No", entry_warehouse_record_No);
        } else if (op.equals("next")) {
            entry_warehouse_start = entry_warehouse_start + 25;
            entry_warehouse_pageNo++;
            entry_warehouse_record_No = entry_warehouse_record_No + 25;
            session.setAttribute("session_entry_warehouse_record_No", entry_warehouse_record_No);
        }
        entry_warehouseList = entry_warehouseService.get_All_Entry_Warehouse_ByPage(entry_warehouse_start, 25);
        if (session.getAttribute("session_all_entry_warehouse") == null) {
            session.setAttribute("session_all_entry_warehouse", entry_warehouseList);
        } else if (op.equals("update_entry_warehouse")) {
            //session
        }
        session.setAttribute("session_entry_warehouse_PageCount", entry_warehouseService.getRecordCount());
        session.setAttribute("session_entry_warehouse_PageNo", entry_warehouse_pageNo);
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("entry_warehouse", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "entry_warehouse_list";
    }

    /*//根据仓库编号显示该仓库的入库单
    @RequestMapping(value = {"/entry_warehouse_list_limit"}, method = RequestMethod.POST)
    public void entry_warehouse_list_limit(HttpServletRequest request,HttpServletResponse response,HttpSession session){

    }*/

    //删除entry_warehouse中选中的数据
    @RequestMapping(value = {"/delete_entry_warehouse_by_flow_number"}, method = RequestMethod.POST)
    public void delete_entry_warehouse_by_flow_number(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String flow_number="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("entry_warehouse", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                entry_warehouseService.delete_entry_warehouse_by_flow_number(check_val[i].toString());

                flow_number = flow_number + "||" + check_val[i].toString();
            }
            logger.info("用户,删除入库" + flow_number + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        json.put("result","success");
        out.print(json);
    }

    //根据条件查询入库信息
    @RequestMapping(value = {"/query_entry_warehouse"}, method = RequestMethod.POST)
    public String query_entry_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ParseException {
        String query_txt_flow_number = request.getParameter("query_txt_flow_number");
        String query_txt_list_no = request.getParameter("query_txt_list_no");
        String query_txt_goods_name = request.getParameter("query_txt_goods_name");
        String query_txt_start_date = request.getParameter("query_txt_start_date");
        String query_txt_end_date = request.getParameter("query_txt_end_date");
        String select_warehouse_no=request.getParameter("select_warehouse_no");
        String query_txt_status=request.getParameter("query_txt_status");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_start_date = null,util_end_date=null;
        java.sql.Timestamp sql_start_date = null;
        java.sql.Timestamp sql_end_date = null;

        //判断start_date,end_date的格式,根据start_date,end_date的格式做相应的转换
        boolean date_format_judge_1=true,date_format_judge_2=true;
        if((!query_txt_start_date.equals(""))&&(!query_txt_end_date.equals(""))) {
            try {
                Date date = format1.parse(query_txt_start_date);
            } catch (ParseException e) {
                date_format_judge_1 = false;
            }
            if (date_format_judge_1 == true) {
                util_start_date = format1.parse(query_txt_start_date);
            } else {
                util_start_date = format1.parse(format1.format(format2.parse(query_txt_start_date)));
            }
            try {
                Date date = format1.parse(query_txt_end_date);
            } catch (ParseException e) {
                date_format_judge_2 = false;
            }
            if (date_format_judge_2 == true) {
                util_end_date = format1.parse(query_txt_end_date);
            } else {
                util_end_date = format1.parse(format1.format(format2.parse(query_txt_end_date)));
            }
            sql_start_date = new java.sql.Timestamp(util_start_date.getTime());
            sql_end_date = new java.sql.Timestamp(util_end_date.getTime());
        }

        List<Entry_Warehouse> entry_warehouseList = new ArrayList<Entry_Warehouse>();
        if(!select_warehouse_no.equals("")){
            entry_warehouseList=entry_warehouseService.queryEntry_Warehouse_By_warehouse_no(select_warehouse_no);

        }
        if (!query_txt_flow_number.equals("")) {
            entry_warehouseList.add(entry_warehouseService.queryEntry_Warehouse_By_flow_number(query_txt_flow_number));
        }
        if (query_txt_flow_number.equals("")) {
            if ((!query_txt_list_no.equals("")) &&
                    query_txt_goods_name.equals("") &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_list_no(query_txt_list_no);
            } else if ((!query_txt_goods_name.equals("")) &&
                    query_txt_list_no.equals("") &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_goods_name(query_txt_goods_name);
            } else if ((!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    query_txt_list_no.equals("") &&
                    query_txt_goods_name.equals("")) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_entry_date(sql_start_date, sql_end_date);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_goods_name.equals("")) &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_list_no_And_goods_name(query_txt_list_no, query_txt_goods_name);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_goods_name.equals("")) &&
                    (!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals(""))) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_list_no_And_goods_name_And_entry_date(query_txt_list_no, query_txt_goods_name, sql_start_date, sql_end_date);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    (query_txt_list_no.equals(""))) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_list_no_And_entry_date(query_txt_list_no, sql_start_date, sql_end_date);
            } else if ((!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    query_txt_goods_name.equals("") &&
                    query_txt_list_no.equals("")) {
                entry_warehouseList = entry_warehouseService.queryEntry_Warehouse_By_entry_date(sql_start_date, sql_end_date);
            }else if (!query_txt_status.equals("")){
                entry_warehouseList=entry_warehouseService.queryEntry_Warehouse_By_status(query_txt_status);
            }
        }
        //System.out.println(entry_warehouseList.get(0).toString());
        //entry_warehouseList=entry_warehouseService.queryEntry_Warehouse_By(query_txt_flow_number,query_txt_list_no,query_txt_goods_name,query_txt_start_date,query_txt_end_date);
        session.setAttribute("session_all_entry_warehouse", entry_warehouseList);
        return "redirect:/erp/entry_warehouse_list?op=query_entry_warehouse";
    }

    //显示选中的入库信息，该方法用来接收前端传递的ajax
    @RequestMapping(value = {"/show_selected_entry_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_entry_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Entry_Warehouse entry_warehouse = new Entry_Warehouse();
        entry_warehouse = entry_warehouseService.queryEntry_Warehouse_By_flow_number(check_val[0].toString());
        System.out.println(entry_warehouse.toString());
        json.put("json_flow_number", entry_warehouse.getFlow_number());
        json.put("json_list_no", entry_warehouse.getList_no());
        json.put("json_goods_no", entry_warehouse.getGoods_no());
        json.put("json_kind", entry_warehouse.getKind());
        json.put("json_specification", entry_warehouse.getSpecification());
        json.put("json_goods_name", entry_warehouse.getGoods_name());
        json.put("json_single_price", entry_warehouse.getSingle_price());
        json.put("json_count", entry_warehouse.getCount());
        json.put("json_total_price", entry_warehouse.getTotal_price());
        json.put("json_manufacturer", entry_warehouse.getManufacturer());
        json.put("json_entry_date", entry_warehouse.getEntry_date());
        json.put("json_status", entry_warehouse.getStatus());
        json.put("json_warehouse_no", entry_warehouse.getWarehouse_no());
        json.put("json_remarks", entry_warehouse.getRemarks());
        out.print(json);
        //return JSONObject.toJSONString()
    }


    //修改选中的entry_warehouse信息
    @RequestMapping(value = {"/update_entry_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void update_entry_warehouse(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws ParseException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String dlg_txt_flow_number = request.getParameter("dlg_txt_flow_number");
        String dlg_txt_list_no = request.getParameter("dlg_txt_list_no");
        String dlg_txt_goods_no = request.getParameter("dlg_txt_goods_no");
        String dlg_txt_kind = request.getParameter("dlg_txt_kind");
        String dlg_txt_specification = request.getParameter("dlg_txt_specification");
        String dlg_txt_goods_name = request.getParameter("dlg_txt_goods_name");
        String dlg_txt_single_price = request.getParameter("dlg_txt_single_price");
        String dlg_txt_count = request.getParameter("dlg_txt_count");
        double total_price = Integer.parseInt(dlg_txt_count) * Double.valueOf(dlg_txt_single_price);
        String dlg_txt_manufacturer = request.getParameter("dlg_txt_manufacturer");
        String dlg_txt_entry_date = request.getParameter("dlg_txt_entry_date");
        String dlg_txt_status = request.getParameter("dlg_txt_status");
        String dlg_txt_warehouse_no = request.getParameter("dlg_txt_warehouse_no");
        String dlg_txt_remarks = request.getParameter("dlg_txt_remarks");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_entry_date = null;
        List<String> flow_numberList=new ArrayList<String>();
        flow_numberList=entry_out_againService.get_flow_number_by_status("已入库");
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(dlg_txt_entry_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_entry_date=format1.parse(dlg_txt_entry_date);
        }
        else{
            util_entry_date=format1.parse(format1.format(format2.parse(dlg_txt_entry_date)));
        }
        java.sql.Timestamp sql_entry_date = new java.sql.Timestamp(util_entry_date.getTime());
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }

        Entry_Warehouse original_ew=entry_warehouseService.queryEntry_Warehouse_By_flow_number(dlg_txt_flow_number);
        int original_count=original_ew.getCount();
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("entry_warehouse", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            entry_warehouseService.updateEntry_Warehouse(dlg_txt_flow_number, dlg_txt_list_no, dlg_txt_goods_no, dlg_txt_kind, dlg_txt_specification, dlg_txt_goods_name,
                    Double.valueOf(dlg_txt_single_price), Integer.valueOf(dlg_txt_count), total_price, dlg_txt_manufacturer, sql_entry_date, dlg_txt_status,
                    dlg_txt_warehouse_no, dlg_txt_remarks);
            if (dlg_txt_status.equals("已入库")&&(!flow_numberList.contains(dlg_txt_flow_number))) {  //若入库单状态为“已入库”，并且第一次入库（无重复入库）则更新库存统计中的数量
                List<Stock_statistics> stock_statisticsList = new ArrayList<Stock_statistics>();
                stock_statisticsList = stockstatisticsService.get_All_Stock_statistics();
                List<String> stock_statistics_goods_no_List = new ArrayList<String>();
                for (Stock_statistics ss : stock_statisticsList) {
                    stock_statistics_goods_no_List.add(ss.getGoods_no());
                }
                //*如果在库存统计表中有相应的goods_no的物品，则更新其数量*//*
                if (stock_statistics_goods_no_List.contains(dlg_txt_goods_no)) {
                    int newcount = stockstatisticsService.get_by_goods_no(dlg_txt_goods_no).getCount() + Integer.parseInt(dlg_txt_count);
                    stockstatisticsService.renew_goods_no_count(newcount, dlg_txt_goods_no);
                }
                //如果在库存统计表中没有相应的goods_no的物品，则添加进去，并在库存预警表中新增预警信息
                else {
                    String index_no = stockstatisticsService.get_seq_stock_statistics_index_no();
                    Stock_statistics stock_statistics = new Stock_statistics(index_no, dlg_txt_warehouse_no, dlg_txt_list_no,
                            dlg_txt_goods_no, dlg_txt_kind, dlg_txt_specification, dlg_txt_goods_name,
                            Double.valueOf(dlg_txt_single_price), Integer.parseInt(dlg_txt_count), total_price, dlg_txt_manufacturer, null,
                            null, "", dlg_txt_remarks);
                    stockstatisticsService.add_stock_statistics_items(stock_statistics);
                    stock_warningService.add_stock_warning(dlg_txt_warehouse_no, dlg_txt_goods_no, 0, 0);
                }
            }
           /* //根据新增的入库信息，更新库存余额
            List<Stock_statistics> stock_statisticsList = new ArrayList<Stock_statistics>();
            stock_statisticsList = stockstatisticsService.get_All_Stock_statistics();
            List<String> stock_statistics_goods_no_List = new ArrayList<String>();
            for (Stock_statistics ss : stock_statisticsList) {
                stock_statistics_goods_no_List.add(ss.getGoods_no());
            }
            *//*如果在库存余额表中有相应入库单的goods_no的物品，则更新其数量*//*
            if (stock_statistics_goods_no_List.contains(dlg_txt_goods_no)) {
                int newcount = stockstatisticsService.get_by_goods_no(dlg_txt_goods_no).getCount() + Integer.parseInt(dlg_txt_count) - original_count;
                stockstatisticsService.renew_goods_no_count(newcount, dlg_txt_goods_no);
            }*/
            logger.info("用户,修改入库" + dlg_txt_flow_number + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //添加entry_warehouse信息
    @RequestMapping(value = {"/add_entry_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void add_entry_warehouse(HttpServletRequest request, HttpSession session, HttpServletResponse response) throws ParseException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String add_dlg_txt_flow_number = entry_warehouseService.get_seq_entry_warehouse_flow_number();
        String add_dlg_txt_list_no = request.getParameter("add_dlg_txt_list_no");
        String add_dlg_txt_goods_no = request.getParameter("add_dlg_txt_goods_no");
        String add_dlg_txt_kind = request.getParameter("add_dlg_txt_kind");
        String add_dlg_txt_specification = request.getParameter("add_dlg_txt_specification");
        String add_dlg_txt_goods_name = request.getParameter("add_dlg_txt_goods_name");
        String add_dlg_txt_single_price = request.getParameter("add_dlg_txt_single_price");
        String add_dlg_txt_count = request.getParameter("add_dlg_txt_count");
        double total_price = Integer.parseInt(add_dlg_txt_count) * Double.valueOf(add_dlg_txt_single_price);
        String add_dlg_txt_manufacturer = request.getParameter("add_dlg_txt_manufacturer");
        String add_dlg_txt_entry_date = request.getParameter("add_dlg_txt_entry_date");
        String add_dlg_txt_status = request.getParameter("add_dlg_txt_status");
        String add_dlg_txt_warehouse_no = request.getParameter("add_dlg_txt_warehouse_no");
        String add_dlg_txt_remarks = request.getParameter("add_dlg_txt_remarks");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_entry_date = null;
        if(add_dlg_txt_list_no.equals("")){  //如果没有输入单据号，则系统自动生产单据号：入库日期+序列号
            String month=add_dlg_txt_entry_date.substring(0,2);
            String day=add_dlg_txt_entry_date.substring(3,5);
            String year=add_dlg_txt_entry_date.substring(6,10);
            String hour=add_dlg_txt_entry_date.substring(11,13);
            String minute=add_dlg_txt_entry_date.substring(14,16);
            String second=add_dlg_txt_entry_date.substring(17);
            add_dlg_txt_list_no=year+month+day+hour+minute+second+entry_warehouseService.get_seq_entry_warehouse_flow_number();

        }
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(add_dlg_txt_entry_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_entry_date=format1.parse(add_dlg_txt_entry_date);
        }
        else{
            util_entry_date=format1.parse(format1.format(format2.parse(add_dlg_txt_entry_date)));
        }
        java.sql.Timestamp sql_entry_date = new java.sql.Timestamp(util_entry_date.getTime());
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("entry_warehouse", username, "add");
        if (user_power != null) {
            json.put("result", "success");


            Entry_Warehouse entry_warehouse = new Entry_Warehouse(add_dlg_txt_flow_number, add_dlg_txt_list_no, add_dlg_txt_goods_no,
                    add_dlg_txt_kind, add_dlg_txt_specification, add_dlg_txt_goods_name, Double.valueOf(add_dlg_txt_single_price), Integer.parseInt(add_dlg_txt_count), total_price,
                    add_dlg_txt_manufacturer, sql_entry_date, add_dlg_txt_status, add_dlg_txt_warehouse_no, add_dlg_txt_remarks);
            System.out.println(entry_warehouse.toString());
            entry_warehouseService.addEntry_Warehouse(entry_warehouse);
           /* //根据新增的入库信息，更新库存余额
            List<Stock_statistics> stock_statisticsList = new ArrayList<Stock_statistics>();
            stock_statisticsList = stockstatisticsService.get_All_Stock_statistics();
            List<String> stock_statistics_goods_no_List = new ArrayList<String>();
            for (Stock_statistics ss : stock_statisticsList) {
                stock_statistics_goods_no_List.add(ss.getGoods_no());
            }
            *//*如果在库存余额表中有相应的goods_no的物品，则更新其数量*//*
            if (stock_statistics_goods_no_List.contains(add_dlg_txt_goods_no)) {
                int newcount = stockstatisticsService.get_by_goods_no(add_dlg_txt_goods_no).getCount() + Integer.parseInt(add_dlg_txt_count);
                stockstatisticsService.renew_goods_no_count(newcount, add_dlg_txt_goods_no);
            }
            //如果在库存余额表中没有相应的goods_no的物品，则添加进去
            else {
                String index_no = stockstatisticsService.get_seq_stock_statistics_index_no();
                Stock_statistics stock_statistics = new Stock_statistics(index_no, add_dlg_txt_warehouse_no, add_dlg_txt_list_no,
                        add_dlg_txt_goods_no, add_dlg_txt_kind, add_dlg_txt_specification, add_dlg_txt_goods_name,
                        Double.valueOf(add_dlg_txt_single_price), Integer.parseInt(add_dlg_txt_count), total_price, add_dlg_txt_manufacturer, to_entry_date,
                        null, "", add_dlg_txt_remarks);
                stockstatisticsService.add_stock_statistics_items(stock_statistics);
                stock_warningService.add_stock_warning(add_dlg_txt_warehouse_no, add_dlg_txt_goods_no, 0, 0);
            }*/
            logger.info("用户,添加入库" + add_dlg_txt_flow_number + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);

    }


    /***********************************************************************************************************/
    /*出库 out_warehouse*/
    //跳转到出库单界面
    @RequestMapping(value = {"/out_warehouse_list"}, method = RequestMethod.GET)
    public String out_warehouse_list(HttpServletRequest request, HttpSession session) {
        List<Out_Warehouse> out_warehouseList = new ArrayList<Out_Warehouse>();
        String op = request.getParameter("op");
        session.setAttribute("session_out_warehouse_record_No", out_warehouse_record_No);
        List<Warehouse> warehouseList=new ArrayList<Warehouse>();
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        warehouseList=warehouseService.getAllwarehouse();
        session.setAttribute("session_all_warehouse",warehouseList);
        if (op.equals("previous")) {
            if (out_warehouse_start > 25) {
                out_warehouse_start = out_warehouse_start - 25;
                out_warehouse_pageNo--;
            } else {
                out_warehouse_start = 0;
                out_warehouse_pageNo--;
            }
            out_warehouse_record_No = out_warehouse_record_No - 25;
            session.setAttribute("session_out_warehouse_record_No", out_warehouse_record_No);
        } else if (op.equals("next")) {
            out_warehouse_start = out_warehouse_start + 25;
            out_warehouse_pageNo++;
            out_warehouse_record_No = out_warehouse_record_No + 25;
            session.setAttribute("session_out_warehouse_record_No", out_warehouse_record_No);
        }
        out_warehouseList = out_warehouseService.get_All_Out_Warehouse_ByPage(out_warehouse_start, 25);
        if (session.getAttribute("session_all_out_warehouse") == null) {
            session.setAttribute("session_all_out_warehouse", out_warehouseList);
        }
        session.setAttribute("session_out_warehouse_PageCount", out_warehouseService.getRecordCount());
        session.setAttribute("session_out_warehouse_PageNo", out_warehouse_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("out_warehouse", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "out_warehouse_list";
    }

    //删除out_warehouse中选中的数据
    @RequestMapping(value = {"/delete_out_warehouse_by_flow_number"}, method = RequestMethod.POST)
    public void delete_out_warehouse_by_flow_number(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String flow_number="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("out_warehouse", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                //warehouseService.delete_warehouse_by_warehouse_no(check_val[i].toString());
                out_warehouseService.delete_out_warehouse_by_flow_number(check_val[i].toString());
                flow_number = flow_number + "||" + check_val[i].toString();
            }
            logger.info("用户,删除出库" + flow_number + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //根据条件查询出库信息
    @RequestMapping(value = {"/query_out_warehouse"}, method = RequestMethod.POST)
    public String query_out_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ParseException {
        String query_txt_flow_number = request.getParameter("query_txt_flow_number");
        String query_txt_list_no = request.getParameter("query_txt_list_no");
        String query_txt_goods_name = request.getParameter("query_txt_goods_name");
        String query_txt_start_date = request.getParameter("query_txt_start_date");
        String query_txt_end_date = request.getParameter("query_txt_end_date");
        String query_txt_status=request.getParameter("query_txt_status");
        String select_warehouse_no=request.getParameter("select_warehouse_no");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_start_date = null,util_end_date = null;
        java.sql.Timestamp sql_start_date = null;
        java.sql.Timestamp sql_end_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge_1=true,date_format_judge_2=true;
        if((!query_txt_start_date.equals(""))&&(!query_txt_end_date.equals(""))) {
            try {
                Date date = format1.parse(query_txt_start_date);
            } catch (ParseException e) {
                date_format_judge_1 = false;
            }
            if (date_format_judge_1 == true) {
                util_start_date = format1.parse(query_txt_start_date);
            } else {
                util_start_date = format1.parse(format1.format(format2.parse(query_txt_start_date)));
            }

            try {
                Date date = format1.parse(query_txt_end_date);
            } catch (ParseException e) {
                date_format_judge_2 = false;
            }
            if (date_format_judge_2 == true) {
                util_end_date = format1.parse(query_txt_end_date);
            } else {
                util_end_date = format1.parse(format1.format(format2.parse(query_txt_end_date)));
            }
            sql_start_date = new java.sql.Timestamp(util_start_date.getTime());
            sql_end_date = new java.sql.Timestamp(util_end_date.getTime());
        }



       /* System.out.println(query_txt_flow_number);
        System.out.println(query_txt_start_date+" "+query_txt_end_date);*/
        List<Out_Warehouse> out_warehouseList = new ArrayList<Out_Warehouse>();
        if(!select_warehouse_no.equals("")){
            out_warehouseList=out_warehouseService.queryOut_Warehouse_By_warehouse_no(select_warehouse_no);
        }
        if (!query_txt_flow_number.equals("")) {
            out_warehouseList.add(out_warehouseService.queryOut_Warehouse_By_flow_number(query_txt_flow_number));
        }
        if (query_txt_flow_number.equals("")) {
            if ((!query_txt_list_no.equals("")) &&
                    query_txt_goods_name.equals("") &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_list_no(query_txt_list_no);
            } else if ((!query_txt_goods_name.equals("")) &&
                    query_txt_list_no.equals("") &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_goods_name(query_txt_goods_name);
            } else if ((!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    query_txt_list_no.equals("") &&
                    query_txt_goods_name.equals("")) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_out_date(sql_start_date, sql_end_date);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_goods_name.equals("")) &&
                    query_txt_start_date.equals("") &&
                    query_txt_end_date.equals("")) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_list_no_And_goods_name(query_txt_list_no, query_txt_goods_name);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_goods_name.equals("")) &&
                    (!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals(""))) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_list_no_And_goods_name_And_out_date(query_txt_list_no, query_txt_goods_name, sql_start_date, sql_end_date);
            } else if ((!query_txt_list_no.equals("")) &&
                    (!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    (query_txt_list_no.equals(""))) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_list_no_And_out_date(query_txt_list_no, sql_start_date, sql_end_date);
            } else if ((!query_txt_start_date.equals("")) &&
                    (!query_txt_end_date.equals("")) &&
                    query_txt_goods_name.equals("") &&
                    query_txt_list_no.equals("")) {
                out_warehouseList = out_warehouseService.queryOut_Warehouse_By_out_date(sql_start_date, sql_end_date);
            }else if (!query_txt_status.equals("")){
                out_warehouseList=out_warehouseService.queryOut_Warehouse_By_status(query_txt_status);
            }
        }
        //System.out.println(entry_warehouseList.get(0).toString());
        //entry_warehouseList=entry_warehouseService.queryEntry_Warehouse_By(query_txt_flow_number,query_txt_list_no,query_txt_goods_name,query_txt_start_date,query_txt_end_date);
        session.setAttribute("session_all_out_warehouse", out_warehouseList);
        return "redirect:/erp/out_warehouse_list?op=query_out_warehouse";    //此处op=query_out_warehouse传的值没有用，只是为了语法
    }

    //显示选中的出库信息，该方法用来接收前端传递的ajax
    @RequestMapping(value = {"/show_selected_out_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_out_warehouse(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Out_Warehouse out_warehouse = new Out_Warehouse();
        out_warehouse = out_warehouseService.queryOut_Warehouse_By_flow_number(check_val[0].toString());
        System.out.println(out_warehouse.toString());
        json.put("json_flow_number", out_warehouse.getFlow_number());
        json.put("json_list_no", out_warehouse.getList_no());
        json.put("json_goods_no", out_warehouse.getGoods_no());
        json.put("json_kind", out_warehouse.getKind());
        json.put("json_specification", out_warehouse.getSpecification());
        json.put("json_goods_name", out_warehouse.getGoods_name());
        json.put("json_single_price", out_warehouse.getSingle_price());
        json.put("json_count", out_warehouse.getCount());
        json.put("json_total_price", out_warehouse.getTotal_price());
        json.put("json_manufacturer", out_warehouse.getManufacturer());
        json.put("json_out_date", out_warehouse.getOut_date());
        json.put("json_status", out_warehouse.getStatus());
        json.put("json_warehouse_no", out_warehouse.getWarehouse_no());
        json.put("json_remarks", out_warehouse.getRemarks());
        out.print(json);
        //return JSONObject.toJSONString()
    }

    //修改选中的out_warehouse信息
    @RequestMapping(value = {"/update_out_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void update_out_warehouse(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws ParseException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String dlg_txt_flow_number = request.getParameter("dlg_txt_flow_number");
        String dlg_txt_list_no = request.getParameter("dlg_txt_list_no");
        String dlg_txt_goods_no = request.getParameter("dlg_txt_goods_no");
        String dlg_txt_kind = request.getParameter("dlg_txt_kind");
        String dlg_txt_specification = request.getParameter("dlg_txt_specification");
        String dlg_txt_goods_name = request.getParameter("dlg_txt_goods_name");
        String dlg_txt_single_price = request.getParameter("dlg_txt_single_price");
        String dlg_txt_count = request.getParameter("dlg_txt_count");
        double total_price = Integer.parseInt(dlg_txt_count) * Double.valueOf(dlg_txt_single_price);
        String dlg_txt_manufacturer = request.getParameter("dlg_txt_manufacturer");
        String dlg_txt_out_date = request.getParameter("dlg_txt_out_date");
        String dlg_txt_status = request.getParameter("dlg_txt_status");
        String dlg_txt_warehouse_no = request.getParameter("dlg_txt_warehouse_no");
        String dlg_txt_remarks = request.getParameter("dlg_txt_remarks");
        String username="";
        List<String> flow_numberList=new ArrayList<String>();
        flow_numberList=entry_out_againService.get_flow_number_by_status("已出库");
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_out_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(dlg_txt_out_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_out_date=format1.parse(dlg_txt_out_date);
        }
        else{
            util_out_date=format1.parse(format1.format(format2.parse(dlg_txt_out_date)));
        }
        java.sql.Timestamp sql_out_date = new java.sql.Timestamp(util_out_date.getTime());


        if(dlg_txt_status.equals("已出库")&&(!flow_numberList.contains(dlg_txt_flow_number))) {  //判断是否重复出库
            /*判断库存中的数量是否充足，如果充足则可以出库*/
            int newcount = stockstatisticsService.get_by_goods_no(dlg_txt_goods_no).getCount() - Integer.parseInt(dlg_txt_count);
            System.out.println(newcount);
            System.out.println(stockstatisticsService.get_by_goods_no(dlg_txt_goods_no).getCount());
            System.out.println(Integer.parseInt(dlg_txt_count));
            if (newcount < 0) {
                json.put("result", "failure");
                dlg_txt_status="库存不足，无法出库";
            } else {
                stockstatisticsService.renew_goods_no_count(newcount, dlg_txt_goods_no);
                json.put("result", "success");
            }
            out_warehouseService.updateOut_Warehouse(dlg_txt_flow_number, dlg_txt_list_no, dlg_txt_goods_no, dlg_txt_kind, dlg_txt_specification, dlg_txt_goods_name, Integer.parseInt(dlg_txt_single_price),
                    Integer.valueOf(dlg_txt_count), total_price, dlg_txt_manufacturer, sql_out_date, dlg_txt_status, dlg_txt_warehouse_no, dlg_txt_remarks);
            logger.info("用户,修改出库" + dlg_txt_flow_number + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        /*//权限认证
        User_Power user_power = user_powerService.get_by_tup("out_warehouse", username, "update");
        if (user_power != null) {
            json.put("result1", "success");*/
            /*SimpleDateFormat sdfh = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfx = new SimpleDateFormat("MM/dd/yyyy");
            dlg_txt_out_date = sdfh.format(sdfx.parse(dlg_txt_out_date));
            java.sql.Date to_out_date = java.sql.Date.valueOf(dlg_txt_out_date);
            int original_count = out_warehouseService.queryOut_Warehouse_By_flow_number(dlg_txt_flow_number).getCount();
            List<Stock_statistics> stock_statisticsList = new ArrayList<Stock_statistics>();
            stock_statisticsList = stockstatisticsService.get_All_Stock_statistics();
            List<String> stock_statistics_goods_no_list = new ArrayList<String>();

            //如果在库存余额表中有出库单的goods_no，则更新库存余额表中相应物品的数量

            Stock_statistics stock_statistics = new Stock_statistics();
            stock_statistics = stockstatisticsService.get_by_goods_no(dlg_txt_goods_no);
            int newcount = stock_statistics.getCount() - (Integer.parseInt(dlg_txt_count) - original_count);
            if (newcount < 0) {
                System.out.println("newcount=" + newcount);
                json.put("result", "failure");

            } else {
                out_warehouseService.updateOut_Warehouse(dlg_txt_flow_number, dlg_txt_list_no, dlg_txt_goods_no, dlg_txt_kind, dlg_txt_specification, dlg_txt_goods_name, Integer.parseInt(dlg_txt_single_price),
                        Integer.valueOf(dlg_txt_count), total_price, dlg_txt_manufacturer, to_out_date, dlg_txt_status, dlg_txt_warehouse_no, dlg_txt_remarks);
                stockstatisticsService.renew_goods_no_count(newcount, dlg_txt_goods_no);
                json.put("result", "success");
            }*/

        out.print(json);



    }

    //添加out_warehouse信息
    @RequestMapping(value = {"/add_out_warehouse"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void add_out_warehouse(HttpServletRequest request, HttpSession session, HttpServletResponse response) throws ParseException, IOException, JSONException {
        PrintWriter out=response.getWriter(); //把编码设置放到输出流获取之前
        JSONObject json = new JSONObject();
        String add_dlg_txt_flow_number = "cp" + out_warehouseService.get_seq_out_warehouse_flow_number();
        String add_dlg_txt_list_no = request.getParameter("add_dlg_txt_list_no");
        String add_dlg_txt_goods_no = request.getParameter("add_dlg_txt_goods_no");
        String add_dlg_txt_kind = request.getParameter("add_dlg_txt_kind");
        String add_dlg_txt_specification = request.getParameter("add_dlg_txt_specification");
        String add_dlg_txt_goods_name = request.getParameter("add_dlg_txt_goods_name");
        String add_dlg_txt_single_price = request.getParameter("add_dlg_txt_single_price");
        String add_dlg_txt_count = request.getParameter("add_dlg_txt_count");
        double total_price = Integer.parseInt(add_dlg_txt_count) * Double.parseDouble(add_dlg_txt_single_price);
        String add_dlg_txt_manufacturer = request.getParameter("add_dlg_txt_manufacturer");
        String add_dlg_txt_out_date = request.getParameter("add_dlg_txt_out_date");
        String add_dlg_txt_status = request.getParameter("add_dlg_txt_status");
        String add_dlg_txt_warehouse_no = request.getParameter("add_dlg_txt_warehouse_no");
        String add_dlg_txt_remarks = request.getParameter("add_dlg_txt_remarks");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        if(add_dlg_txt_list_no.equals("")){  //如果没有输入单据号，则系统自动生产单据号：入库日期+序列号
            String month=add_dlg_txt_out_date.substring(0,2);
            String day=add_dlg_txt_out_date.substring(3,5);
            String year=add_dlg_txt_out_date.substring(6,10);
            String hour=add_dlg_txt_out_date.substring(11,13);
            String minute=add_dlg_txt_out_date.substring(14,16);
            String second=add_dlg_txt_out_date.substring(17);

            add_dlg_txt_list_no=year+month+day+hour+minute+second+out_warehouseService.get_seq_out_warehouse_flow_number();

        }
        java.util.Date util_out_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(add_dlg_txt_out_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_out_date=format1.parse(add_dlg_txt_out_date);
        }
        else{
            util_out_date=format1.parse(format1.format(format2.parse(add_dlg_txt_out_date)));
        }
        java.sql.Timestamp sql_out_date = new java.sql.Timestamp(util_out_date.getTime());

        Out_Warehouse out_warehouse = new Out_Warehouse(add_dlg_txt_flow_number, add_dlg_txt_list_no, add_dlg_txt_goods_no,
                add_dlg_txt_kind, add_dlg_txt_specification, add_dlg_txt_goods_name, Double.parseDouble(add_dlg_txt_single_price), Integer.parseInt(add_dlg_txt_count), total_price,
                add_dlg_txt_manufacturer, sql_out_date, add_dlg_txt_status, add_dlg_txt_warehouse_no, add_dlg_txt_remarks);
        out_warehouseService.addOut_Warehouse(out_warehouse);
        json.put("result", "success");
        out.print(json);

    }


    /********************************库存统计**************************************************/

    //跳转到库存统计界面
    @RequestMapping(value = {"/stock_statistics"}, method = RequestMethod.GET)
    public String stock_statistics(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        List<Stock_statistics> stock_statisticsList=new ArrayList<Stock_statistics>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_stock_statistics_record_No", stock_statistics_record_No);
        if (op.equals("previous")) {
            if (stock_statistics_start > 25) {
                stock_statistics_start = stock_statistics_start - 25;
                stock_statistics_pageNo--;
            } else {
                stock_statistics_start = 0;
                stock_statistics_pageNo--;
            }
            stock_statistics_record_No = stock_statistics_record_No - 25;
            session.setAttribute("session_stock_statistics_record_No", stock_statistics_record_No);
        } else if (op.equals("next")) {
            stock_statistics_start = stock_statistics_start + 25;
            stock_statistics_pageNo++;
            stock_statistics_record_No = stock_statistics_record_No + 25;
            session.setAttribute("session_stock_statistics_record_No", stock_statistics_record_No);
        }
        stock_statisticsList = stockstatisticsService.get_All_Stock_statistics_ByPage(stock_statistics_start, 25);
        //System.out.println(stock_statisticsList.toString());
        if (session.getAttribute("session_all_stock_statistics") == null) {
            session.setAttribute("session_all_stock_statistics", stock_statisticsList);
        }
        session.setAttribute("session_all_stock_statistics_PageCount", stockstatisticsService.getRecordCount());
        session.setAttribute("session_all_stock_statistics_PageNo", stock_statistics_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_statistics", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "stock_statistics";
    }

    //显示选中的库存统计信息，该方法用来接收前端传递的ajax
    @RequestMapping(value = {"/show_selected_stock_statistics"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_stock_statistics(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Stock_statistics stock_statistics=new Stock_statistics();
        stock_statistics = stockstatisticsService.get_by_index_no(check_val[0].toString());
        //System.out.println(entry_warehouse.toString());
        json.put("json_index_no",stock_statistics.getIndex_no() );
        json.put("json_warehouse_no",stock_statistics.getWarehouse_no() );
        json.put("json_list_no", stock_statistics.getGoods_no());
        json.put("json_goods_no", stock_statistics.getGoods_no());
        json.put("json_kind", stock_statistics.getKind());
        json.put("json_specification", stock_statistics.getSpecification());
        json.put("json_goods_name", stock_statistics.getGoods_name());
        json.put("json_single_price",stock_statistics.getSingle_price());
        json.put("json_count", stock_statistics.getCount());
        json.put("json_total_price", stock_statistics.getTotal_price());
        json.put("json_manufacturer", stock_statistics.getManufacturer());
        json.put("json_entry_date", stock_statistics.getEntry_date());
        json.put("json_out_date", stock_statistics.getOut_date());
        json.put("json_status", stock_statistics.getStatus());
        json.put("json_remarks",stock_statistics.getRemarks());
        out.print(json);
        //return JSONObject.toJSONString()
    }

    //修改选中的stock_statistics信息
    @RequestMapping(value = {"/update_stock_statistics"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void update_stock_statistics(HttpServletRequest request, HttpServletResponse response,HttpSession session ) throws ParseException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String dlg_txt_index_no = request.getParameter("dlg_txt_index_no");
        String dlg_txt_warehouse_no = request.getParameter("dlg_txt_warehouse_no");
        String dlg_txt_goods_no = request.getParameter("dlg_txt_goods_no");
        String dlg_txt_kind = request.getParameter("dlg_txt_kind");
        String dlg_txt_specification = request.getParameter("dlg_txt_specification");
        String dlg_txt_goods_name = request.getParameter("dlg_txt_goods_name");
        String dlg_txt_single_price = request.getParameter("dlg_txt_single_price");
        String dlg_txt_count = request.getParameter("dlg_txt_count");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_statistics", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            double total_price = Integer.parseInt(dlg_txt_count) * Double.valueOf(dlg_txt_single_price);
            String dlg_txt_remarks = request.getParameter("dlg_txt_remarks");
            stockstatisticsService.update_stock_statistics_by_index_no(dlg_txt_index_no, dlg_txt_warehouse_no, dlg_txt_goods_no, dlg_txt_kind, dlg_txt_specification,
                    dlg_txt_goods_name, Double.valueOf(dlg_txt_single_price), Integer.parseInt(dlg_txt_count), total_price, dlg_txt_remarks);
            logger.info("用户,修改库存" + dlg_txt_warehouse_no + "||" + dlg_txt_goods_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //添加库存统计信息
    @RequestMapping(value = {"/add_stock_statistics"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void add_stock_statistics(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=stockstatisticsService.get_seq_stock_statistics_index_no();
        String add_dlg_txt_warehouse_no=request.getParameter("add_dlg_txt_warehouse_no");
        String add_dlg_txt_goods_no=request.getParameter("add_dlg_txt_goods_no");
        String add_dlg_txt_kind=request.getParameter("add_dlg_txt_kind");
        String add_dlg_txt_specification=request.getParameter("add_dlg_txt_specification");
        String add_dlg_txt_goods_name=request.getParameter("add_dlg_txt_goods_name");
        String add_dlg_txt_single_price=request.getParameter("add_dlg_txt_single_price");
        String add_dlg_txt_count=request.getParameter("add_dlg_txt_count");
        String add_dlg_txt_remarks=request.getParameter("add_dlg_txt_remarks");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;

        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_statistics", username, "add");
        if (user_power != null) {
            json.put("result", "success");
            Double total_price = Double.valueOf(add_dlg_txt_single_price) * Integer.parseInt(add_dlg_txt_count);
            Stock_statistics stock_statistics = new Stock_statistics(index_no, add_dlg_txt_warehouse_no, null, add_dlg_txt_goods_no,
                    add_dlg_txt_kind, add_dlg_txt_specification, add_dlg_txt_goods_name, Double.valueOf(add_dlg_txt_single_price),
                    Integer.parseInt(add_dlg_txt_count), total_price, null, null, null, "", add_dlg_txt_remarks);
            stockstatisticsService.add_stock_statistics_items(stock_statistics);
            stock_warningService.add_stock_warning(add_dlg_txt_warehouse_no, add_dlg_txt_goods_no, 0, 0);
            logger.info("用户,添加库存" + add_dlg_txt_warehouse_no + "||" + add_dlg_txt_goods_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //删除库存统计信息
    @RequestMapping(value = {"/delete_stock_statistics"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void delete_stock_statistics(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_statistics", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                stockstatisticsService.delete_by_index_no(check_val[i].toString());
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除库存" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //根据条件查询指定的库存统计信息
    @RequestMapping(value = {"/query_stock_statistics"}, method = RequestMethod.POST)
    public String query_stock_statistics(HttpServletRequest request,HttpServletResponse response,HttpSession session){
        String goods_no=request.getParameter("query_txt_goods_no");
        String goods_name=request.getParameter("query_txt_goods_name");
        String warehouse_no=request.getParameter("query_txt_warehouse_no");
        Stock_statistics stock_statistics=new Stock_statistics();
        List<Stock_statistics> stock_statisticsList=new ArrayList<Stock_statistics>();
        List<Stock_statistics> asa=new ArrayList<Stock_statistics>();
        if(!goods_no.equals("")){
            stock_statistics=stockstatisticsService.get_by_goods_no(goods_no);
            stock_statisticsList.add(stock_statistics);
        }
        else if((goods_no.equals(""))&&(goods_name.equals(""))&&(!warehouse_no.equals(""))){
            stock_statisticsList=stockstatisticsService.get_by_warehouse_no(warehouse_no);
        }
        session.setAttribute("session_all_stock_statistics",stock_statisticsList);
        //System.out.println(stock_statisticsList.size());
        return "redirect:/erp/stock_statistics?op=query";
    }


    /*******************************************************************************************************/
    /*************************************************库存预警**********************************************/
    /*****************************************************************************************************/
    //跳转到库存预警界面
    @RequestMapping(value = {"/stock_warning"}, method = RequestMethod.GET)
    public String stock_warning(HttpSession session){
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_warning", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "stock_warning";
    }
    //根据仓库号查询仓库预警情况
    @RequestMapping(value = {"/show_stock_statistics_warning"}, method = RequestMethod.POST)
    public String show_stock_statistics_warning(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String warehouse_no = request.getParameter("query_txt_warehouse_no");
        List<Stock_warning> stock_warningList=new ArrayList<Stock_warning>();
        stock_warningList=stock_warningService.get_by_warehouse_no(warehouse_no);
        session.setAttribute("session_stock_warning",stock_warningList);
        session.setAttribute("session_goods_no",warehouse_no);
        return "redirect:/erp/stock_warning";
    }

    //根据仓库编号和物品编号删除预警情况delete_stock_statistics_warning
    @RequestMapping(value = {"/add_delete_stock_statistics_warning"}, method = RequestMethod.POST)
    public String delete_stock_statistics_warning(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String op=request.getParameter("btn_submit");
        String warehouse_no = request.getParameter("txt_warehouse_no");
        String goods_no=request.getParameter("txt_goods_no");
        //String goods_name=request.getParameter("txt_goods_name");
        if(op.equals("添加")){
            stock_warningService.add_stock_warning(warehouse_no,goods_no,0,0);
        }
        else if(op.equals("删除")) {
            stock_warningService.delete_by_warehouse_no_goods_no(warehouse_no, goods_no);
        }
        return "redirect:/erp/stock_warning";
    }

    //修改库存预警
    @RequestMapping(value = {"/update_stock_warning"}, method = RequestMethod.POST)
    public void update_stock_warning(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] sw_warehouse_no_array=request.getParameterValues("sw_warehouse_no_array");
        String[] sw_goods_no_array=request.getParameterValues("sw_goods_no_array");
        String[] sw_goods_name_array=request.getParameterValues("sw_goods_name_array");
        String[] sw_max_array=request.getParameterValues("sw_max_array");
        String[] sw_min_array=request.getParameterValues("sw_min_array");
        String sw_warehouse_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("stock_warning", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < sw_warehouse_no_array.length; i++) {
                stock_warningService.update_max_min(Integer.parseInt(sw_max_array[i]), Integer.parseInt(sw_min_array[i]), sw_warehouse_no_array[i], sw_goods_no_array[i]);
            }
            //修改库存统计表中的状态（过少，正常，过多）
            for (int i = 0; i < sw_warehouse_no_array.length; i++) {
                Stock_statistics stock_statistics = new Stock_statistics();
                Stock_warning stock_warning = new Stock_warning();
                stock_statistics = stockstatisticsService.get_by_warehouse_no_goods_no(sw_warehouse_no_array[i], sw_goods_no_array[i]);
                stock_warning = stock_warningService.get_by_warehouse_no_goods_no(sw_warehouse_no_array[i], sw_goods_no_array[i]);
                //如果库存数量大于预警最大值数量或小于预警最小值数量则发出预警（修改status)
               /* System.out.println(sw_warehouse_no_array.length);
                System.out.println(sw_warehouse_no_array[0]);
                System.out.println(sw_goods_no_array[0]);
                System.out.println(sw_warehouse_no_array[1]);
                System.out.println(sw_goods_no_array[1]);
                System.out.println(stock_warning.toString());
                System.out.println(stock_statistics.toString());*/
                if (stock_statistics.getCount() > stock_warning.getMax()) {
                    stockstatisticsService.update_status("过多", sw_warehouse_no_array[i], sw_goods_no_array[i]);
                } else if (stock_statistics.getCount() < stock_warning.getMin()) {
                    stockstatisticsService.update_status("过少", sw_warehouse_no_array[i], sw_goods_no_array[i]);
                } else {
                    stockstatisticsService.update_status("正常", sw_warehouse_no_array[i], sw_goods_no_array[i]);
                }
                sw_warehouse_no = sw_warehouse_no + "||" + sw_warehouse_no_array[i];

            }
            logger.info("用户,修改库存预警" + sw_warehouse_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
        /*String sw_goods_no=request.getParameter("sw_goods_no");
        String sw_goods_name=request.getParameter("sw_goods_name");
        String sw_max=request.getParameter("sw_max");
        String sw_min=request.getParameter("sw_min");*/
        //System.out.println(sw_goods_no+" "+sw_goods_name+" "+sw_max+" "+sw_min);
        //stock_warningService.add_stock_warning(sw_warehouse_no,sw_goods_no,Integer.parseInt(sw_max),Integer.parseInt(sw_min));
        //stock_warningService.update_max_min(Integer.parseInt(sw_max),Integer.parseInt(sw_min),sw_warehouse_no,sw_goods_no);
        //return "redirect:/erp/stock_warning";

    }

    /*******************************************************************************************************/
    /*************************************************系统设置**********************************************/
    /*****************************************************************************************************/
    //日志管理
    @RequestMapping(value = {"/logback"}, method = RequestMethod.GET)
    public String logback(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ParseException {
        File file = new File("D:\\spring\\log\\spring.log");  // Text文件 D:\spring\log\spring.log.txt
        BufferedReader br = new BufferedReader(new FileReader(file));  // 构造一个BufferedReader类来读取文件
        String op = request.getParameter("op");
        String op1 = request.getParameter("op1");
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String s = null;
        if(op.equals("all")) {
            while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
                if (s.contains(",") && s.contains("=") && s.contains(";") && s.contains(".")) {
                    String operation_time = s.substring(0, 19);
                    String operation_mdrs = s.substring(s.indexOf("=") + 1);
                    String operation_module = operation_mdrs.substring(0, operation_mdrs.indexOf(","));
                    String operation_detail = operation_mdrs.substring(operation_mdrs.indexOf(",") + 1, operation_mdrs.indexOf("."));
                    String operator = operation_mdrs.substring(operation_mdrs.indexOf(".") + 1, operation_mdrs.indexOf(";"));
                    String operation_status = operation_mdrs.substring(operation_mdrs.indexOf(";") + 1);
                    java.util.Date operation_time_1 = null;
                    operation_time_1 = format.parse(operation_time);
                    //注意！！！mysql使用datetime类型要与java中Timestamp对应，否则mysql中的时秒分都为00
                    java.sql.Timestamp sql_operation_time = new java.sql.Timestamp(operation_time_1.getTime());
                    Logback logback = new Logback(operation_module, operation_detail, operator, operation_status, sql_operation_time);
                    logbackService.add_daily_record(logback);
                }
            }
            br.close();
            List<Logback> logback_list = new ArrayList<Logback>();
            session.setAttribute("session_logback_record_No", logback_record_No);
            if (op1.equals("previous")) {
                if (logback_start > 25) {
                    logback_start = logback_start - 25;
                    logback_pageNo--;
                } else {
                    logback_start = 0;
                    logback_pageNo--;
                }
                logback_record_No = logback_record_No - 25;
                session.setAttribute("session_logback_record_No", logback_record_No);
            } else if (op1.equals("next")) {
                logback_start = logback_start + 25;
                logback_pageNo++;
                logback_record_No = logback_record_No + 25;
                session.setAttribute("session_logback_record_No", logback_record_No);
            }

            logback_list = logbackService.get_all_daily_record_by_page(logback_start, 25);
            if (session.getAttribute("session_logback") == null) {
                session.setAttribute("session_logback", logback_list);
            }
            session.setAttribute("session_logback_PageCount", logbackService.getRecordCount());
            session.setAttribute("session_logback_PageNo", logback_pageNo);
        }
        if(op.equals("query")){   //注！！！此处查询分页有bug，只能在第一页显示正确的查询内容
            String operation_module=request.getParameter("query_txt_operation_module");
            String operator=request.getParameter("query_txt_operator");
            String start_time=request.getParameter("query_txt_start_time");
            String end_time=request.getParameter("query_txt_end_time");
            String operation_status=request.getParameter("query_operation_status");
            List<Logback> logback_list = new ArrayList<Logback>();
            session.setAttribute("session_logback_record_No", logback_record_No);
            //session.setAttribute("session_op","query");
            if (op1.equals("previous")) {
                if (query_logback_start > 25) {
                    query_logback_start = query_logback_start - 25;
                    query_logback_pageNo--;
                } else {
                    query_logback_start = 0;
                    query_logback_pageNo--;
                }
                query_logback_record_No = query_logback_record_No - 25;
                session.setAttribute("session_logback_record_No", query_logback_record_No);
            } else if (op1.equals("next")) {
                query_logback_start = query_logback_start + 25;
                query_logback_pageNo++;
                query_logback_record_No = query_logback_record_No + 25;
                session.setAttribute("session_logback_record_No", query_logback_record_No);
            }
            if((!operation_module.equals(""))&&(operator.equals(""))&&(start_time.equals(""))&&(end_time.equals(""))&&(operation_status.equals(""))) {
                //logback_list = logbackService.get_all_daily_record_by_page(query_logback_start, 25);
                logback_list=logbackService.get_by_operation_module(operation_module,query_logback_start,25);
                session.setAttribute("session_logback_PageCount", logbackService.getRecordCount_operation_module(operation_module));
            }
            else if((operation_module.equals(""))&&(!operator.equals(""))&&(start_time.equals(""))&&(end_time.equals(""))&&(operation_status.equals(""))){
                logback_list=logbackService.get_by_operator(operator,query_logback_start,25);
                session.setAttribute("session_logback_PageCount", logbackService.getRecordCount_operator(operator));
            }
            else if((operation_module.equals(""))&&(operator.equals(""))&&(!start_time.equals(""))&&(!end_time.equals(""))&&(operation_status.equals(""))){
                java.util.Date operation_start_time = null,operation_end_time=null;
                operation_start_time = format.parse(format.format(format1.parse(start_time)));
                operation_end_time = format.parse(format.format(format1.parse(end_time)));
                //注意！！！mysql使用datetime类型要与java中Timestamp对应，否则mysql中的时秒分都为00
                java.sql.Timestamp sql_operation_start_time = new java.sql.Timestamp(operation_start_time.getTime());
                java.sql.Timestamp sql_operation_end_time = new java.sql.Timestamp(operation_end_time.getTime());
                logback_list=logbackService.get_by_operation_time(sql_operation_start_time,sql_operation_end_time,query_logback_start,25);
                session.setAttribute("session_logback_PageCount", logbackService.getRecordCount_operation_time(sql_operation_start_time,sql_operation_end_time));
            }
            else if((operation_module.equals(""))&&(operator.equals(""))&&(start_time.equals(""))&&(end_time.equals(""))&&(!operation_status.equals(""))){
                logback_list=logbackService.get_by_operation_status(operation_status,query_logback_start,25);
                session.setAttribute("session_logback_PageCount", logbackService.getRecordCount_operation_status(operation_status));
            }
            else if((!operation_module.equals(""))&&(!operator.equals(""))&&(!start_time.equals(""))&&(!end_time.equals(""))&&(!operation_status.equals(""))){
                java.util.Date operation_start_time = null,operation_end_time=null;

                operation_start_time = format.parse(format.format(format1.parse(start_time)));
                operation_end_time = format.parse(format.format(format1.parse(end_time)));

                //注意！！！mysql使用datetime类型要与java中Timestamp对应，否则mysql中的时秒分都为00
                java.sql.Timestamp sql_operation_start_time = new java.sql.Timestamp(operation_start_time.getTime());
                java.sql.Timestamp sql_operation_end_time = new java.sql.Timestamp(operation_end_time.getTime());
                logback_list=logbackService.get_by_operation_module_time_operator_status(operation_module,sql_operation_start_time,sql_operation_end_time,operator,operation_status,query_logback_start,25);
                session.setAttribute("session_logback_PageCount", logbackService.getRecordCount_operation_module_time_operator_status(operation_module,sql_operation_start_time,sql_operation_end_time,operator,operation_status));
            }

            if (session.getAttribute("session_logback") == null) {
                session.setAttribute("session_logback", logback_list);
            }
            session.setAttribute("session_logback_PageNo", query_logback_pageNo);
        }
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("logback", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "logback";
    }

    //清空所有日志
    @RequestMapping(value = {"/delete_all_logback"}, method = RequestMethod.POST)
    public void delete_all_logback(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String username=request.getParameter("username");
        if(username.equals("superAdmin")) {
            File file = new File("D:\\spring\\log\\spring.log");  //清空日志文件
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write("");
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            logbackService.delete_all();  //清空日志数据库
            json.put("result", "success");
        }
        else {
            json.put("result", "failure");
        }
        out.print(json);
    }

    //用户管理页面
    @RequestMapping(value = {"/user_mamage"}, method = RequestMethod.GET)
    public String user_mamage(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        List<User> userList = new ArrayList<User>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_user_record_No", user_record_No);
        if (op.equals("previous")) {
            if (user_start > 25) {
                user_start = user_start - 25;
                user_pageNo--;
            } else {
                user_start = 0;
                user_pageNo--;
            }
            user_record_No = user_record_No - 25;
            session.setAttribute("session_user_record_No", user_record_No);
        } else if (op.equals("next")) {
            user_start = user_start + 25;
            user_pageNo++;
            user_record_No = user_record_No + 25;
            session.setAttribute("session_user_record_No", user_record_No);
        }
        userList = userService.getAllUsers_bypage(user_start, 25);
        if (session.getAttribute("session_allusers") == null) {
            session.setAttribute("session_allusers", userList);
        }

        session.setAttribute("session_user_PageCount", userService.getCount());
        session.setAttribute("session_user_PageNo", user_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("user", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "user_manage";
    }

    //搜索用户
    @RequestMapping(value = {"/query_user"}, method = RequestMethod.POST)
    public String query_user(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        String name=request.getParameter("query_txt_name");
        String dept=request.getParameter("query_txt_dept");
        List<User> userList=new ArrayList<User>();
        if((!name.equals(""))&&dept.equals("")){
            userList=userService.get_by_name(name);
        }
        else if (name.equals("")&&(!dept.equals(""))){
            userList=userService.get_by_dept(dept);
        }
        else if ((!name.equals(""))&&(!dept.equals(""))){
            userList=userService.get_by_name_dept(name,dept);
        }
        session.setAttribute("session_allusers",userList);
        return "redirect:/erp/user_mamage?op=query";
    }

    //删除user中选中的数据
    @RequestMapping(value = {"/delete_user_by_username"}, method = RequestMethod.POST)
    public void delete_user_by_username(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String delete_username="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("user", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                userService.delete_by_username(check_val[i].toString());
                delete_username = delete_username + "||" + check_val[i].toString();
            }

            logger.info("管理员,删除用户" + delete_username + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //显示选中的用户信息
    @RequestMapping(value = {"/show_selected_user"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_user(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        User user=new User();
        user=userService.get_by_username(check_val[0].toString());
        json.put("json_username", user.getUsername());
        json.put("json_password", user.getPassword());
        json.put("json_name", user.getName());
        json.put("json_sex", user.getSex());
        json.put("json_tel", user.getTel());
        json.put("json_dept", user.getDept());
        json.put("json_position", user.getPosition());
        json.put("json_power", user.getPower());
        out.print(json);
    }

    //修改用户信息
    @RequestMapping(value = {"/update_user"}, method = RequestMethod.POST)
    //ajax会返回response请求，因此想要执行回调函数，需要声明response请求
    public void update_user(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String txt_username=request.getParameter("dlg_txt_username");
        String password=request.getParameter("dlg_txt_password");
        String name=request.getParameter("dlg_txt_name");
        String sex=request.getParameter("dlg_txt_sex");
        String tel=request.getParameter("dlg_txt_tel");
        String dept=request.getParameter("dlg_txt_dept");
        String position=request.getParameter("dlg_txt_position");
        String power=request.getParameter("dlg_txt_power");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("user", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            userService.update_user(password, name, sex, tel, dept, position, power, txt_username);
            logger.info("用户,修改用户" + txt_username + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //添加用户信息
    @RequestMapping(value = {"/add_user"}, method = RequestMethod.POST)
    public void add_user(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String add_dlg_txt_username = request.getParameter("add_dlg_txt_username");
        String password = request.getParameter("add_dlg_txt_password");
        String name = request.getParameter("add_dlg_txt_name");
        String sex = request.getParameter("add_dlg_txt_sex");
        String tel = request.getParameter("add_dlg_txt_tel");
        String dept = request.getParameter("add_dlg_txt_dept");
        String position = request.getParameter("add_dlg_txt_position");
        String power = request.getParameter("add_dlg_txt_power");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("user", username, "add");
        if (user_power != null) {
            json.put("result", "success");
            User user = new User(add_dlg_txt_username, password, name, sex, tel, dept, position, power);
            userService.add_user(user);
            logger.info("用户,添加用户" + add_dlg_txt_username + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //显示选中用户的权限
    @RequestMapping(value = {"/show_selected_user_power"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
    public void show_selected_user_power(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String username=check_val[0].toString();
        List<User_Power> user_powerList_warehouse=new ArrayList<User_Power>();
        List<User_Power> user_powerList_entry_warehouse=new ArrayList<User_Power>();
        List<User_Power> user_powerList_out_warehouse=new ArrayList<User_Power>();
        List<User_Power> user_powerList_stock_statistics=new ArrayList<User_Power>();
        List<User_Power> user_powerList_stock_warning=new ArrayList<User_Power>();
        List<User_Power> user_powerList_logback=new ArrayList<User_Power>();
        List<User_Power> user_powerList_user=new ArrayList<User_Power>();

        List<User_Power> user_powerList_income=new ArrayList<User_Power>();
        List<User_Power> user_powerList_expenditure=new ArrayList<User_Power>();
        List<User_Power> user_powerList_sale_bill=new ArrayList<User_Power>();
        List<User_Power> user_powerList_sale_statistics=new ArrayList<User_Power>();
        List<User_Power> user_powerList_user_power=new ArrayList<User_Power>();

        List<User_Power> user_powerList_budget_apply=new ArrayList<User_Power>();
        List<User_Power> user_powerList_common_file=new ArrayList<User_Power>();
        List<User_Power> user_powerList_purchase_apply=new ArrayList<User_Power>();

        user_powerList_warehouse=user_powerService.get_by_username_tablename(username,"warehouse");
        user_powerList_entry_warehouse=user_powerService.get_by_username_tablename(username,"entry_warehouse");
        user_powerList_out_warehouse=user_powerService.get_by_username_tablename(username,"out_warehouse");
        user_powerList_stock_statistics=user_powerService.get_by_username_tablename(username,"stock_statistics");
        user_powerList_stock_warning=user_powerService.get_by_username_tablename(username,"stock_warning");
        user_powerList_logback=user_powerService.get_by_username_tablename(username,"logback");
        user_powerList_user=user_powerService.get_by_username_tablename(username,"user");

        user_powerList_income=user_powerService.get_by_username_tablename(username,"income");
        user_powerList_expenditure=user_powerService.get_by_username_tablename(username,"expenditure");
        user_powerList_sale_bill=user_powerService.get_by_username_tablename(username,"sale_bill");
        user_powerList_sale_statistics=user_powerService.get_by_username_tablename(username,"sale_statistics");
        user_powerList_user_power=user_powerService.get_by_username_tablename(username,"user_power");

        user_powerList_budget_apply=user_powerService.get_by_username_tablename(username,"budget_apply");
        user_powerList_common_file=user_powerService.get_by_username_tablename(username,"common_file");
        user_powerList_purchase_apply=user_powerService.get_by_username_tablename(username,"purchase_apply");


        String warehouse_power="",entry_warehouse_power="",out_warehouse_power="",stock_statistics_power="",
                stock_warning_power="",logback_power="",alluser_power="",income_power="",expenditure_power="",
                sale_bill_power="",sale_statistics_power="",user_power_power="",budget_apply_power="",common_file_power="",
                purchase_apply_power="";
        for(User_Power user_power:user_powerList_warehouse){
            warehouse_power=warehouse_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_entry_warehouse){
            entry_warehouse_power=entry_warehouse_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_out_warehouse){
            out_warehouse_power=out_warehouse_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_stock_statistics){
            stock_statistics_power=stock_statistics_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_stock_warning){
            stock_warning_power=stock_warning_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_logback){
            logback_power=logback_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_user){
            alluser_power=alluser_power+user_power.getPower()+"||";
        }

        for(User_Power user_power:user_powerList_income){
            income_power=income_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_expenditure){
            expenditure_power=expenditure_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_sale_bill){
            sale_bill_power=sale_bill_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_sale_statistics){
            sale_statistics_power=sale_statistics_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_user_power){
            user_power_power=user_power_power+user_power.getPower()+"||";
        }

        for(User_Power user_power:user_powerList_budget_apply){
            budget_apply_power=budget_apply_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_common_file){
            common_file_power=common_file_power+user_power.getPower()+"||";
        }
        for(User_Power user_power:user_powerList_purchase_apply){
            purchase_apply_power=purchase_apply_power+user_power.getPower()+"||";
        }

        json.put("json_power_username", check_val[0].toString());
        json.put("json_power_table_warehouse", warehouse_power);
        json.put("json_power_entry_warehouse", entry_warehouse_power);
        json.put("json_power_out_warehouse", out_warehouse_power);
        json.put("json_power_stock_statistics", stock_statistics_power);
        json.put("json_power_stock_warning", stock_warning_power);
        json.put("json_power_logback", logback_power);
        json.put("json_power_user", alluser_power);

        json.put("json_power_income", income_power);
        json.put("json_power_expenditure", expenditure_power);
        json.put("json_power_sale_bill", sale_bill_power);
        json.put("json_power_sale_statistics", sale_statistics_power);
        json.put("json_power_user_power", user_power_power);

        json.put("json_power_budget_apply", budget_apply_power);
        json.put("json_power_common_file", common_file_power);
        json.put("json_power_purchase_apply", purchase_apply_power);

        out.print(json);
    }


    /**********权限管理*********/
    @RequestMapping(value = {"/power_manage"}, produces = "text/plain;charset=UTF-8", method = RequestMethod.GET)
    public String power_manage(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        List<User_Power> user_powerList = new ArrayList<User_Power>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_user_power_record_No", user_power_record_No);
        if (op.equals("previous")) {
            if (user_power_start > 25) {
                user_power_start = user_power_start - 25;
                user_power_pageNo--;
            } else {
                user_power_start = 0;
                user_power_pageNo--;
            }
            user_power_record_No = user_power_record_No - 25;
            session.setAttribute("session_user_power_record_No", user_power_record_No);
        } else if (op.equals("next")) {
            user_power_start = user_power_start + 25;
            user_power_pageNo++;
            user_power_record_No = user_power_record_No + 25;
            session.setAttribute("session_user_power_record_No", user_power_record_No);
        }
        user_powerList = user_powerService.get_all_by_page(user_power_start, 25);
        if (session.getAttribute("session_user_power") == null) {
            session.setAttribute("session_user_power", user_powerList);
        }
        session.setAttribute("session_user_power_PageCount",user_powerService.getRecordCount());
        session.setAttribute("session_user_power_PageNo", user_power_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("user_power", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "power_manage";

    }


    //搜索用户
    @RequestMapping(value = {"/query_user_power"}, method = RequestMethod.POST)
    public String query_user_power(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        String tablename=request.getParameter("query_txt_tablename");
        String username=request.getParameter("query_txt_username");
        List<User_Power> user_powerList=new ArrayList<User_Power>();
        if((!tablename.equals(""))&&(username.equals(""))){
            user_powerList=user_powerService.get_by_tablename(tablename);
        }
        else if(tablename.equals("")&&(!username.equals(""))){
            user_powerList=user_powerService.get_by_username(username);
        }
        else if((!tablename.equals(""))&&(!username.equals(""))){
            user_powerList=user_powerService.get_by_username_tablename(username,tablename);
        }
        session.setAttribute("session_user_power",user_powerList);
        return "redirect:/erp/power_manage?op=query";
    }

    //删除user_power中选中的数据
    @RequestMapping(value = {"/delete_user_power_by_index_no"}, method = RequestMethod.POST)
    public void delete_user_power_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power_check = user_powerService.get_by_tup("user_power", username, "delete");
        if (user_power_check != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                User_Power user_power = user_powerService.get_by_index_no(check_val[i].toString());
                user_powerService.delete_by_index_no(check_val[i].toString());
                logger.info("管理员,删除用户权限"+user_power.getUsername()+"||"+user_power.getTablename()+"||"+user_power.getPower()+":"+username+"."+username+";"+"成功");  //写入日志
            }

        }
        out.print(json);
    }

    //添加用户权限
    @RequestMapping(value = {"/add_user_power"}, method = RequestMethod.POST)
    public void add_user_power(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=user_powerService.get_seq_user_power();
        String add_dlg_txt_tablename=request.getParameter("add_dlg_txt_tablename");
        String add_dlg_txt_username=request.getParameter("add_dlg_txt_username");
        String add_dlg_txt_power=request.getParameter("add_dlg_txt_power");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power_check = user_powerService.get_by_tup("user_power", username, "add");
        if (user_power_check != null) {
            json.put("result", "success");
            User_Power user_power = new User_Power(index_no, add_dlg_txt_tablename, add_dlg_txt_username, add_dlg_txt_power);
            user_powerService.add_user_power(user_power);
            logger.info("管理员,修改用户权限" + add_dlg_txt_username + "||" + add_dlg_txt_tablename + "||" + add_dlg_txt_power + ":" + username + "." + username + ";" + "成功");    //写入日志
        }
        out.print(json);
    }


    /**********************************采购管理**************************************************/

    //跳转到采购申请界面
    @RequestMapping(value = {"/my_purchase_apply"}, method = RequestMethod.GET)
    public String my_purchase_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        List<Purchase_apply> purchase_applyList = new ArrayList<Purchase_apply>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_purchase_apply_record_No", purchase_apply_record_No);
        if (op.equals("previous")) {
            if (purchase_apply_start > 25) {
                purchase_apply_start = purchase_apply_start - 25;
                purchase_apply_pageNo--;
            } else {
                purchase_apply_start = 0;
                purchase_apply_pageNo--;
            }
            purchase_apply_record_No = purchase_apply_record_No - 25;
            session.setAttribute("session_purchase_apply_record_No", purchase_apply_record_No);
        } else if (op.equals("next")) {
            purchase_apply_start = purchase_apply_start + 25;
            purchase_apply_pageNo++;
            purchase_apply_record_No = purchase_apply_record_No + 25;
            session.setAttribute("session_purchase_apply_record_No", purchase_apply_record_No);
        }
        purchase_applyList = purchase_applyService.get_Purchase_apply_Byusername_ByPage(username,purchase_apply_start, 25);
        if (session.getAttribute("session_purchase_apply_my") == null) {
            session.setAttribute("session_purchase_apply_my", purchase_applyList);
        }
        session.setAttribute("session_purchase_apply_PageCount",purchase_applyService.getRecordCountByUsername(username));
        session.setAttribute("session_purchase_apply_PageNo", purchase_apply_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("purchase_apply", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "purchase_apply";
    }


    //添加用户自己的采购申请
    @RequestMapping(value = {"/add_my_purchase_apply"}, method = RequestMethod.POST)
    public void add_my_purchase_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ParseException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=purchase_applyService.get_seq_purchase_apply_index_no();
        String apply_dlg_txt_username=request.getParameter("apply_dlg_txt_username");
        String apply_dlg_txt_name=request.getParameter("apply_dlg_txt_name");
        String apply_dlg_txt_dept=request.getParameter("apply_dlg_txt_dept");
        String apply_dlg_txt_kind=request.getParameter("apply_dlg_txt_kind");
        String apply_dlg_txt_goods_name=request.getParameter("apply_dlg_txt_goods_name");
        String apply_dlg_txt_count=request.getParameter("apply_dlg_txt_count");
        String apply_dlg_txt_date=request.getParameter("apply_dlg_txt_date");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        if((!apply_dlg_txt_username.equals(""))&&(!apply_dlg_txt_goods_name.equals(""))&&(!apply_dlg_txt_count.equals(""))
                &&(!apply_dlg_txt_date.equals(""))) {
            SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
            SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            java.util.Date apply_date = null;
            apply_date=format.parse(format.format(format1.parse(apply_dlg_txt_date)));
            java.sql.Timestamp apply_date_sql=new java.sql.Timestamp(apply_date.getTime());
            Purchase_apply purchase_apply = new Purchase_apply(index_no, apply_dlg_txt_username, apply_dlg_txt_name, apply_dlg_txt_dept, apply_dlg_txt_kind,
                    apply_dlg_txt_goods_name, Integer.parseInt(apply_dlg_txt_count), apply_date_sql, "未审核","",0);
            purchase_applyService.add_purchase_apply(purchase_apply);
            logger.info("用户,采购申请" + apply_dlg_txt_goods_name + ":" + username + "." + username + ";" + "成功");   //写入日志
            json.put("result", "success");
        }
        else {
            json.put("result", "failure");
        }
        out.print(json);
    }

    //删除用户自己的采购申请
    @RequestMapping(value = {"/delete_my_purchase_apply"}, method = RequestMethod.POST)
    public void delete_my_purchase_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        for(int i=0;i<check_val.length;i++){
            purchase_applyService.delete_by_index_no(check_val[i].toString());
            logger.info("用户,撤销采购申请" + check_val[0].toString() + ":" + username + "." + username + ";" + "成功");   //写入日志
        }
        json.put("result", "success");
        out.print(json);
    }

    //跳转到采购申请审核界面
    @RequestMapping(value = {"/purchase_apply_check"}, method = RequestMethod.GET)
    public String purchase_apply_check(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        List<Purchase_apply> purchase_applyList = new ArrayList<Purchase_apply>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_all_purchase_apply_record_No", all_purchase_apply_record_No);
        if (op.equals("previous")) {
            if (all_purchase_apply_start > 25) {
                all_purchase_apply_start = all_purchase_apply_start - 25;
                all_purchase_apply_pageNo--;
            } else {
                all_purchase_apply_start = 0;
                all_purchase_apply_pageNo--;
            }
            all_purchase_apply_record_No = all_purchase_apply_record_No - 25;
            session.setAttribute("session_all_purchase_apply_record_No", all_purchase_apply_record_No);
        } else if (op.equals("next")) {
            all_purchase_apply_start = all_purchase_apply_start + 25;
            all_purchase_apply_pageNo++;
            all_purchase_apply_record_No = all_purchase_apply_record_No + 25;
            session.setAttribute("session_all_purchase_apply_record_No", all_purchase_apply_record_No);
        }
        purchase_applyList = purchase_applyService.get_all_Purchase_apply_ByPage(all_purchase_apply_start, 25);
        if (session.getAttribute("session_all_purchase_apply") == null) {
            session.setAttribute("session_all_purchase_apply", purchase_applyList);
        }
        session.setAttribute("session_all_purchase_apply_PageCount",purchase_applyService.getRecordCount());
        session.setAttribute("session_all_purchase_apply_PageNo", all_purchase_apply_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("purchase_apply", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "purchase_apply_check";
    }

    //根据用户名和状态查询采购申请
    @RequestMapping(value = {"/query_purchase_apply"}, method = RequestMethod.POST)
    public String query_purchase_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String query_txt_username = request.getParameter("query_txt_username");
        String query_txt_status=request.getParameter("query_txt_status");
        List<Purchase_apply> purchase_applyList=new ArrayList<Purchase_apply>();
        if(query_txt_status.equals("全部")){
            return "redirect:/erp/purchase_apply_check?op=all";
        }
        if((!query_txt_username.equals(""))&&(query_txt_status.equals(""))){
            purchase_applyList=purchase_applyService.get_Purchase_apply_Byusername_ByPage(query_txt_username,0,10000);
        }
        else if ((query_txt_username.equals(""))&&(!query_txt_status.equals(""))){
            purchase_applyList=purchase_applyService.get_Purchase_apply_Bystatus(query_txt_status);
        }
        else if((!query_txt_username.equals(""))&&(!query_txt_status.equals(""))){
            purchase_applyList=purchase_applyService.get_Purchase_apply_By_username_status(query_txt_username,query_txt_status);
        }
        session.setAttribute("session_all_purchase_apply",purchase_applyList);
        return "redirect:/erp/purchase_apply_check?op=query";
    }

    //修改审核状态
    @RequestMapping(value = {"/update_purchase_apply_status"}, method = RequestMethod.POST)
    public String update_purchase_apply_status(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String index_no=request.getParameter("txt_index_no");
        String status=request.getParameter("select_status");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        purchase_applyService.update_purchase_apply(index_no,status);
        logger.info("管理员,审核采购申请" + index_no + ":" + username + "." + username + ";" + "成功");   //写入日志
        return "redirect:/erp/purchase_apply_check?op=all";
    }

    //跳转到采购清单界面（显示所有已审核通过的采购申请）
    @RequestMapping(value = {"/purchase_list"}, method = RequestMethod.GET)
    public String purchase_list(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        List<Purchase_apply> purchase_applyList = new ArrayList<Purchase_apply>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_purchase_apply_pass_record_No", purchase_apply_pass_record_No);
        if (op.equals("previous")) {
            if (purchase_apply_pass_start > 25) {
                purchase_apply_pass_start = purchase_apply_pass_start - 25;
                purchase_apply_pass_pageNo--;
            } else {
                purchase_apply_pass_start = 0;
                purchase_apply_pass_pageNo--;
            }
            purchase_apply_pass_record_No = purchase_apply_pass_record_No - 25;
            session.setAttribute("session_purchase_apply_pass_record_No", purchase_apply_pass_record_No);
        } else if (op.equals("next")) {
            purchase_apply_pass_start = purchase_apply_pass_start + 25;
            purchase_apply_pass_pageNo++;
            purchase_apply_pass_record_No = purchase_apply_pass_record_No + 25;
            session.setAttribute("session_purchase_apply_pass_record_No", purchase_apply_pass_record_No);
        }
        purchase_applyList = purchase_applyService.get_Purchase_apply_passed_ByPage("通过","已确认","已购买","已到货","已入库",purchase_apply_pass_start, 25);
        if (session.getAttribute("session_purchase_apply_pass") == null) {
            session.setAttribute("session_purchase_apply_pass", purchase_applyList);
        }
        session.setAttribute("session_purchase_apply_pass_PageCount",purchase_applyService.getRecordCount_passed("通过","已确认","已购买","已到货","已入库"));
        session.setAttribute("session_purchase_apply_pass_PageNo", purchase_apply_pass_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("purchase_apply", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "purchase_list";
    }

    //根据用户名或日期查询采购清单
    @RequestMapping(value = {"/query_purchase_list"}, method = RequestMethod.POST)
    public String query_purchase_list(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ParseException {
        String query_txt_username=request.getParameter("query_txt_username");
        String query_txt_start_date=request.getParameter("query_txt_start_date");
        String query_txt_end_date=request.getParameter("query_txt_end_date");
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        List<Purchase_apply> purchase_applyList=new ArrayList<Purchase_apply>();
        if(!query_txt_username.equals("")){
            purchase_applyList=purchase_applyService.get_Purchase_apply_passed_by_username(query_txt_username);
        }
        if ((query_txt_username.equals("")&&(!query_txt_start_date.equals("")) && (!query_txt_end_date.equals("")))) {
            java.util.Date start_time = null,end_time=null;
            start_time = format.parse(format.format(format1.parse(query_txt_start_date)));
            end_time = format.parse(format.format(format1.parse(query_txt_end_date)));
            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(start_time.getTime());
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(end_time.getTime());
            purchase_applyList=purchase_applyService.get_Purchase_apply_passed_by_date(sql_start_time,sql_end_time);
        }
        if (session.getAttribute("session_purchase_apply_pass") == null) {
            session.setAttribute("session_purchase_apply_pass", purchase_applyList);
        }
        return "redirect:/erp/purchase_list?op=query";
    }

    //确认要采购的物品（填写供应商，预算总金额）
    @RequestMapping(value = {"/purchase_list_confirm"}, method = RequestMethod.POST)
    public void purchase_list_confirm(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Purchase_apply purchase_apply=new Purchase_apply();
        System.out.println(check_val[0].toString());
        //entry_warehouse = entry_warehouseService.queryEntry_Warehouse_By_flow_number(check_val[0].toString());
        purchase_apply=purchase_applyService.get_by_index_no(check_val[0].toString());
        json.put("json_index_no", purchase_apply.getIndex_no());
        json.put("json_username", purchase_apply.getUsername());
        json.put("json_name", purchase_apply.getName());
        json.put("json_dept", purchase_apply.getDept());
        json.put("json_kind", purchase_apply.getKind());
        json.put("json_goods_name", purchase_apply.getGoods_name());
        json.put("json_count", purchase_apply.getCount());
        json.put("json_apply_time", purchase_apply.getApply_time());
        json.put("json_status", purchase_apply.getStatus());
        json.put("json_manufacturer", purchase_apply.getManufacturer());
        json.put("json_total_price", purchase_apply.getTotal_price());
        out.print(json);
    }

    //提交确认的采购物品信息
    @RequestMapping(value = {"/purchase_list_confirm_submit"}, method = RequestMethod.POST)
    public void purchase_list_confirm_submit(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String apply_dlg_txt_index_no=request.getParameter("apply_dlg_txt_index_no");
        String apply_dlg_txt_username=request.getParameter("apply_dlg_txt_username");
        String apply_dlg_txt_name=request.getParameter("apply_dlg_txt_name");
        String apply_dlg_txt_dept=request.getParameter("apply_dlg_txt_dept");
        String apply_dlg_txt_kind=request.getParameter("apply_dlg_txt_kind");
        String apply_dlg_txt_goods_name=request.getParameter("apply_dlg_txt_goods_name");
        String apply_dlg_txt_count=request.getParameter("apply_dlg_txt_count");
        String apply_dlg_txt_date=request.getParameter("apply_dlg_txt_date");
        String apply_dlg_txt_status=request.getParameter("apply_dlg_txt_status");
        String apply_dlg_txt_manufacturer=request.getParameter("apply_dlg_txt_manufacturer");
        String apply_dlg_txt_total_price=request.getParameter("apply_dlg_txt_total_price");
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        //SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date apply_time = null;
        apply_time = format.parse(apply_dlg_txt_date);
        java.sql.Timestamp sql_apply_time = new java.sql.Timestamp(apply_time.getTime());
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        if(apply_dlg_txt_total_price.equals("")||apply_dlg_txt_count.equals("")){
            json.put("result", "failure");
        }
        else if((!apply_dlg_txt_total_price.equals(""))&&(!apply_dlg_txt_count.equals(""))) {
            Purchase_apply purchase_apply = new Purchase_apply(apply_dlg_txt_index_no,apply_dlg_txt_username,apply_dlg_txt_name,apply_dlg_txt_dept,apply_dlg_txt_kind,
                    apply_dlg_txt_goods_name,Integer.parseInt(apply_dlg_txt_count),sql_apply_time,apply_dlg_txt_status,apply_dlg_txt_manufacturer,
                    Double.valueOf(apply_dlg_txt_total_price));
            purchase_applyService.confirm_purchase_apply(apply_dlg_txt_status,apply_dlg_txt_manufacturer,Double.valueOf(apply_dlg_txt_total_price),apply_dlg_txt_index_no);
            json.put("result", "success");
            logger.info("用户,采购单确认" + apply_dlg_txt_index_no + ":" + username + "." + username + ";" + "成功");   //写入日志
        }
        out.print(json);
    }



    //跳转到预算申请界面
    @RequestMapping(value = {"/budget_apply"}, method = RequestMethod.GET)
    public String budget_apply(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Budget_apply> budget_applyList = new ArrayList<Budget_apply>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_budget_apply_record_No", budget_apply_record_No);
        if (op.equals("previous")) {
            if (budget_apply_start > 25) {
                budget_apply_start = budget_apply_start - 25;
                budget_apply_pageNo--;
            } else {
                budget_apply_start = 0;
                budget_apply_pageNo--;
            }
            budget_apply_record_No = budget_apply_record_No - 25;
            session.setAttribute("session_budget_apply_record_No", budget_apply_record_No);
        } else if (op.equals("next")) {
            budget_apply_start = budget_apply_start + 25;
            budget_apply_pageNo++;
            budget_apply_record_No = budget_apply_record_No + 25;
            session.setAttribute("session_budget_apply_record_No", budget_apply_record_No);
        }
        budget_applyList=budget_applyService.get_All_Budget_apply_byPage(budget_apply_start,25);
        //purchase_applyList = purchase_applyService.get_Purchase_apply_Bystatus1_status2_ByPage("通过","已确认",purchase_apply_pass_start, 25);
        if (session.getAttribute("session_budget_apply") == null) {
            session.setAttribute("session_budget_apply", budget_applyList);
        }
        session.setAttribute("session_budget_apply_PageCount",budget_applyService.getRecordCount());
        session.setAttribute("session_budget_apply_PageNo", budget_apply_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("budget_apply", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "budget_apply";
    }

    @RequestMapping(value = {"/query_budget_apply"}, method = RequestMethod.POST)
    public String query_budget_apply(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        String apply_user=request.getParameter("txt_apply_user");
        List<Budget_apply> budget_applyList=new ArrayList<Budget_apply>();
        budget_applyList=budget_applyService.get_by_username_apply(apply_user);
        session.setAttribute("session_budget_apply", budget_applyList);
        return "redirect:/erp/budget_apply?op=query";
    }




    //上传预算申请表
    @PostMapping("/upload_budget_apply_table")
    @ResponseBody
    public String upload_budget_apply_table(@RequestParam("file") MultipartFile file,HttpServletRequest request,HttpSession session) throws ParseException {
        String filename=request.getParameter("filename");
        String upload_time=request.getParameter("upload_time");
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date upload_time_1 = null;
        upload_time_1 = format.parse(format.format(format1.parse(upload_time)));
        //注意！！！mysql使用datetime类型要与java中Timestamp对应，否则mysql中的时秒分都为00
        java.sql.Timestamp sql_upload_time = new java.sql.Timestamp(upload_time_1.getTime());
        String index_no=budget_applyService.get_seq_budget_apply_index_no();
        String username_apply="";
        if(session.getAttribute("session_username")!=null){
            username_apply=(String)session.getAttribute("session_username") ;
        }
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String fileName = file.getOriginalFilename();
        //创建一个以用户名命名的文件夹
        File file1=new File("D://spring//upload//budget_apply_table//"+username_apply);
        file1.mkdir();
        // 这需要填写一个本地路径
        String filePath ="D://spring//upload//budget_apply_table//"+username_apply+"//";
        File dest = new File(filePath + fileName);
        try {
            file.transferTo(dest);
            Budget_apply budget_apply=new Budget_apply(index_no,fileName,filePath,username_apply,"采购部",sql_upload_time,"未审批","",null);
            budget_applyService.add_budget_apply(budget_apply);
            return "上传成功";
        } catch (IOException e) {
        }
        return "上传失败！";
    }

    //下载上传的预算申请表附件
    @RequestMapping(value = "/download_appendix", method = RequestMethod.GET)
    public ResponseEntity<Object> download_appendix(HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException {
        String index_no=request.getParameter("ck_index_no");
        Budget_apply budget_apply=new Budget_apply();
        budget_apply=budget_applyService.get_by_index_no(index_no);
        String fileName=budget_apply.getFile_path()+budget_apply.getFile_name();
        File file = new File(fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition",String.format("attachment;filename=\"%s\"",file.getName()));
        headers.add("Cache-Control","no-cache,no-store,must-revalidate");
        headers.add("Pragma","no-cache");
        headers.add("Expires","0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }




    //根据index_no删除budget_apply
    @RequestMapping(value = {"/delete_budget_apply_by_index_no"}, method = RequestMethod.POST)
    public void delete_budget_apply_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("budget_apply", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                Budget_apply budget_apply=new Budget_apply();
                budget_apply=budget_applyService.get_by_index_no(check_val[i].toString());
                String filepath=budget_apply.getFile_path()+budget_apply.getFile_name();
                File file=new File(filepath);
                file.delete();    //删除目录下的文件
                budget_applyService.delete_budget_apply_by_index_no(check_val[i].toString());  //删除数据库记录信息
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除预算申请" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }



    //删除purchase_apply中选中的数据
    @RequestMapping(value = {"/delete_purchase_apply_by_index_no"}, method = RequestMethod.POST)
    public void delete_purchase_apply_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power=user_powerService.get_by_tup("purchase_apply",username,"delete");
        if(user_power!=null) {
            json.put("result", "success");

            for (int i = 0; i < check_val.length; i++) {
                //warehouseService.delete_warehouse_by_warehouse_no(check_val[i].toString());
                purchase_applyService.delete_by_index_no(check_val[i].toString());
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除采购审核信息" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //删除选中的采购清单
    @RequestMapping(value = {"/delete_purchase_list_by_index_no"}, method = RequestMethod.POST)
    public void delete_purchase_list_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power=user_powerService.get_by_tup("purchase_apply",username,"delete");
        if(user_power!=null) {
            json.put("result", "success");

            for (int i = 0; i < check_val.length; i++) {
                //warehouseService.delete_warehouse_by_warehouse_no(check_val[i].toString());
                purchase_applyService.delete_by_index_no(check_val[i].toString());
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除采购审核信息" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }





    /******************************************财务管理*************************************************************/

    //跳转到预算审核界面
    @RequestMapping(value = {"/budget_check_view"}, method = RequestMethod.GET)
    public String budget_check_view(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Budget_apply> budget_apply_checkList = new ArrayList<Budget_apply>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_budget_apply_check_record_No", budget_apply_check_record_No);
        if (op.equals("previous")) {
            if (budget_apply_check_start > 25) {
                budget_apply_check_start = budget_apply_check_start - 25;
                budget_apply_check_pageNo--;
            } else {
                budget_apply_check_start = 0;
                budget_apply_check_pageNo--;
            }
            budget_apply_check_record_No = budget_apply_check_record_No - 25;
            session.setAttribute("session_budget_apply_check_record_No", budget_apply_check_record_No);
        } else if (op.equals("next")) {
            budget_apply_check_start = budget_apply_check_start + 25;
            budget_apply_check_pageNo++;
            budget_apply_check_record_No = budget_apply_check_record_No + 25;
            session.setAttribute("session_budget_apply_check_record_No", budget_apply_check_record_No);
        }
        budget_apply_checkList=budget_applyService.get_All_Budget_apply_byPage(budget_apply_start,25);
        //purchase_applyList = purchase_applyService.get_Purchase_apply_Bystatus1_status2_ByPage("通过","已确认",purchase_apply_pass_start, 25);
        if (session.getAttribute("session_budget_apply_check") == null) {
            session.setAttribute("session_budget_apply_check", budget_apply_checkList);
        }
        session.setAttribute("session_budget_apply_check_PageCount",budget_applyService.getRecordCount());
        session.setAttribute("session_budget_apply_check_PageNo", budget_apply_check_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("budget_apply", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "budget_check";
    }

    @RequestMapping(value = {"/query_budget_check"}, method = RequestMethod.POST)
    public String query_budget_check(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        String apply_user=request.getParameter("txt_apply_user");
        String status=request.getParameter("txt_status");
        List<Budget_apply> budget_applyList=new ArrayList<Budget_apply>();
        if(!apply_user.equals("")) {
            budget_applyList = budget_applyService.get_by_username_apply(apply_user);
        }
        else if ((apply_user.equals(""))&&(!status.equals(""))){
            budget_applyList=budget_applyService.get_by_status(status);
        }
        session.setAttribute("session_budget_apply", budget_applyList);
        return "redirect:/erp/budget_check_view?op=query";
    }

    //展示选中的信息
    @RequestMapping(value = {"/show_selected_budget_apply"}, method = RequestMethod.POST)
    public void show_selected_budget_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        //Purchase_apply purchase_apply=new Purchase_apply();
        Budget_apply budget_apply=new Budget_apply();
        budget_apply=budget_applyService.get_by_index_no(check_val[0].toString());
        json.put("json_index_no", budget_apply.getIndex_no());
        json.put("json_username_apply", budget_apply.getUsername_apply());
        json.put("json_dept", budget_apply.getDept());
        json.put("json_apply_time", budget_apply.getApply_time());
        json.put("json_username_deal", budget_apply.getUsername_deal());
        json.put("json_deal_time", budget_apply.getDeal_time());
        json.put("json_status", budget_apply.getStatus());

        out.print(json);
    }

    //更新选中的预算申请信息
    @RequestMapping(value = {"/update_selected_budget_apply"}, method = RequestMethod.POST)
    public void update_selected_budget_apply(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String dlg_txt_index_no=request.getParameter("dlg_txt_index_no");
        String dlg_txt_username_apply=request.getParameter("dlg_txt_username_apply");
        String dlg_txt_dept=request.getParameter("dlg_txt_dept");
        String dlg_txt_apply_time=request.getParameter("dlg_txt_apply_time");
        String dlg_txt_username_deal=request.getParameter("dlg_txt_username_deal");
        String dlg_txt_deal_time=request.getParameter("dlg_txt_deal_time");
        String dlg_txt_status=request.getParameter("dlg_txt_status");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date apply_time = null,deal_time=null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(dlg_txt_deal_time);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            deal_time=format1.parse(dlg_txt_deal_time);
        }
        else{
            deal_time=format1.parse(format1.format(format2.parse(dlg_txt_deal_time)));
        }
        java.sql.Timestamp sql_deal_time = new java.sql.Timestamp(deal_time.getTime());
        budget_applyService.update_by_index_no(dlg_txt_status,dlg_txt_username_deal,sql_deal_time,dlg_txt_index_no);
        json.put("result","success");
        out.print(json);
    }




    //收入单
    @RequestMapping(value = {"/income"}, method = RequestMethod.GET)
    public String income(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Income> incomeList = new ArrayList<Income>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_income_record_No", income_record_No);
        if (op.equals("previous")) {
            if (income_start > 25) {
                income_start = income_start - 25;
                income_pageNo--;
            } else {
                income_start = 0;
                income_pageNo--;
            }
            income_record_No = income_record_No - 25;
            session.setAttribute("session_income_record_No", income_record_No);
        } else if (op.equals("next")) {
            income_start = income_start + 25;
            income_pageNo++;
            income_record_No = income_record_No + 25;
            session.setAttribute("session_income_record_No", income_record_No);
        }
        //sale_billList=sale_billService.get_All_Sale_list_byPage(sale_bill_start,25);
        incomeList=incomeService.getAllIncome_by_page(income_start,25);
        if (session.getAttribute("session_income") == null) {
            session.setAttribute("session_income", incomeList);
        }
        session.setAttribute("session_income_PageCount",incomeService.getRecordCount());
        session.setAttribute("session_income_PageNo", income_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("income", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "income_list";
    }

    //添加收入单
    @PostMapping("/add_income")
    @ResponseBody
    public String add_income(@RequestParam("file") MultipartFile file,HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException, JSONException {
       /* response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();*/
        String index_no=incomeService.get_seq_income_index_no();
        String list_no=request.getParameter("add_dlg_txt_list_no");
        String list_date=request.getParameter("add_dlg_txt_list_date");
        String related_company=request.getParameter("add_dlg_txt_related_company");
        String pay_account=request.getParameter("add_dlg_txt_pay_account");
        String deal_person=request.getParameter("add_dlg_txt_deal_person");
        String receive_account=request.getParameter("add_dlg_txt_receive_account");
        String total_price=request.getParameter("add_dlg_txt_total_price");
        String remark=request.getParameter("add_dlg_txt_remark");
        String filename=request.getParameter("filename");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        if(list_no.equals("")){
            String month=list_date.substring(0,2);
            String day=list_date.substring(3,5);
            String year=list_date.substring(6,10);
            String hour=list_date.substring(11,13);
            String minute=list_date.substring(14,16);
            String second=list_date.substring(17);
            list_no=year+month+day+hour+minute+second+incomeService.get_seq_income_index_no();
        }
        java.util.Date util_list_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(list_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_list_date=format1.parse(list_date);
        }
        else{
            util_list_date=format1.parse(format1.format(format2.parse(list_date)));
        }
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());

        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("income", username, "add");
        if (user_power != null) {
            logger.info("用户,添加收入单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志

            if (!filename.equals("")) {
                if (file.isEmpty()) {
                    return "上传失败，请选择文件";
                }
                String fileName = file.getOriginalFilename();
                //创建一个以用户名命名的文件夹
                File file1=new File("D://spring//upload//income//"+username);
                file1.mkdir();
                System.out.println(file1.exists());
                // 这需要填写一个本地路径
                String filePath = "D://spring//upload//income//"+username+"//";
                String appendix = "D://spring//upload//income//"+username+"//"+fileName;  //存在数据库中的文件路径
                File dest = new File(filePath + fileName);
                try {
                    file.transferTo(dest);
                    Income income = new Income(index_no, list_no, sql_list_date, related_company, pay_account, deal_person, receive_account,
                            Double.valueOf(total_price), remark, appendix);
                    incomeService.add_income(income);
                    return "redirect:/erp/income?op=all";
                } catch (IOException e) {
                }
                return "上传失败！";
            }
        }
        //json.put("result", "success");
        return "上传成功";
    }

    //显示选中的收入单
    @RequestMapping(value = {"/show_selected_income"}, method = RequestMethod.POST)
    public void show_selected_income(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Income income=new Income();
        income=incomeService.get_by_index_no(check_val[0].toString());
        System.out.println(check_val[0].toString());
        System.out.println(income.toString());
        json.put("json_index_no", income.getIndex_no());
        json.put("json_list_no", income.getList_no());
        json.put("json_list_time", income.getList_time());
        json.put("json_related_company",income.getRelated_company());
        json.put("json_pay_account",income.getPay_account());
        json.put("json_deal_person",income.getDeal_person());
        json.put("json_receive_account",income.getReceive_account());
        json.put("json_total_price",income.getTotal_price());
        json.put("json_remark",income.getRemark());
        out.print(json);
    }

    //根据index_no删除income
    @RequestMapping(value = {"/delete_income_by_index_no"}, method = RequestMethod.POST)
    public void delete_income_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("income", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                Income income=new Income();
                income=incomeService.get_by_index_no(check_val[i].toString());
                String filepath=income.getAppendix();
                File file=new File(filepath);
                file.delete();    //删除目录下的文件
                incomeService.delete_income_by_index_no(check_val[i].toString()); //删除数据库记录信息
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除收入单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }


    //修改选中的收入单信息
    @RequestMapping(value = {"/update_income"}, method = RequestMethod.POST)
    public void update_income(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=request.getParameter("dlg_txt_index_no");
        String list_no=request.getParameter("edit_dlg_txt_list_no");
        String list_date=request.getParameter("edit_dlg_txt_list_date");
        String related_company=request.getParameter("edit_dlg_txt_related_company");
        String pay_account=request.getParameter("edit_dlg_txt_pay_account");
        String deal_person=request.getParameter("edit_dlg_txt_deal_person");
        String receive_account=request.getParameter("edit_dlg_txt_receive_account");
        String total_price=request.getParameter("edit_dlg_txt_total_price");
        String remark=request.getParameter("edit_dlg_txt_remark");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_list_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(list_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_list_date=format1.parse(list_date);
        }
        else{
            util_list_date=format1.parse(format1.format(format2.parse(list_date)));
        }
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("income", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            logger.info("用户,修改销售订单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            incomeService.updateIncome(sql_list_date,related_company,pay_account,deal_person,receive_account,Double.valueOf(total_price),remark,index_no);
        }
        //json.put("result", "success");
        out.print(json);
    }

    //下载附件
    @RequestMapping(value = "/download_income_appendix", method = RequestMethod.GET)
    public ResponseEntity<Object> download_income_appendix(HttpServletRequest request) throws FileNotFoundException {
        String index_no=request.getParameter("index_no");
        //String filePath="D://spring//upload//";
        //String fileName = common_fileService.get_by_remark("预算申请表").getFile_path()+common_fileService.get_by_remark("预算申请表").getFile_name();
        String fileName=incomeService.get_by_index_no(index_no).getAppendix();
        File file = new File(fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition",String.format("attachment;filename=\"%s\"",file.getName()));
        headers.add("Cache-Control","no-cache,no-store,must-revalidate");
        headers.add("Pragma","no-cache");
        headers.add("Expires","0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }


    //根据条件查询收入单
    @RequestMapping(value = {"/query_income"}, method = RequestMethod.POST)
    public String query_income(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws ParseException {
        String list_no=request.getParameter("query_txt_list_no");
        String start_time=request.getParameter("query_txt_start_time");
        String end_time=request.getParameter("query_txt_end_time");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        List<Income> incomeList=new ArrayList<Income>();
        if(!list_no.equals("")){
            incomeList=incomeService.query_income_by_list_no(list_no);
        }
        else if(list_no.equals("")&&(!start_time.equals(""))&&(!end_time.equals(""))){
            java.util.Date util_start_time = null,util_end_time=null;
            //判断list_date的格式,根据list_date的格式做相应的转换
            boolean date_format_judge_start_time=true,date_format_judge_end_time=true;
            try
            {
                Date date = format1.parse(start_time);
            } catch (ParseException e) {
                date_format_judge_start_time=false;
            }
            if(date_format_judge_start_time==true){
                util_start_time=format1.parse(start_time);
            }
            else{
                util_start_time=format1.parse(format1.format(format2.parse(start_time)));
            }
            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(util_start_time.getTime());
            try
            {
                Date date = format1.parse(end_time);
            } catch (ParseException e) {
                date_format_judge_end_time=false;
            }
            if(date_format_judge_end_time==true){
                util_end_time=format1.parse(end_time);
            }
            else{
                util_end_time=format1.parse(format1.format(format2.parse(end_time)));
            }
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(util_end_time.getTime());
            incomeList=incomeService.query_income_by_time(sql_start_time,sql_end_time);
        }
        session.setAttribute("session_income", incomeList);
        //return "redirect:/erp/income?op=query";
        return "redirect:/erp/income?op=all";
    }


    //支出单
    @RequestMapping(value = {"/expenditure"}, method = RequestMethod.GET)
    public String expenditure(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Expenditure> expenditureList = new ArrayList<Expenditure>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_expenditure_record_No", expenditure_record_No);
        if (op.equals("previous")) {
            if (expenditure_start > 25) {
                expenditure_start = expenditure_start - 25;
                expenditure_pageNo--;
            } else {
                expenditure_start = 0;
                expenditure_pageNo--;
            }
            expenditure_record_No = expenditure_record_No - 25;
            session.setAttribute("session_expenditure_record_No", expenditure_record_No);
        } else if (op.equals("next")) {
            expenditure_start = expenditure_start + 25;
            expenditure_pageNo++;
            expenditure_record_No = expenditure_record_No + 25;
            session.setAttribute("session_expenditure_record_No", expenditure_record_No);
        }
        expenditureList=expenditureService.get_Expenditure_ByPage(expenditure_start,25);
        if (session.getAttribute("session_expenditure") == null) {
            session.setAttribute("session_expenditure", expenditureList);
        }
        session.setAttribute("session_expenditure_PageCount",expenditureService.getRecordCount());
        session.setAttribute("session_expenditure_PageNo", expenditure_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("expenditure", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "expenditure_list";
    }

    //添加支出单
    @PostMapping("/add_expenditure")
    @ResponseBody
    public String add_expenditure(@RequestParam("file") MultipartFile file,HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException, JSONException {
       /* response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();*/
        //String index_no=incomeService.get_seq_income_index_no();
        String index_no=expenditureService.get_seq_expenditure_index_no();
        String list_no=request.getParameter("add_dlg_txt_list_no");
        String list_date=request.getParameter("add_dlg_txt_list_date");
        String related_company=request.getParameter("add_dlg_txt_related_company");
        String receive_account=request.getParameter("add_dlg_txt_receive_account");
        String deal_person=request.getParameter("add_dlg_txt_deal_person");
        String pay_account=request.getParameter("add_dlg_txt_pay_account");
        String total_price=request.getParameter("add_dlg_txt_total_price");
        String remark=request.getParameter("add_dlg_txt_remark");
        String filename=request.getParameter("filename");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        if(list_no.equals("")){
            String month=list_date.substring(0,2);
            String day=list_date.substring(3,5);
            String year=list_date.substring(6,10);
            String hour=list_date.substring(11,13);
            String minute=list_date.substring(14,16);
            String second=list_date.substring(17);
            list_no=year+month+day+hour+minute+second+incomeService.get_seq_income_index_no();
        }
        java.util.Date util_list_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(list_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_list_date=format1.parse(list_date);
        }
        else{
            util_list_date=format1.parse(format1.format(format2.parse(list_date)));
        }
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());

        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("expenditure", username, "add");
        if (user_power != null) {
            logger.info("用户,添加支出单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志

            if (!filename.equals("")) {
                if (file.isEmpty()) {
                    return "上传失败，请选择文件";
                }
                String fileName = file.getOriginalFilename();
                //创建一个以用户名命名的文件夹
                File file1=new File("D://spring//upload//expenditure//"+username);
                file1.mkdir();
                // 这需要填写一个本地路径
                String filePath = "D://spring//upload//expenditure//"+username+"//";
                String appendix = "D://spring//upload//expenditure//"+username+"//"+fileName; //存在数据库中的文件路径
                File dest = new File(filePath + fileName);
                try {
                    file.transferTo(dest);
                    Expenditure expenditure = new Expenditure(index_no, list_no, sql_list_date, related_company, receive_account, deal_person, pay_account,
                            Double.valueOf(total_price), remark, appendix);
                    //incomeService.add_income(income);
                    expenditureService.add_expenditure(expenditure);
                    return "上传成功";
                } catch (IOException e) {
                }
                return "上传失败！";
            }
        }
        return "上传成功";
    }

    //显示选中的支出单
    @RequestMapping(value = {"/show_selected_expenditure"}, method = RequestMethod.POST)
    public void show_selected_expenditure(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        //Income income=new Income();
        Expenditure expenditure=new Expenditure();
        expenditure=expenditureService.get_by_index_no(check_val[0].toString());
        json.put("json_index_no", expenditure.getIndex_no());
        json.put("json_list_no", expenditure.getList_no());
        json.put("json_list_time", expenditure.getList_time());
        json.put("json_related_company",expenditure.getRelated_company());
        json.put("json_receive_account",expenditure.getReceive_account());
        json.put("json_deal_person",expenditure.getDeal_person());
        json.put("json_pay_account",expenditure.getPay_account());
        json.put("json_total_price",expenditure.getTotal_price());
        json.put("json_remark",expenditure.getRemark());
        out.print(json);
    }

    //根据index_no删除expenditure
    @RequestMapping(value = {"/delete_expenditure_by_index_no"}, method = RequestMethod.POST)
    public void delete_expenditure_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("expenditure", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                Expenditure expenditure=new Expenditure();
                expenditure=expenditureService.get_by_index_no(check_val[i].toString());
                String filepath=expenditure.getAppendix();
                File file=new File(filepath);
                file.delete();    //删除目录下的文件
                expenditureService.delete_expenditure_by_index_no(check_val[i].toString()); //删除数据库记录信息
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除支出单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }


    //修改选中的支出单信息
    @RequestMapping(value = {"/update_expenditure"}, method = RequestMethod.POST)
    public void update_expenditure(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=request.getParameter("dlg_txt_index_no");
        String list_no=request.getParameter("edit_dlg_txt_list_no");
        String list_date=request.getParameter("edit_dlg_txt_list_date");
        String related_company=request.getParameter("edit_dlg_txt_related_company");
        String receive_account=request.getParameter("edit_dlg_txt_receive_account");
        String deal_person=request.getParameter("edit_dlg_txt_deal_person");
        String pay_account=request.getParameter("edit_dlg_txt_pay_account");
        String total_price=request.getParameter("edit_dlg_txt_total_price");
        String remark=request.getParameter("edit_dlg_txt_remark");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_list_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(list_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_list_date=format1.parse(list_date);
        }
        else{
            util_list_date=format1.parse(format1.format(format2.parse(list_date)));
        }
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("expenditure", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            logger.info("用户,修改销售订单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            //incomeService.updateIncome(sql_list_date,related_company,pay_account,deal_person,receive_account,Double.valueOf(total_price),remark,index_no);
            expenditureService.update_expenditure(sql_list_date,related_company,receive_account,deal_person,pay_account,Double.valueOf(total_price),remark,index_no);
        }
        //json.put("result", "success");
        out.print(json);
    }

    //下载附件
    @RequestMapping(value = "/download_expenditure_appendix", method = RequestMethod.GET)
    public ResponseEntity<Object> download_expenditure_appendix(HttpServletRequest request) throws FileNotFoundException {
        String index_no=request.getParameter("index_no");
        //String fileName=incomeService.get_by_index_no(index_no).getAppendix();
        String fileName=expenditureService.get_by_index_no(index_no).getAppendix();
        File file = new File(fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition",String.format("attachment;filename=\"%s\"",file.getName()));
        headers.add("Cache-Control","no-cache,no-store,must-revalidate");
        headers.add("Pragma","no-cache");
        headers.add("Expires","0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }


    //根据条件查询支出单
    @RequestMapping(value = {"/query_expenditure"}, method = RequestMethod.POST)
    public String query_expenditure(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws ParseException {
        String list_no=request.getParameter("query_txt_list_no");
        String start_time=request.getParameter("query_txt_start_time");
        String end_time=request.getParameter("query_txt_end_time");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        List<Expenditure> expenditureList=new ArrayList<Expenditure>();
        if(!list_no.equals("")){
            //incomeList=incomeService.query_income_by_list_no(list_no);
            expenditureList=expenditureService.get_by_list_no(list_no);
        }
        else if(list_no.equals("")&&(!start_time.equals(""))&&(!end_time.equals(""))){
            java.util.Date util_start_time = null,util_end_time=null;
            //判断list_date的格式,根据list_date的格式做相应的转换
            boolean date_format_judge_start_time=true,date_format_judge_end_time=true;
            try
            {
                Date date = format1.parse(start_time);
            } catch (ParseException e) {
                date_format_judge_start_time=false;
            }
            if(date_format_judge_start_time==true){
                util_start_time=format1.parse(start_time);
            }
            else{
                util_start_time=format1.parse(format1.format(format2.parse(start_time)));
            }
            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(util_start_time.getTime());
            try
            {
                Date date = format1.parse(end_time);
            } catch (ParseException e) {
                date_format_judge_end_time=false;
            }
            if(date_format_judge_end_time==true){
                util_end_time=format1.parse(end_time);
            }
            else{
                util_end_time=format1.parse(format1.format(format2.parse(end_time)));
            }
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(util_end_time.getTime());
            //incomeList=incomeService.query_income_by_time(sql_start_time,sql_end_time);
            expenditureList=expenditureService.get_by_list_time(sql_start_time,sql_end_time);
        }
        session.setAttribute("session_expenditure", expenditureList);
        //return "redirect:/erp/income?op=query";
        return "redirect:/erp/expenditure?op=all";
    }



    /******************************************销售管理************************************************/

    //跳转到销售订单页面
    @RequestMapping(value = {"/sale_bill"}, method = RequestMethod.GET)
    public String sale_bill(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Sale_bill> sale_billList = new ArrayList<Sale_bill>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_sale_bill_record_No", sale_bill_record_No);
        if (op.equals("previous")) {
            if (sale_bill_start > 25) {
                sale_bill_start = sale_bill_start - 25;
                sale_bill_pageNo--;
            } else {
                sale_bill_start = 0;
                sale_bill_pageNo--;
            }
            sale_bill_record_No = sale_bill_record_No - 25;
            session.setAttribute("session_sale_bill_record_No", sale_bill_record_No);
        } else if (op.equals("next")) {
            sale_bill_start = sale_bill_start + 25;
            sale_bill_pageNo++;
            sale_bill_record_No = sale_bill_record_No + 25;
            session.setAttribute("session_sale_bill_record_No", sale_bill_record_No);
        }
        sale_billList=sale_billService.get_All_Sale_list_byPage(sale_bill_start,25);
        if (session.getAttribute("session_sale_bill") == null) {
            session.setAttribute("session_sale_bill", sale_billList);
        }
        session.setAttribute("session_sale_bill_PageCount",sale_billService.getRecordCount());
        session.setAttribute("session_sale_bill_PageNo", sale_bill_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_bill", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "sale_bill";
    }

    //显示选中的销售订单
    @RequestMapping(value = {"/show_selected_sale_bill"}, method = RequestMethod.POST)
    public void show_selected_sale_bill(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        Sale_bill sale_bill=new Sale_bill();
        sale_bill=sale_billService.get_by_index_no(check_val[0].toString());
        json.put("json_index_no", sale_bill.getIndex_no());
        json.put("json_customer_name", sale_bill.getCustomer_name());
        json.put("json_company_name", sale_bill.getCompany_name());
        json.put("json_list_no",sale_bill.getList_no());
        json.put("json_list_date",sale_bill.getList_date());
        json.put("json_goods_no",sale_bill.getGoods_no());
        json.put("json_goods_name",sale_bill.getGoods_name());
        json.put("json_count",sale_bill.getCount());
        json.put("json_total_price",sale_bill.getTotal_price());
        json.put("json_operator",sale_bill.getOperator());
        json.put("json_status",sale_bill.getStatus());
        json.put("json_remark",sale_bill.getRemark());
        out.print(json);
    }

    //添加销售订单
    @RequestMapping(value = {"/add_sale_bill"}, method = RequestMethod.POST)
    public void add_sale_bill(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=sale_billService.get_seq_sale_list_index_no();
        String customer_name=request.getParameter("add_dlg_txt_customer_name");
        String company_name=request.getParameter("add_dlg_txt_company_name");
        String list_no=request.getParameter("add_dlg_txt_list_no");
        String list_date=request.getParameter("add_dlg_txt_list_date");
        String goods_no=request.getParameter("add_dlg_txt_goods_no");
        String goods_name=request.getParameter("add_dlg_txt_goods_name");
        String count=request.getParameter("add_dlg_txt_count");
        //String total_price=request.getParameter("add_dlg_txt_total_price");
        String operator=request.getParameter("add_dlg_txt_operator");
        String status=request.getParameter("add_dlg_txt_status");
        String remark=request.getParameter("add_dlg_txt_remark");

        String username="";
        if(list_no.equals("")){  //如果没有输入单据号，则系统自动生产单据号：入库日期+序列号
            String month=list_date.substring(0,2);
            String day=list_date.substring(3,5);
            String year=list_date.substring(6,10);
            String hour=list_date.substring(11,13);
            String minute=list_date.substring(14,16);
            String second=list_date.substring(17);
            list_no=year+month+day+hour+minute+second+sale_statisticsService.get_seq_sale_statistics_index_no();
        }
        Stock_statistics stock_statistics=new Stock_statistics();
        stock_statistics=stockstatisticsService.get_by_goods_no(goods_no);
        double total_price=Integer.parseInt(count)*stock_statistics.getSingle_price();
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_list_date = null;
        util_list_date = format.parse(format.format(format1.parse(list_date)));
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());
        Sale_bill sale_bill=new Sale_bill(index_no,customer_name,company_name,list_no,sql_list_date,goods_no,goods_name,Integer.parseInt(count),
                total_price,operator,status,remark);
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }

        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_bill", username, "add");
        if (user_power != null) {
            json.put("result", "success");
            logger.info("用户,添加销售订单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            sale_billService.add_sale_list(sale_bill);
        }
        //json.put("result", "success");
        out.print(json);

    }


    //修改选中的销售订单信息
    @RequestMapping(value = {"/update_sale_bill"}, method = RequestMethod.POST)
    public void update_sale_bill(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String index_no=request.getParameter("edit_dlg_txt_index_no");
        String customer_name=request.getParameter("edit_dlg_txt_customer_name");
        String company_name=request.getParameter("edit_dlg_txt_company_name");
        String list_no=request.getParameter("edit_dlg_txt_list_no");
        String list_date=request.getParameter("edit_dlg_txt_list_date");
        String goods_no=request.getParameter("edit_dlg_txt_goods_no");
        String goods_name=request.getParameter("edit_dlg_txt_goods_name");
        String count=request.getParameter("edit_dlg_txt_count");
        String total_price=request.getParameter("edit_dlg_txt_total_price");
        String operator=request.getParameter("edit_dlg_txt_operator");
        String status=request.getParameter("edit_dlg_txt_status");
        String remark=request.getParameter("edit_dlg_txt_remark");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_list_date = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(list_date);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_list_date=format1.parse(list_date);
        }
        else{
            util_list_date=format1.parse(format1.format(format2.parse(list_date)));
        }
        java.sql.Timestamp sql_list_date = new java.sql.Timestamp(util_list_date.getTime());
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_bill", username, "update");
        if (user_power != null) {
            json.put("result", "success");
            logger.info("用户,修改销售订单" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
            if (status.equals("销售出库")) {   //销售出库
                String flow_number = out_warehouseService.get_seq_out_warehouse_flow_number();
                Out_Warehouse out_warehouse = new Out_Warehouse(flow_number, list_no, goods_no, null, null, goods_name,
                        0, Integer.parseInt(count), Double.valueOf(total_price), "local", null, "未出库",
                        null, remark);
                out_warehouseService.addOut_Warehouse(out_warehouse);
            }
            if(status.equals("退货入库")){
                String flow_number=entry_warehouseService.get_seq_entry_warehouse_flow_number();
                Entry_Warehouse entry_warehouse=new Entry_Warehouse(flow_number,list_no,goods_no,null,null,goods_name,
                        0,Integer.parseInt(count),Double.valueOf(total_price),"local",null,"未入库",null,remark);
                entry_warehouseService.addEntry_Warehouse(entry_warehouse);
            }
            sale_billService.updateSale_bill(customer_name, company_name, list_no, sql_list_date, goods_no,goods_name, Integer.parseInt(count),
                    Double.valueOf(total_price), operator, status, remark, index_no);
        }
        //json.put("result", "success");
        out.print(json);
    }

    //根据条件查询销售订单信息 return "redirect:/erp/purchase_list?op=query";  session_sale_bill
    @RequestMapping(value = {"/query_sale_bill"}, method = RequestMethod.POST)
    public String query_sale_bill(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws ParseException {
        String list_no=request.getParameter("query_txt_list_no");
        String goods_name=request.getParameter("query_txt_goods_name");
        String start_time=request.getParameter("query_txt_start_time");
        String end_time=request.getParameter("query_txt_end_time");
        String status=request.getParameter("query_txt_status");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        List<Sale_bill> sale_billList=new ArrayList<Sale_bill>();
        if(!list_no.equals("")){
            sale_billList=sale_billService.get_by_list_no(list_no);
        }
        else if ((list_no.equals(""))&&(!goods_name.equals(""))){
            sale_billList=sale_billService.get_by_goods_name(goods_name);
        }
        else if ((list_no.equals(""))&&(goods_name.equals(""))&&(!start_time.equals(""))&&(!end_time.equals(""))&&(status.equals(""))){
            java.util.Date util_start_time = null,util_end_time=null;
            //判断start_time,end_time的格式,根据start_time,end_time的格式做相应的转换
            boolean date_format_judge_1=true,date_format_judge_2=true;

            try
            {
                Date date = format1.parse(start_time);
            } catch (ParseException e) {
                date_format_judge_1=false;
            }
            if(date_format_judge_1==true){
                util_start_time=format1.parse(start_time);
            }
            else{
                util_start_time=format1.parse(format1.format(format2.parse(start_time)));
            }

            try
            {
                Date date = format1.parse(end_time);
            } catch (ParseException e) {
                date_format_judge_2=false;
            }
            if(date_format_judge_2==true){
                util_end_time=format1.parse(end_time);
            }
            else{
                util_end_time=format1.parse(format1.format(format2.parse(end_time)));
            }


            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(util_start_time.getTime());
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(util_end_time.getTime());
            sale_billList=sale_billService.get_by_list_date(sql_start_time,sql_end_time);
        }
        else if ((list_no.equals(""))&&(goods_name.equals(""))&&(start_time.equals(""))&&(end_time.equals(""))&&(!status.equals(""))){
            sale_billList=sale_billService.get_by_status(status);
            session.setAttribute("session_sale_bill",sale_billList);
        }
        else if ((list_no.equals(""))&&(goods_name.equals(""))&&(!start_time.equals(""))&&(!end_time.equals(""))&&(!status.equals(""))){
            java.util.Date util_start_time = null,util_end_time=null;
            //判断start_time,end_time的格式,根据start_time,end_time的格式做相应的转换
            boolean date_format_judge_1=true,date_format_judge_2=true;

            try
            {
                Date date = format1.parse(start_time);
            } catch (ParseException e) {
                date_format_judge_1=false;
            }
            if(date_format_judge_1==true){
                util_start_time=format1.parse(start_time);
            }
            else{
                util_start_time=format1.parse(format1.format(format2.parse(start_time)));
            }

            try
            {
                Date date = format1.parse(end_time);
            } catch (ParseException e) {
                date_format_judge_2=false;
            }
            if(date_format_judge_2==true){
                util_end_time=format1.parse(end_time);
            }
            else{
                util_end_time=format1.parse(format1.format(format2.parse(end_time)));
            }


            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(util_start_time.getTime());
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(util_end_time.getTime());
            sale_billList=sale_billService.get_by_list_date_status(sql_start_time,sql_end_time,status);
        }
        session.setAttribute("session_sale_bill",sale_billList);
        return "redirect:/erp/sale_bill?op=query";
    }

    //根据index_no删除销售订单
    @RequestMapping(value = {"/delete_sale_bill_by_index_no"}, method = RequestMethod.POST)
    public void delete_sale_bill_by_index_no(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        User_Power user_power = user_powerService.get_by_tup("sale_bill", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for(int i=0;i<check_val.length;i++) {
                logger.info("用户,删除销售订单" +check_val[i]+":" + username + "." + username + ";" + "成功");  //写入日志
                sale_billService.delete_sale_bill_by_index_no(check_val[i].toString());
            }
        }
        out.print(json);
    }


    //跳转到销售统计界面
    @RequestMapping(value = {"/sale_statistics"}, method = RequestMethod.GET)
    public String sale_statistics(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Sale_statistics> sale_statisticsList = new ArrayList<Sale_statistics>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_sale_statistics_record_No", sale_statistics_record_No);
        if (op.equals("previous")) {
            if (sale_statistics_start > 25) {
                sale_statistics_start = sale_statistics_start - 25;
                sale_statistics_pageNo--;
            } else {
                sale_statistics_start = 0;
                sale_statistics_pageNo--;
            }
            sale_statistics_record_No = sale_statistics_record_No - 25;
            session.setAttribute("session_sale_statistics_record_No", sale_statistics_record_No);
        } else if (op.equals("next")) {
            sale_statistics_start = sale_statistics_start + 25;
            sale_statistics_pageNo++;
            sale_statistics_record_No = sale_statistics_record_No + 25;
            session.setAttribute("session_sale_statistics_record_No", sale_statistics_record_No);
        }
        //sale_billList=sale_billService.get_All_Sale_list_byPage(sale_bill_start,25);
        sale_statisticsList=sale_statisticsService.get_All_ByPage(sale_statistics_start,25);
        if (session.getAttribute("session_sale_statistics") == null) {
            session.setAttribute("session_sale_statistics", sale_statisticsList);
        }
        session.setAttribute("session_sale_statistics_PageCount",sale_statisticsService.getRecordCount());
        session.setAttribute("session_sale_statistics_PageNo", sale_statistics_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_statistics", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "sale_statistics";
    }

    //添加销售统计
    @PostMapping("/add_sale_statistics")
    @ResponseBody
    public String add_sale_statistics(@RequestParam("file") MultipartFile file,HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException, JSONException {
        String index_no=expenditureService.get_seq_expenditure_index_no();
        String upload_time=request.getParameter("add_dlg_txt_upload_time");
        String uploader=request.getParameter("add_dlg_txt_uploader");
        String dept=request.getParameter("dlg_txt_dept");
        String filename=request.getParameter("filename");
        String remark=request.getParameter("add_dlg_txt_remark");

        String username="";
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        java.util.Date util_upload_time = null;
        //判断list_date的格式,根据list_date的格式做相应的转换
        boolean date_format_judge=true;
        try
        {
            Date date = format1.parse(upload_time);
        } catch (ParseException e) {
            date_format_judge=false;
        }
        if(date_format_judge==true){
            util_upload_time=format1.parse(upload_time);
        }
        else{
            util_upload_time=format1.parse(format1.format(format2.parse(upload_time)));
        }
        java.sql.Timestamp sql_upload_time = new java.sql.Timestamp(util_upload_time.getTime());

        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_statistics", username, "add");
        if (user_power != null) {
            logger.info("用户,添加销售统计" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志

            if (!filename.equals("")) {
                if (file.isEmpty()) {
                    return "上传失败，请选择文件";
                }
                String fileName = file.getOriginalFilename();
                //创建一个以用户名命名的文件夹
                File file1=new File("D://spring//upload//sale_statistics//"+username);
                file1.mkdir();
                // 这需要填写一个本地路径
                String filePath = "D://spring//upload//sale_statistics//"+username+"//";
                String appendix = "D://spring//upload//sale_statistics//"+username+"//"+fileName; //存在数据库中的文件路径
                File dest = new File(filePath + fileName);
                try {
                    file.transferTo(dest);
                    Sale_statistics sale_statistics=new Sale_statistics(index_no,sql_upload_time,uploader,dept,remark,appendix);
                    //expenditureService.add_expenditure(expenditure);
                    sale_statisticsService.add_sale_statistics(sale_statistics);
                    return "上传成功";
                } catch (IOException e) {
                }
                return "上传失败！";
            }
        }
        return "redirect:/erp/sale_statistics?op=all";
    }

    //根据index_no删除sale_statistics
    @RequestMapping(value = {"/delete_sale_statistics_by_index_no"}, method = RequestMethod.POST)
    public void delete_sale_statistics_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("sale_statistics", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                //Income income=new Income();
                Sale_statistics sale_statistics=new Sale_statistics();
                //income=incomeService.get_by_index_no(check_val[i].toString());
                sale_statistics=sale_statisticsService.get_sale_statistics_by_index_no(check_val[i].toString());
                String filepath=sale_statistics.getAppendix();
                File file=new File(filepath);
                file.delete();    //删除目录下的文件
                sale_statisticsService.delete_sale_statistics_by_index_no(check_val[i].toString()); //删除数据库记录信息
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除销售统计" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }

    //下载附件
    @RequestMapping(value = "/download_sale_statistics_appendix", method = RequestMethod.GET)
    public ResponseEntity<Object> download_sale_statistics_appendix(HttpServletRequest request) throws FileNotFoundException {
        String index_no = request.getParameter("index_no");
        String fileName = sale_statisticsService.get_sale_statistics_by_index_no(index_no).getAppendix();
        File file = new File(fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("attachment;filename=\"%s\"", file.getName()));
        headers.add("Cache-Control", "no-cache,no-store,must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }
    //根据时间查询销售统计信息
    @RequestMapping(value = {"/query_sale_statistics"}, method = RequestMethod.POST)
    public String query_sale_statistics(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws ParseException {
        String start_time=request.getParameter("query_txt_start_time");
        String end_time=request.getParameter("query_txt_end_time");
        SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //List<Income> incomeList=new ArrayList<Income>();
        List<Sale_statistics> sale_statisticsList=new ArrayList<Sale_statistics>();

        if((!start_time.equals(""))&&(!end_time.equals(""))){
            java.util.Date util_start_time = null,util_end_time=null;
            //判断list_date的格式,根据list_date的格式做相应的转换
            boolean date_format_judge_start_time=true,date_format_judge_end_time=true;
            try
            {
                Date date = format1.parse(start_time);
            } catch (ParseException e) {
                date_format_judge_start_time=false;
            }
            if(date_format_judge_start_time==true){
                util_start_time=format1.parse(start_time);
            }
            else{
                util_start_time=format1.parse(format1.format(format2.parse(start_time)));
            }
            java.sql.Timestamp sql_start_time = new java.sql.Timestamp(util_start_time.getTime());
            try
            {
                Date date = format1.parse(end_time);
            } catch (ParseException e) {
                date_format_judge_end_time=false;
            }
            if(date_format_judge_end_time==true){
                util_end_time=format1.parse(end_time);
            }
            else{
                util_end_time=format1.parse(format1.format(format2.parse(end_time)));
            }
            java.sql.Timestamp sql_end_time = new java.sql.Timestamp(util_end_time.getTime());
            //incomeList=incomeService.query_income_by_time(sql_start_time,sql_end_time);
            sale_statisticsList=sale_statisticsService.query_sale_statistics_by_upload_time(sql_start_time,sql_end_time);
        }
        session.setAttribute("session_sale_statistics", sale_statisticsList);
        //return "redirect:/erp/income?op=query";
        return "redirect:/erp/sale_statistics?op=all";
    }


    /****************************************公共文件************************************************************************/

    //跳转到销售统计界面
    @RequestMapping(value = {"/common_file"}, method = RequestMethod.GET)
    public String common_file(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        List<Common_file> common_fileList=new ArrayList<Common_file>();
        String op = request.getParameter("op");
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        session.setAttribute("session_common_file_record_No", common_file_record_No);
        if (op.equals("previous")) {
            if (common_file_start > 25) {
                common_file_start = common_file_start - 25;
                common_file_pageNo--;
            } else {
                common_file_start = 0;
                common_file_pageNo--;
            }
            common_file_record_No = common_file_record_No - 25;
            session.setAttribute("session_common_file_record_No", common_file_record_No);
        } else if (op.equals("next")) {
            common_file_start = common_file_start + 25;
            common_file_pageNo++;
            common_file_record_No = common_file_record_No + 25;
            session.setAttribute("session_common_file_record_No", common_file_record_No);
        }
        //common_fileList=sale_statisticsService.get_All_ByPage(sale_statistics_start,25);
        common_fileList=common_fileService.get_all_common_file_bypage(common_file_start,25);
        if (session.getAttribute("session_common_file") == null) {
            session.setAttribute("session_common_file", common_fileList);
        }
        session.setAttribute("session_common_file_PageCount",common_fileService.getRecordCount());
        session.setAttribute("session_common_file_PageNo", common_file_pageNo);
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("common_file", username, "select");
        if (user_power == null) {
            return "no_power";
        }
        return "common_file";
    }

    //添加文件
    @PostMapping("/add_common_file")
    @ResponseBody
    public String add_common_file(@RequestParam("file") MultipartFile file,HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException, JSONException {
        String index_no=common_fileService.get_seq_common_file_index_no();
        String filename=request.getParameter("filename");
        String uploader=request.getParameter("add_dlg_txt_uploader");
        String remark=request.getParameter("add_dlg_txt_remark");
        String username="";

        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("common_file", username, "add");
        if (user_power != null) {
            logger.info("用户,添加公共文件" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志

            if (!filename.equals("")) {
                if (file.isEmpty()) {
                    return "上传失败，请选择文件";
                }
                String fileName = file.getOriginalFilename();
                //创建一个以用户名命名的文件夹
                File file1=new File("D://spring//upload//common_file//"+username);
                file1.mkdir();
                // 这需要填写一个本地路径
                String filePath = "D://spring//upload//common_file//"+username+"//";
                String appendix = "D://spring//upload//common_file//"+username+"//"+fileName; //存在数据库中的文件路径
                File dest = new File(filePath + fileName);
                try {
                    file.transferTo(dest);
                    //Sale_statistics sale_statistics=new Sale_statistics(index_no,sql_upload_time,uploader,dept,remark,appendix);
                    Common_file common_file=new Common_file(index_no,filename,uploader,appendix,remark);
                    //expenditureService.add_expenditure(expenditure);
                    //sale_statisticsService.add_sale_statistics(sale_statistics);
                    common_fileService.add_common_file(common_file);
                    return "上传成功";
                } catch (IOException e) {
                }
                return "上传失败！";
            }
        }
        return "redirect:/erp/common_file?op=all";
    }

    //下载文件
    @RequestMapping(value = "/download_common_file", method = RequestMethod.GET)
    public ResponseEntity<Object> download_common_file(HttpServletRequest request) throws FileNotFoundException {
        String index_no = request.getParameter("index_no");
        //String fileName = sale_statisticsService.get_sale_statistics_by_index_no(index_no).getAppendix();
        String fileName=common_fileService.get_by_index_no(index_no).getFile_path();
        File file = new File(fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("attachment;filename=\"%s\"", file.getName()));
        headers.add("Cache-Control", "no-cache,no-store,must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }


    //根据index_no删除common_file
    @RequestMapping(value = {"/delete_common_file_by_index_no"}, method = RequestMethod.POST)
    public void delete_common_file_by_index_no(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");  //解决out.print()中文乱码
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String[] check_val = request.getParameterValues("check_val");
        String index_no="";
        String username="";
        if(session.getAttribute("session_username")!=null){
            username=(String)session.getAttribute("session_username") ;
        }
        //权限认证
        User_Power user_power = user_powerService.get_by_tup("common_file", username, "delete");
        if (user_power != null) {
            json.put("result", "success");
            for (int i = 0; i < check_val.length; i++) {
                Common_file common_file=new Common_file();
                common_file=common_fileService.get_by_index_no(check_val[i].toString());
                String filepath=common_file.getFile_path();
                File file=new File(filepath);
                file.delete();    //删除目录下的文件
                common_fileService.delete_common_file_by_remark(check_val[i].toString());  //删除数据库记录信息
                index_no = index_no + "||" + check_val[i].toString();
            }
            logger.info("用户,删除公共文件" + index_no + ":" + username + "." + username + ";" + "成功");  //写入日志
        }
        out.print(json);
    }



    /*********************************************新手引导**********************************************************/
    //跳转到新手引导界面
    @RequestMapping(value = {"/guide"}, method = RequestMethod.GET)
    public String guide(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        return "guide";
    }

    //下载文件
    @RequestMapping(value = "/download_guide", method = RequestMethod.GET)
    public ResponseEntity<Object> download_guide(HttpServletRequest request) throws FileNotFoundException {
        String index_no = request.getParameter("index_no");
        //String fileName = sale_statisticsService.get_sale_statistics_by_index_no(index_no).getAppendix();
        //String fileName=common_fileService.get_by_index_no(index_no).getFile_path();
        File file = new File("D://spring//guide//guide.pdf");
        InputStreamResource resource = new InputStreamResource(new FileInputStream((file)));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("attachment;filename=\"%s\"", file.getName()));
        headers.add("Cache-Control", "no-cache,no-store,must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/text"))
                .body(resource);
        return responseEntity;
    }








    //上传公共文件
       /* @PostMapping("/upload_common_file")
        @ResponseBody
        public String upload(@RequestParam("file") MultipartFile file,HttpServletRequest request) {
            String filename=request.getParameter("filename");
            String remark=request.getParameter("remark");
            if (file.isEmpty()) {
                return "上传失败，请选择文件";
            }
            String fileName = file.getOriginalFilename();
            // 这需要填写一个本地路径
            String filePath ="D://spring//upload//common_file//";
            File dest = new File(filePath + fileName);
            try {
                file.transferTo(dest);
                Common_file common_file=new Common_file(filename,filePath,remark);
                common_fileService.add_common_file(common_file);
                return "上传成功";
            } catch (IOException e) {
            }
            return "上传失败！";
        }*/

















    //跳转到原料仓库界面
    @RequestMapping(value = {"/building"},method = RequestMethod.GET)
    public String building_page(){
        return "building";
    }

    @RequestMapping(value = {"/welcome"},method = RequestMethod.GET)
    public String welcome(){
        return "welcome";
    }


    @ResponseBody
    @GetMapping(value = "/example", produces = {"application/json;charset=UTF-8"})
    public void example(@RequestParam(value = "check_val[]", required = false) String[] check_val){
        System.out.println("哈哈哈");
    }



    //跳转到原料仓库界面
    @RequestMapping(value = {"/material_warehouse"},method = RequestMethod.GET)
    public String material_warehouse(){
        return "material_warehouse";
    }

    //跳转到成品仓库界面
    @RequestMapping(value = {"/product_warehouse"},method = RequestMethod.GET)
    public String product_warehouse(){
        return "product_warehouse";
    }

    //跳转到日用品仓库界面
    @RequestMapping(value = {"/common_warehouse"},method = RequestMethod.GET)
    public String common_warehouse(){
        return "common_warehouse";
    }

    //跳转到AAA
    @RequestMapping(value = {"/AAA"},method = RequestMethod.GET)
    public String AAA(){
        return "no_power";
    }
}
