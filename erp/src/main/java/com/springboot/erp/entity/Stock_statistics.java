package com.springboot.erp.entity;

import java.sql.Date;

public class Stock_statistics {
    private String index_no,warehouse_no,list_no,goods_no,kind,specification,goods_name;
    private double single_price;
    private int count;
    private double total_price;
    private String manufacturer;
    private java.sql.Date entry_date,out_date;
    private String status,remarks;

    public Stock_statistics() {
    }

    public Stock_statistics(String index_no, String warehouse_no, String list_no, String goods_no, String kind, String specification, String goods_name, double single_price, int count, double total_price, String manufacturer, Date entry_date, Date out_date, String status, String remarks) {
        this.index_no = index_no;
        this.warehouse_no = warehouse_no;
        this.list_no = list_no;
        this.goods_no = goods_no;
        this.kind = kind;
        this.specification = specification;
        this.goods_name = goods_name;
        this.single_price = single_price;
        this.count = count;
        this.total_price = total_price;
        this.manufacturer = manufacturer;
        this.entry_date = entry_date;
        this.out_date = out_date;
        this.status = status;
        this.remarks = remarks;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getWarehouse_no() {
        return warehouse_no;
    }

    public void setWarehouse_no(String warehouse_no) {
        this.warehouse_no = warehouse_no;
    }

    public String getList_no() {
        return list_no;
    }

    public void setList_no(String list_no) {
        this.list_no = list_no;
    }

    public String getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(String goods_no) {
        this.goods_no = goods_no;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public double getSingle_price() {
        return single_price;
    }

    public void setSingle_price(double single_price) {
        this.single_price = single_price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Date getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(Date entry_date) {
        this.entry_date = entry_date;
    }

    public Date getOut_date() {
        return out_date;
    }

    public void setOut_date(Date out_date) {
        this.out_date = out_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Stock_statistics{" +
                "index_no='" + index_no + '\'' +
                ", warehouse_no='" + warehouse_no + '\'' +
                ", list_no='" + list_no + '\'' +
                ", goods_no='" + goods_no + '\'' +
                ", kind='" + kind + '\'' +
                ", specification='" + specification + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", single_price=" + single_price +
                ", count=" + count +
                ", total_price=" + total_price +
                ", manufacturer='" + manufacturer + '\'' +
                ", entry_date=" + entry_date +
                ", out_date=" + out_date +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
