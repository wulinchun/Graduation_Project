package com.springboot.erp.entity;

public class Warehouse {
    private String warehouse_no,name,location;
    private float volume,rent;

    public Warehouse() {
        super();
    }

    public Warehouse(String warehouse_no, String name, String location, float volume, float rent) {
        this.warehouse_no = warehouse_no;
        this.name = name;
        this.location = location;
        this.volume = volume;
        this.rent = rent;
    }

    public String getWarehouse_no() {
        return warehouse_no;
    }

    public void setWarehouse_no(String warehouse_no) {
        this.warehouse_no = warehouse_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public float getRent() {
        return rent;
    }

    public void setRent(float rent) {
        this.rent = rent;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "warehouse_no='" + warehouse_no + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", volume=" + volume +
                ", rent=" + rent +
                '}';
    }
}
