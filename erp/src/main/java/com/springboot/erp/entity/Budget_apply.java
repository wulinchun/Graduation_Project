package com.springboot.erp.entity;

import java.sql.Timestamp;

public class Budget_apply {
    private String index_no,file_name,file_path,username_apply,dept;
    private java.sql.Timestamp apply_time;
    private String status,username_deal;
    private java.sql.Timestamp deal_time;

    public Budget_apply() {
        super();
    }

    public Budget_apply(String index_no, String file_name, String file_path, String username_apply, String dept, Timestamp apply_time, String status, String username_deal, Timestamp deal_time) {
        this.index_no = index_no;
        this.file_name = file_name;
        this.file_path = file_path;
        this.username_apply = username_apply;
        this.dept = dept;
        this.apply_time = apply_time;
        this.status = status;
        this.username_deal = username_deal;
        this.deal_time = deal_time;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getUsername_apply() {
        return username_apply;
    }

    public void setUsername_apply(String username_apply) {
        this.username_apply = username_apply;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Timestamp getApply_time() {
        return apply_time;
    }

    public void setApply_time(Timestamp apply_time) {
        this.apply_time = apply_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername_deal() {
        return username_deal;
    }

    public void setUsername_deal(String username_deal) {
        this.username_deal = username_deal;
    }

    public Timestamp getDeal_time() {
        return deal_time;
    }

    public void setDeal_time(Timestamp deal_time) {
        this.deal_time = deal_time;
    }

    @Override
    public String toString() {
        return "Budget_apply{" +
                "index_no='" + index_no + '\'' +
                ", file_name='" + file_name + '\'' +
                ", file_path='" + file_path + '\'' +
                ", username_apply='" + username_apply + '\'' +
                ", dept='" + dept + '\'' +
                ", apply_time=" + apply_time +
                ", status='" + status + '\'' +
                ", username_deal='" + username_deal + '\'' +
                ", deal_time=" + deal_time +
                '}';
    }
}
