package com.springboot.erp.entity;

public class Customer {
    private String no,name,tel;
    private int credit_grade;

    public Customer() {
        super();
    }

    public Customer(String no, String name, String tel, int credit_grade) {
        this.no = no;
        this.name = name;
        this.tel = tel;
        this.credit_grade = credit_grade;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getCredit_grade() {
        return credit_grade;
    }

    public void setCredit_grade(int credit_grade) {
        this.credit_grade = credit_grade;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "no='" + no + '\'' +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", credit_grade=" + credit_grade +
                '}';
    }
}
