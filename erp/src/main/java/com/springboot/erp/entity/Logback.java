package com.springboot.erp.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class Logback {
    private String operation_module,operation_detail,operator,operation_status;
    private java.sql.Timestamp operation_time;

    public Logback() {
        super();
    }

    public Logback(String operation_module, String operation_detail, String operator, String operation_status, Timestamp operation_time) {
        this.operation_module = operation_module;
        this.operation_detail = operation_detail;
        this.operator = operator;
        this.operation_status = operation_status;
        this.operation_time = operation_time;
    }

    public String getOperation_module() {
        return operation_module;
    }

    public void setOperation_module(String operation_module) {
        this.operation_module = operation_module;
    }

    public String getOperation_detail() {
        return operation_detail;
    }

    public void setOperation_detail(String operation_detail) {
        this.operation_detail = operation_detail;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperation_status() {
        return operation_status;
    }

    public void setOperation_status(String operation_status) {
        this.operation_status = operation_status;
    }

    public Timestamp getOperation_time() {
        return operation_time;
    }

    public void setOperation_time(Timestamp operation_time) {
        this.operation_time = operation_time;
    }

    @Override
    public String toString() {
        return "Logback{" +
                "operation_module='" + operation_module + '\'' +
                ", operation_detail='" + operation_detail + '\'' +
                ", operator='" + operator + '\'' +
                ", operation_status='" + operation_status + '\'' +
                ", operation_time=" + operation_time +
                '}';
    }
}
