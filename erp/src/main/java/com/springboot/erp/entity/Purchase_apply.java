package com.springboot.erp.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class Purchase_apply {
    private String index_no,username,name,dept,kind,goods_name;
    int count;
    private java.sql.Timestamp apply_time;
    private String status,manufacturer;
    double total_price;

    public Purchase_apply() {
        super();
    }

    public Purchase_apply(String index_no, String username, String name, String dept, String kind, String goods_name, int count, Timestamp apply_time, String status, String manufacturer, double total_price) {
        this.index_no = index_no;
        this.username = username;
        this.name = name;
        this.dept = dept;
        this.kind = kind;
        this.goods_name = goods_name;
        this.count = count;
        this.apply_time = apply_time;
        this.status = status;
        this.manufacturer = manufacturer;
        this.total_price = total_price;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Timestamp getApply_time() {
        return apply_time;
    }

    public void setApply_time(Timestamp apply_time) {
        this.apply_time = apply_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    @Override
    public String toString() {
        return "Purchase_apply{" +
                "index_no='" + index_no + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", kind='" + kind + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", count=" + count +
                ", apply_time=" + apply_time +
                ", status='" + status + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", total_price=" + total_price +
                '}';
    }
}
