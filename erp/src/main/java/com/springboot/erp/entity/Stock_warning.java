package com.springboot.erp.entity;

public class Stock_warning {
    private String warehouse_no,goods_no,goods_name;
    int max,min;

    public Stock_warning() {
        super();
    }

    public Stock_warning(String warehouse_no, String goods_no, String goods_name, int max, int min) {
        this.warehouse_no = warehouse_no;
        this.goods_no = goods_no;
        this.goods_name = goods_name;
        this.max = max;
        this.min = min;
    }

    public String getWarehouse_no() {
        return warehouse_no;
    }

    public void setWarehouse_no(String warehouse_no) {
        this.warehouse_no = warehouse_no;
    }

    public String getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(String goods_no) {
        this.goods_no = goods_no;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "Stock_warning{" +
                "warehouse_no='" + warehouse_no + '\'' +
                ", goods_no='" + goods_no + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", max=" + max +
                ", min=" + min +
                '}';
    }
}
