package com.springboot.erp.entity;

public class User_Power {
    private String index_no,tablename,username,power;

    public User_Power() {
        super();
    }

    public User_Power(String index_no, String tablename, String username, String power) {
        this.index_no = index_no;
        this.tablename = tablename;
        this.username = username;
        this.power = power;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "User_Power{" +
                "index_no='" + index_no + '\'' +
                ", tablename='" + tablename + '\'' +
                ", username='" + username + '\'' +
                ", power='" + power + '\'' +
                '}';
    }
}
