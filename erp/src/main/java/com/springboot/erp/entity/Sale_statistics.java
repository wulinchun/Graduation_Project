package com.springboot.erp.entity;

import java.sql.Timestamp;

public class Sale_statistics {
    private String index_no;
    private java.sql.Timestamp upload_time;
    private String uploader,dept,remark,appendix;

    public Sale_statistics() {
        super();
    }

    public Sale_statistics(String index_no, Timestamp upload_time, String uploader, String dept, String remark, String appendix) {
        this.index_no = index_no;
        this.upload_time = upload_time;
        this.uploader = uploader;
        this.dept = dept;
        this.remark = remark;
        this.appendix = appendix;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public Timestamp getUpload_time() {
        return upload_time;
    }

    public void setUpload_time(Timestamp upload_time) {
        this.upload_time = upload_time;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppendix() {
        return appendix;
    }

    public void setAppendix(String appendix) {
        this.appendix = appendix;
    }

    @Override
    public String toString() {
        return "Sale_statistics{" +
                "index_no='" + index_no + '\'' +
                ", upload_time=" + upload_time +
                ", uploader='" + uploader + '\'' +
                ", dept='" + dept + '\'' +
                ", remark='" + remark + '\'' +
                ", appendix='" + appendix + '\'' +
                '}';
    }
}
