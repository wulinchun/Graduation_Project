package com.springboot.erp.entity;

public class User {
    private String username,password,name,sex,tel,dept,position,power;

    public User() {
        super();
    }

    public User(String username, String password, String name, String sex, String tel, String dept, String position, String power) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.sex = sex;
        this.tel = tel;
        this.dept = dept;
        this.position = position;
        this.power = power;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", tel='" + tel + '\'' +
                ", dept='" + dept + '\'' +
                ", position='" + position + '\'' +
                ", power='" + power + '\'' +
                '}';
    }
}
