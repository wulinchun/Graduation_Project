package com.springboot.erp.entity;

import java.sql.Timestamp;

public class Income {
    private String index_no,list_no;
    private java.sql.Timestamp list_time;
    private String related_company,pay_account,deal_person,receive_account;
    private double total_price;
    private String remark,appendix;

    public Income() {
        super();
    }

    public Income(String index_no, String list_no, Timestamp list_time, String related_company, String pay_account, String deal_person, String receive_account, double total_price, String remark, String appendix) {
        this.index_no = index_no;
        this.list_no = list_no;
        this.list_time = list_time;
        this.related_company = related_company;
        this.pay_account = pay_account;
        this.deal_person = deal_person;
        this.receive_account = receive_account;
        this.total_price = total_price;
        this.remark = remark;
        this.appendix = appendix;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getList_no() {
        return list_no;
    }

    public void setList_no(String list_no) {
        this.list_no = list_no;
    }

    public Timestamp getList_time() {
        return list_time;
    }

    public void setList_time(Timestamp list_time) {
        this.list_time = list_time;
    }

    public String getRelated_company() {
        return related_company;
    }

    public void setRelated_company(String related_company) {
        this.related_company = related_company;
    }

    public String getPay_account() {
        return pay_account;
    }

    public void setPay_account(String pay_account) {
        this.pay_account = pay_account;
    }

    public String getDeal_person() {
        return deal_person;
    }

    public void setDeal_person(String deal_person) {
        this.deal_person = deal_person;
    }

    public String getReceive_account() {
        return receive_account;
    }

    public void setReceive_account(String receive_account) {
        this.receive_account = receive_account;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppendix() {
        return appendix;
    }

    public void setAppendix(String appendix) {
        this.appendix = appendix;
    }

    @Override
    public String toString() {
        return "Income{" +
                "index_no='" + index_no + '\'' +
                ", list_no='" + list_no + '\'' +
                ", list_time=" + list_time +
                ", related_company='" + related_company + '\'' +
                ", pay_account='" + pay_account + '\'' +
                ", deal_person='" + deal_person + '\'' +
                ", receive_account='" + receive_account + '\'' +
                ", total_price=" + total_price +
                ", remark='" + remark + '\'' +
                ", appendix='" + appendix + '\'' +
                '}';
    }
}
