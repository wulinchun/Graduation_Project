package com.springboot.erp.entity;

import java.sql.Timestamp;

public class Sale_bill {
    private String index_no,customer_name,company_name,list_no;
    private java.sql.Timestamp list_date;
    private String goods_no,goods_name;
    private int count;
    private double total_price;
    private String operator,status,remark;

    public Sale_bill() {
        super();
    }

    public Sale_bill(String index_no, String customer_name, String company_name, String list_no, Timestamp list_date, String goods_no, String goods_name, int count, double total_price, String operator, String status, String remark) {
        this.index_no = index_no;
        this.customer_name = customer_name;
        this.company_name = company_name;
        this.list_no = list_no;
        this.list_date = list_date;
        this.goods_no = goods_no;
        this.goods_name = goods_name;
        this.count = count;
        this.total_price = total_price;
        this.operator = operator;
        this.status = status;
        this.remark = remark;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getList_no() {
        return list_no;
    }

    public void setList_no(String list_no) {
        this.list_no = list_no;
    }

    public Timestamp getList_date() {
        return list_date;
    }

    public void setList_date(Timestamp list_date) {
        this.list_date = list_date;
    }

    public String getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(String goods_no) {
        this.goods_no = goods_no;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Sale_bill{" +
                "index_no='" + index_no + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", company_name='" + company_name + '\'' +
                ", list_no='" + list_no + '\'' +
                ", list_date=" + list_date +
                ", goods_no='" + goods_no + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", count=" + count +
                ", total_price=" + total_price +
                ", operator='" + operator + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
