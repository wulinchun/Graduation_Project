package com.springboot.erp.entity;

public class Entry_out_again {
    private String flow_number,status;

    public Entry_out_again() {
        super();
    }

    public Entry_out_again(String flow_number, String status) {
        this.flow_number = flow_number;
        this.status = status;
    }

    public String getFlow_number() {
        return flow_number;
    }

    public void setFlow_number(String flow_number) {
        this.flow_number = flow_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Entry_out_again{" +
                "flow_number='" + flow_number + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
