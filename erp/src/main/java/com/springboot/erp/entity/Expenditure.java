package com.springboot.erp.entity;

import java.sql.Timestamp;

public class Expenditure {
    private String index_no,list_no;
    private java.sql.Timestamp list_time;
    private String related_company,receive_account,deal_person,pay_account;
    private double total_price;
    private String remark,appendix;

    public Expenditure() {
        super();
    }

    public Expenditure(String index_no, String list_no, Timestamp list_time, String related_company, String receive_account, String deal_person, String pay_account, double total_price, String remark, String appendix) {
        this.index_no = index_no;
        this.list_no = list_no;
        this.list_time = list_time;
        this.related_company = related_company;
        this.receive_account = receive_account;
        this.deal_person = deal_person;
        this.pay_account = pay_account;
        this.total_price = total_price;
        this.remark = remark;
        this.appendix = appendix;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getList_no() {
        return list_no;
    }

    public void setList_no(String list_no) {
        this.list_no = list_no;
    }

    public Timestamp getList_time() {
        return list_time;
    }

    public void setList_time(Timestamp list_time) {
        this.list_time = list_time;
    }

    public String getRelated_company() {
        return related_company;
    }

    public void setRelated_company(String related_company) {
        this.related_company = related_company;
    }

    public String getReceive_account() {
        return receive_account;
    }

    public void setReceive_account(String receive_account) {
        this.receive_account = receive_account;
    }

    public String getDeal_person() {
        return deal_person;
    }

    public void setDeal_person(String deal_person) {
        this.deal_person = deal_person;
    }

    public String getPay_account() {
        return pay_account;
    }

    public void setPay_account(String pay_account) {
        this.pay_account = pay_account;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppendix() {
        return appendix;
    }

    public void setAppendix(String appendix) {
        this.appendix = appendix;
    }

    @Override
    public String toString() {
        return "Expenditure{" +
                "index_no='" + index_no + '\'' +
                ", list_no='" + list_no + '\'' +
                ", list_time=" + list_time +
                ", related_company='" + related_company + '\'' +
                ", receive_account='" + receive_account + '\'' +
                ", deal_person='" + deal_person + '\'' +
                ", pay_account='" + pay_account + '\'' +
                ", total_price=" + total_price +
                ", remark='" + remark + '\'' +
                ", appendix='" + appendix + '\'' +
                '}';
    }
}
