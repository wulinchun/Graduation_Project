package com.springboot.erp.entity;

public class Common_file {
    private String index_no,file_name,uploader,file_path,remark;

    public Common_file() {
        super();
    }

    public Common_file(String index_no, String file_name, String uploader, String file_path, String remark) {
        this.index_no = index_no;
        this.file_name = file_name;
        this.uploader = uploader;
        this.file_path = file_path;
        this.remark = remark;
    }

    public String getIndex_no() {
        return index_no;
    }

    public void setIndex_no(String index_no) {
        this.index_no = index_no;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Common_file{" +
                "index_no='" + index_no + '\'' +
                ", file_name='" + file_name + '\'' +
                ", uploader='" + uploader + '\'' +
                ", file_path='" + file_path + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
