package com.springboot.erp.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class Out_Warehouse {
    private String flow_number,list_no,goods_no,kind,specification,goods_name;
    private double single_price;
    private int count;
    private double total_price;
    private String manufacturer;
    private java.sql.Timestamp out_date;
    private String status;
    private String warehouse_no;
    private String remarks;

    public Out_Warehouse() {
        super();
    }

    public Out_Warehouse(String flow_number, String list_no, String goods_no, String kind, String specification, String goods_name, double single_price, int count, double total_price, String manufacturer, Timestamp out_date, String status, String warehouse_no, String remarks) {
        this.flow_number = flow_number;
        this.list_no = list_no;
        this.goods_no = goods_no;
        this.kind = kind;
        this.specification = specification;
        this.goods_name = goods_name;
        this.single_price = single_price;
        this.count = count;
        this.total_price = total_price;
        this.manufacturer = manufacturer;
        this.out_date = out_date;
        this.status = status;
        this.warehouse_no = warehouse_no;
        this.remarks = remarks;
    }

    public String getFlow_number() {
        return flow_number;
    }

    public void setFlow_number(String flow_number) {
        this.flow_number = flow_number;
    }

    public String getList_no() {
        return list_no;
    }

    public void setList_no(String list_no) {
        this.list_no = list_no;
    }

    public String getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(String goods_no) {
        this.goods_no = goods_no;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public double getSingle_price() {
        return single_price;
    }

    public void setSingle_price(double single_price) {
        this.single_price = single_price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Timestamp getOut_date() {
        return out_date;
    }

    public void setOut_date(Timestamp out_date) {
        this.out_date = out_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWarehouse_no() {
        return warehouse_no;
    }

    public void setWarehouse_no(String warehouse_no) {
        this.warehouse_no = warehouse_no;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Out_Warehouse{" +
                "flow_number='" + flow_number + '\'' +
                ", list_no='" + list_no + '\'' +
                ", goods_no='" + goods_no + '\'' +
                ", kind='" + kind + '\'' +
                ", specification='" + specification + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", single_price=" + single_price +
                ", count=" + count +
                ", total_price=" + total_price +
                ", manufacturer='" + manufacturer + '\'' +
                ", out_date=" + out_date +
                ", status='" + status + '\'' +
                ", warehouse_no='" + warehouse_no + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
